#version 330 core
layout(location = 0) in vec3 l_position;
layout(location = 1) in vec3 l_normal;
layout(location = 2) in vec3 l_b1color;
layout(location = 3) in vec3 l_b2color;
layout(location = 4) in float l_biomeValue;

uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_model;

out vec3 vOut_color1;
out vec3 vOut_color2;
out float vOut_biomeValue;
out vec3 vOut_normal;
out vec4 vOut_pos;
out vec3 vOut_modelPos;

void main()
{
    vec4 pos = u_projection*u_view*u_model*vec4(l_position, 1.0f);
    vOut_modelPos = (u_model*vec4(l_position, 1.0f)).xyz;
    vOut_normal = l_normal;
    gl_Position = pos;
    vOut_color1 = l_b1color;
    vOut_color2 = l_b2color;
    vOut_pos = pos;
    vOut_biomeValue = l_biomeValue;
}