#version 330 core
layout(location = 0) in vec3 l_position;
layout(location = 1) in vec3 l_normal;
layout(location = 2) in vec4 l_color;
layout(location = 3) in vec2 l_uv;

uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_model;
uniform mat4 u_instancedModel[64];

uniform bool u_instanced;

out vec4 vOut_color;
out vec2 vOut_uv;
out vec3 vOut_normal;
out vec4 vOut_pos;
out vec3 vOut_modelPos;

void main()
{
    vec4 pos ;

    if(u_instanced) {
        pos = u_projection*u_view*u_instancedModel[gl_InstanceID]*vec4(l_position.x,l_position.y,l_position.z, 1.0f);
        vOut_modelPos = (u_instancedModel[gl_InstanceID]*vec4(l_position, 1.0f)).xyz;
    } else {
        pos = u_projection*u_view*u_model*vec4(l_position.x,l_position.y,l_position.z, 1.0f);
        vOut_modelPos = (u_model*vec4(l_position, 1.0f)).xyz;
    }
    vOut_normal = l_normal;
    gl_Position = pos;
    vOut_color = l_color;
    vOut_pos = pos;
    vOut_uv = l_uv;
}