#version 330 core
layout (location = 0) out vec4 FragColor;

in vec4 vOut_color;
in vec2 vOut_uv;
in vec4 vOut_pos;
in vec3 vOut_modelPos;
in vec3 vOut_normal;

uniform bool u_hasAlbedo;
uniform sampler2D u_textureAlbedo;

uniform bool u_hasNormal;
uniform sampler2D u_textureNormal;

uniform bool u_hasRoughness;
uniform sampler2D u_textureRoughness;

uniform vec3 u_cameraPos;
uniform vec3 u_ambientLightSourcePos = vec3(10.0);
uniform vec3 u_ambientSubLightSourcePos = vec3(-10.0,10.0,-10.0);
uniform vec3 u_ambientColor = vec3(1.0);

void main()
{
    //Assign basic color
    vec4 color = vOut_color;

    //Ambient light calc
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * u_ambientColor;

    //Diffuse
    vec3 norm = normalize(vOut_normal);
    vec3 lightDir = normalize(u_ambientLightSourcePos - vOut_modelPos);
    vec3 subLightDir = normalize(u_ambientSubLightSourcePos - vOut_modelPos);
    float diff = min(max(dot(norm, lightDir), 0.0) + max(dot(norm, subLightDir), 0.0)/5,1.0);
    vec3 diffuse = diff * color.xyz;

    //Albedo
    vec3 base;
    if(u_hasAlbedo) {
        base = texture2D(u_textureAlbedo, vOut_uv).xyz;
    } else {
        base = vOut_color.xyz;
    }

    vec3 result = (ambient + diffuse) * base;

    FragColor = vec4(result, color.a);
}