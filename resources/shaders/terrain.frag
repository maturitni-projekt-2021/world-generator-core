#version 330 core
layout (location = 0) out vec4 FragColor;

in vec3 vOut_color1;
in vec3 vOut_color2;
in float vOut_biomeValue;
in vec3 vOut_normal;
in vec4 vOut_pos;
in vec3 vOut_modelPos;

uniform vec3 u_cameraPos;
uniform vec3 u_ambientLightSourcePos = vec3(10.0);
uniform vec3 u_ambientSubLightSourcePos = vec3(-10.0,10.0,-10.0);
uniform vec3 u_ambientColor = vec3(1.0,0.5,0.5);
uniform float u_worldScale;

void main()
{
    //Color
    vec3 base = mix(vOut_color1,vOut_color2,vOut_biomeValue);

    //Ambient light calc
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * u_ambientColor;

    //Diffuse
    vec3 norm = normalize(vOut_normal);
    vec3 lightDir = normalize(-u_ambientLightSourcePos);
    vec3 subLightDir = normalize(-u_ambientSubLightSourcePos);
    float diff = min(max(dot(norm, lightDir), 0.0) + max(dot(norm, subLightDir), 0.0)/5,1.0);
    vec3 diffuse = diff * base;

    vec3 result = (ambient + diffuse) * base;

    //FragColor = vec4(result, 1.0f);
    FragColor = vec4(result, 1.0f);
}