#version 330 core
layout(location = 0) in vec3 l_position;

uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_model;

void main()
{
    vec4 pos = u_projection*u_view*u_model*vec4(l_position.x,l_position.y,l_position.z, 1.0f);
    gl_Position = pos;
}