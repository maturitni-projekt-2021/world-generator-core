//
// Created by Matty on 5.9.2020.
//

#include "../world-gen/ui/custom/UI_ModelInspector.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imgui_cpp.h"
#include "../world-gen/data/ProjectData.h"

void WorldGenerator::UI::Custom::UI_ModelInspector::CustomWindowSetup()
{
    UIWindow::CustomWindowSetup();
    //ImGui::SetNextWindowPos(ImVec2(ImGui::GetWindowViewport()->Size.x/2-375.0f, ImGui::GetWindowViewport()->Size.y/2-200.0f));
    // ImGui::SetNextWindowSize(ImVec2(750.0f, 400.0f));
}
void WorldGenerator::UI::Custom::UI_ModelInspector::CustomDraw()
{
    using namespace WorldGenerator::Data;
    auto projectData = ProjectData::m_ProjectData;

    static char currentAlbedoItem[128] = "";//strs.at(m_pSelectedTextureAlbedo).c_str();
    static char currentNormalItem[128] = "";//strs.at(m_pSelectedTextureAlbedo).c_str();
    static char currentRoughnessItem[128] = "";//strs.at(m_pSelectedTextureAlbedo).c_str();

    /*
    ImGui::Text("Select model:");
    ImGui::SameLine(ImGui::GetWindowWidth()-40.0f-ImGui::CalcTextSize("Select mesh:").x);
    ImGui::Text("Select mesh:");
    */
    if (!projectData->m_ModelData.empty()) {
        if (projectData->m_SelectedModel.second->m_ModelLoaded) {

            std::vector<std::string> strs;
            strs.reserve(projectData->m_TextureData.size()+1);
            strs.emplace_back("<none>");

            std::shared_ptr<WorldGenerator::Render::Model> selectedMesh;
            std::vector<std::string> meshStrs;
            uint32_t meshIndex = 0;
            for (auto& mesh : projectData->m_SelectedModel.second->m_Models) {
                if (meshIndex==m_pSelectedMesh) {
                    selectedMesh = mesh;
                }
                meshIndex++;
                meshStrs.emplace_back(mesh->GetName());
            }
            ImGui::SameLine();
            ImGui::ListBox("Meshes", &m_pSelectedMesh, meshStrs);

            bool refreshTexture = false;

            if (m_pPrevMeshID!=selectedMesh->GetID()) {
                refreshTexture = true;
                m_pPrevMeshID = selectedMesh->GetID();
            }

            std::pair<std::string, std::shared_ptr<TextureData>> selectedAlbedo;
            std::pair<std::string, std::shared_ptr<TextureData>> selectedNormal;
            std::pair<std::string, std::shared_ptr<TextureData>> selectedRoughness;

            uint32_t index = 0;

            for (auto& texture : projectData->m_TextureData) {
                if (!texture.second->m_TextureLoaded) {
                    index++;
                    continue;
                }
                if (index==m_pSelectedTextureAlbedo-1) {
                    selectedAlbedo.first = texture.first;
                    selectedAlbedo.second = std::shared_ptr<TextureData>(texture.second);
                }

                if (index==m_pSelectedTextureNormal) {
                    selectedNormal.first = texture.first;
                    selectedNormal.second = std::shared_ptr<TextureData>(texture.second);
                }

                if (index==m_pSelectedTextureRoughness) {
                    selectedRoughness.first = texture.first;
                    selectedRoughness.second = std::shared_ptr<TextureData>(texture.second);
                }
                index++;
                std::ostringstream stringStream;
                stringStream << texture.first;
                strs.emplace_back(stringStream.str());
            }

            if (!m_pPrevSelectedInitialized) {
                m_pPrevSelected = projectData->m_SelectedModel.first;

                for (uint32_t i = 0; i<128; i++) {
                    currentAlbedoItem[i] = '\0';
                }
                for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                    currentAlbedoItem[i] = strs.at(0).at(i);
                }

                for (uint32_t i = 0; i<128; i++) {
                    currentNormalItem[i] = '\0';
                }
                for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                    currentNormalItem[i] = strs.at(0).at(i);
                }

                for (uint32_t i = 0; i<128; i++) {
                    currentRoughnessItem[i] = '\0';
                }
                for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                    currentRoughnessItem[i] = strs.at(0).at(i);
                }

                m_pPrevSelectedInitialized = true;
            }
            else {
                if (m_pPrevSelected!=projectData->m_SelectedModel.first || refreshTexture) {
                    //Albedo
                    for (uint32_t i = 0; i<128; i++) {
                        currentAlbedoItem[i] = '\0';
                    }
                    if (selectedMesh->HasAlbedoTexture()) {
                        std::string textureName;
                        bool found = false;
                        for (auto& a : projectData->m_TextureData) {
                            if (a.second==selectedMesh->GetAlbedoTexture()) {
                                textureName = a.first;
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            for (uint32_t i = 0; i<textureName.size(); i++) {
                                currentAlbedoItem[i] = textureName.at(i);
                            }
                        }
                        else {
                            for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                                currentAlbedoItem[i] = strs.at(0).at(i);
                            }
                        }

                    }
                    else {
                        for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                            currentAlbedoItem[i] = strs.at(0).at(i);
                        }
                    }

                    //Normal
                    for (uint32_t i = 0; i<128; i++) {
                        currentNormalItem[i] = '\0';
                    }
                    if (selectedMesh->HasNormalTexture()) {
                        std::string textureName;
                        bool found = false;
                        for (auto& a : projectData->m_TextureData) {
                            if (a.second==selectedMesh->GetNormalTexture()) {
                                textureName = a.first;
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            for (uint32_t i = 0; i<textureName.size(); i++) {
                                currentNormalItem[i] = textureName.at(i);
                            }
                        }
                        else {
                            for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                                currentNormalItem[i] = strs.at(0).at(i);
                            }
                        }

                    }
                    else {
                        for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                            currentNormalItem[i] = strs.at(0).at(i);
                        }
                    }

                    //Roughness
                    for (uint32_t i = 0; i<128; i++) {
                        currentRoughnessItem[i] = '\0';
                    }
                    if (selectedMesh->HasRoughnessTexture()) {
                        std::string textureName;
                        bool found = false;
                        for (auto& a : projectData->m_TextureData) {
                            if (a.second==selectedMesh->GetRoughnessTexture()) {
                                textureName = a.first;
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            for (uint32_t i = 0; i<textureName.size(); i++) {
                                currentRoughnessItem[i] = textureName.at(i);
                            }
                        }
                        else {
                            for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                                currentRoughnessItem[i] = strs.at(0).at(i);
                            }
                        }

                    }
                    else {
                        for (uint32_t i = 0; i<strs.at(0).size(); i++) {
                            currentRoughnessItem[i] = strs.at(0).at(i);
                        }
                    }

                    m_pPrevSelected = projectData->m_SelectedModel.first;
                }
            }

            if (ImGui::BeginCombo("Albedo", currentAlbedoItem)) {
                for (int n = 0; n<strs.size(); n++) {
                    bool is_selected = (currentAlbedoItem==strs.at(n).c_str());
                    if (ImGui::Selectable(strs.at(n).c_str(), is_selected)) {

                        for (uint32_t i = 0; i<128; i++) {
                            currentAlbedoItem[i] = '\0';
                        }

                        for (uint32_t i = 0; i<strs.at(n).size(); i++) {
                            currentAlbedoItem[i] = strs.at(n).at(i);
                        }

                        m_pSelectedTextureAlbedo = n;
                        uint32_t alindex = 0;
                        for (auto& texture : projectData->m_TextureData) {
                            if (alindex==m_pSelectedTextureAlbedo-1) {
                                selectedAlbedo.first = texture.first;
                                selectedAlbedo.second = std::shared_ptr<TextureData>(texture.second);
                            }
                            alindex++;
                        }
                        if (n>0) {
                            projectData->m_SelectedModel.second->m_Models.at(m_pSelectedMesh)
                                    ->SetAlbedoTexture(selectedAlbedo.second);
                            projectData->m_SelectedModel.second->m_ModelTextures[m_pSelectedMesh] = selectedAlbedo.first;
                        }
                        else {
                            projectData->m_SelectedModel.second->m_Models.at(m_pSelectedMesh)->ClearAlbedoTexture();
                            projectData->m_SelectedModel.second->m_ModelTextures.erase(m_pSelectedMesh);
                        }
                    }
                    if (is_selected)
                        ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
                }

                ImGui::EndCombo();
            }

            if (ImGui::BeginCombo("Normal", currentNormalItem)) {
                for (int n = 0; n<strs.size(); n++) {
                    bool is_selected = (currentNormalItem==strs.at(n).c_str());
                    if (ImGui::Selectable(strs.at(n).c_str(), is_selected)) {

                        for (uint32_t i = 0; i<128; i++) {
                            currentNormalItem[i] = '\0';
                        }

                        for (uint32_t i = 0; i<strs.at(n).size(); i++) {
                            currentNormalItem[i] = strs.at(n).at(i);
                        }

                        m_pSelectedTextureNormal = n;
                        uint32_t nmindex = 0;
                        for (auto& texture : projectData->m_TextureData) {
                            if (nmindex==m_pSelectedTextureNormal-1) {
                                selectedNormal.first = texture.first;
                                selectedNormal.second = std::shared_ptr<TextureData>(texture.second);
                            }
                            nmindex++;
                        }
                        if (n>0) {
                            projectData->m_SelectedModel.second->m_Models.at(m_pSelectedMesh)
                                    ->SetNormalTexture(selectedNormal.second);
                        }
                        else {
                            projectData->m_SelectedModel.second->m_Models.at(m_pSelectedMesh)->ClearNormalTexture();
                        }
                    }
                    if (is_selected)
                        ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
                }

                ImGui::EndCombo();
            }

            if (ImGui::BeginCombo("Roughness", currentRoughnessItem)) {
                for (int n = 0; n<strs.size(); n++) {
                    bool is_selected = (currentRoughnessItem==strs.at(n).c_str());
                    if (ImGui::Selectable(strs.at(n).c_str(), is_selected)) {

                        for (uint32_t i = 0; i<128; i++) {
                            currentRoughnessItem[i] = '\0';
                        }

                        for (uint32_t i = 0; i<strs.at(n).size(); i++) {
                            currentRoughnessItem[i] = strs.at(n).at(i);
                        }

                        m_pSelectedTextureRoughness = n;
                        uint32_t rgindex = 0;
                        for (auto& texture : projectData->m_TextureData) {
                            if (rgindex==m_pSelectedTextureRoughness-1) {
                                selectedRoughness.first = texture.first;
                                selectedRoughness.second = std::shared_ptr<TextureData>(texture.second);
                            }
                            rgindex++;
                        }
                        if (n>0) {
                            projectData->m_SelectedModel.second->m_Models.at(m_pSelectedMesh)
                                    ->SetRoughnessTexture(selectedRoughness.second);
                        }
                        else {
                            projectData->m_SelectedModel.second->m_Models.at(m_pSelectedMesh)->ClearRoughnessTexture();
                        }
                    }
                    if (is_selected)
                        ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
                }

                ImGui::EndCombo();
            }

        } else {
            ImGui::TextColored({0.8f,0.3f,0.3f,1.0f},"Selected model is not yet loaded");
        }
    } else {
        ImGui::TextColored({0.8f,0.3f,0.3f,1.0f},"No models loaded, load some from model browser");
    }
}
WorldGenerator::UI::Custom::UI_ModelInspector::UI_ModelInspector()
        :UIWindow("Model Inspector")
{

}
