//
// Created by Matty on 7.5.2020.
//

#include "../world-gen/render/Vertex.h"
#include "../world-gen/glad.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

void WorldGenerator::Render::VertexLayoutComponent::setAttributePointer(uint32_t& index, uint32_t& startingOffset,
        uint32_t totalSize)
{
    glVertexAttribPointer(index, m_pSize, GL_FLOAT, GL_FALSE, totalSize*sizeof(float), (void*) (startingOffset));
    glEnableVertexAttribArray(index++);

    startingOffset += m_pSize*sizeof(float);
}

#pragma GCC diagnostic pop

void WorldGenerator::Render::VertexLayout::setAttributePointers()
{
    uint32_t startingIndex = 0;
    uint32_t startingSize = 0;
    for (auto& a : m_pComponents) {
        a.setAttributePointer(startingIndex, startingSize, m_pTotalSize);
    }
}
void WorldGenerator::Render::VertexLayout::addAttributeComponent(
        WorldGenerator::Render::VertexLayoutComponent& component)
{
    m_pTotalSize += component.m_pSize;
    m_pComponents.emplace_back(component);
}
