//
// Created by Matty on 11/9/2020.
//

#include "../world-gen/world_generator/data/NoiseLayer.h"

WorldGenerator::TerrainGeneration::Data::NoiseLayer::NoiseLayer(
        WorldGenerator::TerrainGeneration::Data::NoiseType type) : m_pType(type) {
}

float WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetValue(float x, float y, float z) {
    return GetValue(glm::vec3(x, y, z));
}

WorldGenerator::TerrainGeneration::Data::NoiseType
WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetType() const {
    return m_pType;
}

float WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetScale() const {
    return m_pScale;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetScale(float mPScale) {
    m_pScale = mPScale;
}

const glm::vec3 &WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetOffset() const {
    return m_pOffset;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetOffset(const glm::vec3 &mPOffset) {
    m_pOffset = mPOffset;
}

float WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetIntensity() const {
    return m_pIntensity;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetIntensity(float mPIntensity) {
    m_pIntensity = mPIntensity;
}

const char *WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetTypeName() const {
    switch (m_pType) {
        case WG_NOISE_PERLIN_2D:
            return "Perlin noise 2D";
        case WG_NOISE_PERLIN_3D:
            return "Perlin noise 3D";
        case WG_NOISE_SIMPLEX_2D:
            return "Simplex noise 2D";
        case WG_NOISE_SIMPLEX_3D:
            return "Simplex noise 3D";
        case WG_NOISE_WHITE_2D:
            return "White noise 2D";
        case WG_NOISE_WHITE_3D:
            return "White noise 3D";
        case WG_NOISE_FRACTAL_2D:
            return "Fractal noise 2D";
        case WG_NOISE_FRACTAL_3D:
            return "Fractal noise 3D";
        case WG_NOISE_CELLUAR_2D:
            return "Celluar noise 2D";
        case WG_NOISE_CELLUAR_3D:
            return "Celluar noise 3D";
        case WG_NOISE_CUBIC_2D:
            return "Cubic noise 2D";
        case WG_NOISE_CUBIC_3D:
            return "Cubic noise 3D";
    }
    return "INVALID";
}

WorldGenerator::TerrainGeneration::Data::AdditionType WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetAddition() const {
    return m_pAddition;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetAddition(AdditionType newVal) {
    m_pAddition = newVal;
}

float WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetRangeOffset() const {
    return m_pRangeOffset;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetRangeOffset(float mPRangeOffset) {
    m_pRangeOffset = mPRangeOffset;
}

bool WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetClampAfter() const {
    return m_pClampNoiseAfter;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetClampAfter(bool newVal) {
    m_pClampNoiseAfter = newVal;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::_CopyCore(
        WorldGenerator::TerrainGeneration::Data::NoiseLayer *res) {
    res->m_pOffset = GetOffset();
    res->m_pScale = GetScale();
    res->m_pIntensity = GetIntensity();
    res->m_pType = GetType();
    res->m_pAddition = GetAddition();
    res->m_pRangeOffset = GetRangeOffset();
    res->m_pClampNoiseAfter = GetClampAfter();
    res->m_pApplyBias = GetApplyBias();
    res->m_pBiasValue = GetBiasValue();
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetBiasValue(float val) {
    m_pBiasValue = val;
}

bool WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetApplyBias() const {
    return m_pApplyBias;
}

void WorldGenerator::TerrainGeneration::Data::NoiseLayer::SetApplyBias(bool newVal) {
    m_pApplyBias = newVal;
}

float WorldGenerator::TerrainGeneration::Data::NoiseLayer::GetBiasValue() const {
    return m_pBiasValue;
}
/*
WorldGenerator::TerrainGeneration::Data::NoiseLayer::NoiseLayer(
        const WorldGenerator::TerrainGeneration::Data::NoiseLayer &cpy) {
    this->m_pOffset = cpy.m_pOffset;
    this->m_pScale = cpy.m_pScale;
    this->m_pIntensity = cpy.m_pIntensity;
    this->m_pType = cpy.m_pType;
}
*/
