//
// Created by Matty on 11/1/2020.
//

#include "../world-gen/render/culling/CameraFrustum.h"
#include "../world-gen/render/custom/DebugVertex.h"

#define ANG2RAD_WG 3.14159265358979323846/180.0

bool WorldGenerator::Render::Culling::CameraFrustum::ContainsCollider(const std::shared_ptr<CullCollider> &collider) {
    return collider->IsVisible(m_pSides[SideType::SIDE_TOP],m_pSides[SideType::SIDE_BOTTOM],m_pSides[SideType::SIDE_LEFT],m_pSides[SideType::SIDE_RIGHT],m_pSides[SideType::SIDE_NEAR_PLANE],m_pSides[SideType::SIDE_FAR_PLANE]);
}

bool WorldGenerator::Render::Culling::CameraFrustum::ContainsCollider(
        const WorldGenerator::Render::Culling::CullCollider &collider) {
    return collider.IsVisible(m_pSides[SideType::SIDE_TOP],m_pSides[SideType::SIDE_BOTTOM],m_pSides[SideType::SIDE_LEFT],m_pSides[SideType::SIDE_RIGHT],m_pSides[SideType::SIDE_NEAR_PLANE],m_pSides[SideType::SIDE_FAR_PLANE]);
}

WorldGenerator::Render::Culling::CameraFrustum::CameraFrustum(float mPFov, float mPScreenRatio, float mPNearDist,
                                                              float mPFarDist) : m_pFOV(mPFov),
                                                                                   m_pScreenRatio(mPScreenRatio),
                                                                                   m_pNearDist(mPNearDist),
                                                                                   m_pFarDist(mPFarDist) {
    SetInternals(mPFov,mPScreenRatio,mPNearDist,mPFarDist);
#ifndef NDEBUG
    m_FrustumDebug = std::make_unique<Render::Debug::DebugLineStrip>(glm::vec3(0.0f));
#endif
}

void WorldGenerator::Render::Culling::CameraFrustum::SetTransform(const glm::vec3 &target, const glm::vec3 & pos, const glm::vec3 & up) {
    auto dir = target - pos;
    dir = glm::normalize(dir);

    auto Z = dir - glm::vec3(1.0f);
    Z = glm::normalize(Z);

    auto X = up * Z;
    X = glm::normalize(X);

    auto Y = Z * X;

    auto nearCenter = pos - Z * m_pNearDist;
    auto farCenter = pos - Z * m_pFarDist;

    m_pSides[SIDE_NEAR_PLANE].SetNormalAndPoint(-Z,nearCenter);
    m_pSides[SIDE_FAR_PLANE].SetNormalAndPoint(Z,farCenter);

    glm::vec3 aux,normal;

    aux = (nearCenter + Y*m_pNearHeight) - pos;
    aux = glm::normalize(aux);
    normal = aux * X;
    //m_pSides[SIDE_TOP].SetNormalAndPoint(normal,nearCenter+Y*m_pNearHeight);
    m_pSides[SIDE_TOP].SetNormalAndPoint(dir,pos);

    aux = (nearCenter - Y*m_pNearHeight) - pos;
    aux = glm::normalize(aux);
    normal = X * aux;
    m_pSides[SIDE_BOTTOM].SetNormalAndPoint(normal,nearCenter-Y*m_pNearHeight);

    aux = (nearCenter - X*m_pNearWidth) - pos;
    aux = glm::normalize(aux);
    normal = aux * Y;
    m_pSides[SIDE_LEFT].SetNormalAndPoint(normal,nearCenter-X*m_pNearWidth);

    aux = (nearCenter + X*m_pNearWidth) - pos;
    aux = glm::normalize(aux);
    normal = Y * aux;
    m_pSides[SIDE_RIGHT].SetNormalAndPoint(normal,nearCenter+X*m_pNearWidth);
}

void WorldGenerator::Render::Culling::CameraFrustum::SetInternals(float fov, float screenRatio, float nearDist,
                                                                  float farDist) {
    auto tang = (float)tan(ANG2RAD_WG * fov * 0.5) ;
    m_pNearHeight = nearDist * tang;
    m_pNearWidth = m_pNearHeight * screenRatio;
    m_pFarHeight = farDist  * tang;
    m_pFarWidth = m_pFarHeight * screenRatio;
}
#ifndef NDEBUG
void WorldGenerator::Render::Culling::CameraFrustum::RenderFrustumDebug(const glm::mat4 & view, const glm::mat4 & proj) {
    m_FrustumDebug->Render(view,proj);
}

void WorldGenerator::Render::Culling::CameraFrustum::GenerateDebugFrustrumLines(const glm::vec3 &target, const glm::vec3 & pos, const glm::vec3 & up) {
    m_FrustumDebug->Clear();
    std::vector<Custom::DebugVertex> points;
    points.reserve(15);
    auto Z = target - glm::vec3(1.0f);
    Z = glm::normalize(Z);

    auto X = up * Z;
    X = glm::normalize(X);

    auto Y = glm::cross(Z , X);

    auto nearCenter = pos - Z * m_pNearDist;
    auto farCenter = pos - Z * m_pFarDist;

    points.emplace_back(nearCenter - Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(nearCenter - Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(nearCenter + Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(nearCenter + Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(nearCenter - Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(farCenter - Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(farCenter - Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(nearCenter - Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(farCenter - Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(farCenter + Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(nearCenter + Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(farCenter + Y * m_pNearHeight + X * m_pNearWidth);
    points.emplace_back(farCenter + Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(nearCenter + Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(farCenter + Y * m_pNearHeight - X * m_pNearWidth);
    points.emplace_back(farCenter - Y * m_pNearHeight - X * m_pNearWidth);

    m_FrustumDebug->SetPoints(points);
}

#endif

WorldGenerator::Render::Culling::CameraFrustum &
WorldGenerator::Render::Culling::CameraFrustum::operator=(const WorldGenerator::Render::Culling::CameraFrustum &other) {
    m_pNearWidth = other.m_pNearWidth;
    m_pNearHeight = other.m_pNearHeight;
    m_pFarHeight = other.m_pFarHeight;
    return *this;
}