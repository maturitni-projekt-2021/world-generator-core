//
// Created by Matty on 2021-02-01.
//

#include "../world-gen/ui/custom/UI_WorldObjectEditor.h"

WorldGenerator::UI::Custom::UI_WorldObjectEditor::UI_WorldObjectEditor() : UIWindow("World object spawning") {

}

void WorldGenerator::UI::Custom::UI_WorldObjectEditor::CustomDraw() {

}

void WorldGenerator::UI::Custom::UI_WorldObjectEditor::CustomWindowSetup() {
    UIWindow::CustomWindowSetup();
}
