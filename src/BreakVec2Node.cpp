//
// Created by mattyvrba on 9/9/20.
//

#include "../world-gen/node_editor/nodes/BreakVec2Node.h"
#include "../world-gen/data/EditorIds.h"

WorldGenerator::NodeEditor::Nodes::BreakVec2Node::BreakVec2Node(uint32_t id,
                                                                std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage)
        : Node(id, pinStorage) {

    m_pName = ("Break Vec2");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<Vec2Pin>(pinID,"Input",false,m_pValueIN);
    m_pInputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValX = std::make_shared<FloatPin>(pinID,"X",true,m_pValueOutX);
    m_pOutputPins.emplace_back(pinValX);
    pinStorage[pinID] = pinValX;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValY = std::make_shared<FloatPin>(pinID,"Y",true,m_pValueOutY);
    m_pOutputPins.emplace_back(pinValY);
    pinStorage[pinID] = pinValY;
}
uint32_t WorldGenerator::NodeEditor::Nodes::BreakVec2Node::GetOutputCount() const
{
    return 2;
}
uint32_t WorldGenerator::NodeEditor::Nodes::BreakVec2Node::GetInputCount() const
{
    return 1;
}
void WorldGenerator::NodeEditor::Nodes::BreakVec2Node::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}
