//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/custom/TerrainVertex.h"
WorldGenerator::Render::VertexLayout WorldGenerator::Render::Custom::TerrainVertex::getVertexLayout()
{
    auto layout = VertexLayout(5);

    auto posComponent = VertexLayoutComponent(3);
    auto normalComponent = VertexLayoutComponent(3);
    auto b1ColorComponent = VertexLayoutComponent(3);
    auto b2ColorComponent = VertexLayoutComponent(3);
    auto biomeValComponent = VertexLayoutComponent(1);

    layout.addAttributeComponent(posComponent);
    layout.addAttributeComponent(normalComponent);
    layout.addAttributeComponent(b1ColorComponent);
    layout.addAttributeComponent(b2ColorComponent);
    layout.addAttributeComponent(biomeValComponent);
    return layout;
}
uint32_t WorldGenerator::Render::Custom::TerrainVertex::getFloatCount()
{
    return 3+3+3+3+1;
}
WorldGenerator::Render::Custom::TerrainVertex::TerrainVertex(const glm::vec3& pos,
        const glm::vec3& normal, const glm::vec3& b1color, const glm::vec3& b2color, float biomeVal)
{
    m_pData.reserve(getFloatCount());

    m_pData.emplace_back(pos.x);
    m_pData.emplace_back(pos.y);
    m_pData.emplace_back(pos.z);

    m_pData.emplace_back(normal.x);
    m_pData.emplace_back(normal.y);
    m_pData.emplace_back(normal.z);

    m_pData.emplace_back(b1color.x);
    m_pData.emplace_back(b1color.y);
    m_pData.emplace_back(b1color.z);

    m_pData.emplace_back(b2color.x);
    m_pData.emplace_back(b2color.y);
    m_pData.emplace_back(b2color.z);

    m_pData.emplace_back(biomeVal);
}

WorldGenerator::Render::VertexLayout WorldGenerator::Render::Custom::TerrainVertex::GetModelVertexLayout() {
    auto layout = VertexLayout(5);

    auto posComponent = VertexLayoutComponent(3);
    auto normalComponent = VertexLayoutComponent(3);
    auto b1ColorComponent = VertexLayoutComponent(3);
    auto b2ColorComponent = VertexLayoutComponent(3);
    auto biomeValComponent = VertexLayoutComponent(1);

    layout.addAttributeComponent(posComponent);
    layout.addAttributeComponent(normalComponent);
    layout.addAttributeComponent(b1ColorComponent);
    layout.addAttributeComponent(b2ColorComponent);
    layout.addAttributeComponent(biomeValComponent);
    return layout;
}
