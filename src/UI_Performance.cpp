//
// Created by Matty on 2.9.2020.
//

#include "../world-gen/ui/custom/UI_Performance.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/data/PerformanceData.h"


WorldGenerator::UI::Custom::UI_Performance::UI_Performance() : UIWindow("Performance")
{

}
void WorldGenerator::UI::Custom::UI_Performance::CustomWindowSetup()
{
    UIWindow::CustomWindowSetup();
}
void WorldGenerator::UI::Custom::UI_Performance::CustomDraw()
{
    ImGuiIO& io = ImGui::GetIO();
    ///ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);

    WorldGenerator::Data::PerformanceData::m_PerformanceData->m_FPS = io.Framerate;
    WorldGenerator::Data::PerformanceData::m_PerformanceData->AddFrameTime(1000.0f/io.Framerate);

    ImGui::PlotLines("Frame time graph",
            WorldGenerator::Data::PerformanceData::m_PerformanceData->GetFrameTimeStorage(),
            WorldGenerator::Data::PerformanceData::m_PerformanceData->GetFrameTimeStorageValueCount(),
            WorldGenerator::Data::PerformanceData::m_PerformanceData->GetFrameTimeStorageOffset());
    ImGui::Text("FrameTime: %.3f ms", 1000.0f / io.Framerate);
    ImGui::Text("FPS: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_FPS);
    ImGui::Text("TPS: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_TPS);
    ImGui::Text("Draw calls: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_DrawCalls);
    ImGui::Text("Requested chunks: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_RequestedChunks);
    ImGui::Text("Finished chunks: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_FinishedChunks);
    ImGui::Text("Loaded chunks: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_LoadedChunks);
    ImGui::Text("Working chunk generators chunks: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_WorkingChunkGenerators.operator unsigned int());
    ImGui::Text("Finished filled chunks: %d", WorldGenerator::Data::PerformanceData::m_PerformanceData->m_GeneratedFilledChunks.operator unsigned int());

}
