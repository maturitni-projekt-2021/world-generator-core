//
// Created by Matty on 10.4.2020.
//

#include "../world-gen/Exceptions.h"
WorldGenerator::Exceptions::WGException::WGException(const std::string& reason, const std::string& source)
{
    m_pMessage = std::string(source+" > "+reason);
}
const char* WorldGenerator::Exceptions::WGException::what() const noexcept
{
    return m_pMessage.c_str();
}
