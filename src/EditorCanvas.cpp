//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/node_editor/EditorCanvas.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imgui_internal.h"
#include "../world-gen/Log.h"
#include "../world-gen/node_editor/nodes/FloatNode.h"
#include "../world-gen/data/EditorIds.h"
#include "../world-gen/node_editor/NodeColors.h"
#include "../world-gen/node_editor/nodes/WorldPosNode.h"
#include "../world-gen/node_editor/nodes/BreakVec2Node.h"
#include "../world-gen/node_editor/nodes/BreakVec3Node.h"

#include <GLFW/glfw3.h>

void WorldGenerator::NodeEditor::EditorCanvas::Draw() {

    imnodes::EditorContextSet(m_pContext);
    imnodes::BeginNodeEditor();

    if (m_pFirstFrame) {
        //imnodes::SetNodeGridSpacePos(0, ImVec2(200.0f, 200.0f));
    }

    const auto dataPass = PinHolders(m_pInputConnectedPins, m_pOutputConnectedPins);
    for (auto &node : m_pNodes) {
        node.second->Draw(dataPass);
    }

    int32_t index = 0;
    for (auto &connection : m_pConnections) {
        const auto data = connection.second->GetIndices();
        imnodes::PushColorStyle(imnodes::ColorStyle_Link, connection.second->GetColor());
        imnodes::Link(connection.first, data.first, data.second);
        imnodes::PopColorStyle();
    }
/*
    int32_t pinID;
    if(imnodes::IsLinkStarted(&pinID)) {
    }
*/
    imnodes::EndNodeEditor();

    m_pDeleteSelection = ImGui::IsKeyReleased(GLFW_KEY_DELETE);

    int32_t startID, endID;
    if (imnodes::IsLinkCreated(&startID, &endID)) {

        if (m_pPins.at(startID)->IsOutput() != m_pPins.at(endID)->IsOutput()) {
            if (m_pPins.at(startID)->GetType() == m_pPins.at(endID)->GetType()) {

                int pins[2] = {startID, endID};

                int inputPinIndex = !m_pPins.at(startID)->IsOutput() ? 0 : 1;

                if (m_pInputConnectedPins.find(pins[inputPinIndex]) != m_pInputConnectedPins.end()) {
                    const auto connectionToClearID = m_pInputConnectedPins.at(pins[inputPinIndex]).first;
                    m_pConnections.erase(connectionToClearID);
                    const auto oldOutputPin = m_pInputConnectedPins.at(pins[inputPinIndex]).second;
                    m_pInputConnectedPins.erase(pins[inputPinIndex]);
                    m_pOutputConnectedPins.erase(oldOutputPin);
                }

                const auto connID = WorldGenerator::Data::EditorIds::m_IDs->GetConnectionID();
                m_pConnections[connID] = std::make_shared<Connection>(startID, endID,
                                                                      WorldGenerator::NodeEditor::GetColorFromPinType(
                                                                              m_pPins.at(
                                                                                      pins[inputPinIndex])->GetType()));
                m_pInputConnectedPins[pins[inputPinIndex]] = std::pair<uint32_t, uint32_t>(connID,
                                                                                           pins[1 - inputPinIndex]);
                m_pOutputConnectedPins[pins[1 - inputPinIndex]][pins[inputPinIndex]] = {connID, pins[inputPinIndex]};
            }
        }
    }

    int32_t delLinkID;
    if (imnodes::IsLinkDestroyed(&delLinkID)) {
        const auto link = m_pConnections[delLinkID];

        const uint32_t pins[2] = {link->GetIndices().first, link->GetIndices().second};

        const auto inputPinIndex = m_pInputConnectedPins.count(pins[0]) > 0 ? 0 : 1;

        m_pConnections.erase(delLinkID);
        m_pInputConnectedPins.erase(pins[inputPinIndex]);
        m_pOutputConnectedPins.erase(pins[1 - inputPinIndex]);
    }

    {
        uint32_t selectedNodeCount = imnodes::NumSelectedNodes();
        uint32_t selectedLinkCount = imnodes::NumSelectedLinks();

        if (selectedNodeCount > 0) {
            auto *selectedNodes = new int32_t[selectedNodeCount];

            imnodes::GetSelectedNodes(selectedNodes);
            for (uint32_t i = 0; i < selectedNodeCount; i++) {
                if (m_pDeleteSelection) {
                    if (m_pNodes.at(selectedNodes[i])->IsDeletable()) {
                        auto pins = m_pNodes.at(selectedNodes[i])->GetAllPins();
                        for (auto &pin : pins) {
                            if (m_pInputConnectedPins.count(pin) > 0) {
                                const auto connPin = m_pInputConnectedPins.at(pin);
                                auto inputConnID = connPin.first;
                                auto otherInputPin = connPin.second;
                                m_pInputConnectedPins.erase(pin);
                                m_pOutputConnectedPins.erase(otherInputPin);
                                m_pConnections.erase(inputConnID);
                            }

                            if (m_pOutputConnectedPins.find(pin) !=  m_pOutputConnectedPins.end()) {
                                const auto connPin = m_pOutputConnectedPins.at(pin);
                                for(auto & conn : connPin) {
                                    auto outputConnID = conn.second.first;
                                    auto otherInputPin = conn.second.second;
                                    m_pInputConnectedPins.erase(otherInputPin);
                                    m_pConnections.erase(outputConnID);
                                }
                                m_pOutputConnectedPins.erase(pin);
                            }

                        }

                        m_pNodes.erase(selectedNodes[i]);
                    }
                }
            }
            delete[] selectedNodes;
        } else if (selectedLinkCount > 0) {
            auto *selectedLinks = new int32_t[selectedLinkCount];
            imnodes::GetSelectedLinks(selectedLinks);

            for (uint32_t i = 0; i < selectedLinkCount; i++) {
                if (m_pDeleteSelection) {
                    const auto link = m_pConnections.at(selectedLinks[i]);

                    const uint32_t pins[2] = {link->GetIndices().first, link->GetIndices().second};

                    const auto inputPinIndex = m_pInputConnectedPins.count(pins[0]) > 0 ? 0 : 1;

                    m_pConnections.erase(selectedLinks[i]);
                    m_pInputConnectedPins.erase(pins[inputPinIndex]);
                    m_pOutputConnectedPins.erase(pins[1 - inputPinIndex]);
                }
            }
            delete[] selectedLinks;
        } else {
            m_pDeleteSelection = false;
        }

    }

    //if()
    {
        const auto userInput = ImGui::IsMouseReleased(ImGuiMouseButton_Right);
        const auto editorHovered = imnodes::IsEditorHovered();
        const auto windowFocused = ImGui::IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows);

        const bool open_popup = windowFocused && editorHovered && userInput;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.f, 8.f));
        if (!ImGui::IsAnyItemHovered() && open_popup) {
            ImGui::OpenPopup("Add node");
        }
        if (ImGui::BeginPopup("Add node")) {
            const ImVec2 click_pos = ImGui::GetMousePosOnOpeningCurrentPopup();

            if (ImGui::MenuItem("Float")) {
                std::shared_ptr<Nodes::FloatNode> node;
                const auto id = WorldGenerator::Data::EditorIds::m_IDs->GetID();
                node = std::make_shared<Nodes::FloatNode>(id, m_pPins);
                m_pNodes[id] = node;
                imnodes::SetNodeScreenSpacePos(id, click_pos);
            }
            if (ImGui::MenuItem("World position")) {
                std::shared_ptr<Nodes::WorldPosNode> node;
                const auto id = WorldGenerator::Data::EditorIds::m_IDs->GetID();
                node = std::make_shared<Nodes::WorldPosNode>(id, m_pPins);
                m_pNodes[id] = node;
                imnodes::SetNodeScreenSpacePos(id, click_pos);
            }
            if (ImGui::MenuItem("Break vec2")) {
                std::shared_ptr<Nodes::BreakVec2Node> node;
                const auto id = WorldGenerator::Data::EditorIds::m_IDs->GetID();
                node = std::make_shared<Nodes::BreakVec2Node>(id, m_pPins);
                m_pNodes[id] = node;
                imnodes::SetNodeScreenSpacePos(id, click_pos);
            }

            if (ImGui::MenuItem("Break vec3")) {
                std::shared_ptr<Nodes::BreakVec3Node> node;
                const auto id = WorldGenerator::Data::EditorIds::m_IDs->GetID();
                node = std::make_shared<Nodes::BreakVec3Node>(id, m_pPins);
                m_pNodes[id] = node;
                imnodes::SetNodeScreenSpacePos(id, click_pos);
            }

            ImGui::EndPopup();
        }
        ImGui::PopStyleVar();
    }
    m_pFirstFrame = false;
}

WorldGenerator::NodeEditor::EditorCanvas::EditorCanvas() {
    //m_pContext = imnodes::EditorContextCreate();
}

WorldGenerator::NodeEditor::EditorCanvas::~EditorCanvas() {
    imnodes::EditorContextFree(m_pContext);
}

void WorldGenerator::NodeEditor::EditorCanvas::_Init() {
    m_pContext = imnodes::EditorContextCreate();
    imnodes::PushAttributeFlag(imnodes::AttributeFlags_EnableLinkDetachWithDragClick);

    imnodes::IO &io = imnodes::GetIO();
    io.link_detach_with_modifier_click.modifier = &ImGui::GetIO().KeyCtrl;
}
