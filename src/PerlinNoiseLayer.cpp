//
// Created by Matty on 11/9/2020.
//

#include "../world-gen/world_generator/data/PerlinNoiseLayer.h"
#include "../world-gen/Log.h"

float WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer2D::GetValue(const glm::vec3 &pos, bool raw2Ddata,
                                                                            float nextVal, Mask2DOperation operation) {
    auto noiseVal = WorldGenerator::Data::WorldData::m_Noiser.GetPerlin((pos.x * m_pScale) + m_pOffset.x,
                                                                        (pos.z * m_pScale) +
                                                                        m_pOffset.y);

    float heightSubVal = (raw2Ddata || GetAddition() == AdditionType::WG_NOISE_MULTIPLY) ? 0.0f : pos.y;

    switch (operation) {
        case WG_NOISE_MASK_2D_ADDITIVE:
            return (m_pRangeOffset + noiseVal * m_pIntensity + nextVal - heightSubVal);
        case WG_NOISE_MASK_2D_SUBTRACT:
            return (m_pRangeOffset + noiseVal * m_pIntensity - nextVal - heightSubVal);
        case WG_NOISE_MASK_2D_MULTIPLY:
            return (m_pRangeOffset + noiseVal * m_pIntensity * nextVal - heightSubVal);
    }
    return 0;
}

WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer2D::PerlinNoiseLayer2D() : NoiseLayer(
        WorldGenerator::TerrainGeneration::Data::NoiseType::WG_NOISE_PERLIN_2D) {

}

WorldGenerator::TerrainGeneration::Data::NoiseLayer *
WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer2D::Copy() {
    auto res = new PerlinNoiseLayer2D();
    _CopyCore(res);
    return res;
}

float WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer3D::GetValue(const glm::vec3 &pos, bool raw2Ddata,
                                                                            float nextVal, Mask2DOperation operation) {
    return m_pRangeOffset + WorldGenerator::Data::WorldData::m_Noiser.GetPerlin((pos.x * m_pScale) + m_pOffset.x,
                                                                                (pos.y * m_pScale) + m_pOffset.y,
                                                                                (pos.z * m_pScale) + m_pOffset.z) *
                            m_pIntensity;
}

WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer3D::PerlinNoiseLayer3D() : NoiseLayer(
        WorldGenerator::TerrainGeneration::Data::NoiseType::WG_NOISE_PERLIN_3D) {

}

WorldGenerator::TerrainGeneration::Data::NoiseLayer *
WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer3D::Copy() {
    auto res = new PerlinNoiseLayer3D();
    _CopyCore(res);
    return res;
}

WorldGenerator::TerrainGeneration::Data::PerlinNoiseLayer3D::~PerlinNoiseLayer3D() {
}
