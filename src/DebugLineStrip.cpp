//
// Created by Matty on 11/2/2020.
//

#ifndef NDEBUG

#include "../world-gen/render/debug/DebugLineStrip.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/glad.h"
#include "../world-gen/data/PerformanceData.h"
#include "../world-gen/render/custom/DebugVertex.h"

void
WorldGenerator::Render::Debug::DebugLineStrip::Render(const glm::mat4 &viewMatrix, const glm::mat4 &projectionMatrix) {
    if (!Data::RenderData::m_RenderData->m_DebugShader) return;
    if (m_pVertexBuffer->bind()) {
        //glDisable(GL_CULL_FACE);
        glLineWidth(6.0f);
        auto shader = Data::RenderData::m_RenderData->m_DebugShader;

        shader->activate();

        shader->setUniformMat4("u_projection", projectionMatrix);
        shader->setUniformMat4("u_view", viewMatrix);
        shader->setUniformMat4("u_model", _GetModelMatrix());
        shader->setUniformVec3("u_color", m_pColor);

        if (m_pModified) _LoadDataToVertexBuffer();
        Custom::DebugVertex::GetDebugVertexLayout().setAttributePointers();
        if (m_pVertexBuffer->getElementCount() > 0) {
            glDrawElements(GL_LINE_STRIP, m_pVertexBuffer->getElementCount(), GL_UNSIGNED_INT, nullptr);
            WorldGenerator::Data::PerformanceData::m_PerformanceData->m_DrawCalls++;
        }
        //glEnable(GL_CULL_FACE);
    }
}

void WorldGenerator::Render::Debug::DebugLineStrip::Clear() {
    m_pPoints.clear();
    m_pLastIndex = 0;
    m_pIndices.clear();
}

WorldGenerator::Render::Debug::DebugLineStrip::DebugLineStrip(const glm::vec3 &pos, const glm::vec3 &color,
                                                              const glm::vec3 &rot,
                                                              const glm::vec3 &scale, uint32_t expectedMaxPoints)
        : DebugPrimitive(pos, rot, scale, color) {
    m_pPoints.reserve(expectedMaxPoints);
    m_pIndices.reserve(expectedMaxPoints);
}

void WorldGenerator::Render::Debug::DebugLineStrip::SetPoints(std::vector<Custom::DebugVertex> points) {
    m_pPoints = std::move(points);
    for (uint32_t i = 1; i < m_pPoints.size(); i++) {
        m_pIndices.emplace_back(m_pLastIndex++);
    }
    m_pModified = true;
}

void WorldGenerator::Render::Debug::DebugLineStrip::AddPoint(const Custom::DebugVertex &point) {
    m_pPoints.emplace_back(point);
    m_pIndices.emplace_back(m_pLastIndex++);
    m_pModified = true;
}

void WorldGenerator::Render::Debug::DebugLineStrip::_LoadDataToVertexBuffer() {
    auto vertices = std::vector<float>();
    vertices.reserve(m_pPoints.size() * 3);
    for(auto && a : m_pPoints) {
        vertices.insert(vertices.end(),a.data().begin(),a.data().end());
    }
    m_pVertexBuffer->setVertexData(vertices.size(), vertices.data(), DrawType::STATIC_DRAW);
    m_pVertexBuffer->setIndexData(m_pIndices.size(), m_pIndices.data(), DrawType::STATIC_DRAW);
    m_pVertexBuffer->setElementCount(m_pIndices.size());
    m_pModified = false;
}

#endif