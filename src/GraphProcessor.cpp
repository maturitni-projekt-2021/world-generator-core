//
// Created by Matty on 13.9.2020.
//

#include "../world-gen/node_editor/logic/GraphProcessor.h"
void WorldGenerator::NodeEditor::GraphProcessor::ProcessGraph(WorldGenerator::NodeEditor::GraphData& data)
{
    m_pResult = std::make_shared<WorldGenerator::NodeEditor::WorldGenConfig>();

    _CheckValidity(data);
    _CheckRecursion(data);

    _CountNodes(data);
    _AssignsOutputs(data);
}
void WorldGenerator::NodeEditor::GraphProcessor::Flush()
{

}
std::shared_ptr<WorldGenerator::NodeEditor::WorldGenConfig> WorldGenerator::NodeEditor::GraphProcessor::GetConfig()
{
    return m_pResult;
}
void WorldGenerator::NodeEditor::GraphProcessor::_CheckValidity(WorldGenerator::NodeEditor::GraphData& data)
{

}
void WorldGenerator::NodeEditor::GraphProcessor::_CheckRecursion(WorldGenerator::NodeEditor::GraphData& data)
{

}
void WorldGenerator::NodeEditor::GraphProcessor::_CountNodes(WorldGenerator::NodeEditor::GraphData& data)
{
    for(auto & node : data.m_Nodes) {

    }
}
void WorldGenerator::NodeEditor::GraphProcessor::_AssignsOutputs(WorldGenerator::NodeEditor::GraphData& data)
{

}
void WorldGenerator::NodeEditor::GraphProcessor::_AddOperations(WorldGenerator::NodeEditor::GraphData& data)
{

}
void WorldGenerator::NodeEditor::GraphProcessor::_SortOperations(WorldGenerator::NodeEditor::GraphData& data)
{

}
