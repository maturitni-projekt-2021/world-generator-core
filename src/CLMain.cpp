//
// Created by Matty on 11/21/2020.
//
#include "../world-gen/gpugen/cl/CLMain.h"
#include "../world-gen/Log.h"
#include "../world-gen/data/ProjectData.h"

#define CL_ERROR_CHECK() if(err != 0) WG_LOG_ERROR((std::string("OpenCL error code: ") + std::to_string(err)).c_str());

WorldGenerator::GPU::CL::CLMain::CLMain() {

}

void WorldGenerator::GPU::CL::CLMain::Init() {
    auto err = cl::Platform::get(&m_pPlatforms);
    CL_ERROR_CHECK();

    for (uint32_t i = 0; i < m_pPlatforms.size(); i++) {
        m_pDevices[i] = std::vector<cl::Device>();
        err = m_pPlatforms.at(i).getDevices(CL_DEVICE_TYPE_GPU, &m_pDevices.at(i));
        CL_ERROR_CHECK();
        for (uint32_t j = 0; j < m_pDevices.at(i).size(); j++) {
            auto info = std::make_shared<DeviceInfoCL>(m_pDevices.at(i).at(j));
            m_pDeviceInfos[std::pair<uint32_t, uint32_t>(i, j)] = info;
        }

        if (!m_pSupported && !m_pDevices.at(i).empty()) {
            m_pSupported = true;
            m_pActiveDevice = std::make_pair(i, 0);
        }
    }
}

bool WorldGenerator::GPU::CL::CLMain::IsSupported() const {
    return m_pSupported;
}

void WorldGenerator::GPU::CL::CLMain::GenerateShader() {
    if (!m_pActivated) return;

    if (m_pActiveShader) {
        //TODO: Drop old shader
        m_pActiveShader = false;
    }

    auto layers = Data::ProjectData::m_ProjectData->m_WorldGenDataActive;

    //TODO: Build shader from layers

    auto shader = std::string("sdadasd");

    //TODO: Upload shader to gpu
    auto source = cl::Program::Sources(1, std::make_pair(shader.c_str(), shader.length() + 1));

    auto prg = cl::Program(m_pContext, source);

    auto err = prg.build("-cl-std=1.2");
    CL_ERROR_CHECK()

    assert(err == 0);

    m_pVoxelKernels.reserve(m_pKernelCount);

    for (uint32_t i = 0; i < m_pKernelCount; i++) {
        m_pVoxelKernels.emplace_back(std::make_shared<cl::Kernel>(prg, "Voxel", &err));
        CL_ERROR_CHECK()
    }

    m_pActiveVersion = layers->m_Version;
    m_pActiveShader = true;

    WG_LOG_INFO("GEN SHADER");
}

void WorldGenerator::GPU::CL::CLMain::Activate() {
    m_pContext = cl::Context(m_pDevices.at(m_pActiveDevice.first).at(m_pActiveDevice.second));
    m_pActivated = true;
    m_pKernelCount = 1; //FIXME
    m_pFreeWorkers = 1;
    m_pFinishCallbacks = std::vector<cl::Event>(1);
}

WorldGenerator::GPU::CL::CLMain::~CLMain() {
    m_pContext = nullptr;
}

uint32_t
WorldGenerator::GPU::CL::CLMain::ProcessChunk(const WorldGenerator::World::WorldChunkPos &pos, uint32_t resolution) {
    uint32_t workerID = 0; //FIXME: Fix worker id

    assert(m_pActiveWorkers.find(workerID) == m_pActiveWorkers.end());
    //1. Calculate buffer size
    //2. Create buffer and put it to map of worker to std::vector
    m_WorkerData[workerID] = std::make_shared<WorkerData>(pos.x, pos.y, pos.z, resolution);
    //3. Select one of free kernels
    auto kernel = m_pVoxelKernels.at(workerID);
    //4. Create buffer objects
    m_WorkerData[workerID]->m_ResolutionBuffer = std::make_shared<cl::Buffer>(m_pContext,
                                                                              CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                                                                              sizeof(uint32_t));
    m_WorkerData[workerID]->m_PosBuffer = std::make_shared<cl::Buffer>(m_pContext,
                                                                       CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                                                                       sizeof(int) * 3);
    m_WorkerData[workerID]->m_ResultBuffer = std::make_shared<cl::Buffer>(m_pContext,
                                                                          CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
                                                                          sizeof(float) *
                                                                          m_WorkerData[workerID]->m_Result.size());
    //5. Set args
    kernel->setArg(0, *m_WorkerData[workerID]->m_PosBuffer);
    kernel->setArg(1, *m_WorkerData[workerID]->m_ResolutionBuffer);
    kernel->setArg(2, *m_WorkerData[workerID]->m_ResultBuffer);
    //6. Start work
    cl::CommandQueue queue(m_pContext, m_pDevices.at(m_pActiveDevice.first).at(m_pActiveDevice.second));
    queue.enqueueNDRangeKernel(*kernel, cl::NullRange, cl::NDRange(resolution, resolution, resolution), cl::NullRange,
                               nullptr, &m_pFinishCallbacks[workerID]);
    queue.enqueueReadBuffer(*m_WorkerData[workerID]->m_ResultBuffer, CL_FALSE, 0,
                            sizeof(float) * m_WorkerData[workerID]->m_Result.size(),
                            m_WorkerData[workerID]->m_Result.data());

    m_pActiveWorkers.emplace(workerID);
    m_pFinishCallbacks.at(workerID) = {};
    return workerID;
}

void WorldGenerator::GPU::CL::CLMain::Update() {
    if (!m_pActivated) return;
    for (uint32_t i = 0; i < m_pFinishCallbacks.size(); i++) {
        if (m_pActiveWorkers.find(i) != m_pActiveWorkers.end()) {
            auto &&event = m_pFinishCallbacks.at(i);
            if (event.getInfo<CL_EVENT_COMMAND_EXECUTION_STATUS>() == CL_COMPLETE) {
                //TODO : Set task status to finished
                //TODO : Finished
                assert(false);
            }
        }
    }
}

WorldGenerator::GPU::CL::DeviceInfoCL::DeviceInfoCL(const cl::Device &device) {
    auto err = 0;
    m_Vendor = device.getInfo<CL_DEVICE_VENDOR>(&err);
    CL_ERROR_CHECK();
    m_Version = device.getInfo<CL_DEVICE_VERSION>(&err);
    CL_ERROR_CHECK();
    m_Name = device.getInfo<CL_DEVICE_NAME>(&err);
    CL_ERROR_CHECK();
}

WorldGenerator::GPU::CL::WorkerData::WorkerData(int32_t x, int32_t y, int32_t z, uint32_t resolution) {
    m_Pos[0] = x;
    m_Pos[1] = y;
    m_Pos[2] = z;
    m_Result = std::vector<float>(resolution * resolution * resolution);
    m_Resolution = resolution;
}
