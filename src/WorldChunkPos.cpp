//
// Created by mattyvrba on 9/17/20.
//

#include "../world-gen/world/WorldChunkPos.h"

bool WorldGenerator::World::WorldChunkPos::operator==(const WorldGenerator::World::WorldChunkPos &other) const {
    return (x == other.x && y == other.y && z == other.z);
}

bool WorldGenerator::World::WorldChunkPos::operator<(const WorldGenerator::World::WorldChunkPos &other) const {
    if (x < other.x)
        return true;
    else if (x > other.x)
        return false;

    if (y < other.y)
        return true;
    else if (y > other.y)
        return false;

    if (z < other.z)
        return true;
    else if (z > other.z)
        return false;

    return false;
}
