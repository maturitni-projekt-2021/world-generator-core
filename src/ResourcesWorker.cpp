//
// Created by Matty on 4.9.2020.
//

#include "../world-gen/resources/ResourcesWorker.h"

void WorldGenerator::Resources::ResourcesWorker::_Stop()
{
//    glfwDestroyWindow(m_pContext);
    m_pToIncreaseOnEnd++;
}
void WorldGenerator::Resources::ResourcesWorker::_Setup()
{
//    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
//    m_pContext = glfwCreateWindow(128, 128, "", NULL, NULL);
//    glfwMakeContextCurrent(m_pContext);
}
void WorldGenerator::Resources::ResourcesWorker::_Run()
{
    _Setup(); // Init of core variables
    while (!m_pStop) { // Main loop
        std::unique_lock<std::mutex> lock = std::unique_lock<std::mutex>(m_pMutex);//CV Mutex

        if (m_pTaskQueue.Empty()) { //Is queue empty? if yes, sleep (lock-free operation)
            m_pCV.wait(lock);
        }

        if (m_pStop) { // If thread should stop, we will do it now, before we pop
            break;
        }
        try {
            auto task = m_pTaskQueue.Pop(); //Pop task
            task(); // Process task
        }
        catch (std::exception& e) {
            //Pop throws exception if nothing could be popped
        }
    }
    _Stop(); // Cleanup
}
WorldGenerator::Resources::ResourcesWorker::ResourcesWorker(std::condition_variable& cv, std::atomic_int& endCheckCount,
        bool& stop, WorldGenerator::Util::ThreadSafeQueue<std::function<void()>>& queue)
        :m_pCV(cv), m_pToIncreaseOnEnd(endCheckCount), m_pStop(stop), m_pTaskQueue(queue)
{
    m_pThread = std::thread([this]() { _Run(); });
    WorldGenerator::Util::UtilThread::SetThreadPrio(m_pThread,Util::WG_THREAD_PRIORITY_BELOW_NORMAL);
}
void WorldGenerator::Resources::ResourcesWorker::Join()
{
    if (m_pThread.joinable()) {
        m_pThread.join();
    }
}
