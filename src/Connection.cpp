//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/node_editor/Connection.h"
std::pair<uint32_t, uint32_t> WorldGenerator::NodeEditor::Connection::GetIndices() const
{
    return std::pair<uint32_t, uint32_t>(m_pFirst,m_pSecond);
}
bool WorldGenerator::NodeEditor::Connection::HasPinConnection(uint32_t pinID) const
{
    return m_pFirst == pinID || m_pSecond == pinID;
}
WorldGenerator::NodeEditor::Connection::Connection(uint32_t first,uint32_t second,int32_t color) : m_pFirst(first), m_pSecond(second), m_Color(color)
{

}
