//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/data/EditorData.h"
WorldGenerator::Data::EditorData::EditorData()
{
    m_VoxelCanvas = std::make_shared<NodeEditor::Canvases::VoxelEditorCanvas>();
    m_PopulationCanvas = std::make_shared<NodeEditor::Canvases::PopulationEditorCanvas>();
}

std::unique_ptr<WorldGenerator::Data::EditorData> WorldGenerator::Data::EditorData::m_EditorData;