//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/world/World.h"

#include "../world-gen/data/RenderData.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/world_generator/WorldGenerator.h"
#include "../world-gen/data/PerformanceData.h"
#include "../world-gen/Log.h"
#include "../world-gen/data/SettingsData.h"
#include "../world-gen/data/ProjectData.h"

void WorldGenerator::World::World::Render() {
    for (int32_t i = 0; i < Data::SettingsData::m_SettingsData->m_MaxLoadedChunks->m_Value; i++) {
        if (m_pTerrain[i]) {
            if (m_pTerrain[i]->m_Terrain)
#ifndef NDEBUG
                m_pDrawBatch->AddTerrain(m_pTerrain[i]->m_Terrain, &m_pTerrain[i]->m_CullCollider,
                                         m_pTerrain[i]->m_DebugCenterLine);
#else
            m_pDrawBatch->AddTerrain(m_pTerrain[i]->m_Terrain, &m_pTerrain[i]->m_CullCollider);
#endif
            for (auto &a : m_pTerrain[i]->m_WorldObjects) {
                a->Render(m_pDrawBatch);
            }
        }
    }

    m_pDrawBatch->RenderBatch(m_pCameras);
    m_pDrawBatch->Empty();
}

WorldGenerator::World::World::World() {
    m_pCameras.emplace_back(Data::RenderData::m_RenderData->m_ViewportCamera);
    m_pDrawBatch = std::make_shared<WorldGenerator::Render::DrawBatch>();
    m_pTerrain = new std::shared_ptr<TerrainChunk>[Data::SettingsData::m_SettingsData->m_MaxLoadedChunks->m_Value];
    _FillFreeSlots();
}

void WorldGenerator::World::World::Update() {
    using namespace WorldGenerator::Data;

    while (!m_pInserts.Empty()) {
        try {
            const auto task = m_pInserts.Pop();
            if (task->m_Data) {
                if (task->m_Version == m_pConfigVersion) {
                    if (m_pLoadedChunks.find(task->m_Pos) != m_pLoadedChunks.end()) {
                        ClearTerrain(task->m_Pos);
                    }

                    m_pTerrain[task->m_Slot] = std::make_shared<WorldGenerator::World::TerrainChunk>(task->m_Pos,
                                                                                                     task->m_Data,
                                                                                                     task->m_WorldObjects,
                                                                                                     task->m_Resolution);
                    PerformanceData::m_PerformanceData->m_FinishedChunks++;
                    m_pLoadedChunks.emplace(task->m_Pos);
                    m_pChunkSlots[task->m_Pos] = task->m_Slot;
                    m_pRequestedChunks.erase(task->m_Pos);
                }
            } else {
                WG_LOG_ERROR("Received task without data");
            }
        } catch (std::exception &e) {

        }
    }

    if (m_pStopping) return;

    int32_t genRange = Data::SettingsData::m_SettingsData->m_ChunkGenDistance->m_Value;
    int32_t delRange = Data::SettingsData::m_SettingsData->m_ChunkGenDistance->m_Value + 1;
    WorldData::m_CenterPosition = RenderData::m_RenderData->m_ViewportCamera->GetPosition();
    auto center = WorldData::m_CenterPosition;

    const WorldChunkPos centerChunk = WorldGenerator::World::WorldChunkPos(
            center.x / (Global::s_pChunkSize * Global::s_WorldScale),
            center.y / (Global::s_pChunkSize * Global::s_WorldScale),
            center.z / (Global::s_pChunkSize * Global::s_WorldScale));


    std::vector<WorldGenerator::World::WorldChunkPos> toDelete;
    toDelete.reserve(Data::SettingsData::m_SettingsData->m_MaxLoadedChunks->m_Value);

    for (int32_t i = 0; i < Data::SettingsData::m_SettingsData->m_MaxLoadedChunks->m_Value; i++) {
        if (m_pTerrain[i]) {
            if (abs(m_pTerrain[i]->m_Position.x - centerChunk.x) > delRange ||
                abs(m_pTerrain[i]->m_Position.y - centerChunk.y) > delRange ||
                abs(m_pTerrain[i]->m_Position.z - centerChunk.z) > delRange) {
                toDelete.emplace_back(m_pTerrain[i]->m_Position);
            }
        }
    }

    for (auto &a : toDelete) {
        ClearTerrain(a);
    }

    std::vector<std::shared_ptr<WorldGenerator::TerrainGeneration::ChunkGenerationTask>> requests;
    requests.reserve((genRange + genRange + 1) * (genRange + genRange + 1));

    // 1. Get free threads
    // 2. Add them to vector
    auto freeWorkers = TerrainGeneration::WorldGenerator::m_WorldGenerator->GetFreeWorkers();

    if (Data::SettingsData::m_SettingsData->m_Quality->m_Value <
        1) { Data::SettingsData::m_SettingsData->m_Quality->m_Value = 1; }
    if (Data::SettingsData::m_SettingsData->m_Quality->m_Value >
        4) { Data::SettingsData::m_SettingsData->m_Quality->m_Value = 4; }


    if (Data::ProjectData::m_ProjectData->m_WorldGenDataActive->m_Version ==
        Data::ProjectData::m_ProjectData->m_WorldGenDataEditable->m_Version) {
        if (!freeWorkers.empty()) {
            uint32_t requestedCount = 0;

            // Y +-1 around camera
            //MAX Prio low res
            _GenChunkLODSameLevel(genRange / 3 * 1, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 2,
                                  requestedCount, freeWorkers, centerChunk,3);

            //MAX Prio
            _GenChunkLODSameLevel(genRange / 3 * 1, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 4,
                                  requestedCount, freeWorkers, centerChunk,3);

            //LOW Prio
            _GenChunkLODSameLevel(genRange, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 2,
                                  requestedCount, freeWorkers, centerChunk, 2);

            //MED Prio
            _GenChunkLODSameLevel(genRange / 3 * 2, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 3,
                                  requestedCount, freeWorkers, centerChunk);

            /*
            //MAX Prio
            _GenChunkLOD(genRange / 3 * 1, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 4, requestedCount,
                         freeWorkers, centerChunk);
            //LOW Prio
            _GenChunkLOD(genRange, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 2, requestedCount,
                         freeWorkers, centerChunk);

            //MED Prio
            _GenChunkLOD(genRange / 3 * 2, Data::SettingsData::m_SettingsData->m_Quality->m_Value * 3, requestedCount,
                         freeWorkers, centerChunk);
            */


            if (requestedCount > 0) TerrainGeneration::WorldGenerator::m_WorldGenerator->NotifyWorkers();
        }
    } else {
        if (freeWorkers.size() ==
            WorldGenerator::TerrainGeneration::WorldGenerator::m_WorldGenerator->GetWorkersCount()) {
            Data::ProjectData::m_ProjectData->m_WorldGenDataActive = std::shared_ptr<WorldGenerator::Data::WorldGenerationData>(
                    new WorldGenerator::Data::WorldGenerationData(
                            *Data::ProjectData::m_ProjectData->m_WorldGenDataEditable.get()));
            _ClearTerrainForce();
        }
    }
    // 3. Get currently most important chunks based on main camera direction
    // 4. Request most prio threads, with further chunks having lower resolution
    // 5. Notify CV

}

void WorldGenerator::World::World::SpawnWorldObject(std::shared_ptr<WorldObject> spawn) {
    //m_pWorldObjects.emplace_back(std::move(spawn));
}

void
WorldGenerator::World::World::AddTerrain(const WorldGenerator::World::WorldChunkPos &pos,
                                         std::shared_ptr<Terrain> data, uint32_t slot,
                                         std::vector<std::shared_ptr<WorldObject>> worldObjects, uint32_t res,
                                         uint32_t version) {
    m_pInserts.Push(std::make_shared<TerrainInsert>(std::move(data), pos, slot, std::move(worldObjects), res, version));
}

void WorldGenerator::World::World::ClearTerrain(const WorldGenerator::World::WorldChunkPos &pos) {
    try {
        uint32_t slot = m_pChunkSlots.at(pos);

        if (m_pTerrain[slot]) {
            if (m_pTerrain[slot]->m_Position == pos) {
                m_pTerrain[slot].reset();
                m_pLoadedChunks.erase(pos);
                m_pRequestedSlots.erase(pos);
                m_pChunkSlots.erase(pos);
                m_pFreeSlots.push(slot);
            } else {
                WG_LOG_ERROR("Slot position doesnt match with generated position");
            }
        } else {
            WG_LOG_ERROR("Slot to clear does not contain data");
        }
    } catch (std::exception &e) {
        WG_LOG_ERROR(e.what());
    }
}

WorldGenerator::World::World::~World() {
    delete[] m_pTerrain;
}

void WorldGenerator::World::World::_GenChunkLOD(int32_t maxRad, uint32_t resolution, uint32_t &requestCount,
                                                const std::vector<uint32_t> &workers,
                                                const WorldChunkPos &centerChunk) {
    for (int32_t x = -maxRad; x <= maxRad; x++) {
        for (int32_t y = -maxRad; y <= maxRad; y++) {
            for (int32_t z = -maxRad; z <= maxRad; z++) {
                const auto chunkPos = WorldGenerator::World::WorldChunkPos(centerChunk.x + x, centerChunk.y + y,
                                                                           centerChunk.z + z);
                if ((m_pLoadedChunks.find(chunkPos) == m_pLoadedChunks.end() ||
                     m_pTerrain[m_pChunkSlots.at(chunkPos)]->m_Resolution < resolution) &&
                    m_pRequestedChunks.find(chunkPos) == m_pRequestedChunks.end()) {
                    if (requestCount < workers.size()) {
                        if (!m_pFreeSlots.empty()) {
                            TerrainGeneration::WorldGenerator::m_WorldGenerator->AddWorkerJob(
                                    workers.at(requestCount++),
                                    std::make_shared<WorldGenerator::TerrainGeneration::ChunkGenerationTask>(
                                            chunkPos,
                                            m_pFreeSlots.front(),
                                            resolution, m_pConfigVersion));
                            m_pFreeSlots.pop();
                            m_pRequestedChunks.emplace(chunkPos);
                        }
                    }
                }
            }
        }
    }

}

void WorldGenerator::World::World::_GenChunkLODSameLevel(int32_t maxRad, uint32_t resolution, uint32_t &requestCount,
                                                         const std::vector<uint32_t> &workers,
                                                         const WorldGenerator::World::WorldChunkPos &centerChunk,
                                                         int32_t rangeHorizontal) {
    for (int32_t x = -maxRad; x <= maxRad; x++) {
        for (int y = -rangeHorizontal; y <= rangeHorizontal; y++) {
            for (int32_t z = -maxRad; z <= maxRad; z++) {
                const auto chunkPos = WorldGenerator::World::WorldChunkPos(centerChunk.x + x, centerChunk.y + y,
                                                                           centerChunk.z + z);
                if ((m_pLoadedChunks.find(chunkPos) == m_pLoadedChunks.end() ||
                     m_pTerrain[m_pChunkSlots.at(chunkPos)]->m_Resolution < resolution) &&
                    m_pRequestedChunks.find(chunkPos) == m_pRequestedChunks.end()) {
                    if (requestCount < workers.size()) {
                        if (!m_pFreeSlots.empty()) {
                            TerrainGeneration::WorldGenerator::m_WorldGenerator->AddWorkerJob(
                                    workers.at(requestCount++),
                                    std::make_shared<WorldGenerator::TerrainGeneration::ChunkGenerationTask>(
                                            chunkPos,
                                            m_pFreeSlots.front(),
                                            resolution,
                                            m_pConfigVersion));
                            m_pFreeSlots.pop();
                            m_pRequestedChunks.emplace(chunkPos);
                        }
                    }
                }
            }
        }
    }
}

void WorldGenerator::World::World::ResetBuild() {
    std::set<WorldGenerator::World::WorldChunkPos> toDelete = m_pLoadedChunks;
    TerrainGeneration::WorldGenerator::m_WorldGenerator->DropData();
    Data::ProjectData::m_ProjectData->m_WorldGenDataEditable->m_Version = ++m_pConfigVersion;
}

void WorldGenerator::World::World::_ClearTerrainForce() {
    try {
        for (uint32_t i = 0; i < Data::SettingsData::m_SettingsData->m_MaxLoadedChunks->m_Value; i++) {
            if (m_pTerrain[i]) {
                m_pTerrain[i].reset();
            }
        }

        m_pLoadedChunks.clear();
        m_pRequestedSlots.clear();
        m_pRequestedChunks.clear();
        m_pChunkSlots.clear();
        WorldGenerator::Data::WorldData::m_Noiser = FastNoise(Data::ProjectData::m_ProjectData->m_Seed);
        WorldGenerator::Data::WorldData::m_Noiser.SetFrequency(Global::s_WorldScale);
        TerrainGeneration::WorldGenerator::m_WorldGenerator->RebuildGPUData();
        while (!m_pFreeSlots.empty()) m_pFreeSlots.pop();
        _FillFreeSlots();
    } catch (std::exception &e) {
        WG_LOG_ERROR(e.what());
    }
}

void WorldGenerator::World::World::_FillFreeSlots() {
    for (uint32_t i = 0; i < Data::SettingsData::m_SettingsData->m_MaxLoadedChunks->m_Value; i++) {
        m_pFreeSlots.push(i);
    }
}

void WorldGenerator::World::World::DisableWorldGen() {
    m_pStopping = true;
}

WorldGenerator::World::TerrainInsert::TerrainInsert(std::shared_ptr<Terrain> data,
                                                    const WorldGenerator::World::WorldChunkPos &pos, uint32_t slot,
                                                    std::vector<std::shared_ptr<WorldObject>> worldObjects,
                                                    uint32_t res, uint32_t version) : m_Data(
        std::move(data)),
                                                                                      m_Pos(pos),
                                                                                      m_Slot(slot),
                                                                                      m_WorldObjects(
                                                                                              std::move(
                                                                                                      worldObjects)),
                                                                                      m_Resolution(res),
                                                                                      m_Version(version) {}

WorldGenerator::World::TerrainChunk::TerrainChunk(const WorldGenerator::World::WorldChunkPos &mPosition,
                                                  const std::shared_ptr<Terrain> &mTerrain,
                                                  std::vector<std::shared_ptr<WorldObject>> worldObjects, uint32_t res)
        : m_Position(mPosition),
          m_Terrain(mTerrain),
          m_WorldObjects(std::move(worldObjects)), m_Resolution(res),
          m_CullCollider(
                  glm::vec3( (mPosition.x) * Global::s_pChunkSize * Global::s_WorldScale + (Global::s_pChunkSize/2 * Global::s_WorldScale),
                                    (mPosition.y) * Global::s_pChunkSize * Global::s_WorldScale - (Global::s_pChunkSize/2 * Global::s_WorldScale),
                                    (mPosition.z) * Global::s_pChunkSize * Global::s_WorldScale + (Global::s_pChunkSize/2 * Global::s_WorldScale)),
                  glm::vec3(Global::s_pChunkSize * Global::s_WorldScale)) {
#ifndef NDEBUG
    m_DebugCenterLine = std::make_shared<Render::Debug::DebugLineStrip>(m_CullCollider.GetPosition());
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {-Global::s_pChunkSize / 2, Global::s_pChunkSize / 2, Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {Global::s_pChunkSize / 2, Global::s_pChunkSize / 2, Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {Global::s_pChunkSize / 2, Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {-Global::s_pChunkSize / 2, Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {-Global::s_pChunkSize / 2, Global::s_pChunkSize / 2, Global::s_pChunkSize / 2}));

    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {-Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2, Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2, Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {-Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2}));
    m_DebugCenterLine->AddPoint(Render::Custom::DebugVertex(
            {-Global::s_pChunkSize / 2, -Global::s_pChunkSize / 2, Global::s_pChunkSize / 2}));
#endif
}

WorldGenerator::World::TerrainChunk::~TerrainChunk() {
    m_Terrain.reset();
}
