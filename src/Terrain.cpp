//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/world/Terrain.h"
#include "../world-gen/render/custom/ModelVertex.h"
#include "../world-gen/data/PerformanceData.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/render/custom/TerrainVertex.h"
#include "../world-gen/Log.h"


#include <glm/gtc/matrix_transform.hpp>
#include <glad/glad.h>

const WorldGenerator::World::WorldChunkPos &WorldGenerator::World::Terrain::GetChunkPosition() const {
    return m_pChunkPosition;
}

WorldGenerator::World::Terrain::Terrain(std::shared_ptr<Render::VertexBuffer> vertexBuffer,
                                        WorldGenerator::World::WorldChunkPos chunkPosition)
        : m_pVertexBuffer(vertexBuffer), m_pChunkPosition(chunkPosition) {
    WorldGenerator::Data::PerformanceData::m_PerformanceData->m_LoadedChunks++;
}

void WorldGenerator::World::Terrain::Render(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &pos) {
    using namespace WorldGenerator::Render;
    using namespace WorldGenerator::Data;

    if (!m_pVertexBuffer) {
        return;
    }

    uint32_t vertCount = 0;

    try {
        vertCount = m_pVertexBuffer->getElementCount();
    }
    catch (std::exception &e) {
        return;
    }

    if (vertCount == 0) {
        return;
    }

    if (m_pVertexBuffer->bind()) {

        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_FRONT);
        if (!m_pModelMatrixCached) {
            auto model = glm::mat4(1.0f);
            model = glm::translate(model, {m_pChunkPosition.x * Global::s_pChunkSize * Global::s_WorldScale,
                                           m_pChunkPosition.y * Global::s_pChunkSize * Global::s_WorldScale,
                                           m_pChunkPosition.z * Global::s_pChunkSize * Global::s_WorldScale});
            model = glm::scale(model, glm::vec3(Global::s_pChunkSize * Global::s_WorldScale));
            m_pModelMatrixCached = true;
            m_pModelMatrix = model;
        }


        RenderData::m_RenderData->m_TerrainShader->activate();
        RenderData::m_RenderData->m_TerrainShader->setUniformMat4("u_projection", projection);
        RenderData::m_RenderData->m_TerrainShader->setUniformMat4("u_view", view);
        RenderData::m_RenderData->m_TerrainShader->setUniformMat4("u_model", m_pModelMatrix);
        RenderData::m_RenderData->m_TerrainShader->setUniformVec3("u_cameraPos", pos);
        RenderData::m_RenderData->m_TerrainShader->setUniformFloat("u_worldScale", Global::s_WorldScale);


        auto layout = Custom::TerrainVertex::GetModelVertexLayout();
        layout.setAttributePointers();

        glDrawArrays(GL_TRIANGLES, 0, vertCount);
        PerformanceData::m_PerformanceData->m_DrawCalls++;
        glDisable(GL_CULL_FACE);
    } else {
        WG_LOG_DEBUG("FAILED TO BIND BUFFER");
    }

}

WorldGenerator::World::Terrain::~Terrain() {
    if (m_pVertexBuffer) {
        m_pVertexBuffer.reset();
    }
    m_pModelMatrixCached = false;
    WorldGenerator::Data::PerformanceData::m_PerformanceData->m_LoadedChunks--;
}
