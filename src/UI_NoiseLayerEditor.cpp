//
// Created by Matty on 11/9/2020.
//

#include "../world-gen/ui/custom/UI_NoiseLayerEditor.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/util/UtilString.h"
#include "../world-gen/imgui/imgui_internal.h"

WorldGenerator::UI::Custom::UI_NoiseLayerEditor::UI_NoiseLayerEditor() : UIWindow("Noise editor") {
}

void WorldGenerator::UI::Custom::UI_NoiseLayerEditor::CustomDraw() {
    using namespace WorldGenerator::Data;
    auto projectData = ProjectData::m_ProjectData;
    static bool update = true;

    static int32_t switchFrom = -1;
    static int32_t switchTo = -1;

    static bool rebuildAfterDrag = false;

    static bool requestOpenPopup = false;
    static int32_t popupIndex = 0;

    ImGui::BeginChild("##leftSideNoiseL", {146.0f, ImGui::GetWindowSize().y - 24.0f});
    ImGui::TextUnformatted("Layers");
    ImGui::SameLine(146.0f - 40.0f);
    if (ImGui::Button("Add", {40.0f, 20.0f})) {
        ImGui::OpenPopup("Select noise");
    }
    if (ImGui::ListBoxHeader("##Layers", {146.0f, ImGui::GetWindowSize().y - 24.0f})) {
        int32_t i = 0;
        for (auto &l : projectData->m_WorldGenDataEditable->m_NoiseLayers) {
            if (ImGui::Selectable(l->GetName().c_str(), i == m_pSelectedIndex,
                                  ImGuiSelectableFlags_AllowDoubleClick)) {
                if (i != m_pSelectedIndex) {
                    m_pSelectedIndex = i;
                    update = true;
                }
            }
            if (ImGui::IsMouseClicked(ImGuiMouseButton_Right) && ImGui::IsItemHovered()) {
                requestOpenPopup = true;
                popupIndex = i;
            }

            ImGuiDragDropFlags src_flags = 0;
            src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;     // Keep the source displayed as hovered
            src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers; // Because our dragging is local, we disable the feature of opening foreign treenodes/tabs while dragging
            //src_flags |= ImGuiDragDropFlags_SourceNoPreviewTooltip; // Hide the tooltip

            if (ImGui::BeginDragDropSource(src_flags)) {
                if (!(src_flags & ImGuiDragDropFlags_SourceNoPreviewTooltip))
                    ImGui::Text("%s", l->GetName().c_str());
                ImGui::SetDragDropPayload("dragLayer", &i, sizeof(int32_t));
                ImGui::EndDragDropSource();
            }

            if (ImGui::BeginDragDropTarget()) {
                ImGuiDragDropFlags target_flags = 0;
                target_flags |= ImGuiDragDropFlags_AcceptBeforeDelivery;    // Don't wait until the delivery (release mouse button on a target) to do something
                target_flags |= ImGuiDragDropFlags_AcceptNoDrawDefaultRect; // Don't display the yellow rectangle
                if (const ImGuiPayload *payload = ImGui::AcceptDragDropPayload("dragLayer", target_flags)) {
                    switchFrom = *(const int32_t *) payload->Data;
                    switchTo = i;
                }
                ImGui::EndDragDropTarget();
            }
            i++;
        }
        ImGui::ListBoxFooter();
    } //176.0f

    if (requestOpenPopup) {
        ImGui::OpenPopup("noiseLayerMenu");
        requestOpenPopup = false;
    }

    if (ImGui::BeginPopup("noiseLayerMenu")) {
        if (ImGui::MenuItem("Remove")) {
            projectData->m_WorldGenDataEditable->m_NoiseLayers.erase(
                    projectData->m_WorldGenDataEditable->m_NoiseLayers.cbegin() + popupIndex);
            if (popupIndex == m_pSelectedIndex) {
                if (m_pSelectedIndex == projectData->m_WorldGenDataEditable->m_NoiseLayers.size() &&
                    m_pSelectedIndex != 0) {
                    m_pSelectedIndex--;
                }
            }
            WorldData::m_World->ResetBuild();
            update = true;
        }
        if (ImGui::MenuItem("Duplicate")) {
            projectData->AddNoiseLayer(projectData->m_WorldGenDataEditable->m_NoiseLayers.at(popupIndex),
                                       popupIndex);
            update = true;
        }
        if (ImGui::BeginMenu("Add to group:")) {
            if (ImGui::MenuItem("Root")) {
            }
            if (ImGui::MenuItem("NoRoot")) {
            }
            if (ImGui::MenuItem("MoRoot")) {
            }
            ImGui::EndMenu();
        }
        ImGui::EndPopup();
    }

    if (switchFrom != -1 && switchTo != -1) {
        projectData->m_WorldGenDataEditable->SwapLayers(switchFrom, switchTo);
        m_pSelectedIndex = switchTo;
        update = true;
        rebuildAfterDrag = true;
        switchFrom = -1;
        switchTo = -1;
    }

    if (rebuildAfterDrag) {
        if (!ImGui::IsMouseDown(ImGuiMouseButton_Left)) {
            WorldData::m_World->ResetBuild();
            rebuildAfterDrag = false;
        }
    }

    if (ImGui::BeginPopup("Select noise")) {
        if (ImGui::MenuItem("Perlin 2D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_PERLIN_2D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Perlin 3D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_PERLIN_3D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Simplex 2D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_SIMPLEX_2D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Simplex 3D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_SIMPLEX_3D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("White 2D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_WHITE_2D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("White 3D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_WHITE_3D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Fractal 2D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_FRACTAL_2D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Fractal 3D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_FRACTAL_3D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Celluar 2D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CELLUAR_2D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Celluar 3D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CELLUAR_3D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Cubic 2D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CUBIC_2D, m_pSelectedIndex);
            update = true;
        }
        if (ImGui::MenuItem("Cubic 3D")) {
            projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CUBIC_3D, m_pSelectedIndex);
            update = true;
        }
        ImGui::EndPopup();
    }

    ImGui::EndChild();

    if (projectData->m_WorldGenDataEditable->m_NoiseLayers.empty()) update = false;

    static char selectedName[128];
    static float scale;
    static float intesity;
    static float rangeOffset;
    static int selectedAddition;
    static bool clampAfter;
    static bool canBeIntensityMask;
    static float bias;
    static bool enableBias;
    static bool enable;

    static const char *const selectionOptions[] = {"Additive", "Subtractive", "Multiply", "Additive (2D Mask)",
                                                   "Subtractive (2D Mask)", "Multiply (2D Mask)"};
    static std::string noiseTypeName;
    if (update) {
        for (uint32_t i = 0; i < 128; i++) {
            selectedName[i] =
                    projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetName().size() > i
                    ? projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetName().at(i) : '\0';
        }
        scale = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->GetScale();
        intesity = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->GetIntensity();
        selectedAddition = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->GetData()->GetAddition();
        rangeOffset = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->GetData()->GetRangeOffset();
        clampAfter = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->GetData()->GetClampAfter();
        noiseTypeName = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->GetData()->GetTypeName();
        bias = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->GetData()->GetBiasValue();
        enableBias = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->GetData()->GetApplyBias();

        enable = projectData->m_WorldGenDataEditable->m_NoiseLayers.at(
                m_pSelectedIndex)->IsEnabled();

        if (m_pSelectedIndex == 0) {
            canBeIntensityMask = false;
        } else {
            if (projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex - 1)->GetData()->GetType() >=
                6 &&
                projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->GetType() >= 6) {
                canBeIntensityMask = true;
            }
        }
        update = false;
    }

    if (!projectData->m_WorldGenDataEditable->m_NoiseLayers.empty()) {
        ImGui::SameLine(162.0f);
        ImGui::BeginChild("##layerSettings");
        ImGui::Text("Noise type: %s", noiseTypeName.c_str());
        ImGui::Checkbox("Enabled",&enable);
        if (ImGui::InputText("Name", selectedName, IM_ARRAYSIZE(selectedName), ImGuiInputTextFlags_EnterReturnsTrue)) {
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->SetName(
                    Util::trim_copy(std::string(selectedName)));
        }
        ImGui::DragFloat("Scale", &scale, 0.1f);
        ImGui::DragFloat("Intensity", &intesity, 0.1f);

        if (ImGui::BeginCombo("Addition", selectionOptions[selectedAddition])) {
            for (int32_t i = 0; i < IM_ARRAYSIZE(selectionOptions); i++) {
                if (!canBeIntensityMask && i >= 3) {
                    if (ImGui::Selectable(selectionOptions[i], selectedAddition == i,
                                          ImGuiSelectableFlags_Disabled)) {
                    }
                } else {
                    if (ImGui::Selectable(selectionOptions[i], selectedAddition == i,
                                          ImGuiSelectableFlags_AllowDoubleClick)) {
                        selectedAddition = i;
                    }
                }

            }
            ImGui::EndCombo();
        }
        ImGui::DragFloat("Range offset", &rangeOffset, 0.1f);
        ImGui::Checkbox("Clamp noise after applying", &clampAfter);
        ImGui::Checkbox("Apply bias after applying", &enableBias);
        if (!enableBias) {
            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::GetStyle().Alpha = 0.25f;
        }
        ImGui::SliderFloat("Bias Value", &bias, 0.0f, 4.0f, "%.3f", ImGuiSliderFlags_ClampOnInput);
        if (!enableBias) {
            ImGui::PopItemFlag();
            ImGui::GetStyle().Alpha = 1.0f;
        }
        ImGui::TextUnformatted("Settings here");

        ImGui::BeginChild("offsetLa", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing() - 15.0f));
        ImGui::EndChild();
        ImGui::Dummy({0, 0});
        ImGui::SameLine(ImGui::GetWindowContentRegionMax().x - 56.0f);
        if (ImGui::Button("Apply", {56.0f, 20.0f})) {
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->SetName(
                    Util::trim_copy(std::string(selectedName)));
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetScale(scale);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetIntensity(intesity);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetAddition(
                    (TerrainGeneration::Data::AdditionType) selectedAddition);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetRangeOffset(
                    rangeOffset);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetClampAfter(
                    clampAfter);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetBiasValue(
                    bias);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->GetData()->SetApplyBias(
                    enableBias);
            projectData->m_WorldGenDataEditable->m_NoiseLayers.at(m_pSelectedIndex)->SetEnabled(enable);

            update = true;
            WorldData::m_World->ResetBuild();
        }

        ImGui::EndChild();
    }
}

void WorldGenerator::UI::Custom::UI_NoiseLayerEditor::CustomWindowSetup() {
    UIWindow::CustomWindowSetup();
}
