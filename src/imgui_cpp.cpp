//
// Created by Matty on 5.9.2020.
//

#include "../world-gen/imgui/imgui_cpp.h"

bool ImGui::Combo(const char* label, int* currIndex, std::vector<std::string>& values)
{
    if (values.empty()) { return false; }
    return ImGui::Combo(label, currIndex, ImGui::vector_getter,
            static_cast<void*>(&values), values.size());
}

bool ImGui::ListBox(const char* label, int* currIndex, std::vector<std::string>& values)
{
    if (values.empty()) { return false; }
    return ImGui::ListBox(label, currIndex, ImGui::vector_getter,
            static_cast<void*>(&values), values.size());
}