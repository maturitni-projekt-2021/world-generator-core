//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/world/WorldObject.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/util/UtilMath.h"
#include "../world-gen/data/SettingsData.h"
#include <glm/gtc/matrix_transform.hpp>

void WorldGenerator::World::WorldObject::Render(const std::shared_ptr<WorldGenerator::Render::DrawBatch> &drawBatch) {
    if (Util::UtilMath::Dist2(GetPosition(), Data::WorldData::m_CenterPosition) <
        std::pow(Data::SettingsData::m_SettingsData->m_DebrisMaxRenderDistance->m_Value, 2.0f))
        //TODO: LOD
    {
        if (m_pHasModel) {
            if (m_pModels->m_ModelLoaded) {
                for (auto &b : m_pModels->m_Models) {
                    auto tfm = glm::mat4(1.0f);
                    tfm = glm::translate(tfm, m_pPosition);
                    tfm = glm::rotate(tfm, m_pRotation.x, {1, 0, 0});
                    tfm = glm::rotate(tfm, m_pRotation.y, {0, 1, 0});
                    tfm = glm::rotate(tfm, m_pRotation.z, {0, 0, 1});
                    tfm = glm::scale(tfm, m_pScale);
                    tfm = glm::scale(tfm, glm::vec3(Global::s_WorldScale / 100.0));
                    drawBatch->AddModelTransform(b->GetID(), tfm * b->GetBaseOffset());
                }
            }
        }
    }
}

void WorldGenerator::World::WorldObject::SetScale(const glm::vec3 &scale) {
    m_pScale = scale;
}

void WorldGenerator::World::WorldObject::SetPosition(const glm::vec3 &pos) {
    m_pPosition = pos;
}

void WorldGenerator::World::WorldObject::SetRotation(const glm::vec3 &rot) {
    m_pRotation = rot;
}

void WorldGenerator::World::WorldObject::SetModels(
        const std::shared_ptr<WorldGenerator::Data::ModelData> &models) {
    m_pModels = models;
}

const glm::vec3 &WorldGenerator::World::WorldObject::GetPosition() const {
    return m_pPosition;
}

const glm::vec3 &WorldGenerator::World::WorldObject::GetRotation() const {
    return m_pRotation;
}

const glm::vec3 &WorldGenerator::World::WorldObject::GetScale() const {
    return m_pScale;
}
