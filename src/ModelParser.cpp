//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/resources/ModelParser.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/Log.h"
#include "../world-gen/util/UtilFile.h"
#include "../world-gen/render/custom/ModelVertex.h"
#include "../world-gen/resources/ResourcesWorkerPool.h"
#include "../world-gen/render/Renderer.h"
#include "../world-gen/render/RenderTask.h"
#include "../world-gen/data/ProjectData.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <map>
#include <glm/gtc/matrix_transform.hpp>

void WorldGenerator::Resources::ModelParser::LoadModel(
        const std::shared_ptr<WorldGenerator::Data::ModelData> &data) {
    WorldGenerator::Resources::ResourcesWorkerPool::m_ResourcesWorkerPool->AddTask([data]() { _LoadModelToGPU(data); });
}

void WorldGenerator::Resources::ModelParser::_LoadModelToGPU(
        const std::shared_ptr<WorldGenerator::Data::ModelData> &data) {
    Assimp::Importer importer;

    auto dataD = Data::ProjectData::m_ProjectData->GetModelToMemory(data->m_Path);

    const aiScene * scene = importer.ReadFileFromMemory(dataD.first,dataD.second,aiProcess_Triangulate);

    //const aiScene *scene = importer.ReadFile(data->m_Path, aiProcess_Triangulate);

    if (!scene) {
        WG_LOG_ERROR(importer.GetErrorString());
        data->m_LoadFailed = true;
        data->m_ErrorText = importer.GetErrorString();
        return;
    }

    auto root = scene->mRootNode;
    _CrawlNode(root, root->mTransformation, data, scene);

    for (uint32_t i = 0; i < data->m_Models.size(); i++) {
        if (data->m_ModelTextures.find(i) != data->m_ModelTextures.end()) {
            if (Data::ProjectData::m_ProjectData->m_TextureData.find(data->m_ModelTextures.at(i)) !=
                Data::ProjectData::m_ProjectData->m_TextureData.end()) {
                data->m_Models.at(i)->SetAlbedoTexture(
                        Data::ProjectData::m_ProjectData->m_TextureData.at(data->m_ModelTextures.at(i)));
            } else {
                //TODO: Use missing texture
            }
        }
    }

    data->m_ModelLoaded = true;
}

void WorldGenerator::Resources::ModelParser::_BuildModel(const std::shared_ptr<WorldGenerator::Data::ModelData> &data,
                                                         const aiMesh *mesh,
                                                         aiMatrix4x4 transform, const aiMaterial *mat) {
    std::vector<Render::Custom::ModelVertex> vertices;

    vertices.reserve(mesh->mNumVertices);

    bool hasNormals = mesh->HasNormals();
    bool hasUV = mesh->HasTextureCoords(0);
    bool hasColors = mesh->HasVertexColors(0);

    for (int32_t i = 0; i < mesh->mNumVertices; i++) {
        const auto importedVertex = mesh->mVertices[i];
        const auto convertedVertex = glm::vec3(importedVertex.x, importedVertex.y, importedVertex.z);

        aiVector3t<ai_real> importedNormal;
        if (hasNormals) {
            importedNormal = mesh->mNormals[i];
        }
        const auto convertedNormal = hasNormals ? glm::vec3(importedNormal.x, importedNormal.y, importedNormal.z)
                                                : glm::vec3(0.0f);

        aiVector3t<ai_real> importedUV;
        if (hasUV) {
            importedUV = mesh->mTextureCoords[0][i];
        }
        const auto convertedUV = hasUV ? glm::vec2(importedUV.x, importedUV.y) : glm::vec2(0.0f);

        aiColor4t<ai_real> importedVertexColor;
        if (hasColors) {
            importedVertexColor = mesh->mColors[i][0];
        }
        const auto convertedVertexColor = hasColors ? glm::vec4(importedVertexColor.r, importedVertexColor.g,
                                                                importedVertexColor.b, importedVertexColor.a)
                                                    : glm::vec4(1.0f);

        vertices.emplace_back(
                WorldGenerator::Render::Custom::ModelVertex(convertedVertex, convertedVertexColor, convertedUV,
                                                            convertedNormal));
    }

    std::vector<uint32_t> indices;
    indices.reserve(mesh->mNumFaces * 3);

    for (uint32_t i = 0; i < mesh->mNumFaces; i++) {
        if (mesh->mFaces[i].mNumIndices == 3) {
            for (uint32_t j = 0; j < mesh->mFaces[i].mNumIndices; j++) {
                indices.emplace_back(mesh->mFaces[i].mIndices[j]);
            }
        } else {
            WG_LOG_WARN((std::string("Imported model with face containing face thats not triangulated! : ") +
                         data->m_Path).c_str());
        }
    }

    std::vector<float> rawVertices;
    rawVertices.reserve(vertices.size() * vertices.at(0).getFloatCount());

    for (auto &v : vertices) {
        rawVertices.insert(rawVertices.end(), v.data().begin(), v.data().end());
    }

    //TODO: Move to static function in UtilConvert
    glm::mat4 offsetFinal = glm::mat4(1.0f);

    offsetFinal[0][0] = transform.a1;
    offsetFinal[1][0] = transform.a2;
    offsetFinal[2][0] = transform.a3;
    offsetFinal[3][0] = transform.a4;

    offsetFinal[0][1] = transform.b1;
    offsetFinal[1][1] = transform.b2;
    offsetFinal[2][1] = transform.b3;
    offsetFinal[3][1] = transform.b4;

    offsetFinal[0][2] = transform.c1;
    offsetFinal[1][2] = transform.c2;
    offsetFinal[2][2] = transform.c3;
    offsetFinal[3][2] = transform.c4;

    offsetFinal[0][3] = transform.d1;
    offsetFinal[1][3] = transform.d2;
    offsetFinal[2][3] = transform.d3;
    offsetFinal[3][3] = transform.d4;

    std::string name = mesh->mName.C_Str();

    auto task = std::make_shared<WorldGenerator::Render::RenderTaskUploadModelData>(rawVertices, indices,
                                                                                    mesh->mNumFaces * 3, offsetFinal,
                                                                                    name);

    WorldGenerator::Render::Renderer::m_Renderer->AddRenderTask(task);

    while (!task->GetDone()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(333));
    }

    auto model = task->GetResult();

    WorldGenerator::Data::RenderData::m_RenderData->m_LoadedCustomModels[model->GetID()] = model;
    data->m_Models.emplace_back(model);
}

void WorldGenerator::Resources::ModelParser::_CrawlNode(aiNode *node, aiMatrix4x4 transform,
                                                        const std::shared_ptr<WorldGenerator::Data::ModelData> &data,
                                                        const aiScene *scene) {
    const auto nextTransform = node->mTransformation * transform;
    for (uint32_t i = 0; i < node->mNumChildren; i++) {
        for (uint32_t j = 0; j < node->mChildren[i]->mNumMeshes; j++) {
            _BuildModel(data, scene->mMeshes[node->mChildren[i]->mMeshes[j]],
                        node->mChildren[i]->mTransformation * nextTransform,
                        scene->mMaterials[scene->mMeshes[node->mChildren[i]->mMeshes[j]]->mMaterialIndex]);
        }
        _CrawlNode(node->mChildren[i], nextTransform, data, scene);
    }
}
