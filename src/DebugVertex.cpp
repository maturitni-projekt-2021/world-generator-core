//
// Created by Matty on 11/2/2020.
//

#include "../world-gen/render/custom/DebugVertex.h"

WorldGenerator::Render::VertexLayout WorldGenerator::Render::Custom::DebugVertex::GetDebugVertexLayout() {
    auto layout = VertexLayout(1);
    auto posComponent = VertexLayoutComponent(3);
    layout.addAttributeComponent(posComponent);
    return layout;
}

WorldGenerator::Render::Custom::DebugVertex::DebugVertex(const glm::vec3 &pos) {
    m_pData.reserve(getFloatCount());
    m_pData.emplace_back(pos.x);
    m_pData.emplace_back(pos.y);
    m_pData.emplace_back(pos.z);
}

uint32_t WorldGenerator::Render::Custom::DebugVertex::getFloatCount() {
    return 3 ;
}

WorldGenerator::Render::VertexLayout WorldGenerator::Render::Custom::DebugVertex::getVertexLayout() {
    auto layout = VertexLayout(1);

    auto posComponent = VertexLayoutComponent(3);

    layout.addAttributeComponent(posComponent);
    return layout;
}
