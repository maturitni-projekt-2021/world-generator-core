//
// Created by Matty on 11/11/2020.
//

#include "../world-gen/data/WorldGenerationData.h"

#include <memory>
#include "../world-gen/util/UtilFormat.h"
#include "../world-gen/world_generator/data/PerlinNoiseLayer.h"
#include "../world-gen/world_generator/data/SimplexNoiseLayer.h"
#include "../world-gen/world_generator/data/WhiteNoiseLayer.h"
#include "../world-gen/world_generator/data/FractalNoiseLayer.h"
#include "../world-gen/world_generator/data/CelluarNoiseLayer.h"
#include "../world-gen/world_generator/data/CubicNoiseLayer.h"
#include "../world-gen/Log.h"

void WorldGenerator::Data::WorldGenerationData::Save(nlohmann::json &json) {
    auto jzon = nlohmann::json::array();
    for (auto &a : m_NoiseLayers) {
        a->Save(jzon);
    }
    auto biomeData = nlohmann::json::array();
    for (auto &biome : m_Biomes) {
        auto biomeJson = nlohmann::json();
        biomeJson["id"] = biome.first;
        biome.second->Save(biomeJson);
        biomeData.emplace_back(biomeJson);
    }
    json["biomes"] = biomeData;
    json["noise_layers"] = jzon;
}

void WorldGenerator::Data::WorldGenerationData::Load(nlohmann::json &json) {
    if (json.contains("noise_layers")) {
        m_NoiseLayers.reserve(json.at("noise_layers").size());
        for (auto &noiseJson : json.at("noise_layers")) {
            auto name = noiseJson.contains("name") ? noiseJson.at("name").get<std::string>() : "name_import_failed";
            auto type = noiseJson.contains("type") ? noiseJson.at("type").get<uint32_t>() : 99999;
            if (type == 99999) { continue; }
            auto noiseLayer = GetNoiseLayerFromType(type);
            noiseLayer->SetScale(noiseJson.contains("scale") ? noiseJson.at("scale").get<float>() : 1.0f);
            noiseLayer->SetIntensity(noiseJson.contains("intensity") ? noiseJson.at("intensity").get<float>() : 1.0f);
            noiseLayer->SetOffset(
                    noiseJson.contains("offset") ? Util::UtilFormat::JsonToVec3(noiseJson.at("offset")) : glm::vec3(
                            0.0f));
            noiseLayer->SetClampAfter(noiseJson.contains("clamp") ? noiseJson.at("clamp").get<bool>() : false);
            noiseLayer->SetAddition((TerrainGeneration::Data::AdditionType)(noiseJson.contains("addition_type") ? noiseJson.at("addition_type").get<int8_t>() : 0));
            noiseLayer->SetApplyBias((TerrainGeneration::Data::AdditionType)(noiseJson.contains("bias_active") ? noiseJson.at("bias_active").get<bool>() : false));
            noiseLayer->SetBiasValue((TerrainGeneration::Data::AdditionType)(noiseJson.contains("bias_value") ? noiseJson.at("bias_value").get<float>() : 0.0f));

            m_NoiseLayers.emplace_back(std::make_shared<Data::NoiseLayerData>(name, noiseLayer));
        }
    }
    for (auto &biome : json.at("biomes")) {
        auto res = std::make_shared<WorldGenerator::World::Biome>();
        res->Load(biome);
        m_Biomes[biome.at("id").get<uint32_t>()] = res;
    }
}

std::shared_ptr<WorldGenerator::TerrainGeneration::Data::NoiseLayer>
WorldGenerator::Data::WorldGenerationData::GetNoiseLayerFromType(uint32_t type) {
    auto typeEn = (WorldGenerator::TerrainGeneration::Data::NoiseType) type;

    switch (typeEn) {
        case TerrainGeneration::Data::WG_NOISE_PERLIN_2D:
            return std::make_shared<TerrainGeneration::Data::PerlinNoiseLayer2D>();
        case TerrainGeneration::Data::WG_NOISE_PERLIN_3D:
            return std::make_shared<TerrainGeneration::Data::PerlinNoiseLayer3D>();
        case TerrainGeneration::Data::WG_NOISE_SIMPLEX_2D:
            return std::make_shared<TerrainGeneration::Data::SimplexNoiseLayer2D>();
        case TerrainGeneration::Data::WG_NOISE_SIMPLEX_3D:
            return std::make_shared<TerrainGeneration::Data::SimplexNoiseLayer3D>();
        case TerrainGeneration::Data::WG_NOISE_WHITE_2D:
            return std::make_shared<TerrainGeneration::Data::WhiteNoiseLayer2D>();
        case TerrainGeneration::Data::WG_NOISE_WHITE_3D:
            return std::make_shared<TerrainGeneration::Data::WhiteNoiseLayer3D>();
        case TerrainGeneration::Data::WG_NOISE_FRACTAL_2D:
            return std::make_shared<TerrainGeneration::Data::FractalNoiseLayer2D>();
        case TerrainGeneration::Data::WG_NOISE_FRACTAL_3D:
            return std::make_shared<TerrainGeneration::Data::FractalNoiseLayer3D>();
        case TerrainGeneration::Data::WG_NOISE_CELLUAR_2D:
            return std::make_shared<TerrainGeneration::Data::CelluarNoiseLayer2D>();
        case TerrainGeneration::Data::WG_NOISE_CELLUAR_3D:
            return std::make_shared<TerrainGeneration::Data::CelluarNoiseLayer3D>();
        case TerrainGeneration::Data::WG_NOISE_CUBIC_2D:
            return std::make_shared<TerrainGeneration::Data::CubicNoiseLayer2D>();
        case TerrainGeneration::Data::WG_NOISE_CUBIC_3D:
            return std::make_shared<TerrainGeneration::Data::CubicNoiseLayer3D>();
    }
    return std::make_shared<TerrainGeneration::Data::PerlinNoiseLayer2D>();
}

WorldGenerator::Data::WorldGenerationData::WorldGenerationData(const WorldGenerator::Data::WorldGenerationData &cpy) {
    this->m_NoiseLayers.clear();
    this->m_NoiseLayers.reserve(cpy.m_NoiseLayers.size());
    for (auto &l : cpy.m_NoiseLayers) {
        this->m_NoiseLayers.emplace_back(std::make_shared<NoiseLayerData>(*l));
    }
    for (auto &l : cpy.m_Biomes) {
        this->m_Biomes[l.first] = (std::make_shared<World::Biome>(*l.second));
    }
    this->m_Version = cpy.m_Version;
}

void WorldGenerator::Data::WorldGenerationData::SwapLayers(uint32_t a, uint32_t b) {
    if(a == b) return;
    if(m_NoiseLayers.size() <= a || m_NoiseLayers.size() <= b) return;
    std::iter_swap(m_NoiseLayers.begin() + a, m_NoiseLayers.begin() + b);
}
