//
// Created by Matty on 29.8.2020.
//

#include "../world-gen/App.h"
#include "../world-gen/ui/UI_Main.h"
#include "../world-gen/resources/ModelParser.h"
#include "../world-gen/util/UtilTick.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/Log.h"
#include "../world-gen/data/PerformanceData.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/data/EditorIds.h"
#include "../world-gen/resources/ResourcesWorkerPool.h"
#include "../world-gen/data/EditorData.h"
#include "../world-gen/world_generator/WorldGenerator.h"
#include "../world-gen/data/SettingsData.h"
#include "../world-gen/resources/TextureParser.h"

#include <memory>

void WorldGenerator::App::_MainLoop() {
    using namespace WorldGenerator::Render;
    using namespace WorldGenerator::Data;

    while (m_pRunning) {
        Util::UtilTick::FrameStart(); // Tell our util for framing that we are starting new frame
        PerformanceData::m_PerformanceData->m_DrawCalls = 0; // Reset draw call count
        glfwPollEvents(); // Get data from system
        Renderer::m_Renderer->SetWireframeMode(RenderData::m_RenderData->m_ViewWireframe);
        m_pWindow->PreRender(); // Do stuff in window before rendering
        m_pInput->Update(m_pWindow->GetGlfwWindow()); // Update input on max pull rate

        SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value = std::clamp(
                SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value, 0, 2);

        if (m_pUiProjSelect->IsProjectLoaded() && !m_pProjectSelected) {
            m_pProjectSelected = true;
            Data::WorldData::m_Noiser = FastNoise(ProjectData::m_ProjectData->m_Seed);
            WorldData::m_Noiser.SetFrequency(Global::s_WorldScale);
            glfwSetWindowTitle(m_pWindow->GetGlfwWindow().get(),
                               (std::string("World Generator : ") + ProjectData::m_ProjectData->m_ProjectName).c_str());
        } // If project is loaded, set window name to match project name

        while (Util::UtilTick::ShouldTick()) {
            Renderer::m_Renderer->Update();
            if (m_pProjectSelected) WorldData::m_World->Update();
            if (m_pProjectSelected) m_pInput->FixedUpdate();
            RenderData::m_RenderData->m_ViewportCamera->Update();
            m_pGPUWorker->Update();
        } // Update main components, these are called tickRate times per second

        RenderData::m_RenderData->m_FBViewport->FullBind(); // Bind main viewport, rendering code
        if (m_pUi->IsWorldViewVisible() && m_pProjectSelected) {
            WorldData::m_World->Render();
        }  // Render what main camera sees if we see viewport

        FrameBuffer::UnbindAll(); // Unbind all frame buffers, rendering code
        if (RenderData::m_RenderData->m_ViewWireframe) Renderer::m_Renderer->SetWireframeMode(false);
        if (!m_pStopping) {
            if (m_pProjectSelected) {
                m_pUi->Render(m_pUiData);
            } else {
                m_pUiProjSelect->Render(m_pUiData);
            }
        } else {
            //TODO: Stopping UI
        } // Render UI if we are not stopping app, else stopping UI
        m_pWindow->FinalizeRender(); // Switch buffers and show new frame on screen

        if (glfwWindowShouldClose(m_pWindow->GetGlfwWindow().get())) {
            m_pStopping = true;
        } // Check if we clicked on close button

        if (m_pStopping) {
            WorldData::m_World->DisableWorldGen();
            if (TerrainGeneration::WorldGenerator::m_WorldGenerator->GetFreeWorkers().size() ==
                TerrainGeneration::WorldGenerator::m_WorldGenerator->GetWorkersCount()) {
                m_pRunning = false;
            }
        } // If stopping, check if all workers are ready to stop

        Util::UtilTick::FrameEnd(); // End of frame code, thread sleep inside
    }
}

void WorldGenerator::App::_Init() {
    _MakeWindow();
    _InitUi();
    _InitSettings();
    _InitRenderData();
    _InitPerformanceData();
    Util::UtilTick::Init();
    _InitInput();
    Data::WorldData::m_World = std::make_unique<World::World>();
    _InitResources();
    _InitNodeEditor();
    _InitWorldGenerator();
}

int32_t WorldGenerator::App::Start() {
    // Loads all modules
    _Init();
    // Does main loop until stopped by user
    _MainLoop();
    // Cleanup
    _Stop();
    // If we want to restart app, we send return code 1 else we send 0 to stop
    if (m_pRestart) {
        return 1;
    }
    return 0;
}

void WorldGenerator::App::_Stop() {
    if (Data::ProjectData::m_ProjectData) {
        Data::ProjectData::m_ProjectData->Save();
    }
    m_pUiData->Stop();
    WorldGenerator::Resources::ResourcesWorkerPool::m_ResourcesWorkerPool->Stop();
    WorldGenerator::TerrainGeneration::WorldGenerator::m_WorldGenerator->Stop();
    Render::Renderer::m_Renderer.reset();
    Data::RenderData::m_RenderData.reset();
    Data::WorldData::m_World.reset();
    Data::ProjectData::m_ProjectData.reset();
}

void WorldGenerator::App::_MakeWindow() {
    auto builder = std::make_shared<Render::WindowBuilder>(1280, 720)
            ->setWindowName("World generator")
            ->setBackgroundColor(glm::vec4(1.0f));

    m_pWindow = std::shared_ptr<Render::Window>(builder->finish());
    glfwSetWindowUserPointer(m_pWindow->GetGlfwWindow().get(), m_pInput.get());
    Render::Renderer::m_Renderer->Start();
}

void WorldGenerator::App::_InitUi() {
    m_pUiData = std::make_shared<UI::ImGUI_Core>(m_pWindow->GetGlfwWindow());
    m_pUiData->Init();

    m_pUi = std::make_shared<UI::UI_Main>();
    m_pUi->Setup();
    m_pUiProjSelect = std::make_shared<UI::UI_ProjectSelect>();
    m_pUiProjSelect->Setup();
    WG_LOG_INFO("Initialized UI");
}

WorldGenerator::App::App() {
    glfwInit();
    m_pRestart = false;
    m_pStopping = false;
}

WorldGenerator::App::~App() {
    //
}

void WorldGenerator::App::_InitRenderData() {
    using namespace WorldGenerator::Data;
    using namespace WorldGenerator::Render;

    //Initializes basic data
    RenderData::m_RenderData = std::make_unique<RenderData>();
    RenderData::m_RenderData->m_FBViewport = std::make_shared<FBViewport>(1920, 1080);
    RenderData::m_RenderData->m_FBViewport->SetClearColor({0.4f, 0.4f, 0.35f, 1.0f});
    RenderData::m_RenderData->m_ViewportCameraFB = std::make_shared<FBViewportCamera>(1920, 1080, true);
    RenderData::m_RenderData->m_ViewportCameraFB->SetClearColor({0.4f, 0.4f, 0.35f, 1.0f});
    if (SettingsData::m_SettingsData->m_EnableAntiAliasing->m_Value &&
        SettingsData::m_SettingsData->m_AntiAliasingLevel->m_Value > 0) {
        RenderData::m_RenderData->m_ViewportCamera = std::make_shared<Camera>(75.0f, 0.1f, 100.0f, 16.0f / 9.0f,
                                                                              RenderData::m_RenderData->m_ViewportCameraFB);
    } else {
        RenderData::m_RenderData->m_ViewportCamera = std::make_shared<Camera>(75.0f, 0.1f, 100.0f, 16.0f / 9.0f,
                                                                              RenderData::m_RenderData->m_FBViewport);
    }
    // Sets camera starting position
    RenderData::m_RenderData->m_ViewportCamera->SetRotation({125, -27, 0});
    RenderData::m_RenderData->m_ViewportCamera->SetPosition({2.0f, 1.0f, 2.0f});
    RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();

    auto modelShaderBuilder = std::make_shared<ShaderBuilder>("resources/shaders/model.vert",
                                                              "resources/shaders/model.frag");

    auto terrainShaderBuilder = std::make_shared<ShaderBuilder>("resources/shaders/terrain.vert",
                                                                "resources/shaders/terrain.frag");

#ifndef NDEBUG
    auto debugShaderBuilder = std::make_shared<ShaderBuilder>("resources/shaders/debug.vert",
                                                              "resources/shaders/debug.frag");
#endif

    try {
        auto shaderModelRaw = modelShaderBuilder->finish();
        RenderData::m_RenderData->m_ModelShader = std::shared_ptr<Shader>(shaderModelRaw);
        auto shaderTerrainRaw = terrainShaderBuilder->finish();
        RenderData::m_RenderData->m_TerrainShader = std::shared_ptr<Shader>(shaderTerrainRaw);
#ifndef NDEBUG
        auto shaderDebugRaw = debugShaderBuilder->finish();
        RenderData::m_RenderData->m_DebugShader = std::shared_ptr<Shader>(shaderDebugRaw);
#endif
    }
    catch (WorldGenerator::Exceptions::WGException &e) {
        WG_LOG_ERROR(e.what());
    }

    WG_LOG_INFO("Initialized render data");
}

void WorldGenerator::App::_InitPerformanceData() {
    Data::PerformanceData::m_PerformanceData = std::make_unique<Data::PerformanceData>();
}

void WorldGenerator::App::Restart() {
    m_pRestart = true;
    m_pStopping = true;
}

void WorldGenerator::App::StopGLFW() {
    glfwTerminate();
}

void WorldGenerator::App::Exit() {
    m_pStopping = true;
}

void WorldGenerator::App::_InitResources() {
    using namespace WorldGenerator::Resources;
    ResourcesWorkerPool::m_ResourcesWorkerPool = std::make_unique<ResourcesWorkerPool>();
    ResourcesWorkerPool::m_ResourcesWorkerPool->SetupWorkers();
    auto noTex = std::make_shared<Data::TextureData>();
    noTex->m_Name = "missing";
    noTex->m_Path = "resources/textures/no_texture.png";
    Data::ProjectData::m_MissingTexture = noTex;
    Resources::TextureParser::LoadTexture(Data::ProjectData::m_MissingTexture, true);
}

void WorldGenerator::App::_InitNodeEditor() {
    WorldGenerator::Data::EditorIds::m_IDs = std::make_unique<WorldGenerator::Data::EditorIds>();
    WorldGenerator::Data::EditorData::m_EditorData = std::make_unique<WorldGenerator::Data::EditorData>();
}

void WorldGenerator::App::_InitWorldGenerator() {
    Global::s_pChunkSize = 128.0f;
    Global::s_WorldScale = 1.0f / 64.0f;
    m_pGPUWorker = std::make_unique<GPU::GPUWorker>();
    m_pGPUWorker->Init();
    if (Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value != 0) {
        m_pGPUWorker->Activate(Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value == 1
                               ? GPU::GPUWorkerType::WG_GPU_WORKER_CL : GPU::GPUWorkerType::WG_GPU_WORKER_CUDA);
    }
    WorldGenerator::TerrainGeneration::WorldGenerator::m_WorldGenerator = std::make_unique<WorldGenerator::TerrainGeneration::WorldGenerator>(m_pGPUWorker);
}

void WorldGenerator::App::_InitSettings() {
    using namespace WorldGenerator::Data;
    SettingsData::m_SettingsData = std::make_unique<SettingsData>();
    SettingsData::m_SettingsData->Load();
    SettingsData::m_SettingsData->Save();
}

void WorldGenerator::App::_InitInput() {
    m_pInput = std::make_shared<Input::InputController>();
    m_pInput->Init(m_pWindow->GetGlfwWindow());
}

bool WorldGenerator::App::m_pRestart;
bool WorldGenerator::App::m_pStopping;
