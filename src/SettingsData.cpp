//
// Created by Matty on 10/24/2020.
//

#include "../world-gen/data/SettingsData.h"

#include <utility>
#include "../world-gen/util/UtilFile.h"

std::unique_ptr<WorldGenerator::Data::SettingsData> WorldGenerator::Data::SettingsData::m_SettingsData;

void WorldGenerator::Data::SettingsData::Load() {
    if(WorldGenerator::Util::UtilFile::FileExists("config.toml")){
        try {
            auto config = cpptoml::parse_file("config.toml");
            for(auto & s : m_pSettings) {
                s->Load(config);
            }
        } catch (std::exception & e) {

        }
    }
}

WorldGenerator::Data::SettingsData::SettingsData() {
    WG_SETTING_SOURCE(EnableAntiAliasing)
    WG_SETTING_SOURCE(NoiseOctaves)
    WG_SETTING_SOURCE(Seed)
    WG_SETTING_SOURCE(AntiAliasingLevel)
    WG_SETTING_SOURCE(DebrisMaxRenderDistance)
    WG_SETTING_SOURCE(ChunkGenDistance)
    WG_SETTING_SOURCE(MaxLoadedChunks)
    WG_SETTING_SOURCE(MovingCam)
    WG_SETTING_SOURCE(Quality)
    WG_SETTING_SOURCE(TerrainGenerationEngine)
}

void WorldGenerator::Data::SettingsData::Save() {
    auto config = cpptoml::make_table();
    for(auto & s : m_pSettings) {
        s->Save(config);
    }
    auto ostream = std::ofstream("config.toml", std::ofstream::out);
    ostream << *config;
    ostream.close();
}

WorldGenerator::Data::Setting::Setting(std::string mName) : m_Name(std::move(mName)) {
}

void WorldGenerator::Data::SettingFloat::Load(const std::shared_ptr<cpptoml::table> &toml) {
    m_Value = toml->get_as<double>(m_Name).value_or(m_Value);
}

WorldGenerator::Data::SettingFloat::SettingFloat(const std::string &mName, float mValue) : Setting(mName),
                                                                                           m_Value(mValue) {}

void WorldGenerator::Data::SettingFloat::Save(const std::shared_ptr<cpptoml::table> &toml) {
    toml->insert(m_Name,(double)m_Value);
}

void WorldGenerator::Data::SettingInt::Load(const std::shared_ptr<cpptoml::table> &toml) {
    m_Value = toml->get_as<int32_t>(m_Name).value_or(m_Value);
}

WorldGenerator::Data::SettingInt::SettingInt(const std::string &mName, int32_t mValue) : Setting(mName),
                                                                                         m_Value(mValue) {}

void WorldGenerator::Data::SettingInt::Save(const std::shared_ptr<cpptoml::table> &toml) {
    toml->insert(m_Name,m_Value);
}

void WorldGenerator::Data::SettingString::Load(const std::shared_ptr<cpptoml::table> &toml) {
    m_Value = toml->get_as<std::string>(m_Name).value_or(m_Value);
}

WorldGenerator::Data::SettingString::SettingString(const std::string &mName, std::string mValue) : Setting(mName),
                                                                                                           m_Value(std::move(mValue)) {}

void WorldGenerator::Data::SettingString::Save(const std::shared_ptr<cpptoml::table> &toml) {
    toml->insert(m_Name,m_Value);
}

WorldGenerator::Data::SettingBool::SettingBool(const std::string &mName, bool mValue) : Setting(mName),
                                                                                        m_Value(mValue) {}

void WorldGenerator::Data::SettingBool::Load(const std::shared_ptr<cpptoml::table> &toml) {
    m_Value = toml->get_as<bool>(m_Name).value_or(m_Value);
}

void WorldGenerator::Data::SettingBool::Save(const std::shared_ptr<cpptoml::table> &toml) {
    toml->insert(m_Name,m_Value);
}
