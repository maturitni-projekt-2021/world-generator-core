//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/node_editor/nodes/VoxelNode.h"
#include "../world-gen/data/EditorIds.h"

WorldGenerator::NodeEditor::Nodes::VoxelNode::VoxelNode(uint32_t id,std::map<uint32_t,std::shared_ptr<Pin>> & pinStorage) : Node(id,pinStorage) {
    m_pName = ("Voxel node");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<FloatPin>(pinID,"Value",false,m_pValue);
    m_pInputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinB1 = std::make_shared<BiomePin>(pinID,"Biome 1",false);
    m_pInputPins.emplace_back(pinB1);
    pinStorage[pinID] = pinB1;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinB2 = std::make_shared<BiomePin>(pinID,"Biome 2",false);
    m_pInputPins.emplace_back(pinB2);
    pinStorage[pinID] = pinB2;
}

void WorldGenerator::NodeEditor::Nodes::VoxelNode::_DrawExtra() {
    Node::_DrawExtra();
}

bool WorldGenerator::NodeEditor::Nodes::VoxelNode::IsDeletable() const {
    return false;
}
uint32_t WorldGenerator::NodeEditor::Nodes::VoxelNode::GetOutputCount() const
{
    return 0;
}
uint32_t WorldGenerator::NodeEditor::Nodes::VoxelNode::GetInputCount() const
{
    return 3;
}
void WorldGenerator::NodeEditor::Nodes::VoxelNode::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}
