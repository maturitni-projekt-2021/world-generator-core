//
// Created by Matty on 29.8.2020.
//

#include "../world-gen/data/RenderData.h"
#include "../world-gen/ui/UI_Main.h"
#include "../world-gen/ui/UILog.h"
#include "../world-gen/imgui/imgui_internal.h"

#include "../world-gen/ui/custom/UI_ModelBrowser.h"
#include "../world-gen/ui/custom/UI_TextureBrowser.h"
#include "../world-gen/ui/custom/UI_Performance.h"
#include "../world-gen/ui/custom/UI_Options.h"
#include "../world-gen/App.h"
#include "../world-gen/ui/custom/UI_NodeEditor.h"
#include "../world-gen/world_generator/WorldGenerator.h"
#include "../world-gen/imgui/imgui_action.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/ui/custom/UI_NoiseLayerEditor.h"
#include "../world-gen/data/SettingsData.h"
#include "../world-gen/ui/custom/UI_WorldObjectEditor.h"
#include "../world-gen/ui/custom/UI_BiomeEditor.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

WorldGenerator::UI::UI_Main::UI_Main() {
    m_Windows.reserve(9);
    m_Windows.emplace_back(std::make_shared<Custom::UI_ModelBrowser>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_TextureBrowser>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_Performance>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_Options>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_ModelInspector>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_NoiseLayerEditor>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_NodeEditor>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_WorldObjectEditor>());
    m_Windows.emplace_back(std::make_shared<Custom::UI_BiomeEditor>());

    ImActionManager::Instance().RemoveAll();
    m_pActionSave = ImActionManager::Instance().Create("Save", ImCtrl + 'S');
    m_pActionRebuild = ImActionManager::Instance().Create("Regenerate", ImCtrl + ImAlt + 'B');
}

void WorldGenerator::UI::UI_Main::Render(const std::shared_ptr<ImGUI_Core> &data) {
    UI::Render(data);

    bool collapsed;
    ImGuiWindowFlags flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
    ImGuiViewport *viewport = ImGui::GetMainViewport();
    ImGuiID dockspace_id;
    ImGui::SetNextWindowPos(viewport->GetWorkPos());
    ImGui::SetNextWindowSize(viewport->GetWorkSize());
    ImGui::SetNextWindowViewport(viewport->ID);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
             ImGuiWindowFlags_NoMove;
    flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
        flags |= ImGuiWindowFlags_NoBackground;

    ImGui::Begin("Viewport", &collapsed, flags);

    ImGui::PopStyleVar(2);

    if (data->GetDockingEnabled()) {
        dockspace_id = ImGui::GetID("MyDockSpace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
    }

    ImActionManager::Instance().ResetAll();

    if (ImGui::BeginMenuBar()) {
        if (ImGui::BeginMenu("Main")) {
            ImGui::MenuItem(m_pActionSave);

            ImGui::Separator();

            if (ImGui::MenuItem("Restart", NULL)) { WorldGenerator::App::Restart(); }
            ImGui::Separator();

            if (ImGui::MenuItem("Exit")) { WorldGenerator::App::Exit(); }

            ImGui::Separator();

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("World Generator")) {
            ImGui::MenuItem(m_pActionRebuild);

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Preferences")) {
                if (ImGui::BeginMenu("Chunk Generation Engine")) {
                    if (ImGui::MenuItem("CPU (Stable)",nullptr,Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value == 0)) {

                    }
                    if (ImGui::MenuItem("GPU OpenCL (Experimental)",nullptr,Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value == 1)) {

                    }
                    if (ImGui::MenuItem("GPU CUDA (WIP)",nullptr,Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value == 2,false)) {

                    }
                    ImGui::EndMenu();
                }

            ImGui::Separator();

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Help")) {
            if (ImGui::MenuItem("HiHi")) {}
            if (ImGui::MenuItem("I wont help you")) {}
            if (ImGui::MenuItem("Sorry")) {}
            if (ImGui::BeginMenu("Sorry")) {
                if (ImGui::MenuItem("Not so sorry")) {}
                ImGui::EndMenu();
            }

            ImGui::EndMenu();
        }


        ImGui::EndMenuBar();
    }

    if (m_pActionSave->IsShortcutPressed()) {
            Data::ProjectData::m_ProjectData->Save();
    }
    if(m_pActionRebuild->IsShortcutPressed()) {
        Data::WorldData::m_World->ResetBuild();
    }

    ImGui::End();

    for (auto &window : m_Windows) {
        window->Draw();
    }

    ImGui::ShowMetricsWindow();

    ImGui::SetNextWindowPos(ImVec2(1280.0f - 350.0f, 25.0f), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(350.0f, 545.0f), ImGuiCond_FirstUseEver);

    ImGui::Begin("Scene view");

    ImVec2 size = ImGui::GetWindowSize();

    WorldGenerator::Data::RenderData::m_RenderData->m_ViewportCamera->SetScreenRatio(size.x / size.y);

    size.y = size.y < 35.0f ? 0.0f : size.y - 35.0f;

    auto attachment = Data::RenderData::m_RenderData->m_FBViewport->GetMainColorAttachment();
    ImGui::Image((void *) (uint32_t) *attachment->GetTexture()->GetID().get(), size,{1,1},{0,0});
    m_pWorldViewVisible = ImGui::IsItemVisible();
    if (ImGui::IsItemHovered()) {
        Data::RenderData::m_RenderData->m_ViewportHovered = true;
    } else {
        Data::RenderData::m_RenderData->m_ViewportHovered = false;
    }

    ImGui::End();

    UILog::m_Log->Draw("Logger");

    PostRender(data);
}

void WorldGenerator::UI::UI_Main::Setup() {
    UI::Setup();
}

bool WorldGenerator::UI::UI_Main::IsWorldViewVisible() const {
    return m_pWorldViewVisible;
}

#pragma GCC diagnostic pop