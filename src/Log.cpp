
#include "../world-gen/Log.h"
#include "../world-gen/ui/UILog.h"

void WorldGenerator::GlobalLogWrapper::Log(const char* header, const char* text)
{
    WorldGenerator::UILog::m_Log->AddLog("%s : %s \n", header, text);
}

