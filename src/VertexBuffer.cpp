//
// Created by Matty on 7.5.2020.
//

#include "../world-gen/render/VertexBuffer.h"
#include "../world-gen/glad.h"
#include "../world-gen/Log.h"

bool WorldGenerator::Render::VertexBuffer::bind() {
    if (!m_pDeleted) {
        glBindVertexArray(m_pGlVao);
        glBindBuffer(GL_ARRAY_BUFFER, m_pGlVbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pGlEbo);
#ifdef GPU_DEBUG
        GLenum errorCode;
        while ((errorCode = glGetError()) != GL_NO_ERROR) {
            std::string error;
            switch (errorCode) {
                case GL_INVALID_ENUM:
                    error = "INVALID_ENUM";
                    break;
                case GL_INVALID_VALUE:
                    error = "INVALID_VALUE";
                    break;
                case GL_INVALID_OPERATION:
                    error = "INVALID_OPERATION";
                    break;
                case GL_STACK_OVERFLOW:
                    error = "STACK_OVERFLOW";
                    break;
                case GL_STACK_UNDERFLOW:
                    error = "STACK_UNDERFLOW";
                    break;
                case GL_OUT_OF_MEMORY:
                    error = "OUT_OF_MEMORY";
                    break;
                case GL_INVALID_FRAMEBUFFER_OPERATION:
                    error = "INVALID_FRAMEBUFFER_OPERATION";
                    break;
            }
            std::cout << error << " | " << __FILE__ << " (" << __LINE__ << ")" << std::endl;
        }
#endif
        return true;
    }
    return false;
}

WorldGenerator::Render::VertexBuffer::VertexBuffer(uint32_t VAO, uint32_t VBO, uint32_t EBO, bool instVBO) {
    if (VAO == -1) {
        glGenVertexArrays(1, &m_pGlVao);
        s_pUsedIds.emplace(m_pGlVao);
    } else { m_pGlVao = VAO; }
    if (VBO == -1) {
        glGenBuffers(1, &m_pGlVbo);
    } else { m_pGlVbo = VBO; }
    if (EBO == -1) {
        glGenBuffers(1, &m_pGlEbo);
    } else { m_pGlEbo = EBO; }
    if (instVBO) {
        glGenBuffers(1, &m_pGlInstanceVbo);
    }
}

WorldGenerator::Render::VertexBuffer::~VertexBuffer() {
    m_pDeleted = true;
    s_pUsedIds.erase(m_pGlVao);
    s_pFilledIds.erase(m_pGlVao);
    glDeleteVertexArrays(1, &m_pGlVao);
    glDeleteBuffers(1, &m_pGlVbo);
    glDeleteBuffers(1, &m_pGlEbo);
    glDeleteBuffers(1, &m_pGlInstanceVbo);
}

void WorldGenerator::Render::VertexBuffer::setVertexData(uint32_t size, float *data, int32_t drawType) {
    if (s_pFilledIds.find(m_pGlVao) != s_pFilledIds.end()) {
        WG_LOG_WARN("Overriding vao data");
    }
    glBindVertexArray(m_pGlVao);
    s_pFilledIds.emplace(m_pGlVao);
    glBindBuffer(GL_ARRAY_BUFFER, m_pGlVbo);
    glBufferData(GL_ARRAY_BUFFER, size * sizeof(float), data, drawType);
    glBindVertexArray(0);
}

void WorldGenerator::Render::VertexBuffer::setIndexData(uint32_t size, uint32_t *data, int32_t drawType) {
    glBindVertexArray(m_pGlVao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pGlEbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(uint32_t), data, drawType);
    glBindVertexArray(0);
}

void WorldGenerator::Render::VertexBuffer::setInstancedData(uint32_t size, float *data) {
    glBindVertexArray(m_pGlVao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pGlEbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(uint32_t), data, GL_STREAM_DRAW);
    glBindVertexArray(0);
}

std::set<uint32_t> WorldGenerator::Render::VertexBuffer::s_pUsedIds;
std::set<uint32_t> WorldGenerator::Render::VertexBuffer::s_pFilledIds;
