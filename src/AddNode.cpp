//
// Created by mattyvrba on 9/9/20.
//

#include "../world-gen/node_editor/nodes/AddNode.h"
#include "../world-gen/data/EditorIds.h"


WorldGenerator::NodeEditor::Nodes::AddNodeFF::AddNodeFF(uint32_t id,
                                                        std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage)
                                                        : Node(id,pinStorage) {
    m_pName = ("Add");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<FloatPin>(pinID,"A",false,m_pValueIN_A);
    m_pInputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValX = std::make_shared<FloatPin>(pinID,"B",false,m_pValueIN_B);
    m_pInputPins.emplace_back(pinValX);
    pinStorage[pinID] = pinValX;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValY = std::make_shared<FloatPin>(pinID,"Output",true,m_pValueOut);
    m_pOutputPins.emplace_back(pinValY);
    pinStorage[pinID] = pinValY;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFF::GetOutputCount() const
{
    return 1;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFF::GetInputCount() const
{
    return 2;
}
void WorldGenerator::NodeEditor::Nodes::AddNodeFF::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}

WorldGenerator::NodeEditor::Nodes::AddNodeFV2::AddNodeFV2(uint32_t id,
                                                          std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage) : Node(
        id, pinStorage) {
    m_pName = ("Add");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<FloatPin>(pinID,"A",false,m_pValueIN_A);
    m_pInputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValX = std::make_shared<Vec2Pin>(pinID,"B",false,m_pValueIN_B);
    m_pInputPins.emplace_back(pinValX);
    pinStorage[pinID] = pinValX;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValY = std::make_shared<Vec2Pin>(pinID,"Output",true,m_pValueOut);
    m_pOutputPins.emplace_back(pinValY);
    pinStorage[pinID] = pinValY;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFV2::GetOutputCount() const
{
    return 1;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFV2::GetInputCount() const
{
    return 2;
}
void WorldGenerator::NodeEditor::Nodes::AddNodeFV2::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}

WorldGenerator::NodeEditor::Nodes::AddNodeFV4::AddNodeFV4(uint32_t id,
                                                          std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage) : Node(
        id, pinStorage) {

    m_pName = ("Add");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<FloatPin>(pinID,"A",false,m_pValueIN_A);
    m_pInputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValX = std::make_shared<Vec4Pin>(pinID,"B",false,m_pValueIN_B);
    m_pInputPins.emplace_back(pinValX);
    pinStorage[pinID] = pinValX;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValY = std::make_shared<Vec4Pin>(pinID,"Output",true,m_pValueOut);
    m_pOutputPins.emplace_back(pinValY);
    pinStorage[pinID] = pinValY;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFV4::GetOutputCount() const
{
    return 1;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFV4::GetInputCount() const
{
    return 2;
}
void WorldGenerator::NodeEditor::Nodes::AddNodeFV4::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}

WorldGenerator::NodeEditor::Nodes::AddNodeFV3::AddNodeFV3(uint32_t id,
                                                          std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage) : Node(
        id, pinStorage) {

    m_pName = ("Add");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<FloatPin>(pinID,"A",false,m_pValueIN_A);
    m_pInputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValX = std::make_shared<Vec3Pin>(pinID,"B",false,m_pValueIN_B);
    m_pInputPins.emplace_back(pinValX);
    pinStorage[pinID] = pinValX;

    pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinValY = std::make_shared<Vec3Pin>(pinID,"Output",true,m_pValueOut);
    m_pOutputPins.emplace_back(pinValY);
    pinStorage[pinID] = pinValY;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFV3::GetOutputCount() const
{
    return 1;
}
uint32_t WorldGenerator::NodeEditor::Nodes::AddNodeFV3::GetInputCount() const
{
    return 2;
}
void WorldGenerator::NodeEditor::Nodes::AddNodeFV3::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}
