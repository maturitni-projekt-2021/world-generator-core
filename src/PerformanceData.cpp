//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/data/PerformanceData.h"

std::unique_ptr<WorldGenerator::Data::PerformanceData> WorldGenerator::Data::PerformanceData::m_PerformanceData;

void WorldGenerator::Data::PerformanceData::AddFrameTime(float newFrameTime) {
    if (m_pEveryOther) {
        uint32_t slot = m_pOffset++;
        bool firstDataFill = false;
        if (m_pOffset >= m_frameTimeCount) {
            if (!m_pFrameDataFilled) {
                firstDataFill = true;
                m_pFrameDataFilled = true;
            }
            m_pOffset = 0;
        }

        if (m_pFrameDataFilled && !firstDataFill) {
            m_pFrameTimeStorage.at(slot) = newFrameTime;
        } else {
            m_pFrameTimeStorage.emplace_back(newFrameTime);
        }
    }
    m_pEveryOther = !m_pEveryOther;

}

const float *WorldGenerator::Data::PerformanceData::GetFrameTimeStorage() const {
    return m_pFrameTimeStorage.data();
}

uint32_t WorldGenerator::Data::PerformanceData::GetFrameTimeStorageValueCount() const {
    return m_pFrameTimeStorage.size();
}

WorldGenerator::Data::PerformanceData::PerformanceData() {
    m_pFrameTimeStorage.reserve(m_frameTimeCount);
}

uint32_t WorldGenerator::Data::PerformanceData::GetFrameTimeStorageOffset() const {
    return m_pOffset;
}
