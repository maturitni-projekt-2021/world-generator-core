//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/custom/ModelVertex.h"
uint32_t WorldGenerator::Render::Custom::ModelVertex::getFloatCount()
{
    return 3+3+4+2;
}
WorldGenerator::Render::VertexLayout WorldGenerator::Render::Custom::ModelVertex::getVertexLayout()
{
    auto layout = VertexLayout(4);

    auto posComponent = VertexLayoutComponent(3);
    auto normalComponent = VertexLayoutComponent(3);
    auto colorComponent = VertexLayoutComponent(4);
    auto uvComponent = VertexLayoutComponent(2);

    layout.addAttributeComponent(posComponent);
    layout.addAttributeComponent(normalComponent);
    layout.addAttributeComponent(colorComponent);
    layout.addAttributeComponent(uvComponent);
    return layout;
}
WorldGenerator::Render::Custom::ModelVertex::ModelVertex(const glm::vec3& pos, const glm::vec4& color, const glm::vec2& uv, const glm::vec3 & normal)
{
    m_pData.reserve(getFloatCount());
    m_pData.emplace_back(pos.x);
    m_pData.emplace_back(pos.y);
    m_pData.emplace_back(pos.z);
    m_pData.emplace_back(normal.x);
    m_pData.emplace_back(normal.y);
    m_pData.emplace_back(normal.z);
    m_pData.emplace_back(color.x);
    m_pData.emplace_back(color.y);
    m_pData.emplace_back(color.z);
    m_pData.emplace_back(color.w);
    m_pData.emplace_back(uv.x);
    m_pData.emplace_back(uv.y);
}
WorldGenerator::Render::VertexLayout WorldGenerator::Render::Custom::ModelVertex::GetModelVertexLayout()
{
    auto layout = VertexLayout(4);

    auto posComponent = VertexLayoutComponent(3);
    auto normalComponent = VertexLayoutComponent(3);
    auto colorComponent = VertexLayoutComponent(4);
    auto uvComponent = VertexLayoutComponent(2);

    layout.addAttributeComponent(posComponent);
    layout.addAttributeComponent(normalComponent);
    layout.addAttributeComponent(colorComponent);
    layout.addAttributeComponent(uvComponent);
    return layout;
}
