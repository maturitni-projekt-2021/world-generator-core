//
// Created by Matty on 2021-02-17.
//

#include "../world-gen/ui/custom/UI_BiomeEditor.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/data/ProjectData.h"
#include <glm/gtc/type_ptr.hpp>

WorldGenerator::UI::Custom::UI_BiomeEditor::UI_BiomeEditor() : UIWindow("Biomes") {

}

void WorldGenerator::UI::Custom::UI_BiomeEditor::CustomDraw() {
    static int selectedID = 0;

    static bool update = true;

    auto & biomes = WorldGenerator::Data::ProjectData::m_ProjectData->m_WorldGenDataEditable->m_Biomes;
    ImGui::BeginChild("##select",{80.0f, 24.0f});
    if (ImGui::BeginCombo("Selected biome", biomes[selectedID]->GetName().c_str())) {
        for (int32_t i = 0; i <biomes.size(); i++) {
                if (ImGui::Selectable( biomes[selectedID]->GetName().c_str(), selectedID == i,
                                      ImGuiSelectableFlags_AllowDoubleClick)) {
                    selectedID = i;
                }
        }
        ImGui::EndCombo();
    }
    ImGui::EndChild();


    static float baseColor[3];

    if(update) {
        baseColor[0] =  biomes[selectedID]->GetBaseColor().x;
        baseColor[1] =  biomes[selectedID]->GetBaseColor().y;
        baseColor[2] =  biomes[selectedID]->GetBaseColor().z;

        update = false;
    }
    ImGui::ColorEdit3("Base color",baseColor);

    ImGui::BeginChild("offsetLa", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing() - 15.0f));
    ImGui::EndChild();
    ImGui::Dummy({0, 0});
    ImGui::SameLine(ImGui::GetWindowContentRegionMax().x - 56.0f);
    if (ImGui::Button("Apply", {56.0f, 20.0f})) {
        biomes[selectedID]->setMPBaseColor({ baseColor[0], baseColor[1], baseColor[2]});
        update = true;
        Data::WorldData::m_World->ResetBuild();
    }

/*
    if (ImGui::BeginChild("##leftSideNoiseL", {146.0f, ImGui::GetWindowSize().y - 24.0f})) {
        ImGui::TextUnformatted("Layers");
        ImGui::SameLine(146.0f - 40.0f);
        if (ImGui::Button("Add", {40.0f, 20.0f})) {
            ImGui::OpenPopup("Select noise");
        }
        if (ImGui::ListBoxHeader("##Layers", {146.0f, ImGui::GetWindowSize().y - 24.0f})) {
            int32_t i = 0;
            for (auto &l : projectData->m_WorldGenDataEditable->m_NoiseLayers) {
                if (ImGui::Selectable(l->GetName().c_str(), i == m_pSelectedIndex,
                                      ImGuiSelectableFlags_AllowDoubleClick)) {
                    if (i != m_pSelectedIndex) {
                        m_pSelectedIndex = i;
                        update = true;
                    }
                }
                if (ImGui::IsMouseClicked(ImGuiMouseButton_Right) && ImGui::IsItemHovered()) {
                    requestOpenPopup = true;
                    popupIndex = i;
                }

                ImGuiDragDropFlags src_flags = 0;
                src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;     // Keep the source displayed as hovered
                src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers; // Because our dragging is local, we disable the feature of opening foreign treenodes/tabs while dragging
                //src_flags |= ImGuiDragDropFlags_SourceNoPreviewTooltip; // Hide the tooltip

                if (ImGui::BeginDragDropSource(src_flags)) {
                    if (!(src_flags & ImGuiDragDropFlags_SourceNoPreviewTooltip))
                        ImGui::Text("%s", l->GetName().c_str());
                    ImGui::SetDragDropPayload("dragLayer", &i, sizeof(int32_t));
                    ImGui::EndDragDropSource();
                }

                if (ImGui::BeginDragDropTarget()) {
                    ImGuiDragDropFlags target_flags = 0;
                    target_flags |= ImGuiDragDropFlags_AcceptBeforeDelivery;    // Don't wait until the delivery (release mouse button on a target) to do something
                    target_flags |= ImGuiDragDropFlags_AcceptNoDrawDefaultRect; // Don't display the yellow rectangle
                    if (const ImGuiPayload *payload = ImGui::AcceptDragDropPayload("dragLayer", target_flags)) {
                        switchFrom = *(const int32_t *) payload->Data;
                        switchTo = i;
                    }
                    ImGui::EndDragDropTarget();
                }
                i++;
            }
            ImGui::ListBoxFooter();
        } //176.0f

        if (requestOpenPopup) {
            ImGui::OpenPopup("noiseLayerMenu");
            requestOpenPopup = false;
        }

        if (ImGui::BeginPopup("noiseLayerMenu")) {
            if (ImGui::MenuItem("Remove")) {
                projectData->m_WorldGenDataEditable->m_NoiseLayers.erase(
                        projectData->m_WorldGenDataEditable->m_NoiseLayers.cbegin() + popupIndex);
                if (popupIndex == m_pSelectedIndex) {
                    if (m_pSelectedIndex == projectData->m_WorldGenDataEditable->m_NoiseLayers.size() &&
                        m_pSelectedIndex != 0) {
                        m_pSelectedIndex--;
                    }
                }
                WorldData::m_World->ResetBuild();
                update = true;
            }
            if (ImGui::MenuItem("Duplicate")) {
                projectData->AddNoiseLayer(projectData->m_WorldGenDataEditable->m_NoiseLayers.at(popupIndex),
                                           popupIndex);
                update = true;
            }
            if (ImGui::BeginMenu("Add to group:")) {
                if (ImGui::MenuItem("Root")) {
                }
                if (ImGui::MenuItem("NoRoot")) {
                }
                if (ImGui::MenuItem("MoRoot")) {
                }
                ImGui::EndMenu();
            }
            ImGui::EndPopup();
        }

        if (switchFrom != -1 && switchTo != -1) {
            projectData->m_WorldGenDataEditable->SwapLayers(switchFrom, switchTo);
            m_pSelectedIndex = switchTo;
            update = true;
            rebuildAfterDrag = true;
            switchFrom = -1;
            switchTo = -1;
        }

        if (rebuildAfterDrag) {
            if (!ImGui::IsMouseDown(ImGuiMouseButton_Left)) {
                WorldData::m_World->ResetBuild();
                rebuildAfterDrag = false;
            }
        }

        if (ImGui::BeginPopup("Select noise")) {
            if (ImGui::MenuItem("Perlin 2D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_PERLIN_2D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Perlin 3D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_PERLIN_3D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Simplex 2D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_SIMPLEX_2D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Simplex 3D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_SIMPLEX_3D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("White 2D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_WHITE_2D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("White 3D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_WHITE_3D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Fractal 2D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_FRACTAL_2D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Fractal 3D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_FRACTAL_3D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Celluar 2D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CELLUAR_2D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Celluar 3D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CELLUAR_3D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Cubic 2D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CUBIC_2D, m_pSelectedIndex);
                update = true;
            }
            if (ImGui::MenuItem("Cubic 3D")) {
                projectData->AddNoiseLayer(TerrainGeneration::Data::NoiseType::WG_NOISE_CUBIC_3D, m_pSelectedIndex);
                update = true;
            }
            ImGui::EndPopup();
        }
        ImGui::EndChild();
    }
*/
}

void WorldGenerator::UI::Custom::UI_BiomeEditor::CustomWindowSetup() {
    UIWindow::CustomWindowSetup();
}
