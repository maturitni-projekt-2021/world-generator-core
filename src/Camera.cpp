//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/Camera.h"

#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <utility>

void WorldGenerator::Render::Camera::SetPosition(const glm::vec3 &position) {
    if (m_pSmoothCam) {
        m_pPositionDestination = position;
    } else {
        auto old = m_pPosition;
        auto delta = position - old;
        m_pPosition = position;
        m_pTargetPos += delta;
    }
}

void WorldGenerator::Render::Camera::SetRotation(const glm::vec3 &rotation) {
    m_pRotation = rotation;
}

void WorldGenerator::Render::Camera::SetFOV(float newFov) {
    m_pFOV = newFov;
}

glm::mat4 WorldGenerator::Render::Camera::GetViewMatrix() const {
    auto result = glm::lookAt(m_pPosition,
                              m_pTargetPos,
                              glm::vec3(0.0f, 1.0f, 0.0f));
    return result;
}

glm::mat4 WorldGenerator::Render::Camera::GetProjectionMatrix() const {
    return glm::perspective(glm::radians(m_pFOV), m_pScreenRatio, m_pNearPlane, m_pFarPlane);
}

void WorldGenerator::Render::Camera::SetTarget(const glm::vec3 &position) {
    m_pTargetPos = position;
}

void WorldGenerator::Render::Camera::SetTargetFromRotation() {
    m_pTargetPos.x = cos(glm::radians(m_pRotation.x)) * cos(glm::radians(m_pRotation.y));
    m_pTargetPos.y = sin(glm::radians(m_pRotation.y));
    m_pTargetPos.z = sin(glm::radians(m_pRotation.x)) * cos(glm::radians(m_pRotation.y));
    m_pTargetPos = glm::normalize(m_pTargetPos);
    m_pTargetPos += m_pPosition;
}

WorldGenerator::Render::Camera::Camera(float fov, float nearPlane, float farPlane, float screenRatio,
                                       std::shared_ptr<FrameBuffer> target, bool smoothCam)
        : m_pFOV(fov), m_pNearPlane(nearPlane), m_pFarPlane(farPlane), m_pScreenRatio(screenRatio),
          m_pTargetBuffer(std::move(target)), m_pSmoothCam(smoothCam),
          m_pFrustum(fov, screenRatio, nearPlane, farPlane) {
#ifndef NDEBUG
    GenerateDebugFrustumLines();
#endif
}

void WorldGenerator::Render::Camera::BindTargetBuffer() {
    m_pTargetBuffer->FullBind();
}

void WorldGenerator::Render::Camera::SetScreenRatio(float newScreenRatio) {
    m_pScreenRatio = newScreenRatio;
    m_pFrustum.SetInternals(m_pFOV,m_pScreenRatio,m_pNearPlane,m_pFarPlane);
}

glm::vec3 WorldGenerator::Render::Camera::GetForward() const {
    return glm::normalize(m_pPosition - m_pTargetPos);
}

glm::vec3 WorldGenerator::Render::Camera::GetRight() const {
    const auto up = glm::vec3(0, 1, 0);
    return glm::normalize(glm::cross(up, glm::normalize(m_pTargetPos - m_pPosition)));
}

void WorldGenerator::Render::Camera::PostProcess() {
    m_pTargetBuffer->PostProcess();
}

void WorldGenerator::Render::Camera::Update() {
    if (m_pSmoothCam) m_pPosition = glm::mix(m_pPosition, m_pPositionDestination, 0.5f);
}

bool WorldGenerator::Render::Camera::InFrustum(const std::shared_ptr<Culling::CullCollider> &collider) {
    return m_pFrustum.ContainsCollider(collider);
}

bool WorldGenerator::Render::Camera::InFrustum(const WorldGenerator::Render::Culling::CullCollider &collider) {
    return m_pFrustum.ContainsCollider(collider);
}

void WorldGenerator::Render::Camera::ApplyFrustum() {
    m_pFrustum.SetTransform(m_pPosition,
                            m_pTargetPos,
                            glm::vec3(0.0f, 1.0f, 0.0f));
}

#ifndef NDEBUG
void WorldGenerator::Render::Camera::DrawDebugFrustum() {
    m_pFrustum.RenderFrustumDebug(GetViewMatrix(), GetProjectionMatrix());
}

void WorldGenerator::Render::Camera::GenerateDebugFrustumLines() {
    m_pFrustum.GenerateDebugFrustrumLines(m_pTargetPos,m_pPosition,glm::vec3(0.0f, 1.0f, 0.0f));
}

#endif
