//
// Created by Matty on 10/25/2020.
//

#include "../world-gen/util/UtilThread.h"
#ifdef LINUX_BUILD
#else
#include <processthreadsapi.h>
#endif


bool WorldGenerator::Util::UtilThread::SetThreadPrio(std::thread &thread, ThreadPriority prio) {

#ifdef LINUX_BUILD
    return true;
#else
    return SetThreadPriority((HANDLE)thread.native_handle(),prio) == 0;
#endif
}
