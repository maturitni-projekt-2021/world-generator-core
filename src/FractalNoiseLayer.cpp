//
// Created by Matty on 11/9/2020.
//

#include "../world-gen/world_generator/data/FractalNoiseLayer.h"

float WorldGenerator::TerrainGeneration::Data::FractalNoiseLayer2D::GetValue(const glm::vec3 &pos, bool raw2Ddata, float nextVal, Mask2DOperation operation) {
    auto noiseVal = WorldGenerator::Data::WorldData::m_Noiser.GetValueFractal((pos.x * m_pScale) + m_pOffset.x,
                                                                          (pos.z * m_pScale) +
                                                                          m_pOffset.y);

    float heightSubVal = (raw2Ddata || GetAddition() == AdditionType::WG_NOISE_MULTIPLY) ? 0.0f : pos.y;

    switch (operation) {
        case WG_NOISE_MASK_2D_ADDITIVE:
            return (m_pRangeOffset + noiseVal * m_pIntensity - heightSubVal) + nextVal;
        case WG_NOISE_MASK_2D_SUBTRACT:
            return (m_pRangeOffset + noiseVal * m_pIntensity - heightSubVal) - nextVal;
        case WG_NOISE_MASK_2D_MULTIPLY:
            return (m_pRangeOffset + noiseVal * m_pIntensity - heightSubVal) * nextVal;
    }
    return (m_pRangeOffset + noiseVal * m_pIntensity - heightSubVal) * nextVal;
}

WorldGenerator::TerrainGeneration::Data::FractalNoiseLayer2D::FractalNoiseLayer2D() : NoiseLayer(
        WorldGenerator::TerrainGeneration::Data::NoiseType::WG_NOISE_FRACTAL_2D) {
}

WorldGenerator::TerrainGeneration::Data::NoiseLayer *
WorldGenerator::TerrainGeneration::Data::FractalNoiseLayer2D::Copy() {
    auto res = new FractalNoiseLayer2D();
    _CopyCore(res);
    return res;
}

float WorldGenerator::TerrainGeneration::Data::FractalNoiseLayer3D::GetValue(const glm::vec3 &pos, bool raw2Ddata, float nextVal, Mask2DOperation operation) {
    return m_pRangeOffset + WorldGenerator::Data::WorldData::m_Noiser.GetValueFractal((pos.x * m_pScale) + m_pOffset.x,
                                                                                      (pos.y * m_pScale) + m_pOffset.y,
                                                                                      (pos.z * m_pScale) +
                                                                                      m_pOffset.z) * m_pIntensity;
}

WorldGenerator::TerrainGeneration::Data::FractalNoiseLayer3D::FractalNoiseLayer3D() : NoiseLayer(
        WorldGenerator::TerrainGeneration::Data::NoiseType::WG_NOISE_FRACTAL_3D) {
}

WorldGenerator::TerrainGeneration::Data::NoiseLayer *
WorldGenerator::TerrainGeneration::Data::FractalNoiseLayer3D::Copy() {
    auto res = new FractalNoiseLayer3D();
    _CopyCore(res);
    return res;
}
