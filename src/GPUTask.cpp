//
// Created by Matty on 11/23/2020.
//

#include "../world-gen/gpugen/GPUTask.h"

void WorldGenerator::GPU::ChunkGenTask::Process() {

}

std::vector<float> &WorldGenerator::GPU::ChunkGenTask::GetResult() {
    return m_pResult;
}

WorldGenerator::GPU::ChunkGenTask::ChunkGenTask(const WorldGenerator::World::WorldChunkPos &pos, uint32_t resolution) : m_pResolution(resolution), m_pPosition(pos){
}

uint32_t WorldGenerator::GPU::ChunkGenTask::GetWorkerID() const {
    return m_pWorkerID;
}
