//
// Created by Matty on 29.8.2020.
//

#include "../world-gen/ui/UI.h"
#include "../world-gen/imgui/imgui_impl_glfw.h"
#include "../world-gen/imgui/imgui_impl_opengl3.h"
#include "../world-gen/glad.h"

void WorldGenerator::UI::UI::Render(const std::shared_ptr<ImGUI_Core>& data)
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}
void WorldGenerator::UI::UI::Setup()
{

}
void WorldGenerator::UI::UI::PostRender(const std::shared_ptr<ImGUI_Core>& data)
{
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(data->GetWindow().get(), &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    const auto bgColor = data->GetClearColor();
    glClearColor(bgColor.x, bgColor.y, bgColor.z, bgColor.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    if (data->GetViewportsEnabled()) {
        GLFWwindow* backup_current_context = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backup_current_context);
    }
}
