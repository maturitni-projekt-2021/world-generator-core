//
// Created by Matty on 10/29/2020.
//

#include "../world-gen/world/WOSpawnRules.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/data/ProjectData.h"

std::shared_ptr<WorldGenerator::World::WorldObject> WorldGenerator::World::WOSpawnRules::GetWorldObject(const glm::vec3 & pos) {
    auto res = std::make_shared<WorldObject>();
    res->SetModels(m_pModel);
    auto & noiser = WorldGenerator::Data::WorldData::m_Noiser;
    //TODO: Random rotation etc
    return res;
}

bool WorldGenerator::World::WOSpawnRules::ShouldSpawn(const glm::vec3 & pos) const {
    if(m_Enabled) {
        auto & noiser = WorldGenerator::Data::WorldData::m_Noiser;
        auto val = noiser.GetPerlinFractal(pos.x,pos.y,pos.z);
        val += 1.0f;
        val /= 2;
        return (val >= 1.0f - m_Density);
    }
    return false;
}

WorldGenerator::World::WOSpawnRules::WOSpawnRules(const std::string & model,float mDensity, const glm::vec3 &mScaleVariety,
                                                  const glm::vec3 &mStartScaleVariety,
                                                  const glm::vec3 &mRotationVariety, const glm::vec3 &mStartRotation,
                                                  const glm::vec3 &mPositionOffsetVariety,
                                                  const glm::vec3 &mStartPositionOffset,
                                                  WorldGenerator::World::WORenderDistancePriority mRenderPriority)
        : m_Density(mDensity), m_ScaleVariety(mScaleVariety), m_StartScaleVariety(mStartScaleVariety),
          m_RotationVariety(mRotationVariety), m_StartRotation(mStartRotation),
          m_PositionOffsetVariety(mPositionOffsetVariety), m_StartPositionOffset(mStartPositionOffset),
          m_RenderPriority(mRenderPriority) {
    assert(Data::ProjectData::m_ProjectData->m_ModelData.count(model) > 0);
    m_pModel = Data::ProjectData::m_ProjectData->m_ModelData.at(model);
}
