//
// Created by Matty on 1.9.2020.
//

#include "../world-gen/data/WorldData.h"

glm::vec3 WorldGenerator::Data::WorldData::m_CenterPosition;
std::unique_ptr<WorldGenerator::World::World> WorldGenerator::Data::WorldData::m_World;
FastNoise WorldGenerator::Data::WorldData::m_Noiser;