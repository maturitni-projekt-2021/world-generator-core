//
// Created by Matty on 5.9.2020.
//

#include "../world-gen/ui/custom/UI_TextureBrowser.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imgui_cpp.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/resources/TextureParser.h"

void WorldGenerator::UI::Custom::UI_TextureBrowser::CustomDraw()
{
    using namespace WorldGenerator::Data;

    auto projectData = ProjectData::m_ProjectData;

    std::vector<std::string> strs;
    strs.reserve(projectData->m_TextureData.size());

    std::pair<std::string, std::shared_ptr<TextureData>> selected;

    uint32_t index = 0;
    for (auto& texture : projectData->m_TextureData) {
        if (index==m_pSelectedItem) {
            selected.first = texture.first;
            selected.second = std::shared_ptr<TextureData>(texture.second);
        }
        index++;
        std::ostringstream stringStream;
        stringStream << "Name: " << texture.first << (texture.second->m_TextureLoaded ? " (Loaded)" : " (Unloaded)");
        strs.emplace_back(stringStream.str());
    }

    ImGui::ListBox("Loaded textures", &m_pSelectedItem, strs);

    ImGui::BeginChild("item view", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing()));
    ImGui::EndChild();

    if (ImGui::Button("Load texture", {80.0f, 24.0f})) {
        m_pShowFB = true;
        m_pFileBrowser.Open();
    }

    ImGui::SameLine(200.0f);

    if (m_pFileBrowser.HasSelected() && !m_pFileBrowser.IsOpened()) {
        std::string path = m_pFileBrowser.GetSelected().string();
        m_pPopup->m_Path = std::move(path);
        m_pShowPopup = true;
        ImGui::SetNextWindowFocus();
        m_pFileBrowser.ClearSelected();
    }

    if (m_pPopup->m_Closed) {
        if (m_pPopup->m_DataAdded) {
            Data::ProjectData::m_ProjectData->AddTextureToProject(m_pPopup->m_Name,m_pPopup->m_Path);
        }

        m_pPopup->Flush();
        m_pShowPopup = false;
    }
}
void WorldGenerator::UI::Custom::UI_TextureBrowser::CustomWindowSetup()
{

}
WorldGenerator::UI::Custom::UI_TextureBrowser::UI_TextureBrowser() : UIWindow("Texture browser")
{
    m_pPopup = std::make_shared<UI_TextureAddPopup>();
    m_pFileBrowser.SetTypeFilters({".png", ".jpeg", ".tga"});
}
void WorldGenerator::UI::Custom::UI_TextureBrowser::CustomPostDraw()
{
    UIWindow::CustomPostDraw();

    if (m_pShowFB) {
        m_pFileBrowser.Display();
    }

    if (m_pShowPopup) {
        m_pPopup->Draw();
    }
}
WorldGenerator::UI::Custom::UI_TextureAddPopup::UI_TextureAddPopup() : UIWindow("Add texture")
{

}
void WorldGenerator::UI::Custom::UI_TextureAddPopup::CustomDraw()
{
    static char inputText[128] = "";

    if (m_ShowBadInput) {
        ImGui::TextColored({0.8, 0, 0, 1.0}, "Invalid input, enter 3 or more characters for name!");
    }

    if (m_ShowAlreadyExists) {
        ImGui::TextColored({0.8, 0, 0, 1.0}, "Texture with specified name already exists!");
    }

    ImGui::Text("Name:");
    ImGui::SameLine(52.0f);
    if (m_Init) {
        ImGui::SetKeyboardFocusHere(0);
        m_Init = false;
    }
    bool enterConfirm = ImGui::InputText("##Texture.Name", inputText, IM_ARRAYSIZE(inputText),
            ImGuiInputTextFlags_EnterReturnsTrue);

    ImGui::TextWrapped("Loading texture: %s", m_Path.c_str());

    ImGui::BeginChild("offsetST", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing()-15.0f));
    ImGui::EndChild();

    bool saveBtn = ImGui::Button("Save", {80.0f, 24.0f});

    ImGui::SameLine(ImGui::GetWindowSize().x-100.0f);

    if (ImGui::Button("Cancel", {80.0f, 24.0f})) {
        m_Closed = true;
    }

    if (enterConfirm || saveBtn) {
        if (strlen(inputText)>2) {
            if (WorldGenerator::Data::ProjectData::m_ProjectData->m_TextureData.count(std::string(inputText))==0) {
                m_Name = std::string(inputText);
                m_DataAdded = true;
                m_Closed = true;
                for (char& i : inputText) {
                    i = '\0';
                }
            }
            else {
                m_ShowAlreadyExists = true;
            }
        }
        else {
            m_ShowBadInput = true;
        }
    }
}
void WorldGenerator::UI::Custom::UI_TextureAddPopup::CustomWindowSetup()
{
    UIWindow::CustomWindowSetup();
    ImGuiWindowFlags flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize
            | ImGuiWindowFlags_NoMove;
    m_Flags = flags;

    ImGui::SetNextWindowPos(
            ImVec2(ImGui::GetWindowViewport()->Size.x/2-200.0f, ImGui::GetWindowViewport()->Size.y/2-75.0f));
    ImGui::SetNextWindowSize(ImVec2(400.0f, 150.0f));
}
void WorldGenerator::UI::Custom::UI_TextureAddPopup::Flush()
{
    m_Init = true;
    m_DataAdded = false;
    m_Closed = false;
    m_ShowBadInput = false;
    m_ShowAlreadyExists = false;
}
