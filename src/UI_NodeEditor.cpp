//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/ui/custom/UI_NodeEditor.h"
#include "../world-gen/data/EditorData.h"

void WorldGenerator::UI::Custom::UI_NodeEditor::CustomPostDraw() {
    UIWindow::CustomPostDraw();
}

void WorldGenerator::UI::Custom::UI_NodeEditor::CustomDraw() {
    using namespace WorldGenerator::Data;
    switch (m_pSelected) {
        case VOXEL_EDITOR:
            EditorData::m_EditorData->m_VoxelCanvas->Draw();
            break;
        case POPULATION_EDITOR:
            EditorData::m_EditorData->m_PopulationCanvas->Draw();
            break;
    }

    static char selectedMode[128];

    std::vector<std::string> strs;
    strs.reserve(2);
    strs.emplace_back("Voxel editor");
    strs.emplace_back("Population editor");

    if (m_pChanged) {
        for (char &i : selectedMode) {
            i = '\0';
        }

        if (m_pSelected == VOXEL_EDITOR) {
            auto index = 0;
            for (auto &c : std::string("Voxel editor")) {
                selectedMode[index++] = c;
            }
        } else {
            auto index = 0;
            for (auto &c : std::string("Population editor")) {
                selectedMode[index++] = c;
            }
        }
        m_pChanged = false;
    }
    /*
    ImGui::SameLine();
    ImGui::BeginChild("Editor selector", ImVec2(0, ImGui::GetWindowHeight()-164.0f));
    ImGui::EndChild();
    */
    /*
    ImGui::SetNextItemWidth(140.0f);
    if (ImGui::BeginCombo("##SelectEditorGraph", selectedMode)) {
        for (uint32_t i = 0; i < strs.size(); i++) {
            bool selected = selectedMode == strs.at(i).c_str();
            if (ImGui::Selectable(strs.at(i).c_str(), selected)) {
                m_pChanged = m_pSelected != (SelectedEditor) i;
                m_pSelected = (SelectedEditor) i;
            }
            if (selected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    */
}

void WorldGenerator::UI::Custom::UI_NodeEditor::CustomWindowSetup() {
    UIWindow::CustomWindowSetup();
}

WorldGenerator::UI::Custom::UI_NodeEditor::UI_NodeEditor() : UIWindow("Node editor") {

}
