//
// Created by Matty on 4.9.2020.
//

#include "../world-gen/resources/ResourcesWorkerPool.h"
void WorldGenerator::Resources::ResourcesWorkerPool::SetupWorkers()
{
    if(!m_pInit) {
        const auto cpuCount = std::thread::hardware_concurrency();
        for(uint32_t i = 0; i < cpuCount ; i++) {
            m_pWorkers.emplace_back(std::make_unique<ResourcesWorker>(m_pCV,m_pFinishedWorkers,m_pStop,m_pProcessQueue));
        }
        m_pWorkerCount = cpuCount;
        m_pInit = true;
    }
}
void WorldGenerator::Resources::ResourcesWorkerPool::AddTask(std::function<void()> task)
{
    m_pProcessQueue.Push(std::move(task));
    m_pCV.notify_all();
}
void WorldGenerator::Resources::ResourcesWorkerPool::AddTasks(const std::vector<std::function<void()>> & tasks)
{
    m_pProcessQueue.PushMultiple(tasks);
    m_pCV.notify_all();
}
void WorldGenerator::Resources::ResourcesWorkerPool::Stop()
{
    m_pStop = true;
    m_pCV.notify_all();
    while(m_pWorkerCount != m_pFinishedWorkers) {
        std::this_thread::sleep_for(std::chrono::milliseconds(333));
    }

    for(auto & wrk : m_pWorkers) {
        wrk->Join();
    }

    m_pWorkers.clear();
}

std::unique_ptr<WorldGenerator::Resources::ResourcesWorkerPool> WorldGenerator::Resources::ResourcesWorkerPool::m_ResourcesWorkerPool;