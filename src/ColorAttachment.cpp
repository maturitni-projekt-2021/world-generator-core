//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/ColorAttachment.h"
#include "../world-gen/glad.h"

WorldGenerator::Render::ColorAttachment::ColorAttachment(uint32_t width, uint32_t height,
        WorldGenerator::Render::FrameBufferAttachment attachment, TextureColorType texColorType, uint32_t mipmapLevel,
        bool multisampled)
        :m_pAttachment(attachment), m_pMipmap(mipmapLevel)
{
    TextureType type;

    if(multisampled ) {
        type = TextureType::TEXTURE_2D_MULTISAMPLE;
    } else {
        type = TextureType::TEXTURE_2D;
    }

    m_pTexture = std::make_shared<Texture>(width, height, texColorType,true,type);
}
void WorldGenerator::Render::ColorAttachment::Apply()
{
    m_pTexture->BindTexture();
    glFramebufferTexture2D(GL_FRAMEBUFFER, m_pAttachment, GL_TEXTURE_2D, *m_pTexture->GetID(), m_pMipmap);
}
std::shared_ptr<WorldGenerator::Render::Texture> WorldGenerator::Render::ColorAttachment::GetTexture()
{
    return m_pTexture;
}
