//
// Created by Matty on 5.9.2020.
//
#include "../world-gen/util/UtilFormat.h"

std::string WorldGenerator::Util::UtilFormat::Vec3ToString(const glm::vec3& input) {
    auto ss = std::stringstream();
    ss << "X: " << std::to_string(input.x) << "Y: " << std::to_string(input.y) << "Z: " << std::to_string(input.z);
    return ss.str();
}

nlohmann::json WorldGenerator::Util::UtilFormat::Vec3ToJson(const glm::vec3& input) {
    auto res = nlohmann::json::array();
    res.emplace_back(input.x);
    res.emplace_back(input.y);
    res.emplace_back(input.z);
    return res;
}

glm::vec3 WorldGenerator::Util::UtilFormat::JsonToVec3(const nlohmann::json &json) {
    auto res = glm::vec3(json.at(0).get<float>(),json.at(1).get<float>(),json.at(2).get<float>());
    return res;
}
