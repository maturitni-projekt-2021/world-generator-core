//
// Created by Matty on 29.8.2020.
//

#include "../world-gen/glad.h"
#include "../world-gen/render/Renderer.h"

void WorldGenerator::Render::Renderer::_Init() {
}

void WorldGenerator::Render::Renderer::Start() {
    _Init();
}

void WorldGenerator::Render::Renderer::Update() {
    if (!m_pRenderTasks.Empty()) {
        try {
            std::vector<std::shared_ptr<RenderTask>> toDelete;
            toDelete.reserve(m_pIndependentRenderTasks.size());
            for (auto &tsk : m_pIndependentRenderTasks) {
                tsk->Process();
                if (tsk->GetDone()) {
                    toDelete.emplace_back(tsk);
                }
            }

            for (auto &a : toDelete) {
                m_pIndependentRenderTasks.erase(a);
            }

            auto task = m_pRenderTasks.Pop();
            if (!task->GetDone()) {
                task->Process();
                if (task->GetDependancy() == WG_RENDER_TASK_INDEPENDENT && !task->GetDone()) {
                    m_pIndependentRenderTasks.emplace(task);
                }
            }
        }
        catch (std::exception &e) {
        }
    }
}

WorldGenerator::Render::Renderer::Renderer() {

}

void WorldGenerator::Render::Renderer::PreRender() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(1.0f, 0.2f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void WorldGenerator::Render::Renderer::SetWireframeMode(bool wireframeActive) {
    if (wireframeActive) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
}

void WorldGenerator::Render::Renderer::AddRenderTask(const std::shared_ptr<RenderTask> &task) {
    m_pRenderTasks.Push(task);
}

WorldGenerator::Render::Renderer::~Renderer() {
    while (!m_pRenderTasks.Empty()) {
        auto toDrop = m_pRenderTasks.Pop();
        toDrop->Drop();
    }
}

std::unique_ptr<WorldGenerator::Render::Renderer> WorldGenerator::Render::Renderer::m_Renderer;
