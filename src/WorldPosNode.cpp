//
// Created by mattyvrba on 9/9/20.
//

#include "../world-gen/node_editor/nodes/WorldPosNode.h"
#include "../world-gen/data/EditorIds.h"

WorldGenerator::NodeEditor::Nodes::WorldPosNode::WorldPosNode(uint32_t id,
                                                              std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage)
        : Node(id, pinStorage) {
    m_pName = ("World position");
    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinVal = std::make_shared<Vec3Pin>(pinID,"Value",true,m_pValue);
    m_pOutputPins.emplace_back(pinVal);
    pinStorage[pinID] = pinVal;

}
uint32_t WorldGenerator::NodeEditor::Nodes::WorldPosNode::GetOutputCount() const
{
    return 1;
}
uint32_t WorldGenerator::NodeEditor::Nodes::WorldPosNode::GetInputCount() const
{
    return 0;
}
void WorldGenerator::NodeEditor::Nodes::WorldPosNode::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}
