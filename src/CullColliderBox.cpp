//
// Created by Matty on 11/1/2020.
//

#include "../world-gen/render/culling/CullColliderBox.h"

WorldGenerator::Render::Culling::CullColliderBox::CullColliderBox(const glm::vec3 &mPPosition, const glm::vec3 &mPScale)
        : CullCollider(mPPosition), m_pScale(mPScale) {}

bool
WorldGenerator::Render::Culling::CullColliderBox::IsVisible(const WorldGenerator::Render::Culling::FrustumSide &topSide,
                                                            const WorldGenerator::Render::Culling::FrustumSide &bottomSide,
                                                            const WorldGenerator::Render::Culling::FrustumSide &leftSide,
                                                            const WorldGenerator::Render::Culling::FrustumSide &rightSide,
                                                            const WorldGenerator::Render::Culling::FrustumSide &nearSide,
                                                            const WorldGenerator::Render::Culling::FrustumSide &farSide) const {
    bool result = true;
    int32_t out, in;

    const auto halfscale = m_pScale / 2.0f;

    glm::vec3 corners[8]{
            m_pPosition + glm::vec3{halfscale.x, halfscale.y, halfscale.z},
            m_pPosition + glm::vec3{halfscale.x, -halfscale.y, halfscale.z},
            m_pPosition + glm::vec3{-halfscale.x, -halfscale.y, halfscale.z},
            m_pPosition + glm::vec3{-halfscale.x, halfscale.y, halfscale.z},
            m_pPosition + glm::vec3{halfscale.x, halfscale.y, -halfscale.z},
            m_pPosition + glm::vec3{halfscale.x, -halfscale.y, -halfscale.z},
            m_pPosition + glm::vec3{-halfscale.x, -halfscale.y, -halfscale.z},
            m_pPosition + glm::vec3{-halfscale.x, halfscale.y, -halfscale.z}
    };

    WorldGenerator::Render::Culling::FrustumSide pl[6] = {topSide, bottomSide, leftSide, rightSide, nearSide, farSide};

    // for each plane do ...
    for (auto &i : pl) {

        // reset counters for corners in and out
        out = 0;
        in = 0;
        // for each corner of the box do ...
        // get out of the cycle as soon as a box as corners
        // both inside and out of the frustum
        for (int k = 0; k < 8 && (in == 0 || out == 0); k++) {

            // is the corner outside or inside
            if (i.InFront(corners[k])) {
                in++;
            } else {
                out++;
            }
        }
        //if all corners are out
        if (in > 0)
            return true;
        else
            return false;
    }
    return (result);
}
