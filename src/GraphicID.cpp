//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/GraphicID.h"
#include "../world-gen/glad.h"

WorldGenerator::Render::TextureID::~TextureID()
{
    glDeleteTextures(1, &m_pID);
}
