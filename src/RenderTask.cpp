//
// Created by Matty on 4.9.2020.
//
#include "../world-gen/data/RenderData.h"
#include "../world-gen/render/RenderTask.h"

void WorldGenerator::Render::RenderTaskUploadModelData::Process()
{
    auto newID = WorldGenerator::Data::RenderData::m_RenderData->GetModelID();

    m_pResult = std::make_shared<WorldGenerator::Render::Model>(newID);

    auto buffer = m_pResult->GetVertexBuffer();
    buffer->setIndexData(m_pIndices.size(), m_pIndices.data(), WorldGenerator::Render::DrawType::STATIC_DRAW);
    buffer->setVertexData(m_pVertices.size(), m_pVertices.data(), WorldGenerator::Render::DrawType::STATIC_DRAW);
    buffer->setElementCount(m_pElementCount);
    m_pResult->SetBaseOffset(m_pTransform);
    m_pResult->SetName(m_pName);

    Finish();
}
WorldGenerator::Render::RenderTaskUploadModelData::RenderTaskUploadModelData(std::vector<float> vertices,
        std::vector<uint32_t> indices, uint32_t elementCount, const glm::mat4& tfm,const std::string & name)
        :m_pIndices(std::move(indices)), m_pElementCount(elementCount), m_pVertices(std::move(vertices)),
         m_pTransform(tfm), m_pName(name)
{
}
std::shared_ptr<WorldGenerator::Render::Model> WorldGenerator::Render::RenderTaskUploadModelData::GetResult()
{
    return GetDone() ? m_pResult : nullptr;
}
std::shared_ptr<WorldGenerator::Render::Texture> WorldGenerator::Render::RenderTaskUploadTextureData::GetResult()
{
    return GetDone() ? m_pResult : nullptr;
}
WorldGenerator::Render::RenderTaskUploadTextureData::RenderTaskUploadTextureData(uint8_t* textureData, int32_t width,
        int32_t height, WorldGenerator::Render::TextureColorType colorType)
        :m_pTextureData(textureData), m_pWidth(width), m_pHeight(height), m_pColorType(colorType)
{
}
void WorldGenerator::Render::RenderTaskUploadTextureData::Process()
{
    using namespace WorldGenerator::Render;

    m_pResult = std::make_shared<Texture>(m_pWidth,m_pHeight,m_pColorType,true);
    m_pResult->BindTexture();
    m_pResult->UploadData(m_pTextureData);

    Finish();
}
void WorldGenerator::Render::RenderTaskUploadTerrainData::Process()
{
    auto buffer = std::make_shared<WorldGenerator::Render::VertexBuffer>();
    buffer->setVertexData(m_pVertices.size(), m_pVertices.data(), WorldGenerator::Render::DrawType::STATIC_DRAW);
    buffer->setElementCount(m_pElementCount);
    m_Result = std::make_shared<WorldGenerator::World::Terrain>(buffer,m_pPosition);

    Finish();
}
std::shared_ptr<WorldGenerator::World::Terrain> WorldGenerator::Render::RenderTaskUploadTerrainData::GetResult()
{
    return m_Result;
}
WorldGenerator::Render::RenderTaskUploadTerrainData::RenderTaskUploadTerrainData(
        const WorldGenerator::World::WorldChunkPos& position, std::vector<float>& vertices, uint32_t elementCount)
        : m_pPosition(position), m_pVertices(vertices), m_pElementCount(elementCount) { }

WorldGenerator::Render::RenderTaskDropRenderBuffers::RenderTaskDropRenderBuffers(
        std::shared_ptr<WorldGenerator::Render::VertexBuffer> data) : m_pBuffer(std::move(data)) {
}

void WorldGenerator::Render::RenderTaskDropRenderBuffers::Process() {
    if(m_pBuffer.use_count() == 1) {
        m_pBuffer.reset();
        Finish();
    }
}

WorldGenerator::Render::RenderTaskDependancy
WorldGenerator::Render::RenderTaskDropRenderBuffers::GetDependancy() const {
    return RenderTaskDependancy::WG_RENDER_TASK_INDEPENDENT;
}
