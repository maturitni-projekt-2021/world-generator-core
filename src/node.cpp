//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/node_editor/Node.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imnodes.h"

WorldGenerator::NodeEditor::Node::Node(uint32_t id,std::map<uint32_t,std::shared_ptr<Pin>> & pinStorage) {
    m_pID = id;
}

void WorldGenerator::NodeEditor::Node::Draw(const PinHolders & pinData) {

    imnodes::BeginNode(m_pID);
    imnodes::BeginNodeTitleBar();
    ImGui::TextUnformatted(m_pName.c_str());
    imnodes::EndNodeTitleBar();

    for(auto & pin : m_pInputPins) {
        pin->Draw(pinData.m_InputPins.count(pin->GetID()) > 0);
    }

    for(auto & pin : m_pOutputPins) {
        pin->Draw(pinData.m_OutputPins.count(pin->GetID()) > 0);
    }

    imnodes::EndNode();
}

std::vector<uint32_t> WorldGenerator::NodeEditor::Node::GetAllPins() const {
    auto result = std::vector<uint32_t>();
    result.reserve(m_pInputPins.size() + m_pOutputPins.size());
    for(auto & pin : m_pInputPins) {
        result.emplace_back(pin->GetID());
    }

    for(auto & pin : m_pOutputPins) {
        result.emplace_back(pin->GetID());
    }
    return result;
}
