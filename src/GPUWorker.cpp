//
// Created by Matty on 11/19/2020.
//

#include "../world-gen/gpugen/GPUWorker.h"

void WorldGenerator::GPU::GPUWorker::Init() {
    m_pCLGen = std::make_unique<CL::CLMain>();
    m_pCLGen->Init();
}

bool WorldGenerator::GPU::GPUWorker::IsSupported(WorldGenerator::GPU::GPUWorkerType type) {
    switch (type) {
        case WG_GPU_WORKER_CL:
            return m_pCLGen->IsSupported();
        case WG_GPU_WORKER_CUDA:
            return false;
    }
    return false;
}

void WorldGenerator::GPU::GPUWorker::Update() {
    switch (m_pActiveWorker) {
        case WG_GPU_WORKER_CL:
            m_pCLGen->Update();
            break;
        case WG_GPU_WORKER_CUDA:
            //m_pCUDAGen->Update();
            break;
    }
}

void WorldGenerator::GPU::GPUWorker::Activate(WorldGenerator::GPU::GPUWorkerType type) {
    switch (type) {
        case WG_GPU_WORKER_CL:
            m_pCLGen->Activate();
            break;
        case WG_GPU_WORKER_CUDA:
            break;
    }
}

void WorldGenerator::GPU::GPUWorker::RebuildShaders() {
    switch (m_pActiveWorker) {
        case WG_GPU_WORKER_CL:
            m_pCLGen->GenerateShader();
            break;
        case WG_GPU_WORKER_CUDA:
            //m_pCUDAGen->Update();
            break;
    }
}
