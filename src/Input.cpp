//
// Created by Matty on 29.8.2020.
//

#include "../world-gen/input/Input.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/Log.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/data/SettingsData.h"

void WorldGenerator::Input::InputController::Init(
        std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor> &window) {
    //glfwSetKeyCallback(window.get(), [](GLFWwindow *window, int key, int scancode, int action, int mods) {
    //    _KeyCallback(window, key, scancode, action, mods);
    //});

}

void WorldGenerator::Input::InputController::Update(
        std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor> &window) {
    using namespace WorldGenerator::Data;

    if (m_pMousePosSet) {
        if (glfwGetKey(window.get(), GLFW_KEY_W) == GLFW_PRESS) {
            RenderData::m_RenderData->m_ViewportCamera->SetPosition(
                    RenderData::m_RenderData->m_ViewportCamera->GetPosition() -
                    RenderData::m_RenderData->m_ViewportCamera->GetForward() * 0.5f * Global::s_WorldScale);
            RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();
        }

        if (glfwGetKey(window.get(), GLFW_KEY_S) == GLFW_PRESS) {
            RenderData::m_RenderData->m_ViewportCamera->SetPosition(
                    RenderData::m_RenderData->m_ViewportCamera->GetPosition() +
                    RenderData::m_RenderData->m_ViewportCamera->GetForward() * 0.5f * Global::s_WorldScale);
            RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();
        }
        if (glfwGetKey(window.get(), GLFW_KEY_A) == GLFW_PRESS) {
            RenderData::m_RenderData->m_ViewportCamera->SetPosition(
                    RenderData::m_RenderData->m_ViewportCamera->GetPosition() -
                    RenderData::m_RenderData->m_ViewportCamera->GetRight() * 0.5f * Global::s_WorldScale);
            RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();
        }
        if (glfwGetKey(window.get(), GLFW_KEY_D) == GLFW_PRESS) {
            RenderData::m_RenderData->m_ViewportCamera->SetPosition(
                    RenderData::m_RenderData->m_ViewportCamera->GetPosition() +
                    RenderData::m_RenderData->m_ViewportCamera->GetRight() * 0.5f * Global::s_WorldScale);
            RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();
        }
    }

    if (RenderData::m_RenderData->m_ViewportLocked) {
        double xpos, ypos;
        glfwGetCursorPos(window.get(), &xpos, &ypos);

        if (!m_pMousePosSet) {
            m_pMouseStartX = xpos;
            m_pMouseStartY = ypos;
            m_pMousePosSet = true;
            m_pStartRotation = RenderData::m_RenderData->m_ViewportCamera->GetRotation();
        } else {
            float newYaw = m_pStartRotation.x - (xpos - m_pMouseStartX) / 10;
            float newPitch = m_pStartRotation.y - (ypos - m_pMouseStartY) / 10;

            if (newPitch > 89.0f)
                newPitch = 89.0f;
            if (newPitch < -89.0f)
                newPitch = -89.0f;

            RenderData::m_RenderData->m_ViewportCamera->SetRotation(glm::vec3(newYaw, newPitch, m_pStartRotation.z));
            RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();
        }
    } else {
        m_pMousePosSet = false;
    }

    //Viewport lock
    if (glfwGetMouseButton(window.get(), GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
        if (RenderData::m_RenderData->m_ViewportHovered) {
            RenderData::m_RenderData->m_ViewportLocked = true;
            glfwSetInputMode(window.get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    } else {
        if (RenderData::m_RenderData->m_ViewportLocked) {
            RenderData::m_RenderData->m_ViewportLocked = false;
            glfwSetInputMode(window.get(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
    }
}

void WorldGenerator::Input::InputController::FixedUpdate() {
    if (Data::SettingsData::m_SettingsData->m_MovingCam->m_Value) {
        auto cPos = Data::RenderData::m_RenderData->m_ViewportCamera->GetPosition();
        cPos.x -= 0.015f;
        Data::RenderData::m_RenderData->m_ViewportCamera->SetPosition(cPos);
    }
}
