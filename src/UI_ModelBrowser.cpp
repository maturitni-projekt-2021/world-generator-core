//
// Created by Matty on 2.9.2020.
//

#include "../world-gen/ui/custom/UI_ModelBrowser.h"

#include "../world-gen/data/ProjectData.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/resources/ModelParser.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imgui_cpp.h"
#include "../world-gen/Log.h"

void WorldGenerator::UI::Custom::UI_ModelBrowser::CustomDraw() {
    auto projectData = WorldGenerator::Data::ProjectData::m_ProjectData;

    std::vector<std::string> strs;
    strs.reserve(projectData->m_ModelData.size());

    std::pair<std::string, std::shared_ptr<WorldGenerator::Data::ModelData>> selected;

    uint32_t index = 0;
    for (auto &model : projectData->m_ModelData) {
        if (index == m_pSelectedItem) {
            selected.first = model.first;
            selected.second = std::shared_ptr<WorldGenerator::Data::ModelData>(model.second);
        }
        index++;
        std::ostringstream stringStream;
        stringStream << "Name: " << model.first << (model.second->m_ModelLoaded ? " (Loaded)" : " (Unloaded)");
        strs.emplace_back(stringStream.str());
    }
    WorldGenerator::Data::ProjectData::m_ProjectData->m_SelectedModel = selected;

    ImGui::ListBox("Loaded models", &m_pSelectedItem, strs);

    ImGui::BeginChild("item view", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing()));
    ImGui::EndChild();

    if (ImGui::Button("Load model", {80.0f, 24.0f})) {
        m_pShowFB = true;
        m_pFileBrowser.Open();
    }

    if (m_pFileBrowser.HasSelected() && !m_pFileBrowser.IsOpened()) {
        std::string path = m_pFileBrowser.GetSelected().string();
        m_pPopup->m_Path = std::move(path);
        m_pShowPopup = true;
        ImGui::SetNextWindowFocus();
        m_pFileBrowser.ClearSelected();
    }

    if (m_pPopup->m_Closed) {
        if (m_pPopup->m_DataAdded) {
            WorldGenerator::Data::ProjectData::m_ProjectData->m_SelectedModel
                    = std::pair<std::string, std::shared_ptr<WorldGenerator::Data::ModelData>>(m_pPopup->m_Name,
                                                                                               WorldGenerator::Data::ProjectData::m_ProjectData->AddModelToProject(
                                                                                                       m_pPopup->m_Name,
                                                                                                       m_pPopup->m_Path));
        }
        m_pPopup->Flush();
        m_pShowPopup = false;
    }

}

void WorldGenerator::UI::Custom::UI_ModelBrowser::CustomWindowSetup() {
    UIWindow::CustomWindowSetup();
}

WorldGenerator::UI::Custom::UI_ModelBrowser::UI_ModelBrowser()
        : UIWindow("Model browser") {
    m_pPopup = std::make_shared<UI_ModelAddPopup>();
    m_pFileBrowser.SetTypeFilters({".fbx", ".dae", ".obj"});
}

void WorldGenerator::UI::Custom::UI_ModelBrowser::CustomPostDraw() {
    UIWindow::CustomPostDraw();
    if (m_pShowFB) {
        m_pFileBrowser.Display();
    }

    if (m_pShowPopup) {
        m_pPopup->Draw();
    }
}

void WorldGenerator::UI::Custom::UI_ModelAddPopup::CustomDraw() {
    static char inputText[128] = "";

    if (m_ShowBadInput) {
        ImGui::TextColored({0.8, 0, 0, 1.0}, "Invalid input, enter 3 or more characters for name!");
    }

    if (m_ShowAlreadyExists) {
        ImGui::TextColored({0.8, 0, 0, 1.0}, "Model with specified name already exists!");
    }

    ImGui::Text("Name:");
    ImGui::SameLine(52.0f);
    if (m_Init) {
        ImGui::SetKeyboardFocusHere(0);
        m_Init = false;
    }
    bool enterConfirm = ImGui::InputText("##Model.Name", inputText, IM_ARRAYSIZE(inputText),
                                         ImGuiInputTextFlags_EnterReturnsTrue);

    ImGui::TextWrapped("Loading model: %s", m_Path.c_str());

    ImGui::BeginChild("offsetS", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing() - 15.0f));
    ImGui::EndChild();

    bool saveBtn = ImGui::Button("Save", {80.0f, 24.0f});

    ImGui::SameLine(ImGui::GetWindowSize().x - 100.0f);

    if (ImGui::Button("Cancel", {80.0f, 24.0f})) {
        m_Closed = true;
    }

    if (enterConfirm || saveBtn) {
        if (strlen(inputText) > 2) {
            if (WorldGenerator::Data::ProjectData::m_ProjectData->m_ModelData.count(std::string(inputText)) == 0) {
                m_Name = std::string(inputText);
                m_DataAdded = true;
                m_Closed = true;
                for (char &i : inputText) {
                    i = '\0';
                }
            } else {
                m_ShowAlreadyExists = true;
            }
        } else {
            m_ShowBadInput = true;
        }
    }
}

WorldGenerator::UI::Custom::UI_ModelAddPopup::UI_ModelAddPopup()
        : UIWindow("ID_ADD_MODEL", WindowType::WINDOW) {
}

void WorldGenerator::UI::Custom::UI_ModelAddPopup::CustomWindowSetup() {
    UIWindow::CustomWindowSetup();
    ImGuiWindowFlags flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize
                             | ImGuiWindowFlags_NoMove;
    m_Flags = flags;

    ImGui::SetNextWindowPos(
            ImVec2(ImGui::GetWindowViewport()->Size.x / 2 - 200.0f, ImGui::GetWindowViewport()->Size.y / 2 - 75.0f));
    ImGui::SetNextWindowSize(ImVec2(400.0f, 150.0f));
}

WorldGenerator::UI::Custom::UI_ModelAddPopup::~UI_ModelAddPopup() {
}

void WorldGenerator::UI::Custom::UI_ModelAddPopup::Flush() {
    m_Init = true;
    m_DataAdded = false;
    m_Closed = false;
    m_ShowBadInput = false;
    m_ShowAlreadyExists = false;
}
