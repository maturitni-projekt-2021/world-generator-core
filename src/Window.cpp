//
// Created by Matty on 10.4.2020.
//

#include <iostream>
#include "../world-gen/render/Window.h"
#include "../world-gen/glad.h"
#include "../world-gen/Exceptions.h"
#include "../world-gen/Global.h"

const std::string& WorldGenerator::Render::Window::GetWindowName()
{
    return m_pWindowName;
}
std::unique_ptr<GLFWwindow,
                WorldGenerator::Render::GLFWWindowDestructor>& WorldGenerator::Render::Window::GetGlfwWindow()
{
    return m_pGLFWWindow;
}
void WorldGenerator::Render::Window::PreRender()
{
    Render::Renderer::m_Renderer->PreRender();
}
void WorldGenerator::Render::Window::FinalizeRender()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, m_pWindowWidth, m_pWindowHeight);
    glfwSwapBuffers(m_pGLFWWindow.get());
}

WorldGenerator::Render::WindowBuilder::WindowBuilder(uint32_t width, uint32_t height)
        :m_pWidth(width), m_pHeight(height), m_pRenderer(std::make_unique<WorldGenerator::Render::Renderer>())
{

}
WorldGenerator::Render::Window* WorldGenerator::Render::WindowBuilder::finish()
{

    if (!m_pHasColor) {
        throw WorldGenerator::Exceptions::WGException("Background color is not set!", "Window Creation");
    }

    if (!m_pHasName) {
        throw WorldGenerator::Exceptions::WGException("Window name is not set!", "Window Creation");
    }

    auto finished = new WorldGenerator::Render::Window();
    finished->m_pBackgroundColor = m_pBackgroundColor;
    WorldGenerator::Render::Window::m_pWindowWidth = m_pWidth;
    WorldGenerator::Render::Window::m_pWindowHeight = m_pHeight;
    WorldGenerator::Render::Renderer::m_Renderer = std::move(m_pRenderer);
    finished->m_pWindowName = m_pWindowName;
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if !defined(NDEBUG) && defined(GPU_DEBUG)
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif
    GLFWwindow* window = glfwCreateWindow((int32_t) WorldGenerator::Render::Window::m_pWindowWidth,
            (int32_t) WorldGenerator::Render::Window::m_pWindowHeight, finished->m_pWindowName.c_str(), NULL,
            NULL);
    if (window==nullptr) {
        glfwTerminate();
        throw WorldGenerator::Exceptions::WGException("Failed to initialize GLFW Window", "Window Creation");
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval( 1 );
    glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, int32_t width, int32_t height) {
      auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
      self.FbSizeCallback(window, width, height);
    });

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        throw WorldGenerator::Exceptions::WGException("Failed to initialize GLAD", "App initialization");
    }
    else {
        WorldGenerator::Global::s_pGladInitialized = true;
    }

#if !defined(NDEBUG) && defined(GPU_DEBUG)
    int flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(glDebugOutput, nullptr);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    }
#endif
    finished->m_pGLFWWindow = std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor>(window);

    return finished;
}
std::shared_ptr<WorldGenerator::Render::WindowBuilder> WorldGenerator::Render::WindowBuilder::setBackgroundColor(
        const glm::vec4& bgColor)
{
    this->m_pBackgroundColor = bgColor;
    this->m_pHasColor = true;
    return shared_from_this();
}
std::shared_ptr<WorldGenerator::Render::WindowBuilder> WorldGenerator::Render::WindowBuilder::setWindowName(
        const std::string& name)
{
    this->m_pHasName = true;
    this->m_pWindowName = name;
    return shared_from_this();
}

void WorldGenerator::Render::WindowBuilder::glDebugOutput(GLenum source, GLenum type, unsigned int id, GLenum severity,
                                                          GLsizei length, const char *message, const void *userParam) {
// ignore non-significant error/warning codes
    if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " <<  message << std::endl;

    switch (source)
    {
        case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
        case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
        case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
    } std::cout << std::endl;

    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
        case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
        case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
        case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
        case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
        case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
        case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
    } std::cout << std::endl;

    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
        case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
        case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
    } std::cout << std::endl;
    std::cout << std::endl;
};

void WorldGenerator::Render::Window::FbSizeCallback(GLFWwindow* window, int32_t width, int32_t height)
{
    glViewport(0, 0, width, height);
}

uint32_t WorldGenerator::Render::Window::m_pWindowWidth;
uint32_t WorldGenerator::Render::Window::m_pWindowHeight;