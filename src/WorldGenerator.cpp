//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/world_generator/WorldGenerator.h"
void WorldGenerator::TerrainGeneration::WorldGenerator::SetConfig(
        const std::shared_ptr<NodeEditor::WorldGenConfig>& config)
{
    m_pConfig = config;
    m_pHasConfig = true;
}
void WorldGenerator::TerrainGeneration::WorldGenerator::DropData()
{
    for(auto & a : m_pJobStatuses) {
        a.m_Drop = true;
    }
}

WorldGenerator::TerrainGeneration::WorldGenerator::WorldGenerator(const std::unique_ptr<GPU::GPUWorker> & gpuWorker) : m_pGPUWorker(gpuWorker)
{
    const auto cpuCount = std::thread::hardware_concurrency();
    m_pGenerators.reserve(cpuCount);
    m_pJobStatuses = std::vector<ChunkGenerationJobStatus>(cpuCount);

    for(uint32_t i = 0; i < cpuCount ; i++) {
        m_pGenerators.emplace_back(std::make_unique<ChunkGenerator>(m_pCV,m_pFinishedGeneratorCount,m_pStop,m_pJobStatuses.at(i)));
    }
    m_pGeneratorCount = cpuCount;
}
void WorldGenerator::TerrainGeneration::WorldGenerator::Stop()
{
    m_pStop = true;
    m_pCV.notify_all();

    while(m_pFinishedGeneratorCount != m_pGeneratorCount) {
        m_pStop = true;
        std::this_thread::sleep_for(std::chrono::milliseconds(333));
    }

    for(auto & generator : m_pGenerators) {
        generator->Join();
    }
}

std::vector<uint32_t> WorldGenerator::TerrainGeneration::WorldGenerator::GetFreeWorkers() const {
    auto res = std::vector<uint32_t>();
    res.reserve(m_pGeneratorCount);

    for(uint32_t i = 0; i < m_pGeneratorCount; i++) {
        if(!m_pJobStatuses.at(i).m_Active) {
            res.emplace_back(i);
        }
    }

    return res;
}

void WorldGenerator::TerrainGeneration::WorldGenerator::AddWorkerJob(uint32_t worker,
                                                                     const std::shared_ptr<ChunkGenerationTask> &task) {
    assert(!m_pJobStatuses.at(worker).m_Active);

    m_pJobStatuses.at(worker).m_Size = glm::vec3(1.0f); //TODO: Implement
    m_pJobStatuses.at(worker).m_Slot = task->m_Slot;
    m_pJobStatuses.at(worker).m_Resolution = task->m_Resolution;
    m_pJobStatuses.at(worker).m_Pos = task->m_Pos;
    m_pJobStatuses.at(worker).m_Version = task->m_Version;
    m_pJobStatuses.at(worker).m_Active = true;
    m_pJobStatuses.at(worker).m_Drop = false;
}

void WorldGenerator::TerrainGeneration::WorldGenerator::NotifyWorkers() {
    m_pCV.notify_all();
}

void WorldGenerator::TerrainGeneration::WorldGenerator::DropConfig() {
    m_pConfig.reset();
    m_pHasConfig = false;
}

uint32_t WorldGenerator::TerrainGeneration::WorldGenerator::GetWorkersCount() const {
    return m_pGeneratorCount;
}

void WorldGenerator::TerrainGeneration::WorldGenerator::RebuildGPUData() {
    //TODO: Tell opencl or cude to drop data and rebuild shader
    m_pGPUWorker->RebuildShaders();
}

std::unique_ptr<WorldGenerator::TerrainGeneration::WorldGenerator> WorldGenerator::TerrainGeneration::WorldGenerator::m_WorldGenerator;