//
// Created by Matty on 5.9.2020.
//

#include "../world-gen/resources/TextureParser.h"
#include "../world-gen/util/UtilFile.h"
#include "../world-gen/Exceptions.h"
#include "../world-gen/resources/ResourcesWorkerPool.h"
#include "../world-gen/Log.h"

#define STB_IMAGE_IMPLEMENTATION

#include "../world-gen/resources/lib/stb_image.h"
#include "../world-gen/render/Renderer.h"
#include "../world-gen/data/ProjectData.h"

void WorldGenerator::Resources::TextureParser::LoadTexture(
        const std::shared_ptr<WorldGenerator::Data::TextureData> &data, bool diskPath) {
    WorldGenerator::Resources::ResourcesWorkerPool::m_ResourcesWorkerPool->AddTask(
            [data, diskPath]() { _LoadTextureToGPU(data, diskPath); });
}

void WorldGenerator::Resources::TextureParser::_LoadTextureToGPU(
        const std::shared_ptr<WorldGenerator::Data::TextureData> &data, bool diskPath) {
    int32_t textureWidth, textureHeight;
    stbi_set_flip_vertically_on_load(true);
    int nrChannels = 4;
    uint8_t *loadedData = nullptr;
    if (diskPath) {
        loadedData = stbi_load(data->m_Path.c_str(), &textureWidth, &textureHeight, &nrChannels,
                               STBI_rgb_alpha);
    } else {
        auto dataD = Data::ProjectData::m_ProjectData->GetTextureToMemory(data->m_Path);
        loadedData = stbi_load_from_memory((stbi_uc *) dataD.first,
                                           dataD.second, &textureWidth,
                                           &textureHeight, &nrChannels,
                                           STBI_rgb_alpha);
    }

    if (data) {
        auto task = std::make_shared<WorldGenerator::Render::RenderTaskUploadTextureData>(loadedData, textureWidth,
                                                                                          textureHeight,
                                                                                          WorldGenerator::Render::TextureColorType::RGBA);

        WorldGenerator::Render::Renderer::m_Renderer->AddRenderTask(task);

        while (!task->GetDone()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(333));
        }
        data->m_Texture = task->GetResult();
    } else {
        auto log = std::stringstream();
        log << "Failed to load texture : " << data->m_Path;
        WG_LOG_ERROR(log.str().c_str());
        data->m_ErrorText = log.str();
        data->m_LoadFailed = true;
    }

    stbi_image_free(loadedData);


    data->m_TextureLoaded = true;
}
