//
// Created by Matty on 1.9.2020.
//

#include <json.hpp>
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/resources/ModelParser.h"
#include "../world-gen/Log.h"
#include "../world-gen/util/UtilString.h"
#include "../world-gen/resources/TextureParser.h"
#include "../world-gen/resources/lib/Miniz_zip.hpp"
#include "../world-gen/data/SettingsData.h"

std::shared_ptr<WorldGenerator::Data::ProjectData> WorldGenerator::Data::ProjectData::m_ProjectData;
std::shared_ptr<WorldGenerator::Data::TextureData> WorldGenerator::Data::ProjectData::m_MissingTexture;

bool has_suffix(const std::string &str, const std::string &suffix);

uint32_t WorldGenerator::Data::ProjectData::GetBiomeID() {
    return m_pBiomeID++;
}

void WorldGenerator::Data::ProjectData::LoadProjectData(const std::string &path) {


    m_ProjectData = std::make_unique<ProjectData>();
    m_ProjectData->m_Path = path;

    std::ifstream ifs(m_ProjectData->m_Path);

    m_ProjectData->m_pSaveArchive = std::make_shared<miniz_cpp::zip_file>();

    try {
        m_ProjectData->m_pSaveArchive->load(m_ProjectData->m_Path);
    } catch (std::exception &e) {
        throw WorldGenerator::Exceptions::WGException("Failed to read input file", "Save parser");
    }
    if (m_ProjectData->m_pSaveArchive->has_file("data.json")) {
        auto json = nlohmann::json::parse(m_ProjectData->m_pSaveArchive->read("data.json"));
        m_ProjectData->m_ProjectName = json.at("name").get<std::string>();
        m_ProjectData->m_Seed = json.contains("seed") ? json.at("seed").get<int32_t>()
                                                      : Data::SettingsData::m_SettingsData->m_Seed->m_Value;

        m_ProjectData->LoadInternal(json);

        m_ProjectData->m_pBiomeID = json.at("biomeID_Offset").get<uint32_t>();
    } else {
        throw WorldGenerator::Exceptions::WGException("Failed to read input file", "Save parser");
    }
}

void WorldGenerator::Data::ProjectData::NewProjectData(const std::string &name, const std::string &path) {
    m_ProjectData = std::make_unique<ProjectData>();
    m_ProjectData->m_ProjectName = WorldGenerator::Util::trim_copy(name);

    std::string ending = ".wgenp";
    m_ProjectData->m_Path = path;

    if (!has_suffix(m_ProjectData->m_Path, ending)) {
        m_ProjectData->m_Path += ending;
    }

    m_ProjectData->m_pSaveArchive = std::make_shared<miniz_cpp::zip_file>();


    auto baseBiome = std::make_shared<WorldGenerator::World::Biome>(
            glm::vec3(0.611f, 0.631f, 0.654f));
    baseBiome->SetName("BASE");
    baseBiome->SetHeightBounds({-99999999,99999999});
    baseBiome->SetMoistureBounds({-99999999,99999999});
    baseBiome->SetTemperatureBounds({-99999999,99999999});
    baseBiome->AddLayer({{0.666f, 0.666f, 0.556f},10,100});
    m_ProjectData->m_WorldGenDataEditable = std::make_shared<WorldGenerator::Data::WorldGenerationData>();
    m_ProjectData->m_WorldGenDataEditable->m_Biomes[m_ProjectData->GetBiomeID()] = baseBiome;
    m_ProjectData->m_WorldGenDataActive = std::shared_ptr<WorldGenerator::Data::WorldGenerationData>(
            new WorldGenerator::Data::WorldGenerationData(*m_ProjectData->m_WorldGenDataEditable.get()));
    m_ProjectData->m_Seed = Data::SettingsData::m_SettingsData->m_Seed->m_Value;
    m_ProjectData->Save();
}

void WorldGenerator::Data::ProjectData::Save() {
    nlohmann::json result;

    result["name"] = m_ProjectName;
    result["biomeID_Offset"] = m_pBiomeID;
    result["noise_layer_offset"] = m_pLayerOffset;
    result["seed"] = m_Seed;

    _SaveTextureData(result);
    _SaveModelData(result);
    m_WorldGenDataEditable->Save(result);

    auto file = _GetSaveArchive();
    file->writestr("data.json", result.dump(4));

    try {
        file->save(m_Path);
        /*
        std::ofstream outfile;
        outfile.open(m_Path, std::ios::trunc);
        outfile << result.dump(4);
        outfile.close();
        */
        WG_LOG_INFO("Saved project");
    } catch (std::exception &e) {
        WG_LOG_ERROR("Failed to save project data");
    }
}

void WorldGenerator::Data::ProjectData::_LoadModelData(nlohmann::json &json) {
    for (auto &js : json.at("models")) {
        auto model = std::make_shared<WorldGenerator::Data::ModelData>();
        model->m_Name = js.at("name").get<std::string>();
        model->m_Path = js.at("path").get<std::string>();

        for (auto &modelTex : js.at("model_textures")) {
            model->m_ModelTextures[modelTex.at("mesh_id").get<uint32_t>()] = modelTex.at("texture").get<std::string>();
        }

        m_ProjectData->m_ModelData[model->m_Name] = model;
        Resources::ModelParser::LoadModel(m_ProjectData->m_ModelData[model->m_Name]);
    }
}

void WorldGenerator::Data::ProjectData::_SaveModelData(nlohmann::json &json) {
    auto modelData = nlohmann::json::array();
    for (auto &model : m_ProjectData->m_ModelData) {
        auto modelJson = nlohmann::json();
        modelJson["name"] = model.second->m_Name;
        modelJson["path"] = model.second->m_Path;
        auto modelTextures = nlohmann::json::array();
        for (auto &tex : model.second->m_ModelTextures) {
            auto modTexture = nlohmann::json();
            modTexture["mesh_id"] = tex.first;
            modTexture["texture"] = tex.second;
            modelTextures.emplace_back(modTexture);
        }
        modelJson["model_textures"] = modelTextures;
        modelData.emplace_back(modelJson);
    }
    json["models"] = modelData;
}

void WorldGenerator::Data::ProjectData::_LoadTextureData(nlohmann::json &json) {
    for (auto &tex : json.at("textures")) {
        auto texture = std::make_shared<WorldGenerator::Data::TextureData>();
        texture->m_Path = tex.at("path").get<std::string>();
        texture->m_Name = tex.at("name").get<std::string>();
        m_TextureData[texture->m_Name] = texture;
        Resources::TextureParser::LoadTexture(m_TextureData[texture->m_Name]);
    }
}

void WorldGenerator::Data::ProjectData::_SaveTextureData(nlohmann::json &json) {
    auto textureData = nlohmann::json::array();
    for (auto &tex : m_ProjectData->m_TextureData) {
        auto texJson = nlohmann::json();
        texJson["name"] = tex.second->m_Name;
        texJson["path"] = tex.second->m_Path;
        textureData.emplace_back(texJson);
    }
    json["textures"] = textureData;
}

void WorldGenerator::Data::ProjectData::LoadInternal(nlohmann::json &json) {
    _LoadTextureData(json);
    _LoadModelData(json);
    m_WorldGenDataEditable = std::make_shared<WorldGenerator::Data::WorldGenerationData>();
    m_WorldGenDataEditable->Load(json);
    m_WorldGenDataActive = std::shared_ptr<WorldGenerator::Data::WorldGenerationData>(
            new WorldGenerator::Data::WorldGenerationData(*m_WorldGenDataEditable.get()));
    if (json.contains("noise_layer_offset")) {
        m_pLayerOffset = json.at("noise_layer_offset").get<uint32_t>();
    }
}

void WorldGenerator::Data::ProjectData::AddNoiseLayer(uint32_t type, uint32_t placeAfter) {
    auto autoGenName = std::string("Noise_") + std::to_string(m_pLayerOffset++);
    auto data = m_WorldGenDataEditable->GetNoiseLayerFromType(type);

    if (!m_WorldGenDataEditable->m_NoiseLayers.empty()) {
        m_WorldGenDataEditable->m_NoiseLayers.insert(m_WorldGenDataEditable->m_NoiseLayers.begin() + placeAfter,
                                                     std::make_shared<Data::NoiseLayerData>(autoGenName, data));
    } else {
        m_WorldGenDataEditable->m_NoiseLayers.emplace_back(std::make_shared<Data::NoiseLayerData>(autoGenName, data));
    }
    WorldData::m_World->ResetBuild();
}

void WorldGenerator::Data::ProjectData::AddNoiseLayer(const std::shared_ptr<NoiseLayerData> &cpy, uint32_t placeAfter) {
    auto data = std::make_shared<NoiseLayerData>(*cpy);

    if (!m_WorldGenDataEditable->m_NoiseLayers.empty()) {
        m_WorldGenDataEditable->m_NoiseLayers.insert(m_WorldGenDataEditable->m_NoiseLayers.begin() + placeAfter, data);
    } else {
        m_WorldGenDataEditable->m_NoiseLayers.emplace_back(data);
    }
    WorldData::m_World->ResetBuild();
}

std::shared_ptr<WorldGenerator::Data::ModelData>
WorldGenerator::Data::ProjectData::AddModelToProject(const std::string &name, const std::string &path) {
    m_pWantsToWrite = true;
    auto toParse = std::make_shared<WorldGenerator::Data::ModelData>();

    std::string finalName = std::string("Models/") + name;

    while (m_pActiveAccess) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    m_pActiveAccess = true;
    m_pSaveArchive->write_file(finalName, path);
    m_pActiveAccess = false;
    m_pWantsToWrite = false;

    toParse->m_Name = name;
    toParse->m_Path = finalName;
    WorldGenerator::Resources::ModelParser::LoadModel(toParse);
    WorldGenerator::Data::ProjectData::m_ProjectData->m_ModelData[name] = toParse;
    return toParse;
}

WorldGenerator::Data::ProjectData::~ProjectData() {
    m_pSaveArchive.reset();
}

std::shared_ptr<miniz_cpp::zip_file> WorldGenerator::Data::ProjectData::_GetSaveArchive() {
    auto res = std::make_shared<miniz_cpp::zip_file>();

    auto validTextures = std::set<std::string>();
    auto validModels = std::set<std::string>();

    for (auto &&a : m_TextureData) {
        validTextures.emplace(a.second->m_Path);
    }

    for (auto &&a : m_ModelData) {
        validModels.emplace(a.second->m_Path);
    }

    for (uint32_t i = 0; i < m_pSaveArchive->get_file_count(); i++) {
        auto name = m_pSaveArchive->get_file_name_from_index(i);
        if (name != "data.json") {
            if (validTextures.find(name) != validTextures.end() || validModels.find(name) != validModels.end())
                res->add_file_from_other_archive(*m_pSaveArchive, i);
        }
    }

    return res;
}

std::pair<const void *, size_t> WorldGenerator::Data::ProjectData::GetModelToMemory(const std::string &path) {
    while (m_pWantsToWrite || m_pActiveAccess) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    m_pActiveAccess = true;
    auto res = m_pSaveArchive->read_file(path);
    m_pActiveAccess = false;

    return res;
}

std::pair<const void *, size_t> WorldGenerator::Data::ProjectData::GetTextureToMemory(const std::string &path) {
    while (m_pWantsToWrite || m_pActiveAccess) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    m_pActiveAccess = true;
    auto res = m_pSaveArchive->read_file(path);
    m_pActiveAccess = false;

    return res;
}

std::shared_ptr<WorldGenerator::Data::TextureData>
WorldGenerator::Data::ProjectData::AddTextureToProject(const std::string &name, const std::string &path) {
    m_pWantsToWrite = true;
    auto toParse = std::make_shared<WorldGenerator::Data::TextureData>();

    std::string finalName = std::string("Textures/") + name;

    while (m_pActiveAccess) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    m_pActiveAccess = true;
    m_pSaveArchive->write_file(finalName, path);
    m_pActiveAccess = false;
    m_pWantsToWrite = false;

    toParse->m_Name = name;
    toParse->m_Path = finalName;
    WorldGenerator::Resources::TextureParser::LoadTexture(toParse);
    WorldGenerator::Data::ProjectData::m_ProjectData->m_TextureData[name] = toParse;
    return toParse;
}

void WorldGenerator::Data::ProjectData::RemoveModel(const std::string &name) {
    m_ModelData.erase(name);
    //FIXME: Unbind from all biomes etc.
}

void WorldGenerator::Data::ProjectData::RemoveTexture(const std::string &name) {
    m_TextureData.erase(name);
    //FIXME: Unbind from all models
}

bool has_suffix(const std::string &str, const std::string &suffix) {
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}
