//
// Created by mattyvrba on 9/8/20.
//

#include "../world-gen/node_editor/nodes/FloatNode.h"
#include "../world-gen/data/EditorIds.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imnodes.h"

void WorldGenerator::NodeEditor::Nodes::FloatNode::_DrawExtra()
{
    Node::_DrawExtra();
}
WorldGenerator::NodeEditor::Nodes::FloatNode::FloatNode(uint32_t id,std::map<uint32_t,std::shared_ptr<Pin>> & pinStorage)
        :   Node(id,pinStorage)
{
    m_pName = "Float";

    uint32_t pinID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    const auto pinOutput = std::make_shared<FloatPin>(pinID,"Output",true,m_pValue);
    pinStorage[pinID] = pinOutput;
    m_pOutputPins.emplace_back(pinOutput);
}
uint32_t WorldGenerator::NodeEditor::Nodes::FloatNode::GetOutputCount() const
{
    return 1;
}
uint32_t WorldGenerator::NodeEditor::Nodes::FloatNode::GetInputCount() const
{
    return 0;
}
void WorldGenerator::NodeEditor::Nodes::FloatNode::AddOperation(const std::shared_ptr<WorldGenConfig>& config)
{

}
