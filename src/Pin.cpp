//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/node_editor/Pin.h"

#include <utility>
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/imgui/imnodes.h"
#include "../world-gen/node_editor/NodeColors.h"

void WorldGenerator::NodeEditor::Pin::Draw(bool isConnected) {
    imnodes::PushColorStyle(imnodes::ColorStyle_Pin, GetColorFromPinType(m_pType));
    if (m_pOutput) {
        imnodes::BeginOutputAttribute(m_pID);
    } else {
        imnodes::BeginInputAttribute(m_pID);
    }
    ImGui::TextUnformatted(m_pName.c_str());

    if (!isConnected && !m_pOutput) {
        _NotConnectedDraw();
    }
    if (m_pOutput) {
        imnodes::EndOutputAttribute();
    } else {
        imnodes::EndInputAttribute();
    }
    imnodes::PopColorStyle();
}

WorldGenerator::NodeEditor::Pin::Pin(uint32_t id, const std::string &name, bool isOutput) : m_pID(id), m_pName(name),
                                                                                            m_pOutput(isOutput) {

}

std::string_view WorldGenerator::NodeEditor::Pin::GetText() {
    return std::string_view(m_pName);
}

void WorldGenerator::NodeEditor::Pin::_NotConnectedDraw() {

}

WorldGenerator::NodeEditor::BoolPin::BoolPin(uint32_t id, const std::string &name, bool isOutput) : Pin(id, name,
                                                                                                        isOutput) {
    m_pType = PinType::PIN_BOOL;
}

WorldGenerator::NodeEditor::BiomePin::BiomePin(uint32_t id, const std::string &name, bool isOutput) : Pin(id, name,
                                                                                                          isOutput) {
    m_pType = PinType::PIN_BIOME;
}

WorldGenerator::NodeEditor::Vec2Pin::Vec2Pin(uint32_t id, const std::string &name, bool isOutput,glm::vec2 & value) : Pin(id, name,
                                                                                                        isOutput), m_pValue(value) {
    m_pType = PinType::PIN_VEC2;
}

WorldGenerator::NodeEditor::FloatPin::FloatPin(uint32_t id, const std::string &name, bool isOutput, float &value) : Pin(
        id, name, isOutput), m_pValue(value) {
    m_pType = PinType::PIN_FLOAT;
}

void WorldGenerator::NodeEditor::FloatPin::_NotConnectedDraw() {
    Pin::_NotConnectedDraw();

    constexpr auto nodeWidth = 100.f;
    const auto labelWidth = ImGui::CalcTextSize(m_pName.c_str()).x;

    ImGui::SameLine();
    ImGui::PushItemWidth(nodeWidth - labelWidth);
    ImGui::DragFloat("##hidelabel", &m_pValue, 0.01f);
    ImGui::PopItemWidth();
}

WorldGenerator::NodeEditor::Vec3Pin::Vec3Pin(uint32_t id, const std::string &name, bool isOutput,glm::vec3 & value) : Pin(id, name,
                                                                                                        isOutput) , m_pValue(value){
    m_pType = PinType::PIN_VEC3;
}

WorldGenerator::NodeEditor::Vec4Pin::Vec4Pin(uint32_t id, const std::string &name, bool isOutput,glm::vec4 & value) : Pin(id, name,
                                                                                                        isOutput) , m_pValue(value){
    m_pType = PinType::PIN_VEC4;
}
