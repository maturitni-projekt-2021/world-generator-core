//
// Created by Matty on 1.9.2020.
//

#include "../world-gen/render/DrawBatch.h"
#include "../world-gen/data/RenderData.h"

void WorldGenerator::Render::DrawBatch::AddModelTransform(uint32_t id, const glm::mat4 &transform) {
    m_pBatchData[id].emplace_back(transform);
    m_pBatchCount[id]++;
}

void WorldGenerator::Render::DrawBatch::Empty() {
    m_pBatchData.clear();
    for (auto &a : m_pBatchCount) {
        m_pBatchData[a.first].reserve(a.second);
    }
    m_pBatchCount.clear();
    uint32_t oldSize = m_pTerrain.size();
    m_pTerrain.clear();
    m_pTerrain.reserve(oldSize);
    m_pTerrainPositions.clear();
}

void WorldGenerator::Render::DrawBatch::RenderBatch(std::vector<std::shared_ptr<Render::Camera>> &cameras) {
    for (auto &cam : cameras) {
        cam->BindTargetBuffer();
        cam->ApplyFrustum();
        auto projection = cam->GetProjectionMatrix();
        auto view = cam->GetViewMatrix();

        //TODO: Check if in frustrum
        for (auto &batch : m_pBatchData) {
            if (WorldGenerator::Data::RenderData::m_RenderData->m_LoadedCustomModels.count(batch.first) > 0) {
                WorldGenerator::Data::RenderData::m_RenderData->m_LoadedCustomModels.at(batch.first)->RenderBatched(
                        projection, view, batch.second);
            }
        }
        for (auto &terrain : m_pTerrain) {
            if (terrain.second) {
#ifndef NDEBUG
                if(cam->InFrustum(*terrain.first.first))
                terrain.first.second->Render(projection, view, cam->GetPosition());
#else
                if(cam->InFrustum(*terrain.first))
                terrain.second->Render(projection, view, cam->GetPosition());
#endif
            }
        }

#ifndef NDEBUG
        //cam->DrawDebugFrustum();
#endif

        cam->PostProcess();
    }
}

#ifndef NDEBUG

void WorldGenerator::Render::DrawBatch::AddTerrain(const std::shared_ptr<World::Terrain> &terrain,
                                                   WorldGenerator::Render::Culling::CullCollider *cull,
                                                   const std::shared_ptr<Debug::DebugLineStrip> &debugLineStrip) {
    if (m_pTerrainPositions.find(terrain->GetChunkPosition()) == m_pTerrainPositions.end()) {
        m_pTerrain.emplace_back(
                std::pair<std::pair<Render::Culling::CullCollider *, std::shared_ptr<World::Terrain>>, std::shared_ptr<Debug::DebugLineStrip>>(
                        std::pair<Render::Culling::CullCollider *, std::shared_ptr<World::Terrain>>(cull, terrain),
                        debugLineStrip));
        m_pTerrainPositions.emplace(terrain->GetChunkPosition());
    }
}

#else
void WorldGenerator::Render::DrawBatch::AddTerrain(const std::shared_ptr<World::Terrain> &terrain,
                                                   Render::Culling::CullCollider *cull) {
    if (m_pTerrainPositions.find(terrain->GetChunkPosition()) == m_pTerrainPositions.end()) {
        m_pTerrain.emplace_back(
                std::pair<Render::Culling::CullCollider *, std::shared_ptr<World::Terrain>>(cull, terrain));
        m_pTerrainPositions.emplace(terrain->GetChunkPosition());
    }
}
#endif
