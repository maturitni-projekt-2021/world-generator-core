//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/world/Biome.h"
#include "../world-gen/util/UtilFormat.h"

WorldGenerator::World::BiomeColorLayer::BiomeColorLayer(const glm::vec3 &color, float min, float max) : m_Color(color),
                                                                                                        m_Min(min),
                                                                                                        m_Max(max) {}

void WorldGenerator::World::Biome::AddLayer(WorldGenerator::World::BiomeColorLayer layer) {
    m_pColorLayers.emplace_back(layer);
}

WorldGenerator::World::Biome::Biome(const glm::vec3 &baseColor) : m_pBaseColor(baseColor) {}

const glm::vec3 &WorldGenerator::World::Biome::GetBaseColor() const {
    return m_pBaseColor;
}

glm::vec3 WorldGenerator::World::Biome::GetColorFromHeight(float y) const {
    auto result = m_pBaseColor;
    for (auto &l : m_pColorLayers) {
        if (l.m_Min < y && l.m_Max > y) {
            result = l.m_Color;
            break;
        }
    }
    return result;
}

std::vector<WorldGenerator::World::BiomeColorLayer> WorldGenerator::World::Biome::GetColorLayers() const {
    return m_pColorLayers;
}

void WorldGenerator::World::Biome::RemoveColorLayer(size_t index) {
    if (m_pColorLayers.size() > 1) {
        m_pColorLayers.erase(m_pColorLayers.begin() + index);
    }
}

void WorldGenerator::World::Biome::Save(nlohmann::json &json) {
    json["base_color"] = Util::UtilFormat::Vec3ToJson(m_pBaseColor);
    auto colorLayers = nlohmann::json::array();
    for (const auto &l : m_pColorLayers) {
        auto lJson = nlohmann::json();
        lJson["color"] = Util::UtilFormat::Vec3ToJson(l.m_Color);
        lJson["min"] = l.m_Min;
        lJson["max"] = l.m_Max;
        colorLayers.emplace_back(lJson);
    }

    json["name"] = m_pName;

    {
        auto moisutureArr = nlohmann::json::array();
        moisutureArr.emplace_back(m_pMoistureBounds.x);
        moisutureArr.emplace_back(m_pMoistureBounds.y);
        json["moistureBounds"] = moisutureArr;
    }
    {
        auto temperatureArr = nlohmann::json::array();
        temperatureArr.emplace_back(m_pTemperatureBounds.x);
        temperatureArr.emplace_back(m_pTemperatureBounds.y);
        json["temperatureBounds"] = temperatureArr;
    }
    {
        auto heightArr = nlohmann::json::array();
        heightArr.emplace_back(m_pHeightBounds.x);
        heightArr.emplace_back(m_pHeightBounds.y);
        json["heightBounds"] = heightArr;
    }

    json["layers"] = colorLayers;
}

void WorldGenerator::World::Biome::Load(nlohmann::json &json) {
    m_pBaseColor = Util::UtilFormat::JsonToVec3(json["base_color"]);
    m_pColorLayers.reserve(json.at("layers").size());
    for (auto &layer : json.at("layers")) {
        m_pColorLayers.emplace_back(Util::UtilFormat::JsonToVec3(layer.at("color")), layer.at("min").get<float>(),
                                    layer.at("max").get<float>());
    }

    m_pName = json.at("name").get<std::string>();

    if(json.contains("moisture_bounds")) {
        auto x = json.at("moisture_bounds").at(0).get<float>();

        m_pMoistureBounds = glm::vec2(x,json.at("moisture_bounds").at(1).get<float>());
        m_pTemperatureBounds = glm::vec2(json.at("temperatureBounds").at(0).get<float>(),json.at("temperatureBounds").at(1).get<float>());
        m_pHeightBounds = glm::vec2(json.at("heightBounds").at(0).get<float>(),json.at("heightBounds").at(1).get<float>());
    }

    /*
    m_pWorldObjectSpawnRules.reserve(json.at("world_object_rules").size());
    for(auto & rule : json.at("world_object_rules")) {
        //m_pWorldObjectSpawnRules.emplace_back(Util::UtilFormat::JsonToVec3(layer.at("color")),layer.at("min").get<float>(),layer.at("max").get<float>());
    }*/
}

WorldGenerator::World::Biome::Biome() {

}

WorldGenerator::World::Biome::Biome(const WorldGenerator::World::Biome &cpy) {
    m_pBaseColor = cpy.m_pBaseColor;
    m_pColorLayers = cpy.m_pColorLayers;
    m_pWorldObjectSpawnRules = cpy.m_pWorldObjectSpawnRules;
    m_pMoistureBounds = cpy.m_pMoistureBounds;
    m_pTemperatureBounds = cpy.m_pTemperatureBounds;
    m_pHeightBounds = cpy.m_pHeightBounds;
    m_pMultiplier = cpy.m_pMultiplier;
    m_pName = cpy.m_pName;
}

bool WorldGenerator::World::Biome::IsLegitimate(float moisture, float temperature, float height) const {
    return (m_pHeightBounds.x > height && m_pHeightBounds.y < height) &&
           (m_pMoistureBounds.x > moisture && m_pMoistureBounds.y < moisture) &&
           (m_pTemperatureBounds.x > temperature && m_pTemperatureBounds.y < temperature);
}

void WorldGenerator::World::Biome::setMPBaseColor(const glm::vec3 &mPBaseColor) {
    m_pBaseColor = mPBaseColor;
}

const std::vector<WorldGenerator::World::BiomeColorLayer> &WorldGenerator::World::Biome::getMPColorLayers() const {
    return m_pColorLayers;
}

void WorldGenerator::World::Biome::setMPColorLayers(const std::vector<BiomeColorLayer> &mPColorLayers) {
    m_pColorLayers = mPColorLayers;
}

const std::vector<WorldGenerator::World::WOSpawnRules> &
WorldGenerator::World::Biome::getMPWorldObjectSpawnRules() const {
    return m_pWorldObjectSpawnRules;
}

void
WorldGenerator::World::Biome::setMPWorldObjectSpawnRules(const std::vector<WOSpawnRules> &mPWorldObjectSpawnRules) {
    m_pWorldObjectSpawnRules = mPWorldObjectSpawnRules;
}

const glm::vec2 &WorldGenerator::World::Biome::getMPMoistureBounds() const {
    return m_pMoistureBounds;
}

void WorldGenerator::World::Biome::SetMoistureBounds(const glm::vec2 &mPMoistureBounds) {
    m_pMoistureBounds = mPMoistureBounds;
}

const glm::vec2 &WorldGenerator::World::Biome::GetTemperatureBounds() const {
    return m_pTemperatureBounds;
}

void WorldGenerator::World::Biome::SetTemperatureBounds(const glm::vec2 &mPTemperatureBounds) {
    m_pTemperatureBounds = mPTemperatureBounds;
}

const glm::vec2 &WorldGenerator::World::Biome::GetHeightBounds() const {
    return m_pHeightBounds;
}

void WorldGenerator::World::Biome::SetHeightBounds(const glm::vec2 &mPHeightBounds) {
    m_pHeightBounds = mPHeightBounds;
}

float WorldGenerator::World::Biome::GetMultiplier() const {
    return m_pMultiplier;
}

void WorldGenerator::World::Biome::SetMultiplier(float mPMultiplier) {
    m_pMultiplier = mPMultiplier;
}

const std::string &WorldGenerator::World::Biome::GetName() const {
    return m_pName;
}

void WorldGenerator::World::Biome::SetName(const std::string &mPName) {
    m_pName = mPName;
}
