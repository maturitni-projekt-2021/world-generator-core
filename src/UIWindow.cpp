//
// Created by Matty on 2.9.2020.
//

#include "../world-gen/ui/UIWindow.h"
#include "../world-gen/imgui/imgui.h"

void WorldGenerator::UI::UIWindow::Draw()
{
    CustomWindowSetup();

    if(!m_pPopupInit && m_pType == WindowType::POPUP) {
        ImGui::OpenPopup(m_pName.c_str());
        m_pPopupInit = true;
    }


    bool success = true;

    if (m_pType==WindowType::WINDOW) {
        ImGui::Begin(m_pName.c_str(), &m_pCollapsed, m_Flags);
    }
    else {
        ImGui::PushID(this);
        success = ImGui::BeginPopupModal(m_pName.c_str(), &m_pCollapsed);
    }

    if (success) {
        CustomDraw();
    }
    if (success) {
        if (m_pType==WindowType::WINDOW) {
            ImGui::End();
        }
        else {
            ImGui::EndPopup();
            ImGui::PopID();
        }
    }

    CustomPostDraw();
}
WorldGenerator::UI::UIWindow::UIWindow(std::string name, WorldGenerator::UI::WindowType type)
        :m_pName(std::move(name)), m_pType(type)
{

}
