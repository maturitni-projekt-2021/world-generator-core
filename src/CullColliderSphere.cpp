//
// Created by Matty on 11/1/2020.
//

#include "../world-gen/render/culling/CullColliderSphere.h"

WorldGenerator::Render::Culling::CullColliderSphere::CullColliderSphere(const glm::vec3 &position, float mPRadius)
        : CullCollider(position), m_pRadius(mPRadius) {}

bool WorldGenerator::Render::Culling::CullColliderSphere::IsVisible(
        const WorldGenerator::Render::Culling::FrustumSide &topSide,
        const WorldGenerator::Render::Culling::FrustumSide &bottomSide,
        const WorldGenerator::Render::Culling::FrustumSide &leftSide,
        const WorldGenerator::Render::Culling::FrustumSide &rightSide,
        const WorldGenerator::Render::Culling::FrustumSide &nearSide,
        const WorldGenerator::Render::Culling::FrustumSide &farSide) const {
    WorldGenerator::Render::Culling::FrustumSide sides[] = {topSide,bottomSide,leftSide,rightSide,nearSide,farSide};

    return false;
}
