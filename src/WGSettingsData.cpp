//
// Created by Matty on 10/29/2020.
//

#include "../world-gen/data/WGSettingsData.h"
#include <utility>

WorldGenerator::Data::WGSetting::WGSetting(std::string name) : m_pName(std::move(name)){
}

void WorldGenerator::Data::WGSetting_Float::Load(const nlohmann::json &json) {
    try {
        m_Value = json.at(m_pName).get<float>();
    } catch (std::exception & e) {
    }
}

void WorldGenerator::Data::WGSetting_Float::Save(nlohmann::json &json) {

}

WorldGenerator::Data::WGSetting_Float::WGSetting_Float(const std::string &name, float defaultValue) : WGSetting(name), m_Value(defaultValue) {

}
