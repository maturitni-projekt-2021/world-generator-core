//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/data/EditorIds.h"

std::unique_ptr<WorldGenerator::Data::EditorIds> WorldGenerator::Data::EditorIds::m_IDs;
void WorldGenerator::Data::EditorIds::Flush()
{
    m_pConnectionID = 0;
    m_pNodeID = 0;
}
uint32_t WorldGenerator::Data::EditorIds::GetConnectionID()
{
    return m_pConnectionID++;
}
uint32_t WorldGenerator::Data::EditorIds::GetID()
{
    return m_pNodeID++;
}
