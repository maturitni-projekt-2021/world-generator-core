//
// Created by Matty on 5.9.2020.
//

#include "../world-gen/render/framebuffers/FBViewportCamera.h"
#include "../world-gen/data/RenderData.h"

WorldGenerator::Render::FBViewportCamera::FBViewportCamera(uint32_t width, uint32_t height,bool useMultisample )
        :FrameBuffer(width, height,useMultisample)
{
    m_pAttachments.reserve(1);


    const auto mainLayer = std::make_shared<ColorAttachment>(width, height, FrameBufferAttachment::COLOR_ATTACHMENT0,
            TextureColorType::RGBA, 0,useMultisample);

    m_pAttachments.emplace_back(mainLayer);
    m_pBaseColorAttachment = mainLayer;
    _LoadAttachments();
}
std::shared_ptr<WorldGenerator::Render::ColorAttachment> WorldGenerator::Render::FBViewportCamera::GetMainColorAttachment()
{
    return m_pBaseColorAttachment;
}