//
// Created by Matty on 11/9/2020.
//

#include "../world-gen/data/NoiseLayerData.h"
#include "../world-gen/util/UtilFormat.h"
#include "../world-gen/Log.h"

WorldGenerator::Data::NoiseLayerData::NoiseLayerData(std::string name,
                                                     std::shared_ptr<TerrainGeneration::Data::NoiseLayer> data)
                                                     : m_pName(std::move(name)), m_pLayer(std::move(data)) {
}

void WorldGenerator::Data::NoiseLayerData::SetEnabled(bool newVal) {
    m_pEnabled = newVal;
}

bool WorldGenerator::Data::NoiseLayerData::IsEnabled() const {
    return m_pEnabled;
}

std::string WorldGenerator::Data::NoiseLayerData::GetName() const {
    return m_pName;
}

void WorldGenerator::Data::NoiseLayerData::SetName(const std::string &name) {
    m_pName = name;
}

std::shared_ptr<WorldGenerator::TerrainGeneration::Data::NoiseLayer> WorldGenerator::Data::NoiseLayerData::GetData() const {
    return m_pLayer;
}

void WorldGenerator::Data::NoiseLayerData::Save(nlohmann::json &output) {
    auto res = nlohmann::json();
    res["name"] = m_pName;
    res["enabled"] = m_pEnabled;
    res["type"] = m_pLayer->GetType();
    res["scale"] = m_pLayer->GetScale();
    res["intensity"] = m_pLayer->GetIntensity();
    res["offset"] = Util::UtilFormat::Vec3ToJson(m_pLayer->GetOffset());
    res["clamp"] = m_pLayer->GetClampAfter();
    res["addition_type"] = m_pLayer->GetAddition();
    res["bias_active"] = m_pLayer->GetApplyBias();
    res["bias_value"] = m_pLayer->GetBiasValue();
    output.emplace_back(res);
}

WorldGenerator::Data::NoiseLayerData::NoiseLayerData(const NoiseLayerData & cpy) {
    this->m_pName = cpy.m_pName;
    this->m_pEnabled = cpy.m_pEnabled;
    this->m_pLayer = std::shared_ptr<TerrainGeneration::Data::NoiseLayer>(cpy.m_pLayer->Copy());
}
