//
// Created by Matty on 11/2/2020.
//
#ifndef NDEBUG
#include "../world-gen/render/debug/DebugPrimitive.h"
#include <glm/gtc/matrix_transform.hpp>

glm::mat4 WorldGenerator::Render::Debug::DebugPrimitive::_GetModelMatrix() {
    auto base = glm::mat4(1.0f);
    base = glm::translate(base,m_pPosition);
    base = glm::rotate(base, m_pRotation.x, {1, 0, 0});
    base = glm::rotate(base, m_pRotation.y, {0, 1, 0});
    base = glm::rotate(base, m_pRotation.z, {0, 0, 1});
    base = glm::scale(base, m_pScale);
    return base;
}

WorldGenerator::Render::Debug::DebugPrimitive::DebugPrimitive(const glm::vec3 &mPPosition, const glm::vec3 &mPRotation,
                                                              const glm::vec3 &mPScale, const glm::vec3 & color) : m_pPosition(mPPosition),
                                                                                          m_pRotation(mPRotation),
                                                                                          m_pScale(mPScale),m_pColor(color) {
    m_pVertexBuffer = std::make_unique<WorldGenerator::Render::VertexBuffer>();
}
#endif