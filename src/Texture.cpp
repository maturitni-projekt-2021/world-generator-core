//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/glad.h"
#include "../world-gen/render/Texture.h"
#include "../world-gen/data/RenderData.h"
WorldGenerator::Render::Texture::Texture(uint32_t width, uint32_t height,
        WorldGenerator::Render::TextureColorType color, bool usesLinearSampling,TextureType type)
        : m_pType(type), m_pHeight(height), m_pWidth(width), m_pColor(color)
{

    {
        uint32_t textureIDtmp;
        glGenTextures(1, &textureIDtmp);
        m_pID = std::make_shared<TextureID>(textureIDtmp);
    }

    glBindTexture(m_pType, m_pID->GetID());

    if(m_pType == TextureType::TEXTURE_2D_MULTISAMPLE) {
        glTexImage2DMultisample(m_pType, WorldGenerator::Data::RenderData::m_RenderData->m_AntiAliasingLevel, color, width, height, GL_TRUE);
    } else {
        glTexImage2D(m_pType, 0, color, m_pWidth, m_pHeight, 0, color, GL_UNSIGNED_BYTE, NULL);

    }
    if(m_pType != TextureType::TEXTURE_2D_MULTISAMPLE) {

        glTexParameteri(m_pType, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(m_pType, GL_TEXTURE_WRAP_T, GL_REPEAT);

        if (usesLinearSampling) {
            glTexParameteri(m_pType, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(m_pType, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        }
        else {
            glTexParameteri(m_pType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(m_pType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        }
    }
    glBindTexture(m_pType, 0);
}
void WorldGenerator::Render::Texture::BindTexture()
{
    glBindTexture(m_pType, m_pID->GetID());
}
void WorldGenerator::Render::Texture::UploadData(uint8_t* data)
{
    glTexImage2D(m_pType, 0, m_pColor, m_pWidth, m_pHeight, 0, m_pColor, GL_UNSIGNED_BYTE, data);
    //glGenerateMipmap(GL_TEXTURE_2D);
}
std::string WorldGenerator::Render::Texture::GetName() const
{
    return m_pName;
}
void WorldGenerator::Render::Texture::SetName(const std::string& name)
{
    m_pName = name;
}
