//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/Model.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/data/PerformanceData.h"

#include "../world-gen/glad.h"
#include "../world-gen/data/ProjectData.h"

void WorldGenerator::Render::Model::SetCustomShader(const std::shared_ptr<Shader>& customShader)
{
    m_pHasCustomShader = true;
    m_pCustomShader = std::shared_ptr<WorldGenerator::Render::Shader>(customShader);
}
void WorldGenerator::Render::Model::Render(const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix,
        const glm::mat4& modelMatrix)
{
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    auto shader = m_pHasCustomShader ? m_pCustomShader : m_pShader;

    shader->activate();

    shader->setUniformMat4("u_projection", projectionMatrix);
    shader->setUniformMat4("u_view", viewMatrix);
    shader->setUniformMat4("u_model", modelMatrix);
    shader->setUniformBool("u_instanced", false);

    m_pVertexBuffer->bind();
    Custom::ModelVertex::GetModelVertexLayout().setAttributePointers();
    glDrawElements(GL_TRIANGLES, m_pVertexBuffer->getElementCount(), GL_UNSIGNED_INT, nullptr);
    WorldGenerator::Data::PerformanceData::m_PerformanceData->m_DrawCalls++;
}
void WorldGenerator::Render::Model::RenderBatched(const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix,
        std::vector<glm::mat4>& modelMatrices)
{
    auto shader = m_pHasCustomShader ? m_pCustomShader : m_pShader;

    uint32_t samplerOffset = 0;

    shader->activate();
    shader->setUniformMat4("u_projection", projectionMatrix);
    shader->setUniformMat4("u_view", viewMatrix);
    shader->setUniformBool("u_instanced", true);
    shader->setUniformBool("u_hasAlbedo", m_pHasAlbedoTexture);
    if (m_pHasAlbedoTexture) {
            glActiveTexture(GL_TEXTURE0 + samplerOffset);
        if(m_pTextureAlbedo->m_TextureLoaded) {
            m_pTextureAlbedo->m_Texture->BindTexture();
        } else {
            Data::ProjectData::m_ProjectData->m_MissingTexture->m_Texture->BindTexture();
        }
        shader->setUniformInt("u_textureAlbedo", samplerOffset++);
    }
    //u_hasNormal;
    //u_textureNormal;
    //u_hasRoughness;
    //u_textureRoughness;

    m_pVertexBuffer->bind();
    Custom::ModelVertex::GetModelVertexLayout().setAttributePointers();
    for (uint32_t i = 0; i<modelMatrices.size(); i++) {
        uint32_t stride = i/WorldGenerator::Data::RenderData::m_RenderData->m_MaxInstancesPerCall;
        uint32_t gpuIndex = i%WorldGenerator::Data::RenderData::m_RenderData->m_MaxInstancesPerCall;

        std::stringstream nameStream;
        nameStream << "u_instancedModel[" << gpuIndex << "]";
        shader->setUniformMat4(nameStream.str(), modelMatrices.at(i));

        if (gpuIndex==WorldGenerator::Data::RenderData::m_RenderData->m_MaxInstancesPerCall-1
                || i==modelMatrices.size()-1) {
            uint32_t instanceCount =
                    i==modelMatrices.size()-1 ? (gpuIndex+1) : WorldGenerator::Data::RenderData::m_RenderData
                            ->m_MaxInstancesPerCall;
            glDrawElementsInstanced(GL_TRIANGLES, m_pVertexBuffer->getElementCount(), GL_UNSIGNED_INT, nullptr,
                    instanceCount);
            WorldGenerator::Data::PerformanceData::m_PerformanceData->m_DrawCalls++;
        }
    }
}
WorldGenerator::Render::Model::Model(uint32_t id, uint32_t VAO, uint32_t VBO, uint32_t EBO, bool buildInstVBO)
{
    m_pID = id;
    m_pVertexBuffer = std::make_shared<WorldGenerator::Render::VertexBuffer>(VAO, VBO, EBO, buildInstVBO);
    m_pShader = WorldGenerator::Data::RenderData::m_RenderData->m_ModelShader;
}
void WorldGenerator::Render::Model::SetAlbedoTexture(const std::shared_ptr<WorldGenerator::Data::TextureData>& texture)
{
    m_pTextureAlbedo = texture;
    m_pHasAlbedoTexture = true;
}
void WorldGenerator::Render::Model::SetNormalTexture(const std::shared_ptr<WorldGenerator::Data::TextureData>& texture)
{
    m_pTextureNormal = texture;
    m_pHasNormalTexture = true;
}
void WorldGenerator::Render::Model::SetRoughnessTexture(const std::shared_ptr<WorldGenerator::Data::TextureData>& texture)
{
    m_pTextureRoughness = texture;
    m_pHasRoughnessTexture = true;
}
bool WorldGenerator::Render::Model::HasRoughnessTexture() const
{
    return m_pHasRoughnessTexture;
}
bool WorldGenerator::Render::Model::HasAlbedoTexture() const
{
    return m_pHasAlbedoTexture;
}
bool WorldGenerator::Render::Model::HasNormalTexture() const
{
    return m_pHasNormalTexture;
}
std::shared_ptr<WorldGenerator::Data::TextureData> WorldGenerator::Render::Model::GetAlbedoTexture() const
{
    return m_pTextureAlbedo;
}
std::shared_ptr<WorldGenerator::Data::TextureData> WorldGenerator::Render::Model::GetRoughnessTexture() const
{
    return m_pTextureRoughness;
}
std::shared_ptr<WorldGenerator::Data::TextureData> WorldGenerator::Render::Model::GetNormalTexture() const
{
    return m_pTextureNormal;
}
void WorldGenerator::Render::Model::ClearAlbedoTexture()
{
    m_pTextureAlbedo.reset();
    m_pHasAlbedoTexture = false;
}
void WorldGenerator::Render::Model::ClearNormalTexture()
{
    m_pTextureNormal.reset();
    m_pHasNormalTexture = false;
}
void WorldGenerator::Render::Model::ClearRoughnessTexture()
{
    m_pTextureRoughness.reset();
    m_pHasRoughnessTexture = false;
}
