//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/world_generator/ChunkGenerationTask.h"

WorldGenerator::TerrainGeneration::ChunkGenerationTask::ChunkGenerationTask(
        const WorldGenerator::World::WorldChunkPos &pos, uint32_t slot, uint32_t resolution,uint32_t version) : m_Pos(pos),
                                                                                               m_Resolution(resolution),
                                                                                               m_Slot(slot),
                                                                                               m_Version(version) {
}
