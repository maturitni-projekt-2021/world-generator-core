//
// Created by Matty on 14.6.2020.
//

#include "../world-gen/render/FrameBuffer.h"
#include "../world-gen/Log.h"
#include "../world-gen/glad.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/data/SettingsData.h"

WorldGenerator::Render::FrameBuffer::FrameBuffer(uint32_t width, uint32_t height, bool useMultisample)
        : m_pHeight(height), m_pWidth(width), m_pMultiSamplingEnabled(useMultisample) {
    glGenFramebuffers(1, &m_pID);
    glGenRenderbuffers(1, &m_pRBO);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        WG_LOG_ERROR("ERROR::FRAMEBUFFER:: Framebuffer is not complete!");
}

WorldGenerator::Render::FrameBuffer::~FrameBuffer() {
    glDeleteFramebuffers(1, &m_pID);
    glDeleteRenderbuffers(1, &m_pRBO);
}

WorldGenerator::Render::FrameBuffer::FrameBuffer(FrameBuffer &&other) noexcept {
    this->m_pID = other.m_pID;
    this->m_pHeight = other.m_pHeight;
    this->m_pWidth = other.m_pWidth;
}

void WorldGenerator::Render::FrameBuffer::SetViewPort() const {
    glViewport(0, 0, m_pWidth, m_pHeight);
}

void WorldGenerator::Render::FrameBuffer::Clear() {
    glClearColor(m_pClearColor.r, m_pClearColor.g, m_pClearColor.b, m_pClearColor.a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void WorldGenerator::Render::FrameBuffer::Bind() {
    glDisable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, m_pID);
    glBindRenderbuffer(GL_RENDERBUFFER, m_pRBO);
}

void WorldGenerator::Render::FrameBuffer::FullBind() {
    Bind();
    Clear();
    SetViewPort();
    if (m_pDepthTestingEnabled) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
    }

    if (m_pMultiSamplingEnabled) {
        glEnable(GL_MULTISAMPLE);
    }
}

void WorldGenerator::Render::FrameBuffer::_LoadAttachments() {
    Bind();
    if (m_pMultiSamplingEnabled) {
        glRenderbufferStorageMultisample(GL_RENDERBUFFER,
                                         WorldGenerator::Data::SettingsData::m_SettingsData->m_AntiAliasingLevel->m_Value,
                                         GL_DEPTH24_STENCIL8, m_pWidth,
                                         m_pHeight);
    } else {
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_pWidth, m_pHeight);
    }
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_pRBO);
    for (auto &a : m_pAttachments) {
        a->Apply();
    }
    UnbindAll();
}

void WorldGenerator::Render::FrameBuffer::SetClearColor(const glm::vec4 &newColor) {
    m_pClearColor = newColor;
}

void WorldGenerator::Render::FrameBuffer::UnbindAll() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);
}

void WorldGenerator::Render::FrameBuffer::PostProcess() {
    if (m_pMultiSamplingEnabled) {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_pID);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, WorldGenerator::Data::RenderData::m_RenderData->m_FBViewport->m_pID);
        glBlitFramebuffer(0, 0, m_pWidth, m_pHeight, 0, 0, m_pWidth, m_pHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
}
