//
// Created by Matty on 30.8.2020.
//
#include "../world-gen/data/RenderData.h"
#include "../world-gen/glad.h"
#include "../world-gen/Log.h"

WorldGenerator::Data::RenderData::RenderData() {
    const auto glVer = (const char*)glGetString(GL_VERSION);
    const auto log = std::string("OpenGL Version: ") + std::string(glVer);
    WG_LOG_INFO(log.c_str());
}

std::unique_ptr<WorldGenerator::Data::RenderData> WorldGenerator::Data::RenderData::m_RenderData;