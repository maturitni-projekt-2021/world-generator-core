//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/render/framebuffers/FBViewport.h"
#include "../world-gen/data/RenderData.h"

WorldGenerator::Render::FBViewport::FBViewport(uint32_t width, uint32_t height,bool useMultisample )
        :FrameBuffer(width, height,useMultisample)
{
    m_pAttachments.reserve(1);

    const auto mainLayer = std::make_shared<ColorAttachment>(width, height, FrameBufferAttachment::COLOR_ATTACHMENT0,
            TextureColorType::RGB, 0,useMultisample);

    m_pAttachments.emplace_back(mainLayer);
    m_pBaseColorAttachment = mainLayer;
    _LoadAttachments();
}
std::shared_ptr<WorldGenerator::Render::ColorAttachment> WorldGenerator::Render::FBViewport::GetMainColorAttachment()
{
    return m_pBaseColorAttachment;
}
