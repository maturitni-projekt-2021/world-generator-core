//
// Created by Matty on 7.5.2020.
//
#include <iostream>
#include "../world-gen/render/Shader.h"
#include "../world-gen/glad.h"
#include "../world-gen/Log.h"

void WorldGenerator::Render::Shader::activate() {
    static uint32_t activeShader = -1;
    if (activeShader != m_pShaderProgramId) {
        glUseProgram(m_pShaderProgramId);
        activeShader = m_pShaderProgramId;
    }
}

void WorldGenerator::Render::Shader::setUniformInt(const std::string &variableName, int32_t data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniform1i(location, data);
}

void WorldGenerator::Render::Shader::setUniformBool(const std::string &variableName, bool data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniform1i(location, (int32_t) data);
}

void WorldGenerator::Render::Shader::setUniformFloat(const std::string &variableName, float data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniform1f(location, data);
}

void WorldGenerator::Render::Shader::setUniformVec3(const std::string &variableName, glm::vec3 data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniform3fv(location, 1, &data[0]);
}

void WorldGenerator::Render::Shader::setUniformVec4(const std::string &variableName, glm::vec4 data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniform4fv(location, 1, &data[0]);
}

void WorldGenerator::Render::Shader::setUniformMat4(const std::string &variableName, glm::mat4 data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniformMatrix4fv(location, 1, GL_FALSE, &data[0][0]);
}

void WorldGenerator::Render::Shader::setUniformVec2(const std::string &variableName, glm::vec2 data) const {
    auto location = glGetUniformLocation(m_pShaderProgramId, variableName.c_str());
    glUniform2fv(location, 1, &data[0]);
}

WorldGenerator::Render::Shader::~Shader() {
    glDeleteProgram(m_pShaderProgramId);
}

WorldGenerator::Render::Shader *WorldGenerator::Render::ShaderBuilder::finish() {
    if (!Global::s_pGladInitialized) {
        throw WorldGenerator::Exceptions::WGException("Glad was not initialized before shader creation",
                                                      "Shader creation");
    }

    int32_t shaderCompilationResult;
    char shaderCompilationResultInfo[SHADER_INFO_LENGHT];

    //Check if all files exist
    if (!m_pVertexCodeSpecified && !WorldGenerator::Util::UtilFile::FileExists(m_pVertexShaderPath)) {
        throw WorldGenerator::Exceptions::WGException(
                std::string("Vertex shader file not found : " + m_pVertexShaderPath),
                "Shader creation");
    }
    if (!m_pFragmentCodeSpecified && !WorldGenerator::Util::UtilFile::FileExists(m_pFragmentShaderPath)) {
        throw WorldGenerator::Exceptions::WGException(
                std::string("Fragment shader file not found : " + m_pFragmentShaderPath), "Shader creation");
    }
    if (m_pHasGeometryShader) {
        if (!WorldGenerator::Util::UtilFile::FileExists(m_pGeometryShaderPath)) {
            throw WorldGenerator::Exceptions::WGException(
                    std::string("Geometry shader file not found : " + m_pGeometryShaderPath), "Shader creation");
        }
    }
    if (m_pHasTesselationShader) {
        if (!WorldGenerator::Util::UtilFile::FileExists(m_pTesselationShaderPath)) {
            throw WorldGenerator::Exceptions::WGException(
                    std::string("Tesselation shader file not found : " + m_pTesselationShaderPath),
                    "Shader creation");
        }
    }

    m_pProgramId = glCreateProgram();

    // Try to load shader

    int32_t vertexShader = glCreateShader(GL_VERTEX_SHADER);

    std::string vertexShaderCode;
    if (m_pVertexCodeSpecified) {
        vertexShaderCode = m_pVertexShaderPath;
    } else {
        std::ifstream vertexShaderFile;
        vertexShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        try {
            vertexShaderFile.open(m_pVertexShaderPath);
            std::stringstream vertexStream;
            vertexStream << vertexShaderFile.rdbuf();
            vertexShaderFile.close();
            vertexShaderCode = vertexStream.str();
        }
        catch (std::ifstream::failure &e) {
            glDeleteShader(vertexShader);
            glDeleteProgram(m_pProgramId);
            throw WorldGenerator::Exceptions::WGException("Error while reading vertex shader file", "Shader creation");
        }
    }
    auto charVertexShaderCode = vertexShaderCode.c_str();
    glShaderSource(vertexShader, 1, &charVertexShaderCode, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &shaderCompilationResult);
    if (!shaderCompilationResult) {
        glGetShaderInfoLog(vertexShader, SHADER_INFO_LENGHT, NULL, shaderCompilationResultInfo);
        glDeleteShader(vertexShader);
        glDeleteProgram(m_pProgramId);
        throw WorldGenerator::Exceptions::WGException(std::string(
                "Error while compiling vertex shader code: \n" + std::string(shaderCompilationResultInfo)),
                                                      "Shader creation");
    }
    glAttachShader(m_pProgramId, vertexShader);

    int32_t fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    std::string fragmentShaderCode;
    if (m_pFragmentCodeSpecified) {
        fragmentShaderCode = m_pFragmentShaderPath;
    } else {
        std::ifstream fragmentShaderFile;
        fragmentShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        try {
            fragmentShaderFile.open(m_pFragmentShaderPath);
            std::stringstream fragmentStream;
            fragmentStream << fragmentShaderFile.rdbuf();
            fragmentShaderFile.close();
            fragmentShaderCode = fragmentStream.str();
        }
        catch (std::ifstream::failure &e) {
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);
            glDeleteProgram(m_pProgramId);
            throw WorldGenerator::Exceptions::WGException("Error while reading fragment shader file",
                                                          "Shader creation");
        }
    }
    auto charFragmentShaderCode = fragmentShaderCode.c_str();
    glShaderSource(fragmentShader, 1, &charFragmentShaderCode, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &shaderCompilationResult);
    if (!shaderCompilationResult) {
        glGetShaderInfoLog(fragmentShader, SHADER_INFO_LENGHT, NULL, shaderCompilationResultInfo);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        glDeleteProgram(m_pProgramId);
        throw WorldGenerator::Exceptions::WGException(std::string(
                "Error while compiling fragment shader code: \n" + std::string(shaderCompilationResultInfo)),
                                                      "Shader creation");
    }
    glAttachShader(m_pProgramId, fragmentShader);

    // Geometry shader
    int32_t geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
    if (m_pHasGeometryShader) {
        std::string geometryShaderCode;
        std::ifstream geometryShaderFile;
        geometryShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        try {
            geometryShaderFile.open(m_pGeometryShaderPath);
            std::stringstream geometryStream;
            geometryStream << geometryShaderFile.rdbuf();
            geometryShaderFile.close();
            geometryShaderCode = geometryStream.str();
        }
        catch (std::ifstream::failure &e) {
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);
            glDeleteShader(geometryShader);
            glDeleteProgram(m_pProgramId);
            throw WorldGenerator::Exceptions::WGException("Error while reading geometry shader file",
                                                          "Shader creation");
        }
        auto charGeometryShaderCode = geometryShaderCode.c_str();
        glShaderSource(geometryShader, 1, &charGeometryShaderCode, NULL);
        glCompileShader(geometryShader);
        glGetShaderiv(geometryShader, GL_COMPILE_STATUS, &shaderCompilationResult);
        if (!shaderCompilationResult) {
            glGetShaderInfoLog(geometryShader, SHADER_INFO_LENGHT, NULL, shaderCompilationResultInfo);
            glDeleteShader(fragmentShader);
            glDeleteShader(vertexShader);
            glDeleteShader(geometryShader);
            glDeleteProgram(m_pProgramId);
            throw WorldGenerator::Exceptions::WGException(std::string(
                    "Error while compiling geometry shader code: \n" + std::string(shaderCompilationResultInfo)),
                                                          "Shader creation");
        }
        glAttachShader(m_pProgramId, geometryShader);
        glDeleteShader(geometryShader);
    }

    // TODO Tesselation code

    glLinkProgram(m_pProgramId);
    glGetProgramiv(m_pProgramId, GL_LINK_STATUS, &shaderCompilationResult);
    if (!shaderCompilationResult) {
        glGetProgramInfoLog(m_pProgramId, SHADER_INFO_LENGHT, NULL, shaderCompilationResultInfo);
        glDeleteShader(fragmentShader);
        glDeleteShader(vertexShader);
        glDeleteShader(geometryShader);
        glDeleteProgram(m_pProgramId);
        //glDeleteShader(tesselationShader);
        throw WorldGenerator::Exceptions::WGException(
                std::string("Error while compiling shader program : " + std::string(shaderCompilationResultInfo)),
                "Shader Creation");
    }

    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(geometryShader);
    //glDeleteShader(tesselationShader);

    auto result = new Shader();
    result->m_pShaderProgramId = m_pProgramId;

    std::string log0 = std::string("Loaded shader: vert: ") + m_pVertexShaderPath;
    std::string log1 = std::string("Loaded shader: frag: ") + m_pFragmentShaderPath;

    WG_LOG_INFO(log0.c_str());
    WG_LOG_INFO(log1.c_str());

    return result;
}