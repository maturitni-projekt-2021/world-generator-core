//
// Created by Matty on 11/1/2020.
//

#include "../world-gen/render/culling/CullColliderPoint.h"

WorldGenerator::Render::Culling::CullColliderPoint::CullColliderPoint(const glm::vec3 &position) : CullCollider(
        position) {
}

bool WorldGenerator::Render::Culling::CullColliderPoint::IsVisible(
        const WorldGenerator::Render::Culling::FrustumSide &topSide,
        const WorldGenerator::Render::Culling::FrustumSide &bottomSide,
        const WorldGenerator::Render::Culling::FrustumSide &leftSide,
        const WorldGenerator::Render::Culling::FrustumSide &rightSide,
        const WorldGenerator::Render::Culling::FrustumSide &nearSide,
        const WorldGenerator::Render::Culling::FrustumSide &farSide) const {
    WorldGenerator::Render::Culling::FrustumSide sides[] = {topSide,bottomSide,leftSide,rightSide,nearSide,farSide};
/*
    for(auto & side : sides) {
        if (!side.InFront(m_pPosition))
            return false;
    }
*/

    if(topSide.InFront(m_pPosition)) return true;

    return false;
}
