//
// Created by Matty on 6.9.2020.
//

#include <random>
#include "../world-gen/world_generator/ChunkGenerator.h"
#include "../world-gen/data/WorldData.h"
#include "../world-gen/render/custom/TerrainVertex.h"
#include "../world-gen/render/RenderTask.h"
#include "../world-gen/world_generator/CubeMarcher.h"
#include "../world-gen/render/Renderer.h"
#include "../world-gen/world_generator/lib/FastNoise.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/data/PerformanceData.h"
#include "../world-gen/data/SettingsData.h"

void WorldGenerator::TerrainGeneration::ChunkGenerator::Join() {
    m_pThread.join();
}

void WorldGenerator::TerrainGeneration::ChunkGenerator::_Stop() {
    m_pToIncreaseOnEnd++;
}

void WorldGenerator::TerrainGeneration::ChunkGenerator::_Init() {

}

void WorldGenerator::TerrainGeneration::ChunkGenerator::_Run() {
    _Init();

    while (!m_pStop) {
        std::unique_lock<std::mutex> lock = std::unique_lock<std::mutex>(m_pMutex);

        if (m_pStop) {
            break;
        }

        if (!m_pJobSlot.m_Active) {
            m_pCV.wait(lock);
        }

        if (m_pStop) {
            break;
        }

        if (m_pJobSlot.m_Active) {
            WorldGenerator::Data::PerformanceData::m_PerformanceData->m_WorkingChunkGenerators++;
            _GenerateChunk(m_pJobSlot.m_Pos, m_pJobSlot.m_Size, m_pJobSlot.m_Resolution, m_pJobSlot.m_Slot,
                           m_pJobSlot.m_Drop, m_pJobSlot.m_Version);
            WorldGenerator::Data::PerformanceData::m_PerformanceData->m_WorkingChunkGenerators--;
            m_pJobSlot.m_Active = false;
        }
    }

    _Stop();
}

WorldGenerator::TerrainGeneration::ChunkGenerator::ChunkGenerator(std::condition_variable &cv,
                                                                  std::atomic_int &toIncreaseOnEnd,
                                                                  bool &stop, ChunkGenerationJobStatus &jobSlot)
        : m_pCV(cv), m_pToIncreaseOnEnd(toIncreaseOnEnd), m_pStop(stop), m_pJobSlot(jobSlot) {
    m_pThread = std::thread([this]() { this->_Run(); });
    WorldGenerator::Util::UtilThread::SetThreadPrio(m_pThread, Util::WG_THREAD_PRIORITY_HIGHEST);
}

void
WorldGenerator::TerrainGeneration::ChunkGenerator::_GenerateChunk(
        const World::WorldChunkPos &chunkPos,
        const glm::vec3 &chunkSize,
        uint32_t chunkRes,
        uint32_t chunkSlot,
        bool &drop,
        uint32_t version) {
    auto &world = WorldGenerator::Data::WorldData::m_World;
    std::vector<std::vector<std::vector<Voxel>>> voxels;
    std::vector<float> terrainVertices;
    terrainVertices.reserve(chunkRes * chunkRes * 6 * 13);
    uint32_t elCount = 0;

    auto GetPointPos = [](const glm::vec3 &pos, uint32_t chunkRes) {
        return pos / (float) chunkRes;
    };

    auto GetVoxelColor1 = [](const Voxel &voxel, const glm::vec3 &pos) {
        glm::vec3 col1;
        try {
            col1 = WorldGenerator::Data::ProjectData::m_ProjectData->m_WorldGenDataActive->m_Biomes.at(voxel.m_Biome1)->GetColorFromHeight(
                    pos.y);
        }
        catch (const std::exception &e) {
            col1 = glm::vec3(0.0f);
        }

        return col1;
    };

    auto GetVoxelColor2 = [](const Voxel &voxel, const glm::vec3 &pos) {
        glm::vec3 col1;
        try {
            col1 = WorldGenerator::Data::ProjectData::m_ProjectData->m_WorldGenDataActive->m_Biomes.at(voxel.m_Biome2)->GetColorFromHeight(
                    pos.y);
        }
        catch (const std::exception &e) {
            col1 = glm::vec3(0.0f);
        }

        return col1;
    };

    auto wobjects = std::vector<std::shared_ptr<World::WorldObject>>();

    bool filled = true;
    bool filledBy = false;

    voxels.reserve(chunkRes + 1);

    switch (WorldGenerator::Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value) {
        case 0: { //CPU
            for (uint32_t x = 0; x < chunkRes + 1; x++) {
                voxels.emplace_back();
                voxels.at(x).reserve(chunkRes + 1); //Create new object
                for (uint32_t y = 0; y < chunkRes + 1; y++) {
                    voxels.at(x).emplace_back();
                    voxels.at(x).at(y).reserve(chunkRes + 1);
                    for (uint32_t z = 0; z < chunkRes + 1; z++) {
                        auto actualPosition =
                                glm::vec3(x, y, z) / (float) chunkRes * Global::s_pChunkSize * Global::s_WorldScale +
                                glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) * Global::s_pChunkSize *
                                Global::s_WorldScale;
                        voxels.at(x).at(y).emplace_back(_GenerateVoxel(actualPosition, drop));
                        if (x == 0 && y == 0 && z == 0) filledBy = voxels.at(x).at(y).at(z).m_Value > 0.5f;
                        else {
                            if (filledBy != voxels.at(x).at(y).at(z).m_Value > 0.5f) {
                                filled = false;
                            }
                        }
                        //TODO: change cell generation to happen here

                        if (x > 0 && y > 0 && z > 0) {
                            auto cell = VoxelCell();

                            cell.p[0] = GetPointPos(glm::vec3(x-1, y, z-1), chunkRes);
                            cell.val[0] = voxels.at(x-1).at(y).at(z-1).m_Value;
                            cell.biome[0] = voxels.at(x-1).at(y).at(z-1).m_BiomeValue;
                            cell.col1[0] = GetVoxelColor1(voxels.at(x-1).at(y).at(z-1),
                                                          glm::vec3(x-1, y, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[0] = GetVoxelColor2(voxels.at(x-1).at(y).at(z-1),
                                                          glm::vec3(x-1, y, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[1] = GetPointPos(glm::vec3(x, y, z-1), chunkRes);
                            cell.val[1] = voxels.at(x).at(y).at(z-1).m_Value;
                            cell.biome[1] = voxels.at(x).at(y).at(z-1).m_BiomeValue;
                            cell.col1[1] = GetVoxelColor1(voxels.at(x).at(y).at(z-1),
                                                          glm::vec3(x, y, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[1] = GetVoxelColor2(voxels.at(x).at(y).at(z-1),
                                                          glm::vec3(x, y, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[2] = GetPointPos(glm::vec3(x, y-1, z-1), chunkRes);
                            cell.val[2] = voxels.at(x).at(y-1).at(z-1).m_Value;
                            cell.biome[2] = voxels.at(x).at(y-1).at(z-1).m_BiomeValue;
                            cell.col1[2] = GetVoxelColor1(voxels.at(x).at(y-1).at(z-1),
                                                          glm::vec3(x, y-1, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[2] = GetVoxelColor2(voxels.at(x).at(y-1).at(z-1),
                                                          glm::vec3(x, y-1, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[3] = GetPointPos(glm::vec3(x-1, y-1, z-1), chunkRes);
                            cell.val[3] = voxels.at(x-1).at(y-1).at(z-1).m_Value;
                            cell.biome[3] = voxels.at(x-1).at(y-1).at(z-1).m_BiomeValue;
                            cell.col1[3] = GetVoxelColor1(voxels.at(x-1).at(y-1).at(z-1),
                                                          glm::vec3(x-1, y-1, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[3] = GetVoxelColor2(voxels.at(x-1).at(y-1).at(z-1),
                                                          glm::vec3(x-1, y-1, z-1) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[4] = GetPointPos(glm::vec3(x-1, y, z), chunkRes);
                            cell.val[4] = voxels.at(x-1).at(y).at(z ).m_Value;
                            cell.biome[4] = voxels.at(x-1).at(y).at(z).m_BiomeValue;
                            cell.col1[4] = GetVoxelColor1(voxels.at(x-1).at(y).at(z),
                                                          glm::vec3(x-1, y, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[4] = GetVoxelColor2(voxels.at(x-1).at(y).at(z),
                                                          glm::vec3(x-1, y, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[5] = GetPointPos(glm::vec3(x , y , z), chunkRes);
                            cell.val[5] = voxels.at(x ).at(y ).at(z).m_Value;
                            cell.biome[5] = voxels.at(x ).at(y).at(z).m_BiomeValue;
                            cell.col1[5] = GetVoxelColor1(voxels.at(x).at(y).at(z),
                                                          glm::vec3(x, y, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[5] = GetVoxelColor2(voxels.at(x).at(y).at(z),
                                                          glm::vec3(x, y, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[6] = GetPointPos(glm::vec3(x, y-1, z), chunkRes);
                            cell.val[6] = voxels.at(x).at(y-1).at(z).m_Value;
                            cell.biome[6] = voxels.at(x).at(y-1).at(z).m_BiomeValue;
                            cell.col1[6] = GetVoxelColor1(voxels.at(x).at(y-1).at(z),
                                                          glm::vec3(x, y-1, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[6] = GetVoxelColor2(voxels.at(x).at(y-1).at(z),
                                                          glm::vec3(x, y-1, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            cell.p[7] = GetPointPos(glm::vec3(x-1, y-1, z), chunkRes);
                            cell.val[7] = voxels.at(x-1).at(y-1).at(z).m_Value;
                            cell.biome[7] = voxels.at(x-1).at(y-1).at(z).m_BiomeValue;
                            cell.col1[7] = GetVoxelColor1(voxels.at(x-1).at(y-1).at(z),
                                                          glm::vec3(x-1, y-1, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);
                            cell.col2[7] = GetVoxelColor2(voxels.at(x-1).at(y-1).at(z),
                                                          glm::vec3(x-1, y-1, z) / (float) chunkRes *
                                                          Global::s_pChunkSize +
                                                          glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                          Global::s_pChunkSize);

                            elCount += 3 * CubeMarcher::Polygonise(cell, 0.5f, terrainVertices);
                        }
                    }
                }
            }
            break;
        }
        case 1: { //CL
            //TODO: CL
            //While CL does not have free worker wait
            //If can take one, take it
            //Add task tp that worker
            //Wait for it to finish
            filled = true;
            break;
        }
        case 2: { //CUDA
            //TODO: Cuda
            break;
        }
        default: {
            throw Exceptions::WGException("WTF", "World gen");
        }
    }

    if (!filled)
        //WO GEN
        for (uint32_t x = 0; x < chunkRes + 1; x++) {
            for (uint32_t y = 1; y < chunkRes + 1; y++) {
                for (uint32_t z = 0; z < chunkRes + 1; z++) {
                    auto actualPosition =
                            glm::vec3(x, z, y) / (float) chunkRes * Global::s_pChunkSize *
                            Global::s_WorldScale +
                            glm::vec3(chunkPos.x, chunkPos.z, chunkPos.y) * Global::s_pChunkSize *
                            Global::s_WorldScale;
                    /*
                    auto noiser = FastNoise(WorldGenerator::Data::SettingsData::m_SettingsData->m_Seed->m_Value);
                    if (voxels.at(x).at(y).at(z).m_Value < 0.5f && voxels.at(x).at(y - 1).at(z).m_Value > 0.5f) {
                        if (noiser.GetWhiteNoise(actualPosition.x, actualPosition.y) > 0.90f) {
                            auto selected = WorldGenerator::Data::ProjectData::m_ProjectData->m_ModelData.at(
                                    "test_tree");
                            auto wo = std::make_shared<WorldGenerator::World::WorldObject>();
                            wo->SetModels(selected);
                            wo->SetPosition(glm::vec3(actualPosition.x, actualPosition.z, actualPosition.y));
                            wo->SetRotation(
                                    {0.0f,
                                     noiser.GetWhiteNoise(actualPosition.x * 10.0f, actualPosition.y * 10.0f) * 90,
                                     0.0f});
                            wo->SetScale(glm::vec3(
                                    (noiser.GetWhiteNoise(actualPosition.x * 10.0f, actualPosition.y * 10.0f) * 0.3f) +
                                    1.0f) * 1.2f);
                            wobjects.emplace_back(wo);

                        }
                    }
                    */
                }
            }
        }

    if (WorldGenerator::Data::SettingsData::m_SettingsData->m_TerrainGenerationEngine->m_Value != 0 && !filled)
        for (uint32_t x = 0; x < chunkRes; x++) {
            for (uint32_t y = 0; y < chunkRes; y++) {
                for (uint32_t z = 0; z < chunkRes; z++) {
                    auto cell = VoxelCell();

                    cell.p[0] = GetPointPos(glm::vec3(x, y + 1, z), chunkRes);
                    cell.val[0] = voxels.at(x).at(y + 1).at(z).m_Value;
                    cell.biome[0] = voxels.at(x).at(y + 1).at(z).m_BiomeValue;
                    cell.col1[0] = GetVoxelColor1(voxels.at(x).at(y + 1).at(z),
                                                  glm::vec3(x, y + 1, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[0] = GetVoxelColor2(voxels.at(x).at(y + 1).at(z),
                                                  glm::vec3(x, y + 1, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[1] = GetPointPos(glm::vec3(x + 1, y + 1, z), chunkRes);
                    cell.val[1] = voxels.at(x + 1).at(y + 1).at(z).m_Value;
                    cell.biome[1] = voxels.at(x + 1).at(y + 1).at(z).m_BiomeValue;
                    cell.col1[1] = GetVoxelColor1(voxels.at(x + 1).at(y + 1).at(z),
                                                  glm::vec3(x + 1, y + 1, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[1] = GetVoxelColor2(voxels.at(x + 1).at(y + 1).at(z),
                                                  glm::vec3(x + 1, y + 1, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[2] = GetPointPos(glm::vec3(x + 1, y, z), chunkRes);
                    cell.val[2] = voxels.at(x + 1).at(y).at(z).m_Value;
                    cell.biome[2] = voxels.at(x + 1).at(y).at(z).m_BiomeValue;
                    cell.col1[2] = GetVoxelColor1(voxels.at(x + 1).at(y).at(z),
                                                  glm::vec3(x + 1, y, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[2] = GetVoxelColor2(voxels.at(x + 1).at(y).at(z),
                                                  glm::vec3(x + 1, y, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[3] = GetPointPos(glm::vec3(x, y, z), chunkRes);
                    cell.val[3] = voxels.at(x).at(y).at(z).m_Value;
                    cell.biome[3] = voxels.at(x).at(y).at(z).m_BiomeValue;
                    cell.col1[3] = GetVoxelColor1(voxels.at(x).at(y).at(z),
                                                  glm::vec3(x, y, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[3] = GetVoxelColor2(voxels.at(x).at(y).at(z),
                                                  glm::vec3(x, y, z) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[4] = GetPointPos(glm::vec3(x, y + 1, z + 1), chunkRes);
                    cell.val[4] = voxels.at(x).at(y + 1).at(z + 1).m_Value;
                    cell.biome[4] = voxels.at(x).at(y + 1).at(z + 1).m_BiomeValue;
                    cell.col1[4] = GetVoxelColor1(voxels.at(x).at(y + 1).at(z + 1),
                                                  glm::vec3(x, y + 1, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[4] = GetVoxelColor2(voxels.at(x).at(y + 1).at(z + 1),
                                                  glm::vec3(x, y + 1, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[5] = GetPointPos(glm::vec3(x + 1, y + 1, z + 1), chunkRes);
                    cell.val[5] = voxels.at(x + 1).at(y + 1).at(z + 1).m_Value;
                    cell.biome[5] = voxels.at(x + 1).at(y + 1).at(z + 1).m_BiomeValue;
                    cell.col1[5] = GetVoxelColor1(voxels.at(x).at(y + 1).at(z),
                                                  glm::vec3(x + 1, y + 1, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[5] = GetVoxelColor2(voxels.at(x).at(y + 1).at(z),
                                                  glm::vec3(x + 1, y + 1, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[6] = GetPointPos(glm::vec3(x + 1, y, z + 1), chunkRes);
                    cell.val[6] = voxels.at(x + 1).at(y).at(z + 1).m_Value;
                    cell.biome[6] = voxels.at(x + 1).at(y).at(z + 1).m_BiomeValue;
                    cell.col1[6] = GetVoxelColor1(voxels.at(x + 1).at(y).at(z + 1),
                                                  glm::vec3(x + 1, y, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[6] = GetVoxelColor2(voxels.at(x + 1).at(y).at(z + 1),
                                                  glm::vec3(x + 1, y, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    cell.p[7] = GetPointPos(glm::vec3(x, y, z + 1), chunkRes);
                    cell.val[7] = voxels.at(x).at(y).at(z + 1).m_Value;
                    cell.biome[7] = voxels.at(x).at(y).at(z + 1).m_BiomeValue;
                    cell.col1[7] = GetVoxelColor1(voxels.at(x).at(y).at(z + 1),
                                                  glm::vec3(x, y, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);
                    cell.col2[7] = GetVoxelColor2(voxels.at(x).at(y).at(z + 1),
                                                  glm::vec3(x, y, z + 1) / (float) chunkRes *
                                                  Global::s_pChunkSize +
                                                  glm::vec3(chunkPos.x, chunkPos.y, chunkPos.z) *
                                                  Global::s_pChunkSize);

                    elCount += 3 * CubeMarcher::Polygonise(cell, 0.5f, terrainVertices);
                }
            }
        }

    auto renderTask = std::make_shared<WorldGenerator::Render::RenderTaskUploadTerrainData>(chunkPos, terrainVertices,
                                                                                            elCount);

    Render::Renderer::m_Renderer->AddRenderTask(renderTask);

    while (!renderTask->GetDone()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(16));
    }

    if (auto res = renderTask->GetResult()) {
        WorldGenerator::Data::WorldData::m_World->AddTerrain(chunkPos, res, chunkSlot, wobjects, chunkRes, version);
        if (filled) WorldGenerator::Data::PerformanceData::m_PerformanceData->m_GeneratedFilledChunks++;
    }
}

WorldGenerator::TerrainGeneration::Voxel
WorldGenerator::TerrainGeneration::ChunkGenerator::_GenerateVoxel(const glm::vec3 &worldPosition, bool &drop) {
    using namespace WorldGenerator::Data;
    float noise = 0.0f;
    float noiseNext = 0.0f;
    bool multiplyNext = false;
    Data::Mask2DOperation noiseMaskOperation = Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE;
    for (size_t i = 0;
         i < ProjectData::m_ProjectData->m_WorldGenDataActive->m_NoiseLayers.size(); i++) {
        if (drop) break;
        auto &layer = ProjectData::m_ProjectData->m_WorldGenDataActive->m_NoiseLayers.at(
                ProjectData::m_ProjectData->m_WorldGenDataActive->m_NoiseLayers.size() - 1 - i);

        if(!layer->IsEnabled()) continue;

        switch (layer->GetData()->GetAddition()) {
            case Data::WG_NOISE_ADDITIVE:
                noise += layer->GetData()->GetValue(worldPosition, false, multiplyNext ? noiseNext : 0.0f,
                                                    multiplyNext ? noiseMaskOperation
                                                                 : Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE);
                multiplyNext = false;
                break;
            case Data::WG_NOISE_SUBTRACT:
                noise -= layer->GetData()->GetValue(worldPosition, false, multiplyNext ? noiseNext : 0.0f,
                                                    multiplyNext ? noiseMaskOperation
                                                                 : Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE);
                multiplyNext = false;
                break;
            case Data::WG_NOISE_MULTIPLY:
                noise *= layer->GetData()->GetValue(worldPosition, false, multiplyNext ? noiseNext : 0.0f,
                                                    multiplyNext ? noiseMaskOperation
                                                                 : Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE);
                multiplyNext = false;
                break;
            case Data::WG_NOISE_ADDITIVE_2D:
                noiseNext = layer->GetData()->GetValue(worldPosition, true, multiplyNext ? noiseNext : 0.0f,
                                                       multiplyNext ? noiseMaskOperation
                                                                    : Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE);
                noiseMaskOperation = Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE;
                multiplyNext = true;
                break;
            case Data::WG_NOISE_SUBTRACT_2D:
                noiseNext = layer->GetData()->GetValue(worldPosition, true, multiplyNext ? noiseNext : 0.0f,
                                                       multiplyNext ? noiseMaskOperation
                                                                    : Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE);
                noiseMaskOperation = Data::Mask2DOperation::WG_NOISE_MASK_2D_SUBTRACT;
                multiplyNext = true;
                break;
            case Data::WG_NOISE_MULTIPLY_2D:
                noiseNext = layer->GetData()->GetValue(worldPosition, true, multiplyNext ? noiseNext : 0.0f,
                                                       multiplyNext ? noiseMaskOperation
                                                                    : Data::Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE);
                noiseMaskOperation = Data::Mask2DOperation::WG_NOISE_MASK_2D_MULTIPLY;
                multiplyNext = true;
                break;
        }

        if (layer->GetData()->GetClampAfter()) {
            noise = std::clamp(noise, 0.0f, 1.0f);
            noiseNext = std::clamp(noiseNext, 0.0f, 1.0f);
        }

        if (layer->GetData()->GetApplyBias()) {
            noise = _BiasFunc(noise, -layer->GetData()->GetBiasValue());
        }
    }

    auto moisture = WorldGenerator::Data::WorldData::m_Noiser.GetPerlin(worldPosition.x,worldPosition.y);
    auto tempeture = WorldGenerator::Data::WorldData::m_Noiser.GetPerlin(worldPosition.x-10000,worldPosition.y+10000);
    auto height = WorldGenerator::Data::WorldData::m_Noiser.GetPerlin(worldPosition.x,worldPosition.y);

    auto biomeData = _GetBiomeData(moisture,tempeture, height,worldPosition);

    return Voxel(noise > 0 ? noise : 0.0f, std::get<0>(biomeData), std::get<1>(biomeData), std::get<2>(biomeData));
}

float WorldGenerator::TerrainGeneration::ChunkGenerator::_BiasFunc(float val, float bias) {
    float k = std::pow(1.0f - bias, 3);
    return (val * k) / (val * k - val + 1);
}

std::tuple<uint32_t,uint32_t,float>
WorldGenerator::TerrainGeneration::ChunkGenerator::_GetBiomeData(float moisture, float tempeture, float height,const glm::vec3 & pos) {
    auto & biomes = WorldGenerator::Data::ProjectData::m_ProjectData->m_WorldGenDataActive->m_Biomes;

    uint32_t b1;
    uint32_t b2;
    float ratio;

    std::vector<std::pair<uint32_t,std::shared_ptr<World::Biome>>> candidates;
    candidates.reserve(16);

    std::vector<std::pair<uint32_t,float>> values;
    values.reserve(2);

    for(auto & b : biomes) {
        if(b.second->IsLegitimate(moisture,tempeture,height)) {
            candidates.emplace_back(b);
        }
        if(candidates.size() > 1) {
            break;
        }
    }

    for(auto & b : candidates) {
        values.emplace_back(b.first,0.5f);
    }

    if(values.empty()) {
        return std::tuple<uint32_t,uint32_t,float>(0,0,1.0f);
    }

    b1 = values.at(0).first;
    b2 = values.at(1).first;
    ratio = (1.0f / (values.at(0).second + values.at(1).second)) * (values.at(0).second + values.at(1).second);

    return std::tuple<uint32_t,uint32_t,float>(b1,b2,ratio);
}
