//
// Created by Matty on 10/29/2020.
//

#include "../world-gen/ui/UI_ProjectSelect.h"
#include "../world-gen/App.h"
#include "../world-gen/data/ProjectData.h"

void WorldGenerator::UI::UI_ProjectSelect::Render(const std::shared_ptr<ImGUI_Core> &data) {
    UI::Render(data);
//region Project Filebrowser
    if (m_pShowProjectFB) {
        m_pProjectFileBrowser.Display();
    }

    if (!m_pProjectFileBrowser.IsOpened() && m_pShowProjectFB) {
        if (m_pProjectFileBrowser.HasSelected()) {
            std::string path = m_pProjectFileBrowser.GetSelected().string();
            if (path.size() <= 512) {
                for(auto & c : s_pProjectPath) {
                    c = ' ';
                }
                for (size_t i = 0; i < path.size(); i++) {
                    s_pProjectPath[i] = path.at(i);
                }
                m_pProjectPathStl = path;
                m_pSaveSelected = true;
            }
        }
        m_pShowProjectFB = false;
        m_pNewProjectOpen = false;
        m_pProjectFileBrowser.ClearSelected();
        m_pProjectFileBrowser.Close();
    }
//endregion

//region Filebrowser
    if (m_pShowFB) {
        m_pFileBrowser.Display();
    }

    if (!m_pFileBrowser.IsOpened() && m_pShowFB) {
        if (m_pFileBrowser.HasSelected()) {
            std::string path = m_pFileBrowser.GetSelected().string();
            try {
                Data::ProjectData::LoadProjectData(path);
                m_pProjectLoaded = true;
            } catch (std::exception &e) {
                m_pShowFail = true;
            }
        }
        m_pShowFB = false;
        m_pMainOpen = false;
        m_pFileBrowser.ClearSelected();
        m_pFileBrowser.Close();
    }
//endregion
    if (!m_pShowFB && !m_pShowNewProject && !m_pShowProjectFB && !m_pMainOpen) {
        ImGui::OpenPopup("SelectProject");
        m_pMainOpen = true;
        m_pNewProjectOpen = false;
        ImGui::SetNextWindowSize({435.0f, 290.0f}, ImGuiCond_FirstUseEver);
    }
//region Main popup
    if (ImGui::BeginPopupModal("SelectProject", nullptr, ImGuiWindowFlags_NoDecoration)) {
        ImGui::Text("No project file loaded.");

        if (m_pShowFail)
            ImGui::TextColored({0.8f, 0.0f, 0.0f, 1.0f}, "Failed to load project!");

        ImGui::BeginChild("offsetS", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing() - 18.0f));
        ImGui::EndChild();

        if (ImGui::Button("Close", {80.0f, 30.0f})) {
            App::Exit();
        }

        ImGui::SameLine(ImGui::GetWindowContentRegionWidth() - 75.0f);
        if (ImGui::Button("New", {80.0f, 30.0f})) {
            m_pShowNewProject = true;
        }

        ImGui::SameLine(ImGui::GetWindowContentRegionWidth() - 165.0f);
        if (ImGui::Button("Load", {80.0f, 30.0f})) {
            m_pFileBrowser.Open();
            m_pShowFB = true;
        }


        ImGui::EndPopup();
    }
//endregion

    if (m_pShowNewProject && !m_pNewProjectOpen) {
        ImGui::OpenPopup("NewProject");
        m_pNewProjectOpen = true;
        m_pMainOpen = false;
        ImGui::SetNextWindowSize({435.0f, 290.0f}, ImGuiCond_FirstUseEver);
    }
//region New project popup
    if (ImGui::BeginPopupModal("NewProject", nullptr, ImGuiWindowFlags_NoDecoration)) {
        ImGui::Text("New project name:");
        ImGui::InputText("##inputprojname", s_pProjectName, IM_ARRAYSIZE(s_pProjectName));

        if (m_pShowEmptyNameError)
            ImGui::TextColored({0.8f, 0.0f, 0.0f, 1.0f}, "Empty name is not valid project name!");

        ImGui::Text("New project path:");
        ImGui::InputText("##saveinput", s_pProjectPath, IM_ARRAYSIZE(s_pProjectPath), ImGuiInputTextFlags_ReadOnly);
        ImGui::SameLine(ImGui::CalcItemWidth() + 10.0f);
        if (ImGui::Button("Select", {80.0f, ImGui::GetTextLineHeightWithSpacing()})) {
            m_pShowProjectFB = true;
            m_pProjectFileBrowser.Open();
        }
        if (m_pShowSaveSelectedError)
            ImGui::TextColored({0.8f, 0.0f, 0.0f, 1.0f}, "No save file selected!");

        ImGui::BeginChild("offsetS", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing() - 18.0f));
        ImGui::EndChild();

        if (ImGui::Button("Cancel", {80.0f, 30.0f})) {
            m_pShowNewProject = false;
        }

        ImGui::SameLine(ImGui::GetWindowContentRegionWidth() - 75.0f);
        if (ImGui::Button("Create", {80.0f, 30.0f})) {
            if (strlen(s_pProjectName) < 3) {
                m_pShowEmptyNameError = true;
            } else {
                m_pShowEmptyNameError = false;
            }
            if (!m_pSaveSelected) {
                m_pShowSaveSelectedError = true;
            } else {
                m_pShowSaveSelectedError = false;
            }

            if (!m_pShowEmptyNameError && !m_pShowSaveSelectedError) {
                m_pProjectLoaded = true;
                Data::ProjectData::NewProjectData(s_pProjectName, m_pProjectPathStl);
            }
        }
        ImGui::EndPopup();
    }
//endregion
    PostRender(data);
}

WorldGenerator::UI::UI_ProjectSelect::UI_ProjectSelect() {
    m_pFileBrowser.SetTypeFilters({".wgenp"});
    m_pProjectFileBrowser.SetTypeFilters({".wgenp"});
}

bool WorldGenerator::UI::UI_ProjectSelect::IsProjectLoaded() const {
    return m_pProjectLoaded;
}

void WorldGenerator::UI::UI_ProjectSelect::Flush() {
    for(auto & c : s_pProjectPath) {
        c = ' ';
    }
    for(auto & c : s_pProjectName) {
        c = ' ';
    }
    m_pShowSaveSelectedError = false;
    m_pShowEmptyNameError = false;
    m_pSaveSelected = false;
    m_pProjectPathStl.clear();
}

char WorldGenerator::UI::UI_ProjectSelect::s_pProjectName[256];
char WorldGenerator::UI::UI_ProjectSelect::s_pProjectPath[512];
