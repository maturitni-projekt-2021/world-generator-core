//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/util/UtilMath.h"
/*
ImVec2 WorldGenerator::Util::operator*(ImVec2 a, float b)
{
    return ImVec2(a.x*b, a.y*b);
}
ImVec2 WorldGenerator::Util::operator*(ImVec2 a, const ImVec2& b)
{
    return ImVec2(a.x*b.x, a.y*b.y);
}

ImVec2 WorldGenerator::Util::operator+(ImVec2 a,float b)
{
    return ImVec2(a.x+b, a.y+b);
};

ImVec2 WorldGenerator::Util::operator+(ImVec2 a,const ImVec2& b)
{
    return ImVec2(a.x+b.x, a.y+b.y);
};

ImVec2 WorldGenerator::Util::operator-(ImVec2 a,float b)
{
    return ImVec2(a.x-b, a.y-b);
};

ImVec2 WorldGenerator::Util::operator-(const ImVec2& a,const ImVec2& b)
{
    return ImVec2(a.x-b.x, a.y-b.y);
};
*/
float WorldGenerator::Util::UtilMath::Dist2(const glm::vec3 &a, const glm::vec3 &b) {
    return (std::pow(b.x - a.x,2.0f) + std::pow(b.y - a.y,2.0f) + std::pow(b.z - a.z,2.0f));
}
