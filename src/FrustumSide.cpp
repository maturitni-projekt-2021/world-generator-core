//
// Created by Matty on 11/1/2020.
//

#include "../world-gen/render/culling/FrustumSide.h"

void WorldGenerator::Render::Culling::FrustumSide::SetNormalAndPoint(const glm::vec3 &normal, const glm::vec3 &pos) {
    m_pNormal = normal;
    m_pPos = pos;
}

const glm::vec3 &WorldGenerator::Render::Culling::FrustumSide::GetNormal() const {
    return m_pNormal;
}

const glm::vec3 &WorldGenerator::Render::Culling::FrustumSide::GetPos() const {
    return m_pPos;
}

bool WorldGenerator::Render::Culling::FrustumSide::InFront(const glm::vec3 &point) const {
    auto diff = point - m_pPos;
    auto normal = m_pNormal;
    return glm::dot(diff,normal) < 0;
}

WorldGenerator::Render::Culling::FrustumSide
WorldGenerator::Render::Culling::FrustumSide::operator=(const WorldGenerator::Render::Culling::FrustumSide &side) {
    auto a = WorldGenerator::Render::Culling::FrustumSide();
    a.SetNormalAndPoint(side.GetNormal(),side.GetPos());
    return a;
}
