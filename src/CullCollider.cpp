//
// Created by Matty on 11/1/2020.
//

#include "../world-gen/render/culling/CullCollider.h"

WorldGenerator::Render::Culling::CullCollider::CullCollider(const glm::vec3 &mPPosition) : m_pPosition(mPPosition) {}

void WorldGenerator::Render::Culling::CullCollider::SetPosition(const glm::vec3 &pos) {
    m_pPosition = pos;
}

const glm::vec3 &WorldGenerator::Render::Culling::CullCollider::GetPosition() const {
    return m_pPosition;
}
