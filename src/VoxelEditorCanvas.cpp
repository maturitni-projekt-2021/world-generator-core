//
// Created by Matty on 6.9.2020.
//

#include "../world-gen/node_editor/canvases/VoxelEditorCanvas.h"
#include "../world-gen/data/EditorIds.h"
#include "../world-gen/node_editor/nodes/VoxelNode.h"

WorldGenerator::NodeEditor::Canvases::VoxelEditorCanvas::VoxelEditorCanvas() : EditorCanvas()
{
    m_pName = "VoxelEditor";
    _Init();
    auto mainNodeID = WorldGenerator::Data::EditorIds::m_IDs->GetID();
    m_pNodes[mainNodeID] = std::make_shared<WorldGenerator::NodeEditor::Nodes::VoxelNode>(mainNodeID,m_pPins);
}
