//
// Created by Matty on 30.8.2020.
//

#include "../world-gen/util/UtilTick.h"
#include <GLFW/glfw3.h>

#ifdef MINGW_WINDOWS_THREAD_LIB
#include <mingw_threads/mingw.thread.h>
#else
#include <thread>
#endif

void WorldGenerator::Util::UtilTick::Init()
{
    m_FrameFrequency = 1.0/TICKS_PER_SECOND;
    m_StartFrameTime = 0.0;
    m_EndFrameTime = 0.0;
    m_ElapsedTimeSinceFrame = 0.0f;
}
void WorldGenerator::Util::UtilTick::FrameStart()
{
    m_StartFrameTime = glfwGetTime();
}
void WorldGenerator::Util::UtilTick::FrameEnd()
{
    m_EndFrameTime = glfwGetTime();
    const auto frameTime = m_EndFrameTime-m_StartFrameTime; //Frame duration

    if (frameTime<1.0/FRAMES_PER_SECOND) {
        uint32_t sleepDuration = ((1.0/FRAMES_PER_SECOND)-frameTime)*1000;
        std::this_thread::sleep_for(std::chrono::milliseconds(sleepDuration));
    }

    const auto timeAfterSleep = glfwGetTime();

    const auto frameTimeAfterSleep = timeAfterSleep-m_StartFrameTime;
    m_ElapsedTimeSinceFrame += frameTimeAfterSleep;
}
bool WorldGenerator::Util::UtilTick::ShouldTick()
{
    bool result = m_ElapsedTimeSinceFrame>m_FrameFrequency;
    if (result) {
        m_ElapsedTimeSinceFrame -= m_FrameFrequency;
    }
    return result;
}

float WorldGenerator::Util::UtilTick::GetDelta() {
    return m_ElapsedTimeSinceFrame/m_FrameFrequency;
}

double WorldGenerator::Util::UtilTick::m_FrameFrequency;
double WorldGenerator::Util::UtilTick::m_StartFrameTime;
double WorldGenerator::Util::UtilTick::m_EndFrameTime;
double WorldGenerator::Util::UtilTick::m_ElapsedTimeSinceFrame;