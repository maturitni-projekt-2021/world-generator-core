//
// Created by Matty on 3.9.2020.
//

#include "../world-gen/ui/custom/UI_Options.h"
#include "../world-gen/imgui/imgui.h"
#include "../world-gen/data/RenderData.h"
#include "../world-gen/data/ProjectData.h"
#include "../world-gen/data/SettingsData.h"

WorldGenerator::UI::Custom::UI_Options::UI_Options() : UIWindow("Options")
{

}
void WorldGenerator::UI::Custom::UI_Options::CustomDraw()
{
    using namespace WorldGenerator::Data;

    auto camPos = RenderData::m_RenderData->m_ViewportCamera->GetPosition();
    auto camRot = RenderData::m_RenderData->m_ViewportCamera->GetRotation();

    float camPosF[3] = {camPos.x,camPos.y,camPos.z};
    float camRotF[3] = {camRot.x,camRot.y,camRot.z};

    ImGui::Checkbox("Wireframe",&WorldGenerator::Data::RenderData::m_RenderData->m_ViewWireframe);
    ImGui::Separator();
    ImGui::Text("Camera Transform:");
    ImGui::InputFloat3("Position",camPosF);
    ImGui::InputFloat3("Rotation",camRotF);

    ImGui::Separator();
    ImGui::InputInt("Terrain seed",&WorldGenerator::Data::ProjectData::m_ProjectData->m_Seed);
    ImGui::Checkbox("Auto-Cam",&WorldGenerator::Data::SettingsData::m_SettingsData->m_MovingCam->m_Value);

    RenderData::m_RenderData->m_ViewportCamera->SetPosition({camPosF[0],camPosF[1],camPosF[2]});
    RenderData::m_RenderData->m_ViewportCamera->SetRotation({camRotF[0],camRotF[1],camRotF[2]});
    RenderData::m_RenderData->m_ViewportCamera->SetTargetFromRotation();
}
void WorldGenerator::UI::Custom::UI_Options::CustomWindowSetup()
{
    UIWindow::CustomWindowSetup();
}
