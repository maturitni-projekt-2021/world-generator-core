#include "world-gen/App.h"

int main()
{
    bool restart = true;

    while (restart) {
        try {
            auto app = WorldGenerator::App();
            restart = app.Start() == 1;
        }
        catch (std::exception& e) {
            restart = false;
            std::cout << e.what() << std::endl;
        }
        WorldGenerator::App::StopGLFW();
    }
    return 0;
}