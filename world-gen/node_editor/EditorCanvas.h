//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_EDITORCANVAS_H
#define WORLD_GENERATOR_CORE_EDITORCANVAS_H

#define IMGUI_DEFINE_MATH_OPERATORS
#include "Node.h"
#include "Connection.h"
#include "../imgui/imnodes.h"
#include "../imgui/imgui.h"

#include <map>
#include <memory>

namespace WorldGenerator {
    namespace NodeEditor {
        /// Base class for node editor
        class EditorCanvas {
        private:
            /// If its first frame in tick
            bool m_pFirstFrame = true;
            /// If we want to delete selection
            bool m_pDeleteSelection = false;
            /// Canvas start point, used for where we should be in editor on start
            ImVec2 m_pCanvasStartPoint;
        protected:
            /// Initializes editor
            void _Init();
            /// Name of editor
            std::string m_pName = "EditorCanvas";
            /// Editor internal library context
            imnodes::EditorContext* m_pContext;
            /// Map of all nodes
            std::map<uint32_t,std::shared_ptr<Node>> m_pNodes;
            /// Map of all connections
            std::map<uint32_t,std::shared_ptr<Connection>> m_pConnections;
            /// Map of all pins
            std::map<uint32_t,std::shared_ptr<Pin>> m_pPins;
            /// Map of input pins to pair of connected pin and connection id
            std::map<uint32_t, std::pair<uint32_t,uint32_t>> m_pInputConnectedPins;
            /// Map of output pins to pair of connected pin and connection id
             std::map<uint32_t, std::map<uint32_t,std::pair<uint32_t,uint32_t>>> m_pOutputConnectedPins;
        public:
            EditorCanvas();
            ~EditorCanvas();
            /// Draw node editor
            void Draw();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_EDITORCANVAS_H
