//
// Created by Matty on 8.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_NODECOLORS_H
#define WORLD_GENERATOR_CORE_NODECOLORS_H

#include "PinTypes.h"
#include "../imgui/imgui.h"

namespace WorldGenerator {
    namespace NodeEditor {
        /// Gets color for pin based on type
        /// \param type Type to get color to
        /// \return Color of specified type
        static inline int GetColorFromPinType(WorldGenerator::NodeEditor::PinType type) {
            switch (type) {
            case WorldGenerator::NodeEditor::PIN_FLOAT:
                return IM_COL32(52, 235, 122, 255);
            case WorldGenerator::NodeEditor::PIN_INT:
                return IM_COL32(13, 107, 222, 255);
            case WorldGenerator::NodeEditor::PIN_UNSIGNED_INT:
                return IM_COL32(0, 57, 128, 255);
            case WorldGenerator::NodeEditor::PIN_BOOL:
                return IM_COL32(222, 18, 69, 255);
            case WorldGenerator::NodeEditor::PIN_VEC2:
                return IM_COL32(230, 250, 7, 255);
            case WorldGenerator::NodeEditor::PIN_VEC3:
                return IM_COL32(193, 250, 7, 255);
            case WorldGenerator::NodeEditor::PIN_VEC4:
                return IM_COL32(141, 250, 7, 255);
            case WorldGenerator::NodeEditor::PIN_BIOME:
                return IM_COL32(252, 97, 0, 255);
            }
            return IM_COL32(120, 120, 120, 255);
        }
    }
}

#endif //WORLD_GENERATOR_CORE_NODECOLORS_H
