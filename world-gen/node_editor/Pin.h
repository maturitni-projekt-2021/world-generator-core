//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_PIN_H
#define WORLD_GENERATOR_CORE_PIN_H

#include "PinTypes.h"

#include <glm/glm.hpp>

namespace WorldGenerator {
    namespace NodeEditor {
        /// Basic pin class
        class Pin {
        protected:
            /// Pin type
            PinType m_pType;
            /// Color of node
            glm::vec4 m_pColor;
            /// Name of node
            std::string m_pName;
            /// Constructed only accessible by children
            /// \param id ID of node
            /// \param name Name of node
            /// \param isOutput If node is output
            Pin(uint32_t id, const std::string & name,bool isOutput);
            /// Is output in?
            bool m_pOutput;
            /// Special draw function that is called when pin is not connected, used for inputing value without connecting pin
            virtual void _NotConnectedDraw();
        private:
            /// Pin ID
            uint32_t m_pID;
        public:
            /// Gets id of this pin
            /// \return ID of pin
            uint32_t GetID() const { return m_pID; }
            /// Draw pin
            /// \param isConnected if pin is connected or no
            void Draw(bool isConnected);
            /// Gets pin text
            /// \return Pin text
            std::string_view GetText();
            /// Gets pin type
            /// \return Pin type
            PinType GetType() const { return m_pType;};
            /// Is this pin output pin?
            /// \return if this pin is output pin
            bool IsOutput() const { return m_pOutput; }
        };

        /// Pin of float data type
        class FloatPin final : public Pin {
        private:
            /// Value to which pin is connected
            float & m_pValue;
        public:
        protected:
            void _NotConnectedDraw() override;
        public:
            /// FLoat pin contructor
            /// \param id ID of pin
            /// \param name Name of pin
            /// \param isOutput If pin is output
            /// \param value Value where we should output / input to/from
            FloatPin(uint32_t id,const std::string & name,bool isOutput,float & value);
        };

        class Vec2Pin final : public Pin {
        private:
            glm::vec2 & m_pValue;
        public:
            ///
            /// \param id ID of pin
            /// \param name Name of pin
            /// \param isOutput If pin is output
            /// \param value Value where we should output / input to/from
            Vec2Pin(uint32_t id,const std::string & name,bool isOutput,glm::vec2 & value);
        };

        class Vec3Pin final : public Pin {
        private:
            glm::vec3 & m_pValue;
        public:
            ///
            /// \param id ID of pin
            /// \param name Name of pin
            /// \param isOutput If pin is output
            /// \param value Value where we should output / input to/from
            Vec3Pin(uint32_t id,const std::string & name,bool isOutput,glm::vec3 & value);
        };

        class Vec4Pin final : public Pin {
        private:
            glm::vec4 & m_pValue;
        public:
            ///
            /// \param id ID of pin
            /// \param name Name of pin
            /// \param isOutput If pin is output
            /// \param value Value where we should output / input to/from
            Vec4Pin(uint32_t id,const std::string & name,bool isOutput,glm::vec4 & value);
        };

        class BiomePin final : public Pin {
        public:
            ///
            /// \param id ID of pin
            /// \param name Name of pin
            /// \param isOutput If pin is output
            /// \param value Value where we should output / input to/from
            BiomePin(uint32_t id,const std::string & name,bool isOutput);
        };

        class BoolPin final : public Pin {
        public:
            ///
            /// \param id ID of pin
            /// \param name Name of pin
            /// \param isOutput If pin is output
            /// \param value Value where we should output / input to/from
            BoolPin(uint32_t id,const std::string & name,bool isOutput);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_PIN_H
