//
// Created by mattyvrba on 9/8/20.
//

#ifndef WORLD_GENERATOR_CORE_PINTYPES_H
#define WORLD_GENERATOR_CORE_PINTYPES_H

namespace WorldGenerator {
    namespace NodeEditor {
        /// Different pin types
        enum PinType {
          PIN_FLOAT,
          PIN_INT,
          PIN_UNSIGNED_INT,
          PIN_BOOL,
          PIN_VEC2,
          PIN_VEC3,
          PIN_VEC4,
          PIN_BIOME
        };
    }
}
#endif //WORLD_GENERATOR_CORE_PINTYPES_H
