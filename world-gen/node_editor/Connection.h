//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_CONNECTION_H
#define WORLD_GENERATOR_CORE_CONNECTION_H

namespace WorldGenerator {
    namespace NodeEditor {
        /// Connection class, holding data about connection between two nodes and two pins
        class Connection {
        private:
            /// ID of first connected pin
            uint32_t m_pFirst;
            /// ID of second connected pin
            uint32_t m_pSecond;
            /// Color of connection
            int32_t m_Color;
        public:
            /// Constructs connection
            /// \param first First pin
            /// \param second Second pin
            /// \param color Color of connection
            Connection(uint32_t first, uint32_t second,int32_t color);
            /// Gets indices of pins in this connection
            /// \return Indices of pins in this connection
            std::pair<uint32_t,uint32_t> GetIndices() const;
            /// If pin is connected to this connection
            bool HasPinConnection(uint32_t pinID) const;
            /// Gets connection color
            /// \return Connection color
            int32_t GetColor() const { return m_Color; }
        };
    }
}
#endif //WORLD_GENERATOR_CORE_CONNECTION_H
