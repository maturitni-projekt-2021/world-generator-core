//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_POPULATIONEDITORCANVAS_H
#define WORLD_GENERATOR_CORE_POPULATIONEDITORCANVAS_H

#include "../EditorCanvas.h"

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Canvases {
            /// Population editor canvas, used for creating graph about creating models in world
            class PopulationEditorCanvas final : public EditorCanvas {
            public:
                PopulationEditorCanvas();
            private:
            };
        }
    }
}
#endif //WORLD_GENERATOR_CORE_POPULATIONEDITORCANVAS_H
