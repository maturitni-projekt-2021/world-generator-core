//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_VOXELEDITORCANVAS_H
#define WORLD_GENERATOR_CORE_VOXELEDITORCANVAS_H

#include "../EditorCanvas.h"

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Canvases {
            /// Voxel editor canvas, used for creating graph about creating value for single voxel
            class VoxelEditorCanvas final : public EditorCanvas {
            public:
                VoxelEditorCanvas();
            };
        }
    }
}

#endif //WORLD_GENERATOR_CORE_VOXELEDITORCANVAS_H
