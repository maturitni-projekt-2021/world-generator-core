//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_NODE_H
#define WORLD_GENERATOR_CORE_NODE_H

#include "Pin.h"
#include "PinHolders.h"
#include "logic/WorldGenConfig.h"
#include <map>

namespace WorldGenerator {
    namespace NodeEditor {
        /// Basic parent node, from which all nodes inherit
        class Node {
        protected:
            /// Input pins
            std::vector<std::shared_ptr<Pin>> m_pInputPins;
            /// Output pins
            std::vector<std::shared_ptr<Pin>> m_pOutputPins;
            /// Special draw function for drawing extra stuff on node
            virtual void _DrawExtra() {};
            /// Node name
            std::string m_pName = "<node_name>";
        private:
            /// Node id
            uint32_t m_pID;
        public:
            /// Gets node id
            /// \return Node id
            uint32_t GetID() const { return m_pID; };
            /// Basic contructor for node
            /// \param id ID of this node
            /// \param pinStorage Map of all pins, to add new pins to it
            Node(uint32_t id,std::map<uint32_t,std::shared_ptr<Pin>> & pinStorage);
            /// Draws node
            /// \param pinData data about pins
            virtual void Draw(const PinHolders & pinData);
            /// Gets if node is deletable
            /// \return If pin is deletable
            virtual bool IsDeletable() const {return true;}
            /// Gets all pins ids in one vector
            /// \return Pin ids
            [[nodiscard]] std::vector<uint32_t> GetAllPins() const;
            /// Gets amount of outputs
            /// \return Count of outputs
            virtual uint32_t GetOutputCount() const = 0;
            /// Gets amount of inputs
            /// \return Count of inputs
            virtual uint32_t GetInputCount() const = 0;
            /// Adds operation to config
            /// \param config Config to which we add operation
            virtual void AddOperation(const std::shared_ptr<WorldGenConfig> & config) = 0;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_NODE_H
