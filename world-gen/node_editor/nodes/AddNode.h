//
// Created by mattyvrba on 9/9/20.
//

#ifndef WORLD_GENERATOR_CORE_ADDNODE_H
#define WORLD_GENERATOR_CORE_ADDNODE_H

#include "../Node.h"

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Nodes {
            class AddNodeFF final : public Node {
            private:
                float m_pValueIN_A = 1.0f;
            private:
                float m_pValueIN_B = 1.0f;
                float m_pValueOut = 1.0f;
            public:
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
                AddNodeFF(uint32_t id, std::map<uint32_t, std::shared_ptr<Pin>>& pinStorage);
            };

            class AddNodeFV2 final : public Node {
            private:
                float m_pValueIN_A = 1.0f;
                glm::vec2 m_pValueIN_B = {1.0f, 1.0f};
                glm::vec2 m_pValueOut = {1.0f, 1.0f};
            public:
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
                AddNodeFV2(uint32_t id, std::map<uint32_t, std::shared_ptr<Pin>>& pinStorage);
            };

            class AddNodeFV3 final : public Node {
            private:
                float m_pValueIN_A = 1.0f;
            private:
                glm::vec3 m_pValueIN_B = {1.0f, 1.0f, 1.0f};
                glm::vec3 m_pValueOut = {1.0f, 1.0f, 1.0f};
            public:
                AddNodeFV3(uint32_t id, std::map<uint32_t, std::shared_ptr<Pin>>& pinStorage);
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
            };

            class AddNodeFV4 final : public Node {
            private:
                float m_pValueIN_A = 1.0f;
                glm::vec4 m_pValueIN_B = {1.0f, 1.0f, 1.0f, 1.0f};
                glm::vec4 m_pValueOut = {1.0f, 1.0f, 1.0f, 1.0f};
            public:
                AddNodeFV4(uint32_t id, std::map<uint32_t, std::shared_ptr<Pin>>& pinStorage);
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
            };
        }
    }
}

#endif //WORLD_GENERATOR_CORE_ADDNODE_H
