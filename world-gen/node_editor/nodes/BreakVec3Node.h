//
// Created by mattyvrba on 9/9/20.
//

#ifndef WORLD_GENERATOR_CORE_BREAKVEC3NODE_H
#define WORLD_GENERATOR_CORE_BREAKVEC3NODE_H


#include "../Node.h"

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Nodes {
            class BreakVec3Node final : public Node {
            private:
                glm::vec3 m_pValueIN = {0, 0,0};
                float m_pValueOutX = 1.0f;
                float m_pValueOutY = 1.0f;
            public:
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
            private:
                float m_pValueOutZ = 1.0f;
            public:
                BreakVec3Node(uint32_t id, std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage);
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_BREAKVEC3NODE_H
