//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_VOXELNODE_H
#define WORLD_GENERATOR_CORE_VOXELNODE_H

#include "../Node.h"

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Nodes {
            class VoxelNode final : public Node {
            private:
                float m_pValue = 1.0f;
            public:
                explicit VoxelNode(uint32_t id,std::map<uint32_t,std::shared_ptr<Pin>> & pinStorage);
            protected:
                void _DrawExtra() override;

            public:
                bool IsDeletable() const override;
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
            };
        }
    }
}

#endif //WORLD_GENERATOR_CORE_VOXELNODE_H
