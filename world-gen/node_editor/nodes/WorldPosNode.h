//
// Created by mattyvrba on 9/9/20.
//

#ifndef WORLD_GENERATOR_CORE_WORLDPOSNODE_H
#define WORLD_GENERATOR_CORE_WORLDPOSNODE_H


#include "../Node.h"

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Nodes {
            class WorldPosNode final : public Node {
            private:
                glm::vec3 m_pValue = {0,0,0};
            public:
                WorldPosNode(uint32_t id, std::map<uint32_t, std::shared_ptr<Pin>> &pinStorage);
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_WORLDPOSNODE_H
