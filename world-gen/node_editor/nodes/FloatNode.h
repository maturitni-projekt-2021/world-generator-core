//
// Created by mattyvrba on 9/8/20.
//

#ifndef WORLD_GENERATOR_CORE_FLOATNODE_H
#define WORLD_GENERATOR_CORE_FLOATNODE_H

#include "../Node.h"

#include <map>

namespace WorldGenerator {
    namespace NodeEditor {
        namespace Nodes {
            class FloatNode : public Node {
            private:
                float m_pValue = 1.0f;
            public:
                explicit FloatNode(uint32_t id,std::map<uint32_t,std::shared_ptr<Pin>> & pinStorage);
            protected:
                void _DrawExtra() override;
            public:
                uint32_t GetOutputCount() const override;
                uint32_t GetInputCount() const override;
                void AddOperation(const std::shared_ptr<WorldGenConfig>& config) override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_FLOATNODE_H
