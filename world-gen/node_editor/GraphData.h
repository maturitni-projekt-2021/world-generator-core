//
// Created by Matty on 13.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_GRAPHDATA_H
#define WORLD_GENERATOR_CORE_GRAPHDATA_H

#include "Node.h"
#include "Connection.h"
#include "Pin.h"
#include "PinHolders.h"

#include <map>
#include <memory>

namespace WorldGenerator {
    namespace NodeEditor {
        /// All data about node graph
        struct GraphData {
            /// Data about pins separated to input and output pins
          PinHolders& m_PinData;
          /// Node data
          std::map<uint32_t, std::shared_ptr<Node>>& m_Nodes;
          /// Data about connections
          std::map<uint32_t, std::shared_ptr<Connection>>& m_Connections;
          /// Data about all pins
          std::map<uint32_t, std::shared_ptr<Pin>>& m_Pins;
          /// Constructor
          /// \param pinData Data about pins
          /// \param nodes Data about nodes
          /// \param connections Data about connections
          /// \param pins Data about all pins together
          GraphData(PinHolders& pinData, std::map<uint32_t, std::shared_ptr<Node>>& nodes,
                  std::map<uint32_t, std::shared_ptr<Connection>>& connections,
                  std::map<uint32_t, std::shared_ptr<Pin>>& pins)
                  :m_PinData(pinData), m_Nodes(nodes), m_Connections(connections), m_Pins(pins) { }
        };
    }
}
#endif //WORLD_GENERATOR_CORE_GRAPHDATA_H
