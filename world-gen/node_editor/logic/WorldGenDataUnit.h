//
// Created by mattyvrba on 9/8/20.
//

#ifndef WORLD_GENERATOR_CORE_WORLDGENDATAUNIT_H
#define WORLD_GENERATOR_CORE_WORLDGENDATAUNIT_H

#include <glm/glm.hpp>

namespace WorldGenerator {
    namespace NodeEditor {
        enum WGDataUnitType {
          DATA_FLOAT,
          DATA_UNSIGNED_INT,
          DATA_BOOL,
          DATA_INT,
          DATA_VEC2,
          DATA_VEC3,
          DATA_VEC4
        };

        struct WorldGenDataUnit {
          WGDataUnitType m_Type;
          union {
            float fValue;
            uint32_t uiValue;
            int32_t iValue;
            bool bValue;
            glm::vec4 v4Value;
            glm::vec3 v3Value;
            glm::vec2 v2Value;
          };
        };
    }
}

#endif //WORLD_GENERATOR_CORE_WORLDGENDATAUNIT_H
