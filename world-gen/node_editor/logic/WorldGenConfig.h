//
// Created by mattyvrba on 9/8/20.
//

#ifndef WORLD_GENERATOR_CORE_WORLDGENCONFIG_H
#define WORLD_GENERATOR_CORE_WORLDGENCONFIG_H

#define GLOBAL_WORLD_POS_INDEX 0

#include "WorldGenDataUnit.h"

#include <cstdint>
#include <vector>
#include <functional>
#include <map>

namespace WorldGenerator {
    namespace NodeEditor {
        class WorldGenConfig {
        public:
            std::vector<WorldGenDataUnit> m_GlobalVertexData;
            std::map<uint32_t, WorldGenDataUnit> m_Data;
            std::map<uint32_t, uint32_t> m_DataConnections;
            std::vector<std::function<void(void)>> m_Operations;
        };
    }
}

#endif //WORLD_GENERATOR_CORE_WORLDGENCONFIG_H
