//
// Created by Matty on 13.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_GRAPHPROCESSOR_H
#define WORLD_GENERATOR_CORE_GRAPHPROCESSOR_H

#include "../GraphData.h"
#include "WorldGenConfig.h"

namespace WorldGenerator {
    namespace NodeEditor {
        class GraphProcessor {
        private:
            std::shared_ptr<WorldGenConfig> m_pResult;

            void _CheckValidity     (GraphData& data);
            void _CheckRecursion    (GraphData& data);
            void _CountNodes        (GraphData& data);
            void _AssignsOutputs    (GraphData& data);
            void _AddOperations     (GraphData& data);
            void _SortOperations    (GraphData& data);
        public:
            void Flush();
            void ProcessGraph       (GraphData& data);
            std::shared_ptr<WorldGenConfig> GetConfig();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_GRAPHPROCESSOR_H
