//
// Created by mattyvrba on 9/9/20.
//

#ifndef WORLD_GENERATOR_CORE_PINHOLDERS_H
#define WORLD_GENERATOR_CORE_PINHOLDERS_H

#include <cstdint>
#include <map>

namespace WorldGenerator {
    namespace NodeEditor {
        /// Struct used for passing data about editor in single object, containing references to data in editor
        struct PinHolders {
            /// Data about input pins
          std::map<uint32_t, std::pair<uint32_t, uint32_t>>& m_InputPins;
          /// Data about output pins
          std::map<uint32_t, std::map<uint32_t, std::pair<uint32_t, uint32_t>>>& m_OutputPins;
          /// Creates pin holders object
          /// \param input Input pins map
          /// \param output Output pins map
          PinHolders(
                  std::map<uint32_t, std::pair<uint32_t, uint32_t>>& input,
                  std::map<uint32_t, std::map<uint32_t, std::pair<uint32_t, uint32_t>>>& output)
                  :m_InputPins(input), m_OutputPins(output) { }
        };
    }
}
#endif //WORLD_GENERATOR_CORE_PINHOLDERS_H
