//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_H
#define WORLD_GENERATOR_CORE_UI_H

#include "ImGUI_Core.h"

namespace WorldGenerator {
    namespace UI {
        class UI {
        public:
            virtual void Setup();
            virtual void Render(const std::shared_ptr<ImGUI_Core>& data);
            void PostRender(const std::shared_ptr<ImGUI_Core>& data);
        };
    }
}
#endif //WORLD_GENERATOR_CORE_UI_H
