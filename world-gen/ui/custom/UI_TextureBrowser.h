//
// Created by Matty on 5.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_TEXTUREBROWSER_H
#define WORLD_GENERATOR_CORE_UI_TEXTUREBROWSER_H

#include "../UIWindow.h"
#include "../../imgui/imgui.h"
#include "../../imgui/imfilebrowser.h"
#include "UI_ModelInspector.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            class UI_TextureAddPopup final : public UIWindow {
            public:
                bool m_Init = true;

                bool m_DataAdded = false;
                bool m_Closed = false;

                bool m_ShowBadInput = false;
                bool m_ShowAlreadyExists = false;

                std::string m_Path;
                std::string m_Name;

                UI_TextureAddPopup();
                void Flush();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };

            class UI_TextureBrowser final : public UIWindow {
            private:
                int32_t m_pSelectedItem = 0;
                bool m_pShowPopup = false;
                bool m_pShowFB = false;

                std::shared_ptr<UI_TextureAddPopup> m_pPopup;
                ImGui::FileBrowser m_pFileBrowser;
            public:
                UI_TextureBrowser();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
                void CustomPostDraw() override;
            };

        }
    }
}
#endif //WORLD_GENERATOR_CORE_UI_TEXTUREBROWSER_H
