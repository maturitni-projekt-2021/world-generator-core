//
// Created by Matty on 11/9/2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_NOISELAYEREDITOR_H
#define WORLD_GENERATOR_CORE_UI_NOISELAYEREDITOR_H


#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            class UI_NoiseLayerEditor : public UIWindow {
            private:
                bool m_pRefresh = true;
                uint32_t m_pSelectedIndex = 0;
            public:
                UI_NoiseLayerEditor();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_UI_NOISELAYEREDITOR_H
