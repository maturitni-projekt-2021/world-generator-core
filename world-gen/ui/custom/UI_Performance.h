//
// Created by Matty on 2.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_PERFORMANCE_H
#define WORLD_GENERATOR_CORE_UI_PERFORMANCE_H

#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            class UI_Performance final : public UIWindow {

            public:
                UI_Performance();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };
        };
    }
}

#endif //WORLD_GENERATOR_CORE_UI_PERFORMANCE_H
