//
// Created by Matty on 5.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_MODELINSPECTOR_H
#define WORLD_GENERATOR_CORE_UI_MODELINSPECTOR_H

#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            class UI_ModelInspector final : public UIWindow {
            private:
                int32_t m_pSelectedMesh = 0;
                int32_t m_pSelectedTextureAlbedo = 0;
                int32_t m_pSelectedTextureNormal = 0;
                int32_t m_pSelectedTextureRoughness = 0;

                bool m_pPrevSelectedInitialized = false;

                uint32_t m_pPrevMeshID = 0;
                std::string m_pPrevSelected;
            public:
                UI_ModelInspector();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };
        }
    }
}

#endif //WORLD_GENERATOR_CORE_UI_MODELINSPECTOR_H
