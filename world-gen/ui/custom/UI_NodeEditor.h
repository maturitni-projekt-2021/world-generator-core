//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_NODEEDITOR_H
#define WORLD_GENERATOR_CORE_UI_NODEEDITOR_H

#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            enum SelectedEditor {
              VOXEL_EDITOR,
              POPULATION_EDITOR
            };

            class UI_NodeEditor final : public UIWindow {
            private:
                bool m_pChanged = true;
                SelectedEditor m_pSelected = SelectedEditor::VOXEL_EDITOR;
            public:
                UI_NodeEditor();
            protected:
                void CustomPostDraw() override;
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };
        }
    }
}
#endif //WORLD_GENERATOR_CORE_UI_NODEEDITOR_H
