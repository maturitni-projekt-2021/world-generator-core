//
// Created by Matty on 2021-02-01.
//

#ifndef WORLD_GENERATOR_CORE_UI_WORLDOBJECTEDITOR_H
#define WORLD_GENERATOR_CORE_UI_WORLDOBJECTEDITOR_H


#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
class UI_WorldObjectEditor final : public UIWindow {

public:
    UI_WorldObjectEditor();
protected:
    void CustomDraw() override;
    void CustomWindowSetup() override;
};
        };
    }
}


#endif //WORLD_GENERATOR_CORE_UI_WORLDOBJECTEDITOR_H
