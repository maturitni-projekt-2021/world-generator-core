//
// Created by Matty on 3.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_OPTIONS_H
#define WORLD_GENERATOR_CORE_UI_OPTIONS_H

#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            class UI_Options final : public UIWindow {
            private:
            public:
                UI_Options();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };
        }
    }
}
#endif //WORLD_GENERATOR_CORE_UI_OPTIONS_H
