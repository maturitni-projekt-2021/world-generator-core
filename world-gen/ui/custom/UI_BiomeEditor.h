//
// Created by Matty on 2021-02-17.
//

#ifndef WORLD_GENERATOR_CORE_UI_BIOMEEDITOR_H
#define WORLD_GENERATOR_CORE_UI_BIOMEEDITOR_H


#include "../UIWindow.h"

namespace WorldGenerator {
    namespace UI {
        namespace Custom {
            class UI_BiomeEditor final : public UIWindow {
            public:
                UI_BiomeEditor();
            protected:
                void CustomDraw() override;
                void CustomWindowSetup() override;
            };
        };
    }
}


#endif //WORLD_GENERATOR_CORE_UI_BIOMEEDITOR_H
