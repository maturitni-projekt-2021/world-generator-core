//
// Created by Matty on 10/29/2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_PROJECTSELECT_H
#define WORLD_GENERATOR_CORE_UI_PROJECTSELECT_H

#include "UI.h"
#include "UIWindow.h"
#include "../imgui/imfilebrowser.h"

namespace WorldGenerator {
    namespace UI {
        class UI_ProjectSelect : public WorldGenerator::UI::UI{
        private:
            bool m_pMainOpen = false;

            bool m_pShowNewProject = false;
            bool m_pNewProjectOpen = false;
            bool m_pShowEmptyNameError = false;
            bool m_pSaveSelected = false;
            bool m_pShowSaveSelectedError = false;
            static char s_pProjectName[256];
            static char s_pProjectPath[512];
            std::string m_pProjectPathStl;

            bool m_pShowProjectFB = false;
            ImGui::FileBrowser m_pProjectFileBrowser{ImGuiFileBrowserFlags_CreateNewDir | ImGuiFileBrowserFlags_EnterNewFilename};

            bool m_pShowFB = false;
            ImGui::FileBrowser m_pFileBrowser;

            bool m_pShowFail = false;
            bool m_pProjectLoaded = false;
            std::string m_pSavePath;
        public:
            UI_ProjectSelect();
            void Render(const std::shared_ptr<ImGUI_Core> &data) override;
            bool IsProjectLoaded() const;
            void Flush();
        };
    }
}


#endif //WORLD_GENERATOR_CORE_UI_PROJECTSELECT_H
