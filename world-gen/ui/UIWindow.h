//
// Created by Matty on 2.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UIWINDOW_H
#define WORLD_GENERATOR_CORE_UIWINDOW_H
namespace WorldGenerator {
    namespace UI {
        enum WindowType {
          WINDOW,
          POPUP
        };

        class UIWindow {
        private:
            bool m_pCollapsed = false;
            std::string m_pName;
            WindowType m_pType;
            bool m_pPopupInit = false;
        public:
            UIWindow(std::string name,WindowType type = WindowType::WINDOW);
            void Draw();
            bool GetCollapsed() const { return m_pCollapsed;}
        protected:
            int m_Flags = 0;
            virtual void CustomPostDraw() {};
            virtual void CustomDraw() = 0;
            virtual void CustomWindowSetup() {};
        };
    }
}
#endif //WORLD_GENERATOR_CORE_UIWINDOW_H
