//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_UI_MAIN_H
#define WORLD_GENERATOR_CORE_UI_MAIN_H

#include "UI.h"
#include "UIWindow.h"
#include "../imgui/imgui_action.h"

#include <memory>
#include <vector>

namespace WorldGenerator {
    namespace UI {
        class UI_Main final : public WorldGenerator::UI::UI {
        private:
            bool m_pWorldViewVisible = false;
            ImAction * m_pActionSave = nullptr;
            ImAction * m_pActionRebuild = nullptr;
        public:
            UI_Main();

            std::vector<std::shared_ptr<UIWindow>> m_Windows;

            void Render(const std::shared_ptr<ImGUI_Core> &data) override;

            void Setup() override;

            bool IsWorldViewVisible() const;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_UI_MAIN_H
