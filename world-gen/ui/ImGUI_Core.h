//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_IMGUI_CORE_H
#define WORLD_GENERATOR_CORE_IMGUI_CORE_H

#include "../imgui/imgui.h"
#include "../render/WindowDestructor.h"

#include <GLFW/glfw3.h>
#include <memory>

namespace WorldGenerator {
    namespace UI {
        enum ColorTheme {
          DARK_THEME,
          LIGHT_THEME
        };

        class ImGUI_Core final {
        private:
            ImVec4 m_pClearColor = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
            std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor>& m_pWindow;
        public:
            ImGUI_Core(std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor>& window);
            void Init();
            void SetColorTheme(ColorTheme theme);
            void Stop();
            std::unique_ptr<GLFWwindow,
                            WorldGenerator::Render::GLFWWindowDestructor>& GetWindow() { return m_pWindow; };
            ImVec4 GetClearColor() { return m_pClearColor; }
            bool GetViewportsEnabled();
            bool GetDockingEnabled();
        };
    }
}

#endif //WORLD_GENERATOR_CORE_IMGUI_CORE_H
