//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_TERRAIN_H
#define WORLD_GENERATOR_CORE_TERRAIN_H

#include "../render/VertexBuffer.h"
#include "../render/DrawBatch.h"
#include "WorldChunkPos.h"

namespace WorldGenerator {
    namespace World {
        class Terrain {
        private:
            std::shared_ptr<Render::VertexBuffer> m_pVertexBuffer;
            WorldGenerator::World::WorldChunkPos m_pChunkPosition;
            glm::mat4 m_pModelMatrix;
            bool m_pModelMatrixCached = false;
        public:
            std::shared_ptr<Render::VertexBuffer> GetVertexBuffer() const { return m_pVertexBuffer; };

            Terrain(std::shared_ptr<Render::VertexBuffer> vertexBuffer,
                    WorldGenerator::World::WorldChunkPos chunkPosition);

            virtual ~Terrain();

            const WorldGenerator::World::WorldChunkPos &GetChunkPosition() const;

            void Render(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &pos);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_TERRAIN_H
