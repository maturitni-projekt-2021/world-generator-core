//
// Created by mattyvrba on 9/17/20.
//

#ifndef WORLD_GENERATOR_CORE_WORLDCHUNKPOS_H
#define WORLD_GENERATOR_CORE_WORLDCHUNKPOS_H

#include <cstdint>

namespace WorldGenerator {
    namespace World {
        struct WorldChunkPos {
            int32_t x;
            int32_t y;
            int32_t z;
            WorldChunkPos(int32_t x, int32_t y, int32_t z) : x(x), y(y), z(z) {}
            bool operator<(const WorldChunkPos & other) const ;
            bool operator==(const WorldChunkPos & other) const ;
        };
    }
}

#endif //WORLD_GENERATOR_CORE_WORLDCHUNKPOS_H
