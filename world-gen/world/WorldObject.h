//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_WORLDOBJECT_H
#define WORLD_GENERATOR_CORE_WORLDOBJECT_H

#include "../render/DrawBatch.h"
#include "../data/ModelData.h"

namespace WorldGenerator {
    namespace World {
        class WorldObject {
        private:
            bool m_pHasModel = true;
            std::shared_ptr<Data::ModelData> m_pModels;
            glm::vec3 m_pPosition = glm::vec3(0.0f,0.0f,0.0f);
            glm::vec3 m_pRotation = glm::vec3(0.0f,0.0f,0.0f);
            glm::vec3 m_pScale = glm::vec3(1.0f);
        public:
            WorldObject() = default;
            void SetModels(const std::shared_ptr<Data::ModelData>& models);
            void SetPosition(const glm::vec3 & pos);
            void SetRotation(const glm::vec3 & rot);
            void SetScale(const glm::vec3 & scale);
            const glm::vec3 &GetPosition() const;
            const glm::vec3 &GetRotation() const;
            const glm::vec3 &GetScale() const;
            void Render(const std::shared_ptr<WorldGenerator::Render::DrawBatch>& drawBatch);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_WORLDOBJECT_H
