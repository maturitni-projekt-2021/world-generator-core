//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_BIOME_H
#define WORLD_GENERATOR_CORE_BIOME_H

#include <glm/glm.hpp>
#include <json.hpp>
#include "WOSpawnRules.h"

namespace WorldGenerator {
    namespace World {
        struct BiomeColorLayer {
            glm::vec3 m_Color;
            float m_Min;
            float m_Max;

            BiomeColorLayer(const glm::vec3 &color, float min, float max);
        };

        class Biome {
        private:
            glm::vec3 m_pBaseColor;
            std::vector<BiomeColorLayer> m_pColorLayers;
            std::vector<WOSpawnRules> m_pWorldObjectSpawnRules;
            glm::vec2 m_pMoistureBounds= {0.0f,0.0f};
            glm::vec2 m_pTemperatureBounds= {0.0f,0.0f};
            glm::vec2 m_pHeightBounds = {0.0f,0.0f};
            float m_pMultiplier = 1.0f;
            std::string m_pName;
        public:
            void setMPBaseColor(const glm::vec3 &mPBaseColor);

            const std::vector<BiomeColorLayer> &getMPColorLayers() const;

            void setMPColorLayers(const std::vector<BiomeColorLayer> &mPColorLayers);

            const std::vector<WOSpawnRules> &getMPWorldObjectSpawnRules() const;

            void setMPWorldObjectSpawnRules(const std::vector<WOSpawnRules> &mPWorldObjectSpawnRules);

            const glm::vec2 &getMPMoistureBounds() const;

            void SetMoistureBounds(const glm::vec2 &mPMoistureBounds);

            const glm::vec2 &GetTemperatureBounds() const;

            void SetTemperatureBounds(const glm::vec2 &mPTemperatureBounds);

            const glm::vec2 &GetHeightBounds() const;

            void SetHeightBounds(const glm::vec2 &mPHeightBounds);

            float GetMultiplier() const;

            void SetMultiplier(float mPMultiplier);

            const std::string &GetName() const;

            void SetName(const std::string &mPName);

            bool IsLegitimate(float moisture, float temperature, float height) const;
            explicit Biome();
            Biome(const glm::vec3 &baseColor);
            /// Copy constructor
            /// \param cpy Object to copy
            Biome(const Biome & cpy);

            const glm::vec3 & GetBaseColor() const;

            glm::vec3 GetColorFromHeight(float y) const;

            void AddLayer(BiomeColorLayer layer);

            std::vector<BiomeColorLayer> GetColorLayers() const;

            void RemoveColorLayer(size_t index);

            void Save(nlohmann::json & json);
            void Load(nlohmann::json & json);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_BIOME_H
