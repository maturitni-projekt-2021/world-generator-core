//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_WORLD_H
#define WORLD_GENERATOR_CORE_WORLD_H

#include "WorldObject.h"
#include "../render/Camera.h"
#include "../render/DrawBatch.h"
#include "Terrain.h"
#include "../util/UtilThread.h"
#include "WorldChunkPos.h"
#include "../util/UtilMemory.h"
#include "../render/culling/CullColliderBox.h"

#ifndef NDEBUG
#include "../render/debug/DebugLineStrip.h"

#endif

#include <memory>

namespace WorldGenerator {
    namespace World {
        struct TerrainChunk {
        public:
#ifndef NDEBUG
            std::shared_ptr<Render::Debug::DebugLineStrip> m_DebugCenterLine;
#endif
            WorldGenerator::World::WorldChunkPos m_Position;
            Render::Culling::CullColliderBox m_CullCollider;
            std::shared_ptr<Terrain> m_Terrain;
            std::vector<std::shared_ptr<WorldObject>> m_WorldObjects;
            uint32_t m_Resolution;
            TerrainChunk(const WorldChunkPos &mPosition, const std::shared_ptr<Terrain> &mTerrain, std::vector<std::shared_ptr<WorldObject>> worldObjects,uint32_t res);

            virtual ~TerrainChunk();
        };
        struct TerrainInsert {
            std::shared_ptr<Terrain> m_Data;
            std::vector<std::shared_ptr<WorldObject>> m_WorldObjects;
            WorldChunkPos m_Pos;
            uint32_t m_Slot;
            uint32_t m_Resolution;
            uint32_t m_Version;

            TerrainInsert(std::shared_ptr<Terrain> data, const WorldChunkPos & pos,uint32_t slot ,std::vector<std::shared_ptr<WorldObject>> worldObjects,uint32_t res,uint32_t version);
        };

        class World {
        private:
            std::vector<std::shared_ptr<Render::Camera>> m_pCameras;
            std::vector<Render::Model> m_pPlane;
            std::shared_ptr<Render::DrawBatch> m_pDrawBatch;
            std::set<WorldGenerator::World::WorldChunkPos> m_pRequestedChunks;
            std::map<WorldGenerator::World::WorldChunkPos, uint32_t> m_pRequestedSlots;
            std::shared_ptr<TerrainChunk>* m_pTerrain;
            std::queue<uint32_t> m_pFreeSlots;
            Util::ThreadSafeQueue<std::shared_ptr<TerrainInsert>> m_pInserts;
            std::set<WorldGenerator::World::WorldChunkPos> m_pLoadedChunks;
            std::map<WorldGenerator::World::WorldChunkPos, uint32_t> m_pChunkSlots;
            void _GenChunkLODSameLevel(int32_t maxRad,uint32_t resolution,uint32_t & requestCount, const std::vector<uint32_t> & workers, const WorldChunkPos & centerChunk, int32_t rangeHorizontal = 1);
            void _GenChunkLOD(int32_t maxRad,uint32_t resolution,uint32_t & requestCount, const std::vector<uint32_t> & workers, const WorldChunkPos & centerChunk);
            uint32_t m_pConfigVersion = 0;
            void _ClearTerrainForce();
            void _FillFreeSlots();
            bool m_pStopping = false;
        public:
            void SpawnWorldObject(std::shared_ptr<WorldObject> spawn);
            World();
            virtual ~World();
            void Render();
            void Update();
            void AddTerrain(const WorldGenerator::World::WorldChunkPos & pos,std::shared_ptr<Terrain> data,uint32_t slot,std::vector<std::shared_ptr<WorldObject>> worldObjects,uint32_t res,uint32_t version);
            void ClearTerrain(const WorldGenerator::World::WorldChunkPos & pos);
            void ResetBuild();
            void DisableWorldGen();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_WORLD_H
