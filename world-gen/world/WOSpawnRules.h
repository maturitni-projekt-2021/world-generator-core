//
// Created by Matty on 10/29/2020.
//

#ifndef WORLD_GENERATOR_CORE_WOSPAWNRULES_H
#define WORLD_GENERATOR_CORE_WOSPAWNRULES_H

#include "../data/ModelData.h"
#include "WorldObject.h"

namespace WorldGenerator {
    namespace World {
        enum WORenderDistancePriority : uint8_t {
            WO_RENDER_PRIORITY_NONE,
            WO_RENDER_PRIORITY_LOW,
            WO_RENDER_PRIORITY_MEDIUM,
            WO_RENDER_PRIORITY_HIGH,
            WO_RENDER_PRIORITY_ALWAYS
        };

        class WOSpawnRules {
        private:
            std::shared_ptr<Data::ModelData> m_pModel;
        public:
            bool m_Enabled = true;
            float m_Density = 1.0f/100;
            glm::vec3 m_ScaleVariety = {0.0f,0.0f,0.0f};
            glm::vec3 m_StartScaleVariety = {0.0f,0.0f,0.0f};
            glm::vec3 m_RotationVariety = {0.0f,0.0f,0.0f};
            glm::vec3 m_StartRotation = {0.0f,0.0f,0.0f};
            glm::vec3 m_PositionOffsetVariety = {0.0f,0.0f,0.0f};
            glm::vec3 m_StartPositionOffset = {0.0f,0.0f,0.0f};

            WOSpawnRules(const std::string & model,float mDensity = 1.0f/100, const glm::vec3 &mScaleVariety = {0.0f,0.0f,0.0f}, const glm::vec3 &mStartScaleVariety  = {0.0f,0.0f,0.0f},
                         const glm::vec3 &mRotationVariety  = {0.0f,0.0f,0.0f}, const glm::vec3 &mStartRotation  = {0.0f,0.0f,0.0f},
                         const glm::vec3 &mPositionOffsetVariety  = {0.0f,0.0f,0.0f}, const glm::vec3 &mStartPositionOffset = {0.0f,0.0f,0.0f},
                         WORenderDistancePriority mRenderPriority = WORenderDistancePriority::WO_RENDER_PRIORITY_LOW);

            WORenderDistancePriority m_RenderPriority = WORenderDistancePriority::WO_RENDER_PRIORITY_LOW;
            std::shared_ptr<WorldGenerator::World::WorldObject> GetWorldObject(const glm::vec3 & pos);
            bool ShouldSpawn(const glm::vec3 & pos) const;
        };
    }
}


#endif //WORLD_GENERATOR_CORE_WOSPAWNRULES_H
