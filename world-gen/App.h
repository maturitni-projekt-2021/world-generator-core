//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_APP_H
#define WORLD_GENERATOR_CORE_APP_H

#define IMGUI_DEFINE_MATH_OPERATORS
#include <memory>
#include "render/Window.h"
#include "ui/ImGUI_Core.h"
#include "input/Input.h"
#include "ui/UI_Main.h"
#include "ui/UI_ProjectSelect.h"
#include "gpugen/GPUWorker.h"

namespace WorldGenerator {
/// Main app class
    class App {
    private:
        /// If there is active project right now
        bool m_pProjectSelected = false;
        /// This bool is set to true when we want to restart app after we close it
        static bool m_pRestart;
        /// When this is true, app will try to stop, wait for threads to finish their tasks and then stop
        static bool m_pStopping;
        /// If app is still running, this is true until ReadyToStop is not true
        bool m_pRunning = true;
        /// Is app ready to stop? Set to true after all threads finish their job
        bool m_pReadyToStop = false;
        /// Main app window, wrapping GLFW window class
        std::shared_ptr<Render::Window> m_pWindow;
        /// UI Data, containing init stuff for ImGui and some glfw things
        std::shared_ptr<UI::ImGUI_Core> m_pUiData;
        /// Main UI class, containing all windows etc.
        std::shared_ptr<UI::UI_Main> m_pUi;
        /// UI for selecting projects, only showed when no projects are loaded
        std::shared_ptr<UI::UI_ProjectSelect> m_pUiProjSelect;
        /// Main input management class
        std::shared_ptr<Input::InputController> m_pInput;
        /// GPU worker class, mainting communication with gpu for world gen
        std::unique_ptr<GPU::GPUWorker> m_pGPUWorker;
        /// Initializes all modules in app
        void _Init();
        /// Main loop function, containing while loop that is called on main thread
        void _MainLoop();
        /// Cleanup stop function, calling stop on all modules
        void _Stop();
        /// Internal function used for creating starting window
        void _MakeWindow();
        /// Internal function used for initializing UI for first time
        void _InitUi();
        /// Internal function used for initialization of settings
        void _InitSettings();
        /// Internal function used for initialization of render data
        void _InitRenderData();
        /// Internal function used for initialization of render data
        void _InitPerformanceData();
        /// Internal function used for initialization of resources data
        void _InitResources();
        /// Internal function used for initialization of node editor data
        void _InitNodeEditor();
        /// Internal function used for initialization of world generator
        void _InitWorldGenerator();
        /// Internal function used for initialization of world generator
        void _InitInput();
    public:
        /// Exits app
        static void Exit();
        /// Restarts app
        static void Restart();
        /// Main constructor
        App();
        /// Start function, starting whole up
        int32_t Start();
        /// Destructor
        ~App();
        /// Clears glfw instance
        static void StopGLFW();
    };
}

#endif //WORLD_GENERATOR_CORE_APP_H
