//
// Created by mattyvrba on 6/30/20.
//

#ifndef LEIKO_GLOBAL_H
#define LEIKO_GLOBAL_H

namespace WorldGenerator {
    /// Class containing global data
    class Global {
    public:
        /// Global world scale, for float precision
        static float s_WorldScale;
        /// Size of chunk
        static float s_pChunkSize;
        /// If glad was already initialized
        static bool s_pGladInitialized;
    };
}

#endif //LEIKO_GLOBAL_H
