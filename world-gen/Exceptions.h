//
// Created by Matty on 10.4.2020.
//

#ifndef LEIKO_EXCEPTIONS_H
#define LEIKO_EXCEPTIONS_H

#include <exception>
#include <string>

namespace WorldGenerator {
    namespace Exceptions {
        /// Default exception thrown in world gen app
        class WGException : std::exception {
        private:
            /// Internal final message
            std::string m_pMessage;
        public:
            /// Constructor for Leiko exception
            /// \param reason Reason why exception was thrown
            /// \param source Source where exception was thrown from
            WGException(const std::string& reason, const std::string& source);
            const char* what() const noexcept override;
        };
    }
}

#endif //LEIKO_EXCEPTIONS_H
