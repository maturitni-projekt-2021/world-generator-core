//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_UTILTICK_H
#define WORLD_GENERATOR_CORE_UTILTICK_H

#define TICKS_PER_SECOND 64
#define FRAMES_PER_SECOND 144

namespace WorldGenerator {
    namespace Util {
        class UtilTick final {
        public:
            static double m_FrameFrequency;
            static double m_StartFrameTime;
            static double m_EndFrameTime;
            static double m_ElapsedTimeSinceFrame;

            static float GetDelta();

            static void Init();
            static void FrameStart();
            static void FrameEnd();
            static bool ShouldTick();
        };
    }
}

#endif //WORLD_GENERATOR_CORE_UTILTICK_H
