//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UTILMATH_H
#define WORLD_GENERATOR_CORE_UTILMATH_H

#include <glm/glm.hpp>
#include "../imgui/imgui.h"

namespace WorldGenerator {
    namespace Util {
        class UtilMath final {
        public:
            static float Dist2(const glm::vec3 & a, const glm::vec3 & b);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_UTILMATH_H
