//
// Created by Matty on 5.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UTILFORMAT_H
#define WORLD_GENERATOR_CORE_UTILFORMAT_H

#include <glm/glm.hpp>
#include <json.hpp>

namespace WorldGenerator {
    namespace Util {
        class UtilFormat final {
        public:
            static nlohmann::json Vec3ToJson(const glm::vec3 & input);
            static glm::vec3 JsonToVec3(const nlohmann::json & json);
            static std::string Vec3ToString(const glm::vec3 & input);
        };
    }
};

#endif //WORLD_GENERATOR_CORE_UTILFORMAT_H
