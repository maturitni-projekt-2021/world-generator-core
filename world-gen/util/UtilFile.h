//
// Created by Matty on 23.8.2019.
//

#ifndef LEIKO_UTILFILE_H
#define LEIKO_UTILFILE_H

#include <string>
#include <fstream>
#include <sys/stat.h>

namespace WorldGenerator {
    namespace Util {
        /// Utility class containing function for working with files
        class UtilFile {
        public:
            /// Checks efficiently if file exists
            /// \param path Path to file that should be checked
            /// \return If file exists
            static bool FileExists(const std::string& path)
            {
                struct stat buffer;
                return (stat(path.c_str(), &buffer)==0);
            }
        };
    }
}

#endif //LEIKO_UTILFILE_H
