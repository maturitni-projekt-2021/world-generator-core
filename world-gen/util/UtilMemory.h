//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_UTILMEMORY_H
#define WORLD_GENERATOR_CORE_UTILMEMORY_H
namespace WorldGenerator {
    namespace Util {
        template<typename T>
        struct ArrDeleter {
            void operator()(T const *p) {
                delete[] p;
            }
        };

        class UtilMemory {

        };
    }
}

#endif //WORLD_GENERATOR_CORE_UTILMEMORY_H
