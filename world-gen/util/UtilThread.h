//
// Created by Matty on 4.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_UTILTHREAD_H
#define WORLD_GENERATOR_CORE_UTILTHREAD_H

#ifdef MINGW_WINDOWS_THREAD_LIB
#include <mingw_threads/mingw.thread.h>
#include <mingw_threads/mingw.condition_variable.h>
#else
#include <thread>
#include <condition_variable>
#endif

#include <queue>
#include <mutex>
namespace WorldGenerator {
    namespace Util {
        enum ThreadPriority : int32_t {
            WG_THREAD_PRIORITY_ABOVE_NORMAL = 1,
            WG_THREAD_PRIORITY_BELOW_NORMAL = -1,
            WG_THREAD_PRIORITY_HIGHEST = 2,
            WG_THREAD_PRIORITY_NORMAL = 0,
            WG_THREAD_PRIORITY_LOWEST = -2,
            WG_THREAD_PRIORITY_IDLE = -15,
            WG_THREAD_PRIORITY_TIME_CRITICAL = 15,
        };
        class UtilThread {
        public:
             static bool SetThreadPrio(std::thread & thread, ThreadPriority prio);
        };

// A threadsafe-queue.
        template<class T>
        class ThreadSafeQueue {
        public:
            ThreadSafeQueue(void)
                    :m_pQueue(), m_pMutex(), m_pCV() { }

            ~ThreadSafeQueue(void) { }

            // Add an element to the queue.
            void Push(T t)
            {
                std::lock_guard<std::mutex> lock(m_pMutex);
                m_pQueue.push(std::move(t));
                m_pCV.notify_one();
                m_pEmpty = false;
            }

            void PushMultiple(const std::vector<T> & t)
            {
                std::lock_guard<std::mutex> lock(m_pMutex);
                for(auto & a : t ) {
                    m_pQueue.push(std::move(a));
                }
                m_pCV.notify_one();
                m_pEmpty = false;
            }

            T Pop(void)
            {
                std::unique_lock<std::mutex> lock(m_pMutex);
                if (m_pQueue.empty()) {
                    m_pEmpty = true;
                    throw std::exception();
                }
                T val = m_pQueue.front();
                bool empty = m_pQueue.empty();
                m_pEmpty = empty;
                m_pQueue.pop();
                return val;
            }

            bool Empty() {
                return m_pEmpty;
            }

            void Flush() {
                std::unique_lock<std::mutex> lock(m_pMutex);
                m_pQueue = std::queue<T>();
            }
        private:
            bool m_pEmpty = true;
            std::queue<T> m_pQueue;
            mutable std::mutex m_pMutex;
            std::condition_variable m_pCV;
        };


        template<class T>
        class ThreadSafePriorityQueue {
        public:
            ThreadSafePriorityQueue(void)
                    :m_pQueue(), m_pMutex(), m_pCV() { }

            ~ThreadSafePriorityQueue(void) { }

            // Add an element to the queue.
            void Push(T t)
            {
                std::lock_guard<std::mutex> lock(m_pMutex);
                m_pQueue.push(t);
                m_pCV.notify_one();
                m_pEmpty = false;
            }

            void PushMultiple(const std::vector<T> & t)
            {
                std::lock_guard<std::mutex> lock(m_pMutex);
                for(auto & a : t ) {
                    m_pQueue.push(std::move(a));
                }
                m_pCV.notify_one();
                m_pEmpty = false;
            }

            T Pop(void)
            {
                std::unique_lock<std::mutex> lock(m_pMutex);
                if (m_pQueue.empty()) {
                    m_pEmpty = true;
                    throw std::exception();
                }
                T val = m_pQueue.front();
                bool empty = m_pQueue.empty();
                m_pEmpty = empty;
                m_pQueue.pop();
                return val;
            }

            bool Empty() {
                return m_pEmpty;
            }

            void Flush() {
                std::unique_lock<std::mutex> lock(m_pMutex);
                m_pQueue = std::queue<T>();
            }
        private:
            bool m_pEmpty = true;
            std::priority_queue<T,std::vector<T>> m_pQueue;
            mutable std::mutex m_pMutex;
            std::condition_variable m_pCV;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_UTILTHREAD_H
