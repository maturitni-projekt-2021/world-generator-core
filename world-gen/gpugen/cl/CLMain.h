//
// Created by Matty on 11/21/2020.
//

#ifndef WORLD_GENERATOR_CORE_CLMAIN_H
#define WORLD_GENERATOR_CORE_CLMAIN_H

#include "../../world/WorldChunkPos.h"

#include <NVIDIA/CL/cl.hpp>

namespace WorldGenerator {
    namespace GPU {
        namespace CL {
            struct DeviceInfoCL {
            public:
                std::string m_Name;
                std::string m_Vendor;
                std::string m_Version;
                DeviceInfoCL(const cl::Device & device);
            };
            struct WorkerData{
                int m_Pos[3];
                std::vector<float> m_Result;
                uint32_t m_Resolution;

                std::shared_ptr<cl::Buffer> m_ResultBuffer;
                std::shared_ptr<cl::Buffer> m_PosBuffer;
                std::shared_ptr<cl::Buffer> m_ResolutionBuffer;

                WorkerData(int32_t x,int32_t y,int32_t z,uint32_t resolution);
            };

            class CLMain {
            private:
                std::vector<cl::Platform> m_pPlatforms;
                std::map<uint32_t,std::vector<cl::Device>> m_pDevices;
                bool m_pSupported = false;
                bool m_pActiveShader = false;
                bool m_pActivated = false;
                std::pair<uint32_t,uint32_t> m_pActiveDevice;
                std::vector<std::shared_ptr<cl::Kernel>> m_pVoxelKernels;
                cl::Context m_pContext;
                std::map<std::pair<uint32_t,uint32_t>,std::shared_ptr<DeviceInfoCL>> m_pDeviceInfos;
                uint32_t m_pActiveVersion = 0;
                uint32_t m_pKernelCount = 0;
                std::atomic_uint32_t m_pFreeWorkers = 0;
                std::map<uint32_t,std::shared_ptr<WorkerData>> m_WorkerData;
                std::vector<cl::Event> m_pFinishCallbacks;
                std::set<uint32_t> m_pActiveWorkers;
            public:
                ~CLMain();
                CLMain();
                void Init();
                void Activate();
                bool IsSupported() const;
                void GenerateShader();
                uint32_t ProcessChunk(const World::WorldChunkPos & pos, uint32_t resolution);
                void Update();
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CLMAIN_H
