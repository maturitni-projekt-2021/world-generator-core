//
// Created by Matty on 11/23/2020.
//

#ifndef WORLD_GENERATOR_CORE_GPUTASK_H
#define WORLD_GENERATOR_CORE_GPUTASK_H


#include "../world/WorldChunkPos.h"

namespace WorldGenerator {
    namespace GPU {
        class GPUTask {
        private:
            bool m_pDone = false;
        public:
            bool GetDone() const { return m_pDone; }
            void Drop() { m_pDone = true;}
            virtual void Process() = 0;
        protected:
            void Finish() { m_pDone = true;}
        };

        class ChunkGenTask : public GPUTask {
        private:
            uint32_t m_pWorkerID = -1;
            std::vector<float> m_pResult;
            uint32_t m_pResolution;
            World::WorldChunkPos m_pPosition;
        public:
            ChunkGenTask(const World::WorldChunkPos & pos,uint32_t resolution);
            uint32_t GetWorkerID() const;
            void Process() override;

            std::vector<float> & GetResult();
        };
    }
}

#endif //WORLD_GENERATOR_CORE_GPUTASK_H
