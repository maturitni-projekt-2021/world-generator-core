//
// Created by Matty on 11/19/2020.
//

#ifndef WORLD_GENERATOR_CORE_GPUWORKER_H
#define WORLD_GENERATOR_CORE_GPUWORKER_H


#include "cl/CLMain.h"
#include "cuda/CudaMain.h"

namespace WorldGenerator {
    namespace GPU {
        enum GPUWorkerType {
            WG_GPU_WORKER_CL,
            WG_GPU_WORKER_CUDA
        };

        class GPUWorker {
        private:
            std::unique_ptr<CL::CLMain> m_pCLGen;
            std::unique_ptr<Cuda::CudaMain> m_pCUDAGen;
            GPUWorkerType m_pActiveWorker;
        public:
            void Init();
            bool IsSupported(GPUWorkerType type);
            void Update();
            void Activate(GPUWorkerType type);
            void RebuildShaders();
        };
    }
}


#endif //WORLD_GENERATOR_CORE_GPUWORKER_H
