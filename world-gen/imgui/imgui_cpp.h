//
// Created by Matty on 4.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_IMGUI_CPP_H
#define WORLD_GENERATOR_CORE_IMGUI_CPP_H

#include "imgui.h"

#include <string>
#include <vector>

namespace ImGui
{
    static auto vector_getter = [](void* vec, int idx, const char** out_text)
    {
      auto& vector = *static_cast<std::vector<std::string>*>(vec);
      if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
      *out_text = vector.at(idx).c_str();
      return true;
    };

    bool Combo(const char* label, int* currIndex, std::vector<std::string>& values);

    bool ListBox(const char* label, int* currIndex, std::vector<std::string>& values);

}

#endif //WORLD_GENERATOR_CORE_IMGUI_CPP_H
