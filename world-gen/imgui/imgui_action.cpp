//
// Created by Matty on 11/8/2020.
//

#include "imgui_action.h"
#include <string>
#include <unordered_map>

using std::string;

static const int ImActionKeyModeFlags_Mask = ImCtrl | ImShift | ImAlt | ImSuper;

static string KeyName(int key)
{
    static const char* key_names[ImGuiKey_COUNT] =
            {
                    "Tab",
                    "LeftArrow",
                    "RightArrow",
                    "UpArrow",
                    "DownArrow",
                    "PageUp",
                    "PageDown",
                    "Home",
                    "End",
                    "Insert",
                    "Delete",
                    "Backspace",
                    "Space",
                    "Enter",
                    "Escape",
                    "KeyPadEnter",
                    "A",
                    "C",
                    "V",
                    "X",
                    "Y",
                    "Z"
            };
    const auto& key_maps = ImGui::GetIO().KeyMap;
    for (int i = 0; i < ImGuiKey_COUNT; ++i)
        if (key == key_maps[i])
            return key_names[i];

    return { char(key) };
}

string ShortCutName(const int shortcut)
{
    std::string shortcut_str = KeyName(shortcut & (~ImActionKeyModeFlags_Mask));
    if (shortcut & ImShift)
        shortcut_str = "Shift+" + shortcut_str;
    if (shortcut & ImAlt)
        shortcut_str = "Alt+" + shortcut_str;
    if (shortcut & ImCtrl)
        shortcut_str = "Ctrl+" + shortcut_str;
    if (shortcut & ImSuper)
        shortcut_str = "Super+" + shortcut_str;
    return shortcut_str;
}

struct ImAction::Impl
{
    string name;
    int shortcut;
    bool checkable;
    bool* selected = nullptr;
    bool visiable = true;
    bool enabled = true;
    bool checked = false;
    bool triggered = false;
    string shortcut_name;

    Impl(const char* name, int shortcut, bool* selected)
            : name(name), shortcut(shortcut), checkable(selected != 0)
            , selected(selected)
            , shortcut_name(ShortCutName(shortcut))
    {}
};

ImAction::ImAction(const char* name, int shortcut, bool* selected)
        : d(new Impl(name, shortcut, selected))
{
}

ImAction::~ImAction()
{
    delete d;
}

const char* ImAction::Name() const
{
    return d->name.c_str();
}

const char* ImAction::ShortcutName() const
{
    return d->shortcut_name.c_str();
}

bool ImAction::isVisible()
{
    return d->visiable;
}

bool ImAction::IsEnabled() const
{
    return d->enabled;
}

bool ImAction::IsCheckable() const
{
    return d->checkable;
}

bool ImAction::IsChecked() const
{
    return d->checked;
}

bool ImAction::IsTriggered() const
{
    return d->triggered;
}

void ImAction::setCheckable(bool checkable)
{
    d->checkable = checkable;
}

void ImAction::setVisible(bool visible)
{
    d->visiable = visible;
}

void ImAction::setEnabled(bool enabled)
{
    d->enabled = enabled;
}

void ImAction::setChecked(bool checked)
{
    if (d->checkable)
    {
        d->checked = true;
        if (d->selected) *d->selected = checked;
        d->triggered = true;
    }
}

void ImAction::Trigger()
{
    d->triggered = true;
}

bool ImAction::Toggle()
{
    if (d->checkable)
    {
        setChecked(!d->checked);
        return d->checked;
    }
    else
    {
        return false;
    }
}

bool ImAction::IsShortcutPressed(bool repeat)
{
    const ImGuiIO& io = ImGui::GetIO();
    if (io.KeyCtrl == ((d->shortcut & ImCtrl) != 0) &&
        io.KeyShift == ((d->shortcut & ImShift) != 0) &&
        io.KeyAlt == ((d->shortcut & ImAlt) != 0) &&
        io.KeySuper == ((d->shortcut & ImSuper) != 0) &&
        ImGui::IsKeyPressed((d->shortcut & ~ImActionKeyModeFlags_Mask), repeat))
    {
        Trigger();
        return true;
    }
    return d->triggered;
}

void ImAction::Reset()
{
    d->triggered = false;
}

struct ImActionManager::Impl
{
    std::unordered_map<string, ImAction*> actions;
};

ImActionManager& ImActionManager::Instance()
{
    static ImActionManager s_instance;
    return s_instance;
}

ImActionManager::ImActionManager()
        : d(new Impl())
{
}

ImActionManager::~ImActionManager()
{
    RemoveAll();
    delete d;
}

ImAction* ImActionManager::Create(const char* name, int shortcut, bool* selected)
{
    ImAction* action = new ImAction(name, shortcut, selected);
    d->actions.emplace(name, action);
    return action;
}

ImAction* ImActionManager::Get(const char* name)
{
    auto it = d->actions.find(name);
    return it == d->actions.end() ? nullptr : it->second;
}

const ImAction* ImActionManager::Get(const char* name) const
{
    auto it = d->actions.find(name);
    return it == d->actions.end() ? nullptr : it->second;
}

bool ImActionManager::Remove(ImAction* action)
{
    return Remove(action->Name());
}

bool ImActionManager::Remove(const char* name)
{
    auto it = d->actions.find(name);
    if (it == d->actions.end())
        return false;

    delete it->second;
    d->actions.erase(it);
    return true;
}

void ImActionManager::RemoveAll()
{
    for (auto& kv : d->actions)
        delete kv.second;
    d->actions.clear();
}

void ImActionManager::ResetAll()
{
    for (auto& kv : d->actions)
        kv.second->Reset();
}

bool ImGui::MenuItem(ImAction* action)
{
    if (!action->isVisible())
        return false;

    const bool checkable = action->IsCheckable();
    bool checked = action->IsChecked();
    if (ImGui::MenuItem(action->Name(), action->ShortcutName(), checked, action->IsEnabled()))
    {
        if (checkable)
            action->setChecked(!checked);
        else
            action->Trigger();
        return true;
    }
    return false;
}