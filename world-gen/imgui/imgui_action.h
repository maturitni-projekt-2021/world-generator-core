//
// Created by Matty on 11/8/2020.
//

#ifndef WORLD_GENERATOR_CORE_IMGUI_ACTION_H
#define WORLD_GENERATOR_CORE_IMGUI_ACTION_H

#include "imgui.h"

// for shortcut
enum ImActionKeyModFlags
{
    ImActionKeyMod_Bits = 24,
    ImCtrl  = 1 << (ImActionKeyMod_Bits + 0),
    ImShift = 1 << (ImActionKeyMod_Bits + 1),
    ImAlt   = 1 << (ImActionKeyMod_Bits + 2),
    ImSuper = 1 << (ImActionKeyMod_Bits + 3),
};

class IMGUI_API ImAction
{
public:
    ImAction(const char* name, int shortcut, bool* selected = nullptr);
    ~ImAction();

    const char* Name() const;
    const char* ShortcutName() const;
    bool isVisible();
    bool IsEnabled() const;
    bool IsCheckable() const;
    bool IsChecked() const;
    bool IsTriggered() const;

    void setVisible(bool visible);
    void setEnabled(bool enabled);
    void setCheckable(bool checkable);
    void setChecked(bool checked);
    void Trigger();
    bool Toggle(); // return true when checkable and checked
    bool IsShortcutPressed(bool repeat = false); // return true when triggered
    void Reset(); // reset triggered status

private:
    struct Impl;
    Impl* d;
    friend class ImActionManager;
};

class IMGUI_API ImActionManager
{
public:
    static ImActionManager& Instance();

    ImActionManager();
    ~ImActionManager();

    ImAction* Create(const char* name, int shortcut, bool* selected = nullptr);
    ImAction* Get(const char* name);
    const ImAction* Get(const char* name) const;
    bool Remove(ImAction* action); // return false when not existed
    bool Remove(const char* name); // return false when not existed
    void RemoveAll();
    void ResetAll();

private:
    struct Impl;
    Impl* d;
};

namespace ImGui {

    IMGUI_API bool MenuItem(ImAction* action);  // return true when activated.

} // namespace ImGui

#endif //WORLD_GENERATOR_CORE_IMGUI_ACTION_H
