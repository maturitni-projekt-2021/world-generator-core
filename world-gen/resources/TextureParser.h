//
// Created by Matty on 5.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_TEXTUREPARSER_H
#define WORLD_GENERATOR_CORE_TEXTUREPARSER_H

#include "../data/TextureData.h"

namespace WorldGenerator {
    namespace Resources {
        class TextureParser final {
        public:
            static void LoadTexture(const std::shared_ptr<WorldGenerator::Data::TextureData>& data,bool diskPath = false);
        private:
            static void _LoadTextureToGPU(const std::shared_ptr<WorldGenerator::Data::TextureData>& data,bool diskPath = false);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_TEXTUREPARSER_H
