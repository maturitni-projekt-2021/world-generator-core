//
// Created by Matty on 4.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_RESOURCESWORKER_H
#define WORLD_GENERATOR_CORE_RESOURCESWORKER_H

#ifdef MINGW_WINDOWS_THREAD_LIB
#include <mingw_threads/mingw.thread.h>
#include <mingw_threads/mingw.condition_variable.h>
#else
#include <thread>
#include <condition_variable>
#endif

#include <GLFW/glfw3.h>
#include <atomic>
#include "../util/UtilThread.h"

namespace WorldGenerator {
    namespace Resources {
        class ResourcesWorker final {
        private:
            GLFWwindow* m_pContext = nullptr;
            std::thread m_pThread;
            std::condition_variable& m_pCV;
            std::atomic_int & m_pToIncreaseOnEnd;
            WorldGenerator::Util::ThreadSafeQueue<std::function<void()>> & m_pTaskQueue;

            std::mutex m_pMutex;

            bool & m_pStop;

            void _Run();
            void _Setup();
            void _Stop();
        public:
            ResourcesWorker(std::condition_variable& cv, std::atomic_int & endCheckCount,bool & stop,WorldGenerator::Util::ThreadSafeQueue<std::function<void()>> & taskQueue);
            void Join();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_RESOURCESWORKER_H
