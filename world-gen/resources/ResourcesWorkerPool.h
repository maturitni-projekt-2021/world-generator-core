//
// Created by Matty on 4.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_RESOURCESWORKERPOOL_H
#define WORLD_GENERATOR_CORE_RESOURCESWORKERPOOL_H

#ifdef MINGW_WINDOWS_THREAD_LIB
#include <mingw_threads/mingw.thread.h>
#include <mingw_threads/mingw.condition_variable.h>
#else
#include <thread>
#include <condition_variable>
#endif

#include <atomic>
#include "ResourcesWorker.h"
#include "../util/UtilThread.h"

namespace WorldGenerator {
    namespace Resources {
        class ResourcesWorkerPool final {
        private:
            std::vector<std::unique_ptr<ResourcesWorker>> m_pWorkers;
            uint32_t m_pWorkerCount = 0;
            std::atomic_int m_pFinishedWorkers = 0;
            bool m_pInit = false;
            bool m_pStop = false;
            WorldGenerator::Util::ThreadSafeQueue<std::function<void()>> m_pProcessQueue;
            std::condition_variable m_pCV;
        public:
            static std::unique_ptr<ResourcesWorkerPool> m_ResourcesWorkerPool;
            void SetupWorkers();
            void AddTask(std::function<void()> task);
            void AddTasks(const std::vector<std::function<void()>> & task);
            void Stop();
        };
    }
}

#endif //WORLD_GENERATOR_CORE_RESOURCESWORKERPOOL_H
