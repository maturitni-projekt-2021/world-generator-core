//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_MODELPARSER_H
#define WORLD_GENERATOR_CORE_MODELPARSER_H

#include "../data/ModelData.h"

#include <assimp/scene.h>

namespace WorldGenerator {
    namespace Resources {
        class ModelParser final {
        public:
            static void LoadModel(const std::shared_ptr<WorldGenerator::Data::ModelData>& data);
        private:
            static void _LoadModelToGPU(const std::shared_ptr<WorldGenerator::Data::ModelData>& data);
            static void _CrawlNode(aiNode* node, aiMatrix4x4 transform, const std::shared_ptr<WorldGenerator::Data::ModelData>& data, const aiScene* scene);
            static void _BuildModel(const std::shared_ptr<WorldGenerator::Data::ModelData>& data, const aiMesh* mesh,aiMatrix4x4 transform,const aiMaterial* mat);
        };
    }
}
#endif //WORLD_GENERATOR_CORE_MODELPARSER_H
