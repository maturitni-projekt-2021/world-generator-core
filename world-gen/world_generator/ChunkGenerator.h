//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_CHUNKGENERATOR_H
#define WORLD_GENERATOR_CORE_CHUNKGENERATOR_H

#include <glm/glm.hpp>
#include "ChunkGenerationTask.h"
#include "../util/UtilThread.h"
#include "Voxel.h"

namespace WorldGenerator {
    namespace TerrainGeneration {
        class ChunkGenerator {
        private:
            std::thread m_pThread;
            std::condition_variable& m_pCV;
            std::atomic_int & m_pToIncreaseOnEnd;
            std::mutex m_pMutex;

            bool & m_pStop;

            void _Run();
            void _Init();
            void _Stop();

            float _BiasFunc(float val, float bias);

            void _GenerateChunk(const World::WorldChunkPos &chunkPos, const glm::vec3 & chunkSize, uint32_t chunkRes,uint32_t chunkSlot, bool & drop,uint32_t version);

            ChunkGenerationJobStatus & m_pJobSlot;

            TerrainGeneration::Voxel _GenerateVoxel(const glm::vec3 & worldPosition,bool & drop);
            std::tuple<uint32_t,uint32_t,float> _GetBiomeData(float moisture, float tempeture, float height,const glm::vec3 & pos);
        public:
            ChunkGenerator(std::condition_variable& cv, std::atomic_int& toIncreaseOnEnd, bool& stop,ChunkGenerationJobStatus & jobSlot);
            void Join();

        };
    }
}
#endif //WORLD_GENERATOR_CORE_CHUNKGENERATOR_H
