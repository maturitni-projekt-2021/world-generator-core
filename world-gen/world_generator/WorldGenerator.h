//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_WORLDGENERATOR_H
#define WORLD_GENERATOR_CORE_WORLDGENERATOR_H
#include "../node_editor/logic/WorldGenConfig.h"
#include "ChunkGenerator.h"
#include "../gpugen/GPUWorker.h"

namespace WorldGenerator {
    namespace TerrainGeneration {
        class WorldGenerator {
        private:
            bool m_pHasConfig = false;
            std::shared_ptr<NodeEditor::WorldGenConfig> m_pConfig;
            std::vector<std::unique_ptr<ChunkGenerator>> m_pGenerators;
            uint32_t m_pGeneratorCount = 0;
            std::atomic_int m_pFinishedGeneratorCount = 0;
            std::condition_variable m_pCV;
            bool m_pStop = false;
            const std::unique_ptr<GPU::GPUWorker> & m_pGPUWorker;
            std::vector<ChunkGenerationJobStatus> m_pJobStatuses;
        public:
            static std::unique_ptr<WorldGenerator> m_WorldGenerator;
            WorldGenerator(const std::unique_ptr<GPU::GPUWorker> & gpuWorker);
            void DropData();
            void DropConfig();
            void SetConfig(const std::shared_ptr<NodeEditor::WorldGenConfig> & config);
            void Stop();
            uint32_t GetWorkersCount() const;

            std::vector<uint32_t> GetFreeWorkers() const;
            void AddWorkerJob(uint32_t worker, const std::shared_ptr<ChunkGenerationTask> & task);
            void NotifyWorkers();
            void RebuildGPUData();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_WORLDGENERATOR_H
