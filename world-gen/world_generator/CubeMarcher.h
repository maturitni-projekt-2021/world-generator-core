//
// Created by Matty on 14.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_CUBEMARCHER_H
#define WORLD_GENERATOR_CORE_CUBEMARCHER_H

#define GLM_ENABLE_EXPERIMENTAL
#include "../render/custom/TerrainVertex.h"

namespace WorldGenerator {
    namespace TerrainGeneration {
        struct VoxelCell {
            float val[8];
            glm::vec3 col1[8];
            glm::vec3 col2[8];
            float biome[8];
            glm::vec3 p[8];
        };

        class CubeMarcher {
        public:
            static uint32_t Polygonise(VoxelCell grid, float isolevel, std::vector<float> & triangles);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_CUBEMARCHER_H
