//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_CHUNKGENERATIONTASK_H
#define WORLD_GENERATOR_CORE_CHUNKGENERATIONTASK_H

#include <glm/glm.hpp>
#include "../world/WorldChunkPos.h"

namespace WorldGenerator {
    namespace TerrainGeneration {
        struct ChunkGenerationTask {
            WorldGenerator::World::WorldChunkPos m_Pos;
            uint32_t m_Resolution;
            uint32_t m_Slot;
            uint32_t m_Version;
          ChunkGenerationTask(const WorldGenerator::World::WorldChunkPos& pos,uint32_t slot, uint32_t resolution,uint32_t version);
        };

        struct ChunkGenerationJobStatus {
            bool m_Active = false;
            bool m_Drop = false;
            WorldGenerator::World::WorldChunkPos m_Pos;
            glm::vec3 m_Size;
            uint32_t m_Resolution;
            uint32_t m_Slot;
            uint32_t m_Version;
            ChunkGenerationJobStatus() : m_Pos(0,0,0) {};
        };
    }
}

#endif //WORLD_GENERATOR_CORE_CHUNKGENERATIONTASK_H
