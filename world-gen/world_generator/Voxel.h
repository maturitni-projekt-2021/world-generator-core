//
// Created by Matty on 13.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_VOXEL_H
#define WORLD_GENERATOR_CORE_VOXEL_H

#include <cstdint>

namespace WorldGenerator {
    namespace TerrainGeneration {
        struct Voxel {
          float m_Value;
          uint32_t m_Biome1;
          uint32_t m_Biome2;
          float m_BiomeValue;
          Voxel(float value, uint32_t biome1, uint32_t biome2, float biomeValue)
                  :m_Value(value), m_Biome1(biome1), m_Biome2(biome2), m_BiomeValue(biomeValue) { }
        };
}
}
#endif //WORLD_GENERATOR_CORE_VOXEL_H
