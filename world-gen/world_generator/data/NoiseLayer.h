//
// Created by Matty on 11/9/2020.
//

#ifndef WORLD_GENERATOR_CORE_NOISELAYER_H
#define WORLD_GENERATOR_CORE_NOISELAYER_H

#include <glm/glm.hpp>
#include "../../data/WorldData.h"

namespace WorldGenerator {
    namespace TerrainGeneration {
        namespace Data {
            enum NoiseType {
                WG_NOISE_PERLIN_3D  ,
                WG_NOISE_SIMPLEX_3D ,
                WG_NOISE_WHITE_3D   ,
                WG_NOISE_FRACTAL_3D ,
                WG_NOISE_CELLUAR_3D ,
                WG_NOISE_CUBIC_3D   ,
                WG_NOISE_PERLIN_2D  ,
                WG_NOISE_CUBIC_2D   ,
                WG_NOISE_CELLUAR_2D ,
                WG_NOISE_FRACTAL_2D ,
                WG_NOISE_WHITE_2D   ,
                WG_NOISE_SIMPLEX_2D ,
            };
            enum AdditionType : int8_t {
                WG_NOISE_ADDITIVE = 0,
                WG_NOISE_SUBTRACT = 1,
                WG_NOISE_MULTIPLY = 2,
                WG_NOISE_ADDITIVE_2D = 3,
                WG_NOISE_SUBTRACT_2D = 4,
                WG_NOISE_MULTIPLY_2D = 5,
            };

            enum Mask2DOperation : int8_t {
                WG_NOISE_MASK_2D_ADDITIVE,
                WG_NOISE_MASK_2D_SUBTRACT,
                WG_NOISE_MASK_2D_MULTIPLY,
            };

            class NoiseLayer {
            protected:
                /// Type of noise in this layer
                NoiseType m_pType;
                /// Scale of noise
                float m_pScale = 1.0f;
                /// Offset of noise
                glm::vec3 m_pOffset = glm::vec3(0.0f);
                /// Intensity of noise
                float m_pIntensity = 1.0f;
                /// Addition ro subtraction
                AdditionType m_pAddition = AdditionType::WG_NOISE_ADDITIVE;
                /// Offset of range
                float m_pRangeOffset = 0.0f;
                /// Noise should be clamped after applying
                bool m_pClampNoiseAfter = false;
                void _CopyCore(NoiseLayer* res);
                /// Should bias be applied
                bool m_pApplyBias = false;
                /// Value of bias to apply
                float m_pBiasValue = 0.0f;
                /// Custom noiser for this layer
                FastNoise m_pLayerNoiser;
                /// Custom noiser seed
                int32_t m_pNoiserSeed = 0;
                /// Use custom noiser
                bool m_pCustomNoiser = false;
            public:
                virtual NoiseLayer* Copy() = 0;
                /// Type of noise
                /// \param type of noise
                NoiseLayer(NoiseType type);
                /// Gets value for specified pos in noise
                /// \param x X coord
                /// \param y Y coord
                /// \param z Z coord
                /// \return Value of noise
                float GetValue(float x, float y, float z);
                /// Gets value for specified pos in noise
                /// \param pos Position where to get noise value
                /// \return Value of noise
                virtual float GetValue(const glm::vec3 & pos, bool raw2Ddata = false, float nextVal = 0.0f, Mask2DOperation operation = Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE) = 0;
                /// Gets name of noise function
                /// \return Name of noise function used
                const char* GetTypeName() const;
                /// Gets scale of noise
                /// \return Scale of noise
                float GetScale() const;
                /// Sets scale of noise
                /// \param scale Scale to set
                void SetScale(float scale);
                /// Gets offset of noise
                /// \return Offset of noise
                const glm::vec3 &GetOffset() const;
                /// Sets offset of noise
                /// \param offset Offset to set
                void SetOffset(const glm::vec3 &offset);
                /// Gets intensity
                /// \return Intensity of noise in layer
                float GetIntensity() const;
                /// Sets intensity of noise in layer
                /// \param intensity Intensity to set
                void SetIntensity(float intensity);
                /// Gets type of layer
                /// \return Type of layer
                NoiseType GetType() const;
                /// Gets if noise is additive or subtractive
                /// \return additive or subtractive
                AdditionType GetAddition() const;
                /// Sets if noise is additive or subtractive
                /// \param newVal New value to set
                void SetAddition(AdditionType newVal);
                /// Gets current range offset in scale (scale 5 + offset 5 = range 0 - 10)
                /// \return Current range offset
                float GetRangeOffset() const;
                /// Sets current range offset in scale (scale 5 + offset 5 = range 0 - 10)
                /// \param rangeOffset New value to set
                void SetRangeOffset(float rangeOffset);
                /// Gets if noise should get clamped after this
                /// \return If noise gets clamped after this
                bool GetClampAfter() const;
                /// Sets if noise should be clamped after this
                /// \param newVal New value to set
                void SetClampAfter(bool newVal);
                /// Gets of layer should apply bias
                /// \return If layer should apply bias
                bool GetApplyBias() const;
                /// Sets of layer should apply bias
                /// \param newVal New value
                void SetApplyBias(bool newVal);
                /// Gets value of bias
                /// \return value of bias
                float GetBiasValue() const;
                /// Sets layer bias
                /// \param newVal New value
                void SetBiasValue(float newVal);
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_NOISELAYER_H
