//
// Created by Matty on 11/9/2020.
//

#ifndef WORLD_GENERATOR_CORE_CUBICNOISELAYER_H
#define WORLD_GENERATOR_CORE_CUBICNOISELAYER_H


#include "NoiseLayer.h"

namespace WorldGenerator {
    namespace TerrainGeneration {
        namespace Data {
            class CubicNoiseLayer2D : public NoiseLayer {
            public:
                CubicNoiseLayer2D();
                float GetValue(const glm::vec3 &pos, bool raw2Ddata = false, float nextVal = 0.0f, Mask2DOperation operation = Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE) override;

                NoiseLayer *Copy() override;
            };

            class CubicNoiseLayer3D : public NoiseLayer {
            public:
                CubicNoiseLayer3D();
                float GetValue(const glm::vec3 &pos, bool raw2Ddata = false, float nextVal = 0.0f, Mask2DOperation operation = Mask2DOperation::WG_NOISE_MASK_2D_ADDITIVE) override;

                NoiseLayer *Copy() override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CUBICNOISELAYER_H
