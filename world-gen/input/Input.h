//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_INPUT_H
#define WORLD_GENERATOR_CORE_INPUT_H

#include "../render/WindowDestructor.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

namespace WorldGenerator {
    namespace Input {
        /// Class managing input from user
        class InputController final {
        private:
            /// Start position of mouse, used for calculating deltas
            double m_pMouseStartX, m_pMouseStartY;
            /// If mouse pos is set, this is true after first frame of rotation start
            bool m_pMousePosSet = false;
            /// Start rotation when started rotating, used for calculating delta
            glm::vec3 m_pStartRotation;

            bool m_pSaveReleased = false;
        public:
            /// Initializes input module
            /// \param window GLFW context window
            void Init(std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor>& window);
            /// Update function for input
            /// \param window GLFW context window
            void Update(std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor>& window);

            void FixedUpdate();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_INPUT_H
