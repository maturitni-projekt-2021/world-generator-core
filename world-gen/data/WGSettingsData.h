//
// Created by Matty on 10/29/2020.
//

#ifndef WORLD_GENERATOR_CORE_WGSETTINGSDATA_H
#define WORLD_GENERATOR_CORE_WGSETTINGSDATA_H
#include <json.hpp>

namespace WorldGenerator {
    namespace Data {
        class WGSetting {
        protected:
            std::string m_pName;
        public:
            WGSetting(std::string  name);
            virtual void Load(const nlohmann::json & json) = 0;
            virtual void Save(nlohmann::json & json) = 0;
        };
        class WGSetting_Float : public WGSetting {
        public:
            WGSetting_Float(const std::string & name, float defaultValue);
            void Load(const nlohmann::json &json) override;
            void Save(nlohmann::json &json) override;
            float m_Value;
        };

        class WGSettingsData {

        };
    }
}


#endif //WORLD_GENERATOR_CORE_WGSETTINGSDATA_H
