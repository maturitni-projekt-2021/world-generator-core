//
// Created by Matty on 1.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_WORLDDATA_H
#define WORLD_GENERATOR_CORE_WORLDDATA_H
#include "../world/World.h"
#include "../world_generator/lib/FastNoise.h"

namespace WorldGenerator {
    namespace Data {
        /// Class holding data about world
        class WorldData {
        public:
            /// World object
            static std::unique_ptr<World::World> m_World;
            /// Position of point, around which chunks are generated
            static glm::vec3 m_CenterPosition;
            /// Noiser for terrain
            static FastNoise m_Noiser;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_WORLDDATA_H
