//
// Created by Matty on 11/9/2020.
//

#ifndef WORLD_GENERATOR_CORE_NOISELAYERDATA_H
#define WORLD_GENERATOR_CORE_NOISELAYERDATA_H

#include "../world_generator/data/NoiseLayer.h"
#include <json.hpp>

namespace WorldGenerator {
    namespace Data {
        /// Contains data about noise layer
        class NoiseLayerData {
        private:
            /// Data
            std::shared_ptr<TerrainGeneration::Data::NoiseLayer> m_pLayer;
            /// Layer name
            std::string m_pName;
            /// Is layer enabled
            bool m_pEnabled = true;
        public:
            /// Copy constructor
            /// \param cpy Object to copy
            NoiseLayerData(const NoiseLayerData & cpy);
            /// Constructs new NoiseLayerData
            /// \param name Name of layer
            /// \param data Data for layer
            NoiseLayerData(std::string name,std::shared_ptr<TerrainGeneration::Data::NoiseLayer> data);
            /// Sets layer to enabled or disabled
            /// \param newVal Value to set
            void SetEnabled(bool newVal);
            /// Checks if layer is enabled, this is disabled right now TODO: Implement
            /// \return If layer is enabled
            bool IsEnabled() const;
            /// Gets name of layer
            /// \return Name of layer
            std::string GetName() const;
            /// Sets name for noise layer
            /// \param name New name to set
            void SetName(const std::string & name);
            /// Gets raw noise layer data
            /// \return Noise layer data
            std::shared_ptr<TerrainGeneration::Data::NoiseLayer> GetData() const;
            /// Saves layer
            /// \param output Json array of noise layers
            void Save(nlohmann::json & output);
        };
    }
}


#endif //WORLD_GENERATOR_CORE_NOISELAYERDATA_H
