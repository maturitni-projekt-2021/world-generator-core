//
// Created by Matty on 1.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_PROJECTDATA_H
#define WORLD_GENERATOR_CORE_PROJECTDATA_H

#include "ModelData.h"
#include "TextureData.h"
#include "../world/Biome.h"
#include "NoiseLayerData.h"
#include "WorldGenerationData.h"
#include <map>
#include <json.hpp>
#include "../resources/lib/Miniz_zip.hpp"

namespace WorldGenerator {
    namespace Data {
        /// Class containing data about current project
        class ProjectData {
        private:
            /// Unique id of next biome
            uint32_t m_pBiomeID = 0;
            /// Loads model data
            /// \param json Main json object
            void _LoadModelData(nlohmann::json & json);
            /// Saves model data
            /// \param json Main json object
            void _SaveModelData(nlohmann::json & json);
            /// Loads texture data
            /// \param json Main json object
            void _LoadTextureData(nlohmann::json & json);
            /// Saves texture data
            /// \param json Main json object
            void _SaveTextureData(nlohmann::json & json);
            /// Offset of layer name
            uint32_t m_pLayerOffset = 0;
            /// Save archive
            std::shared_ptr<miniz_cpp::zip_file> m_pSaveArchive;
            /// Gets archive with only data needed for saving
            /// \return Clean archive only containing data that is required
            std::shared_ptr<miniz_cpp::zip_file> _GetSaveArchive();
            /// Do we want to write
            bool m_pWantsToWrite = false;
            /// Is active currently accessed
            bool m_pActiveAccess = false;
        public:
            virtual ~ProjectData();
            /// Adds duplicate of other noise layer
            /// \param cpy Which layer to copy
            /// \param placeBefore Before which layer to put this layer
            void AddNoiseLayer(const std::shared_ptr<NoiseLayerData> & cpy, uint32_t placeBefore = 0);
            /// Adds noise layer
            /// \param type Type of layer to add
            /// \param placeBefore Before which layer to put this layer
            void AddNoiseLayer(uint32_t type,uint32_t placeBefore = 0);
            /// Gets unique id for biome
            /// \return Unique id of next biome
            uint32_t GetBiomeID();
            /// Global project data instance
            static std::shared_ptr<ProjectData> m_ProjectData;
            static std::shared_ptr<WorldGenerator::Data::TextureData> m_MissingTexture;
            /// Loads project
            /// \param path Path where is project file
            /// \throws std::exception if load fails
            static void LoadProjectData(const std::string & path);
            /// Creates new project
            /// \param name Name of project
            /// \param path Path where project file will be saved
            static void NewProjectData(const std::string & name, const std::string & path);
            /// Saves project to specified path
            void Save();
            void LoadInternal(nlohmann::json & json);
            /// Adds model to project
            /// \param name Name of model to add
            /// \param path Path to file on disk
            /// \return ModelData in project
            std::shared_ptr<WorldGenerator::Data::ModelData> AddModelToProject(const std::string & name, const std::string & path);
            /// Adds texture to project
            /// \param name Name of texture
            /// \param path Path to texture file on disc
            /// \return TextureData in project
            std::shared_ptr<WorldGenerator::Data::TextureData> AddTextureToProject(const std::string & name, const std::string & path);
            /// Removes model from project FIXME: Does not unbind
            /// \param name Name of model to delete
            void RemoveModel(const std::string & name);
            /// Removes texture from project FIXME: Does not unbind
            /// \param name Name of texture to delete
            void RemoveTexture(const std::string & name);
            /// Get Model data
            std::pair<const void *, size_t> GetModelToMemory(const std::string & path);
            /// Get Texture data
            std::pair<const void *, size_t> GetTextureToMemory(const std::string & path);
            /// Project name
            std::string m_ProjectName;
            /// Project path, used as cache for saving
            std::string m_Path;
            /// World gen seed
            int32_t m_Seed;
            /// Map from name to model data
            std::map<std::string, std::shared_ptr<ModelData>> m_ModelData;
            /// Map from name to texture data
            std::map<std::string, std::shared_ptr<WorldGenerator::Data::TextureData>> m_TextureData;
            //TODO: Add data about nodes etc
            std::pair<std::string, std::shared_ptr<WorldGenerator::Data::ModelData>> m_SelectedModel;
            /// World generation data used currently for generation
            std::shared_ptr<WorldGenerator::Data::WorldGenerationData> m_WorldGenDataActive;
            /// World generation data that are editable, when we submit, there are copied to active data
            std::shared_ptr<WorldGenerator::Data::WorldGenerationData> m_WorldGenDataEditable;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_PROJECTDATA_H
