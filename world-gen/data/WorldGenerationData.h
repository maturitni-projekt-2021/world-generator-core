//
// Created by Matty on 11/11/2020.
//

#ifndef WORLD_GENERATOR_CORE_WORLDGENERATIONDATA_H
#define WORLD_GENERATOR_CORE_WORLDGENERATIONDATA_H

#include "NoiseLayerData.h"
#include "../world/Biome.h"

namespace WorldGenerator {
    namespace Data {
        /// Data used for voxel generation
        class WorldGenerationData {
        public:
            /// Version of this data
            uint32_t m_Version = 0;
            /// Copy constructor
            /// \param cpy Object to copy
            WorldGenerationData(const WorldGenerationData & cpy);
            /// Gets shared_ptr of layer specified by type
            /// \param type Type of layer to get
            /// \return NoiseLayer based on type passed
            std::shared_ptr<TerrainGeneration::Data::NoiseLayer> GetNoiseLayerFromType(uint32_t type);
            WorldGenerationData() = default;
            /// Saves data
            /// \param json Main json object
            void Save(nlohmann::json & json);
            /// Loads data from json
            /// \param json Json object containing data about noise layers
            void Load(nlohmann::json & json);
            /// Swaps two layers
            /// \param first First layer to swap
            /// \param second Second layer to swap
            void SwapLayers(uint32_t first, uint32_t second);
            /// Vector of all noise layers
            std::vector<std::shared_ptr<WorldGenerator::Data::NoiseLayerData>> m_NoiseLayers;
            std::map<uint32_t,std::shared_ptr<WorldGenerator::World::Biome>> m_Biomes;
        };
    }
}


#endif //WORLD_GENERATOR_CORE_WORLDGENERATIONDATA_H
