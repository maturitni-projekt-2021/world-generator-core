//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_RENDERDATA_H
#define WORLD_GENERATOR_CORE_RENDERDATA_H
#include "../render/framebuffers/FBViewport.h"
#include "../render/framebuffers/FBViewportCamera.h"
#include "../render/Shader.h"
#include "../render/Model.h"
#include "../render/Camera.h"

#include <memory>
#include <map>

namespace WorldGenerator {
    namespace Data {
        /// Class holding data that relates to rendering, like models and textures
        class RenderData {
        private:
            /// Next unique model id
            uint32_t m_pModelID = 0;
            /// Next unique texture id
            uint32_t m_pTextureID = 0;
        public:
            RenderData();
            /// Gets next unique id for model unit
            /// \return unique id for model
            uint32_t GetModelID() { return m_pModelID++;};
            /// Gets next unique id for texture unit
            /// \return unique id for texture unit
            uint32_t GetTextureID() { return m_pTextureID++;};
            /// Global instance of render data
            static std::unique_ptr<RenderData> m_RenderData;
            /// Map of loaded custom models
            std::map<uint32_t, std::shared_ptr<Render::Model>> m_LoadedCustomModels;
            /// Vector of custom shaders loaded by user NOT SUPPORTED YET
            std::vector<std::shared_ptr<Render::Shader>> m_LoadedCustomShaders;

            /// Framebuffer of final viewport showed to user
            std::shared_ptr<Render::FBViewport> m_FBViewport;
            /// Main viewport camera target framebuffer
            std::shared_ptr<Render::FBViewportCamera> m_ViewportCameraFB;
            /// Main viewport camera
            std::shared_ptr<Render::Camera> m_ViewportCamera;
            /// Basic model shader with instancing support
            std::shared_ptr<Render::Shader> m_ModelShader;
            /// Terrain shader
            std::shared_ptr<Render::Shader> m_TerrainShader;
#ifndef NDEBUG
            /// Shader for debug stuff
            std::shared_ptr<Render::Shader> m_DebugShader;
#endif
            /// If viewport is hovered
            bool m_ViewportHovered = false;
            /// If viewport is locked for controls
            bool m_ViewportLocked = false;
            /// If view mode should be set to wireframe
            bool m_ViewWireframe = false;
            /// Antialiasing level, with 0 it is disabled
            uint32_t m_AntiAliasingLevel = 0;
            /// Max instances per draw call with instance batching
            uint32_t m_MaxInstancesPerCall = 64;
        };
    }
}

#endif //WORLD_GENERATOR_CORE_RENDERDATA_H