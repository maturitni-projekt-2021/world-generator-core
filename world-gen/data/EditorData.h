//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_EDITORDATA_H
#define WORLD_GENERATOR_CORE_EDITORDATA_H

#include "../node_editor/canvases/VoxelEditorCanvas.h"
#include "../node_editor/canvases/PopulationEditorCanvas.h"

namespace WorldGenerator {
    namespace Data {
        /// Class containing data about all editors
        class EditorData {
        public:
            EditorData();
            /// Global instance of editor data
            static std::unique_ptr<EditorData> m_EditorData;
            /// Voxel canvas
            std::shared_ptr<NodeEditor::Canvases::VoxelEditorCanvas> m_VoxelCanvas;
            /// Population canvas
            std::shared_ptr<NodeEditor::Canvases::PopulationEditorCanvas> m_PopulationCanvas;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_EDITORDATA_H
