//
// Created by Matty on 5.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_TEXTUREDATA_H
#define WORLD_GENERATOR_CORE_TEXTUREDATA_H

#include "../render/Texture.h"
#include <string>
#include <memory>

namespace WorldGenerator {
    namespace Data {
        /// Struct holding data about texture
        struct TextureData {
            TextureData() = default;
          /// Name of texture
          std::string m_Name;
          /// Path to texture file
          std::string m_Path;
          /// If texture is loaded
          bool m_TextureLoaded = false;
          /// If load failed
          bool m_LoadFailed = false;
          /// Container for error message
          std::string m_ErrorText;
          /// Texture unit on gpu
          std::shared_ptr<WorldGenerator::Render::Texture> m_Texture;
        };
    }
}

#endif //WORLD_GENERATOR_CORE_TEXTUREDATA_H
