//
// Created by Matty on 6.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_EDITORIDS_H
#define WORLD_GENERATOR_CORE_EDITORIDS_H

#include <memory>

namespace WorldGenerator {
    namespace Data {
        /// Class containing data about ids for node editor
        class EditorIds {
        private:
            /// Next unique id for node
            uint32_t m_pNodeID = 0;
            /// Next unique id for connection
            uint32_t m_pConnectionID = 0;
        public:
            /// Global instance of editor ids
            static std::unique_ptr<EditorIds> m_IDs;
            /// Gets unique id for node
            /// \return unique id for node
            uint32_t GetID();
            /// Gets unique id for connection
            /// \return unique id for connection
            uint32_t GetConnectionID();
            /// Clear all editor ids
            void Flush();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_EDITORIDS_H
