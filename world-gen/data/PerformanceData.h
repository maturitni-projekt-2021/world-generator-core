//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_PERFORMANCEDATA_H
#define WORLD_GENERATOR_CORE_PERFORMANCEDATA_H

namespace WorldGenerator {
    namespace Data {
        /// Contains data about performance
        class PerformanceData final {
        private:
            /// Works as switch so that every other is saved
            bool m_pEveryOther = false;
            /// Offset to buffer
            uint32_t m_pOffset = 0;
            /// Amount of frame times saved
            uint32_t m_frameTimeCount = 256;
            /// If frame data storage if full
            bool m_pFrameDataFilled = false;
            /// Vector of frame time data
            std::vector<float> m_pFrameTimeStorage;
        public:
            PerformanceData();
            /// Global instance of performance data
            static std::unique_ptr<PerformanceData> m_PerformanceData;
            /// Adds new data to circular buffer
            /// \param newFrameTime Data to add
            void AddFrameTime(float newFrameTime);
            /// Gets direct pointer to data in storage
            /// \return Direct pointer to data in vector
            const float* GetFrameTimeStorage() const;
            /// Gets amount of frame times stored in buffer
            /// \return Amount of frame times in buffer
            uint32_t GetFrameTimeStorageValueCount() const;
            /// Gets storage offset
            /// \return Storage offset of frame time buffer
            uint32_t GetFrameTimeStorageOffset() const;

            /// Fps count
            uint32_t m_FPS = 0;
            /// Ticks per second
            uint32_t m_TPS = 0;
            /// Amount of requested chunks that should be eventually loaded to world
            uint32_t m_RequestedChunks = 0;
            /// Total amount of created chunks
            uint32_t m_FinishedChunks = 0;
            /// Draw calls in world
            uint32_t m_DrawCalls = 0;
            /// Currently loaded chunks, increased in contructor of chunk and decreased in destructor
            uint32_t m_LoadedChunks = 0;
            /// Right now active chunk generators
            std::atomic_uint32_t m_WorkingChunkGenerators = 0;
            /// Chunks filled with same voxel value, these chunks are not loaded and populated
            std::atomic_uint32_t m_GeneratedFilledChunks = 0;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_PERFORMANCEDATA_H
