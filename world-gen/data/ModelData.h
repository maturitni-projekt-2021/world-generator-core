//
// Created by Matty on 1.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_MODELDATA_H
#define WORLD_GENERATOR_CORE_MODELDATA_H

#include "../render/Model.h"
#include <string>

namespace WorldGenerator {
    namespace Render {
        class Model;
    }

    namespace Data {
        /// Class containing data about model
        struct ModelData {
            ModelData() { m_Models.reserve(4); }
            /// Name of model
            std::string m_Name;
            /// Path to model data
            std::string m_Path;
            /// If model is loaded
            bool m_ModelLoaded = false;
            /// If load failed
            bool m_LoadFailed = false;
            /// Container for error message
            std::string m_ErrorText;
            /// Meshes loaded under model
            std::vector<std::shared_ptr<WorldGenerator::Render::Model>> m_Models;
            /// Textures that are bound to models, these are mainly used for loading
            std::map<uint32_t,std::string> m_ModelTextures;
          };
    }
}
#endif //WORLD_GENERATOR_CORE_MODELDATA_H
