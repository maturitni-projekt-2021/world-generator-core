//
// Created by Matty on 10/24/2020.
//

#ifndef WORLD_GENERATOR_CORE_SETTINGSDATA_H
#define WORLD_GENERATOR_CORE_SETTINGSDATA_H

#define CONCATENATE(e1, e2) e1 ## e2
#define WG_SETTING_SOURCE_INTERNAL(PREFIX, NAME, DEF_VAL) m_pSettings.emplace_back(CONCATENATE(PREFIX,NAME));

#define WG_SETTING_FLOAT_HEADER_INTERNAL(PREFIX, NAME, DEF_VAL) std::shared_ptr<SettingFloat> CONCATENATE(PREFIX,NAME) = std::make_shared<SettingFloat>(#NAME,DEF_VAL);
#define WG_SETTING_INT_HEADER_INTERNAL(PREFIX, NAME, DEF_VAL) std::shared_ptr<SettingInt> CONCATENATE(PREFIX,NAME) = std::make_shared<SettingInt>(#NAME,DEF_VAL);
#define WG_SETTING_STRING_HEADER_INTERNAL(PREFIX, NAME, DEF_VAL) std::shared_ptr<SettingString> CONCATENATE(PREFIX,NAME) = std::make_shared<SettingString>(#NAME,DEF_VAL);
#define WG_SETTING_BOOL_HEADER_INTERNAL(PREFIX, NAME, DEF_VAL) std::shared_ptr<SettingBool> CONCATENATE(PREFIX,NAME) = std::make_shared<SettingBool>(#NAME,DEF_VAL);

#define WG_SETTING_FLOAT_HEADER(NAME, DEF_VAL) WG_SETTING_FLOAT_HEADER_INTERNAL(m_, NAME, DEF_VAL);
#define WG_SETTING_INT_HEADER(NAME, DEF_VAL) WG_SETTING_INT_HEADER_INTERNAL(m_, NAME, DEF_VAL);
#define WG_SETTING_STRING_HEADER(NAME, DEF_VAL) WG_SETTING_STRING_HEADER_INTERNAL(m_, NAME, DEF_VAL);
#define WG_SETTING_BOOL_HEADER(NAME, DEF_VAL) WG_SETTING_BOOL_HEADER_INTERNAL(m_, NAME, DEF_VAL);
#define WG_SETTING_SOURCE(NAME) WG_SETTING_SOURCE_INTERNAL(m_, NAME, DEF_VAL);


#include <cpptoml.h>

namespace WorldGenerator {
    namespace Data {
        class Setting {
        public:
            std::string m_Name;
            virtual void Load(const std::shared_ptr<cpptoml::table> & toml) = 0;
            virtual void Save(const std::shared_ptr<cpptoml::table> & toml) = 0;
            Setting(std::string mName);
        };
        class SettingFloat : public Setting {
        public:
            float m_Value;
            void Load(const std::shared_ptr<cpptoml::table> &toml) override;
            void Save(const std::shared_ptr<cpptoml::table> &toml) override;
            SettingFloat(const std::string &mName, float mValue);
        };
        class SettingInt : public Setting {
        public:
            int32_t m_Value;
            void Load(const std::shared_ptr<cpptoml::table> &toml) override;

            void Save(const std::shared_ptr<cpptoml::table> &toml) override;

            SettingInt(const std::string &mName, int32_t mValue);
        };
        class SettingString : public Setting {
        public:
            std::string m_Value;
            void Load(const std::shared_ptr<cpptoml::table> &toml) override;

            void Save(const std::shared_ptr<cpptoml::table> &toml) override;

            SettingString(const std::string &mName, std::string  mValue);
        };
        class SettingBool : public Setting {
        public:
            bool m_Value;

            void Load(const std::shared_ptr<cpptoml::table> &toml) override;

            void Save(const std::shared_ptr<cpptoml::table> &toml) override;

            SettingBool(const std::string &mName, bool mValue);
        };

        class SettingsData {
        private:
            std::vector<std::shared_ptr<Setting>> m_pSettings;
        public:
            SettingsData();

            /// Global instance of Settings Data
            static std::unique_ptr<SettingsData> m_SettingsData;
            void Load();
            void Save();

            WG_SETTING_BOOL_HEADER(EnableAntiAliasing,false)
            WG_SETTING_INT_HEADER(AntiAliasingLevel, 0)
            WG_SETTING_FLOAT_HEADER(NoiseOctaves,1.0f)
            WG_SETTING_FLOAT_HEADER(DebrisMaxRenderDistance,5.0f)
            WG_SETTING_INT_HEADER(ChunkGenDistance,5)
            WG_SETTING_INT_HEADER(Seed,9999)
            WG_SETTING_INT_HEADER(Quality,1)
            WG_SETTING_INT_HEADER(MaxLoadedChunks,2048)
            WG_SETTING_INT_HEADER(TerrainGenerationEngine,0)
            WG_SETTING_BOOL_HEADER(MovingCam,false);
        };
    }
}


#endif //WORLD_GENERATOR_CORE_SETTINGSDATA_H
