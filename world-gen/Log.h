//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_LOG_H
#define WORLD_GENERATOR_CORE_LOG_H

#define WG_LOG_INFO(text_char_ptr,args...) WorldGenerator::GlobalLogWrapper::Log("[INFO]",text_char_ptr)
#define WG_LOG_DEBUG(text_char_ptr,args...) WorldGenerator::GlobalLogWrapper::Log("[DEBUG]",text_char_ptr)
#define WG_LOG_WARN(text_char_ptr,args...) WorldGenerator::GlobalLogWrapper::Log("[WARN]",text_char_ptr)
#define WG_LOG_ERROR(text_char_ptr,args...) WorldGenerator::GlobalLogWrapper::Log("[ERROR]",text_char_ptr)

namespace WorldGenerator {
    /// Global wrapper around log class, to minimize include count
    class GlobalLogWrapper {
    public:
        /// Log function, used to show log in app console
        /// \param header Header of log message
        /// \param text Text of log message
        static void Log(const char* header, const char* text);
    };
}

#endif //WORLD_GENERATOR_CORE_LOG_H
