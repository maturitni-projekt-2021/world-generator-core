//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_MODEL_H
#define WORLD_GENERATOR_CORE_MODEL_H

#include "custom/ModelVertex.h"
#include "VertexBuffer.h"
#include "Shader.h"
#include "Texture.h"
#include "../data/TextureData.h"

namespace WorldGenerator {
    namespace Render {
        class Model {
        protected:
            glm::mat4 m_pBaseOffset;

            std::string m_pName;

            uint32_t m_pID;
            std::shared_ptr<Shader> m_pShader;
            std::shared_ptr<VertexBuffer> m_pVertexBuffer;

            bool m_pHasAlbedoTexture = false;
            std::shared_ptr<WorldGenerator::Data::TextureData> m_pTextureAlbedo;

            bool m_pHasNormalTexture = false;
            std::shared_ptr<WorldGenerator::Data::TextureData> m_pTextureNormal;

            bool m_pHasRoughnessTexture = false;
            std::shared_ptr<WorldGenerator::Data::TextureData> m_pTextureRoughness;

            bool m_pHasCustomShader = false;
            std::shared_ptr<Shader> m_pCustomShader;
        public:
            /// Sets name
            /// \param name Name to set
            void SetName(const std::string &name) { m_pName = name; }

            std::string GetName() const { return m_pName; }

            glm::mat4 GetBaseOffset() const { return m_pBaseOffset; }

            void SetBaseOffset(const glm::mat4 &offset) { m_pBaseOffset = offset; };

            Model(uint32_t m_pID, uint32_t VAO = -1, uint32_t VBO = -1, uint32_t EBO = -1, bool buildInstVBO = false);

            std::shared_ptr<VertexBuffer> GetVertexBuffer() { return m_pVertexBuffer; };

            uint32_t GetID() const { return m_pID; }

            void Render(const glm::mat4 &projection, const glm::mat4 &viewMatrix, const glm::mat4 &model);

            void RenderBatched(const glm::mat4 &projectionMatrix, const glm::mat4 &viewMatrix,
                               std::vector<glm::mat4> &modelMatrices);

            void SetCustomShader(const std::shared_ptr<Shader> &customShader);

            void SetAlbedoTexture(const std::shared_ptr<WorldGenerator::Data::TextureData> &texture);

            void SetNormalTexture(const std::shared_ptr<WorldGenerator::Data::TextureData> &texture);

            void SetRoughnessTexture(const std::shared_ptr<WorldGenerator::Data::TextureData> &texture);

            bool HasAlbedoTexture() const;

            bool HasNormalTexture() const;

            bool HasRoughnessTexture() const;

            std::shared_ptr<WorldGenerator::Data::TextureData> GetAlbedoTexture() const;

            std::shared_ptr<WorldGenerator::Data::TextureData> GetNormalTexture() const;

            std::shared_ptr<WorldGenerator::Data::TextureData> GetRoughnessTexture() const;

            void ClearAlbedoTexture();

            void ClearNormalTexture();

            void ClearRoughnessTexture();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_MODEL_H
