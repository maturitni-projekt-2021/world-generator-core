//
// Created by Matty on 1.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_DRAWBATCH_H
#define WORLD_GENERATOR_CORE_DRAWBATCH_H

#include <glm/glm.hpp>
#include "Camera.h"
#include "../world/Terrain.h"
#include "../world/WorldChunkPos.h"

#include <map>
namespace WorldGenerator {
    namespace World {
        class Terrain;
    }

    namespace Render {
        class DrawBatch final {
        private:
            /// Map from model loaded ID to model matrix
            std::map<uint32_t, std::vector<glm::mat4>> m_pBatchData;
            /// Map from model loaded ID to count
            std::map<uint32_t, uint32_t> m_pBatchCount;
            /// Vector of chunks to render
#ifndef NDEBUG
            std::vector<std::pair<std::pair<Render::Culling::CullCollider*,std::shared_ptr<World::Terrain>>,std::shared_ptr<Debug::DebugLineStrip>>> m_pTerrain;
#else
            std::vector<std::pair<Render::Culling::CullCollider*,std::shared_ptr<World::Terrain>>> m_pTerrain;
#endif
            /// chunk position check if only one pos is rendered
            std::set<WorldGenerator::World::WorldChunkPos> m_pTerrainPositions;
        public:
            void RenderBatch(std::vector<std::shared_ptr<Render::Camera>>& cameras);
            void AddModelTransform(uint32_t id, const glm::mat4& transform);
#ifndef NDEBUG
            void AddTerrain(const std::shared_ptr<World::Terrain> & terrain,Culling::CullCollider* coll,const std::shared_ptr<Debug::DebugLineStrip> & debugLineStrip);
#else
            void AddTerrain(const std::shared_ptr<World::Terrain> & terrain,Render::Culling::CullCollider* coll);
#endif
            void Empty();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_DRAWBATCH_H
