//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_TEXTURE_H
#define WORLD_GENERATOR_CORE_TEXTURE_H

#include "GraphicID.h"

namespace WorldGenerator {
    namespace Render {

        enum TextureColorType : uint32_t {
          RED = 0x1903,
          GREEN = 0x1904,
          BLUE = 0x1905,
          ALPHA = 0x1906,
          RGB = 0x1907,
          RGBA = 0x1908,
          R3_G3_B2 = 0x2A10,
          RGB4 = 0x804F,
          RGB5 = 0x8050,
          RGB8 = 0x8051,
          RGB10 = 0x8052,
          RGB12 = 0x8053,
          RGB16 = 0x8054,
          RGBA2 = 0x8055,
          RGBA4 = 0x8056,
          RGB5_A1 = 0x8057,
          RGBA8 = 0x8058,
          RGB10_A2 = 0x8059,
          RGBA12 = 0x805A,
          RGBA16 = 0x805B
        };

        enum TextureType : uint32_t {
            TEXTURE_2D = 0x0DE1,
            TEXTURE_2D_MULTISAMPLE = 0x9100
        };

        class Texture {
        private:
            std::shared_ptr<TextureID> m_pID;
            TextureType m_pType;
            TextureColorType m_pColor;
            uint32_t m_pWidth, m_pHeight;
            std::string m_pName;
        public:
            Texture(uint32_t width, uint32_t height, TextureColorType color, bool usesLinearSampling = true,TextureType type = TEXTURE_2D);
            std::shared_ptr<TextureID> GetID() { return m_pID; };
            void SetName(const std::string & name);
            std::string GetName() const;
            void BindTexture();
            void UploadData(uint8_t * data);
        };
    }
}
#endif //WORLD_GENERATOR_CORE_TEXTURE_H
