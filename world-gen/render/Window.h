//
// Created by Matty on 10.4.2020.
//

#ifndef LEIKO_WINDOW_H
#define LEIKO_WINDOW_H

#include "Renderer.h"
#include "WindowDestructor.h"
#include "../glad.h"

#include <glm/glm.hpp>
#include <memory>

namespace WorldGenerator {
    namespace Render {

        class Renderer;

/// Window class, result of window builder object, containing data about window
        class Window final {
        private:
            /// Background color
            glm::vec4 m_pBackgroundColor;
            /// Window name
            std::string m_pWindowName;
            /// GLFW Window object
            std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor> m_pGLFWWindow;
            /// Window dimensions
            static uint32_t m_pWindowWidth, m_pWindowHeight;
        public:
            /// Gets window width
            /// \return Width of window
            static uint32_t GetWindowWidth() { return m_pWindowWidth; };
            /// Gets window height
            /// \return Height of window
            static uint32_t GetWindowHeight() { return m_pWindowHeight; };
            friend class WindowBuilder;
            /// Function called before rendering
            void PreRender();
            /// Function called after rendering
            void FinalizeRender();
            Window() = default;
            /// Gets window name
            /// \return Window name
            const std::string& GetWindowName();
            /// Returns GLFW window ptr
            /// \return GLFWWindow
            std::unique_ptr<GLFWwindow, WorldGenerator::Render::GLFWWindowDestructor>& GetGlfwWindow();
            /// Resize callback for window
            /// \param window Window object
            /// \param width new Width of window
            /// \param height new Height of window
            void FbSizeCallback(GLFWwindow* window, int32_t width, int32_t height);
        };

        class WindowBuilder : public std::enable_shared_from_this<WindowBuilder> {
        public:
            WindowBuilder(uint32_t width, uint32_t height);
            std::shared_ptr<WindowBuilder> setWindowName(const std::string& name);
            std::shared_ptr<WindowBuilder> setBackgroundColor(const glm::vec4& bgColor);
            WorldGenerator::Render::Window* finish();
        private:
            bool m_pHasColor = false;
            glm::vec4 m_pBackgroundColor;
            std::unique_ptr<Renderer> m_pRenderer;
            bool m_pHasName = false;
            std::string m_pWindowName;
            bool m_pHasDecorations = false;
            uint32_t m_pWidth, m_pHeight = 0;
            static void APIENTRY glDebugOutput(GLenum source, GLenum type, unsigned int id, GLenum severity,
                                        GLsizei length, const char *message, const void *userParam);
        };

    }
}
#endif //LEIKO_WINDOW_H
