//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_GRAPHICID_H
#define WORLD_GENERATOR_CORE_GRAPHICID_H

#include <cstdint>
namespace WorldGenerator {
    namespace Render {
        class GraphicID {
        protected:
            uint32_t m_pID;
        public:
            explicit GraphicID(uint32_t id)
                    :m_pID(id) { }
            virtual ~GraphicID() { };
            uint32_t GetID() const { return m_pID; }
        };

        class TextureID final : public GraphicID {
        public:
            explicit TextureID(uint32_t id)
                    :GraphicID(id) { };
            operator uint32_t() const { return m_pID; };
            ~TextureID() override;
        };

    };
}

#endif //WORLD_GENERATOR_CORE_GRAPHICID_H
