//
// Created by Matty on 4.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_RENDERTASK_H
#define WORLD_GENERATOR_CORE_RENDERTASK_H

#include "Model.h"
#include "../world/Terrain.h"
#include "../world/WorldChunkPos.h"

#include <glm/glm.hpp>

namespace WorldGenerator {
    namespace Render {
        enum RenderTaskDependancy {
            WG_RENDER_TASK_DEPENDENT,
            WG_RENDER_TASK_INDEPENDENT
        };

        class RenderTask {
        private:
            bool m_pDone = false;
        public:
            bool GetDone() const { return m_pDone; }
            void Drop() { m_pDone = true;}
            virtual void Process() = 0;

            virtual RenderTaskDependancy GetDependancy() const { return RenderTaskDependancy::WG_RENDER_TASK_DEPENDENT; }
        protected:
            void Finish() { m_pDone = true;}
        };

        class RenderTaskUploadModelData final : public RenderTask {
        private:
            std::vector<float> m_pVertices;
            std::vector<uint32_t> m_pIndices;
            uint32_t m_pElementCount;
            glm::mat4 m_pTransform;
            std::shared_ptr<Model> m_pResult;
            std::string m_pName;
        public:
            std::shared_ptr<Model> GetResult();
            RenderTaskUploadModelData(std::vector<float> vertices,std::vector<uint32_t> indices,uint32_t elementCount,const glm::mat4& tfm,const std::string & name);
            void Process() override;
        };

        class RenderTaskUploadTextureData final : public RenderTask {
        private:
            uint8_t * m_pTextureData;
            int32_t m_pWidth;
            int32_t m_pHeight;
            WorldGenerator::Render::TextureColorType m_pColorType;
            std::shared_ptr<Texture> m_pResult;
        public:
            std::shared_ptr<Texture> GetResult();
            RenderTaskUploadTextureData(uint8_t * textureData, int32_t width, int32_t height,WorldGenerator::Render::TextureColorType colorType);
            void Process() override;
        };

        class RenderTaskUploadTerrainData final : public RenderTask {
        private:
            WorldGenerator::World::WorldChunkPos m_pPosition;
            std::vector<float>& m_pVertices;
            uint32_t m_pElementCount;
        public:
            std::shared_ptr<World::Terrain> m_Result;
            std::shared_ptr<World::Terrain> GetResult();
            RenderTaskUploadTerrainData(const WorldGenerator::World::WorldChunkPos& position, std::vector<float>& vertices, uint32_t elementCount);
            void Process() override;
        };

        class RenderTaskDropRenderBuffers final : public RenderTask {
        private:
            std::shared_ptr<WorldGenerator::Render::VertexBuffer> m_pBuffer;
        public:
            explicit RenderTaskDropRenderBuffers(std::shared_ptr<WorldGenerator::Render::VertexBuffer>  data);
            void Process() override;
            RenderTaskDependancy GetDependancy() const override;
        };
    }
}
#endif //WORLD_GENERATOR_CORE_RENDERTASK_H
