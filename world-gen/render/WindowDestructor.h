//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_WINDOWDESTRUCTOR_H
#define WORLD_GENERATOR_CORE_WINDOWDESTRUCTOR_H

#include <GLFW/glfw3.h>

namespace WorldGenerator {
    namespace Render {
/// Destructor for GLFW windows object
        struct GLFWWindowDestructor {
          void operator()(GLFWwindow* ptr)
          {
              glfwDestroyWindow(ptr);
          }
        };
    }
}
#endif //WORLD_GENERATOR_CORE_WINDOWDESTRUCTOR_H
