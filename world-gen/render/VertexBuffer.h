//
// Created by Matty on 7.5.2020.
//

#ifndef WG_VERTEXBUFFER_H
#define WG_VERTEXBUFFER_H

#include <cstdint>
#include "../Exceptions.h"

namespace WorldGenerator {
    namespace Render {
        enum DrawType : uint32_t {
          STREAM_DRAW = 0x88E0,
          STREAM_READ = 0x88E1,
          STREAM_COPY = 0x88E2,
          STATIC_DRAW = 0x88E4,
          STATIC_READ = 0x88E5,
          STATIC_COPY = 0x88E6,
          DYNAMIC_DRAW = 0x88E8,
          DYNAMIC_READ = 0x88E9,
          DYNAMIC_COPY = 0x88EA
        };

        class VertexBuffer {
        private:
            uint32_t m_pGlVao, m_pGlVbo, m_pGlEbo;
            uint32_t m_pGlInstanceVbo;
            bool m_pHasSize = false;
            uint32_t m_pElementCount;
            static std::set<uint32_t> s_pUsedIds;
            static std::set<uint32_t> s_pFilledIds;
            bool m_pDeleted = false;
        public:
            uint32_t getVaoID() const { return m_pGlVao; }
            bool bind();
            void setVertexData(uint32_t size, float* data, int32_t drawType);
            void setInstancedData(uint32_t size, float* data);
            void setIndexData(uint32_t size, uint32_t* data, int32_t drawType);
            void setElementCount(uint32_t count)
            {
                m_pHasSize = true;
                m_pElementCount = count;
            }
            uint32_t getElementCount()
            {
                if (m_pHasSize) { return m_pElementCount; }
#ifndef NDEBUG
                else
                    throw WorldGenerator::Exceptions::WGException(
                            std::string(__FILE__)+std::string(" : Line: ")+std::to_string(__LINE__)
                                    +std::string(" : Requested element count from VertexBuffer without size specified"),
                            "Vertex Buffer");
#else
                else throw WorldGenerator::Exceptions::WGException(std::string("Requested element count from VertexBuffer without size specified"),
                            "Vertex Buffer");
#endif
            }
            VertexBuffer(uint32_t VAO = -1,uint32_t VBO = -1,uint32_t EBO = -1,bool buildInstVBO = false);
            ~VertexBuffer();
        };
    }
}
#endif //WG_VERTEXBUFFER_H
