//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_CULLCOLLIDERBOX_H
#define WORLD_GENERATOR_CORE_CULLCOLLIDERBOX_H


#include "CullCollider.h"

namespace WorldGenerator {
    namespace Render {
        namespace Culling {
            /// Cull box collider, Axis aligned
            class CullColliderBox : public CullCollider {
            private:
                /// Scale of box collider
                glm::vec3 m_pScale;
            public:
                /// Constructor
                /// \param position Initial position of cull collider
                /// \param scale Scale of cull collider
                CullColliderBox(const glm::vec3 &position, const glm::vec3 &scale);
            private:
                bool IsVisible(const FrustumSide &topSide, const FrustumSide &bottomSide, const FrustumSide &leftSide,
                               const FrustumSide &rightSide, const FrustumSide &nearSide,
                               const FrustumSide &farSide) const override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CULLCOLLIDERBOX_H
