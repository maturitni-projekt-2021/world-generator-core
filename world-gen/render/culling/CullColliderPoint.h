//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_CULLCOLLIDERPOINT_H
#define WORLD_GENERATOR_CORE_CULLCOLLIDERPOINT_H

#include "CullCollider.h"

namespace WorldGenerator {
    namespace Render {
        namespace Culling {
            class CullColliderPoint : public CullCollider {
            private:
            public:
                /// Constructor
                /// \param position Initial position of cull collider
                /// \param scale Scale of cull collider
                CullColliderPoint(const glm::vec3 &position);

            private:
                bool IsVisible(const FrustumSide &topSide, const FrustumSide &bottomSide, const FrustumSide &leftSide,
                               const FrustumSide &rightSide, const FrustumSide &nearSide,
                               const FrustumSide &farSide) const override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CULLCOLLIDERPOINT_H
