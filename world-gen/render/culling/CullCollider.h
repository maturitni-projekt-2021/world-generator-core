//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_CULLCOLLIDER_H
#define WORLD_GENERATOR_CORE_CULLCOLLIDER_H


#include "FrustumSide.h"

namespace WorldGenerator {
    namespace Render {
        namespace Culling {
            class CullCollider {
            protected:
                /// Position of box collider
                glm::vec3 m_pPosition;
            public:
                CullCollider(const glm::vec3 &position);
                virtual bool IsVisible(const FrustumSide &topSide, const FrustumSide &bottomSide, const FrustumSide &leftSide,
                               const FrustumSide &rightSide, const FrustumSide &nearSide, const FrustumSide &farSide) const = 0;
                /// Sets position of collider in world space
                /// \param pos New position to set
                void SetPosition(const glm::vec3 & pos);
                const glm::vec3 & GetPosition() const;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CULLCOLLIDER_H
