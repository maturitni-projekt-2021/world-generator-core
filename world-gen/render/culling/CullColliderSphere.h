//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_CULLCOLLIDERSPHERE_H
#define WORLD_GENERATOR_CORE_CULLCOLLIDERSPHERE_H

#include "CullCollider.h"

namespace WorldGenerator {
    namespace Render {
        namespace Culling {
            class CullColliderSphere : public CullCollider {
            private:
                /// Radius of sphere
                float m_pRadius;
            public:
                CullColliderSphere(const glm::vec3 &position, float radius);

                bool IsVisible(const FrustumSide &topSide, const FrustumSide &bottomSide, const FrustumSide &leftSide,
                               const FrustumSide &rightSide, const FrustumSide &nearSide,
                               const FrustumSide &farSide) const override;
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CULLCOLLIDERSPHERE_H
