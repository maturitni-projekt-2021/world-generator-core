//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_CAMERAFRUSTUM_H
#define WORLD_GENERATOR_CORE_CAMERAFRUSTUM_H

#include <glm/glm.hpp>
#include "CullCollider.h"
#include "FrustumSide.h"
#ifndef NDEBUG
#include "../debug/DebugLineStrip.h"
#endif
namespace WorldGenerator {
    namespace Render {
        namespace Culling {
            class CameraFrustum {
            private:
                FrustumSide m_pSides[6];
                float m_pFOV;
                float m_pScreenRatio;
                float m_pNearDist;
                float m_pFarDist;

                float m_pNearHeight;
                float m_pNearWidth;
                float m_pFarHeight;
                float m_pFarWidth;
#ifndef NDEBUG
                std::unique_ptr<Render::Debug::DebugLineStrip> m_FrustumDebug;
#endif
            public:
                CameraFrustum& operator=(const CameraFrustum& other);
                CameraFrustum(float fov, float screenRatio, float nearDist, float farDist);
                void SetInternals(float fov, float screenRatio, float nearDist, float farDist);
                void SetTransform(const glm::vec3 & target, const glm::vec3 & pos, const glm::vec3 & up);
                bool ContainsCollider(const std::shared_ptr<CullCollider> & collider);
                bool ContainsCollider(const CullCollider & collider);

#ifndef NDEBUG
                void RenderFrustumDebug(const glm::mat4 & view, const glm::mat4 & proj);
                void GenerateDebugFrustrumLines(const glm::vec3 &target, const glm::vec3 & pos, const glm::vec3 & up);
#endif
            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_CAMERAFRUSTUM_H
