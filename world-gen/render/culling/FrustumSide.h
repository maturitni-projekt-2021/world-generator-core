//
// Created by Matty on 11/1/2020.
//

#ifndef WORLD_GENERATOR_CORE_FRUSTUMSIDE_H
#define WORLD_GENERATOR_CORE_FRUSTUMSIDE_H

#include <glm/glm.hpp>

namespace WorldGenerator {
    namespace Render {
        namespace Culling {
            enum SideType {
                SIDE_TOP = 0,
                SIDE_BOTTOM,
                SIDE_LEFT,
                SIDE_RIGHT,
                SIDE_NEAR_PLANE,
                SIDE_FAR_PLANE
            };

            class FrustumSide {
            private:
                glm::vec3 m_pNormal;
                glm::vec3 m_pPos;
            public:
                const glm::vec3 &GetNormal() const;

                const glm::vec3 &GetPos() const;

                void SetNormalAndPoint(const glm::vec3 & normal, const glm::vec3 & pos);

                bool InFront(const glm::vec3 & point) const;

                FrustumSide operator=(const FrustumSide & side);
            };
        }
    }
}

#endif //WORLD_GENERATOR_CORE_FRUSTUMSIDE_H
