//
// Created by Matty on 7.5.2020.
//

#ifndef WG_VERTEX_H
#define WG_VERTEX_H

#include <cstdint>
#include <vector>
#include <glm/glm.hpp>

namespace WorldGenerator {
    namespace Render {

        struct VertexLayoutComponent {
        public:
            friend class VertexLayout;
            void setAttributePointer(uint32_t& index, uint32_t& startingOffset, uint32_t totalSize);
            VertexLayoutComponent(uint32_t size)
                    :m_pSize(size) { }
        private:
            uint32_t m_pSize;
        };

        class VertexLayout {
        private:
            std::vector<VertexLayoutComponent> m_pComponents;
            uint32_t m_pTotalSize = 0;
        public:
            explicit VertexLayout(uint32_t componentCount)
            {
                m_pComponents.reserve(componentCount);
            }
            void addAttributeComponent(VertexLayoutComponent& component);
            void setAttributePointers();
        };

        class Vertex {
        protected:
            std::vector<float> m_pData;
        public:
            std::vector<float>& data() { return m_pData; }
            virtual uint32_t getFloatCount() = 0;
            virtual VertexLayout getVertexLayout() = 0;
        };
    }
}
#endif //WG_VERTEX_H
