//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_CAMERA_H
#define WORLD_GENERATOR_CORE_CAMERA_H

#include <glm/glm.hpp>
#include "FrameBuffer.h"
#include "culling/CameraFrustum.h"

namespace WorldGenerator {
    namespace Render {
        class Camera {
        private:
            glm::vec3 m_pPosition = glm::vec3(0.0f, 0.0f, 0.0f);
            glm::vec3 m_pPositionDestination = glm::vec3(0.0f, 0.0f, 0.0f);
            glm::vec3 m_pRotation = glm::vec3(0.0f);
            glm::vec3 m_pTargetPos = glm::vec3(0.0f);
            float m_pNearPlane, m_pFarPlane;
            float m_pFOV;
            float m_pScreenRatio;
            bool m_pSmoothCam = false;
            std::shared_ptr<FrameBuffer> m_pTargetBuffer;
            Culling::CameraFrustum m_pFrustum;
        public:
            Camera(float fov, float nearPlane, float farPlane, float screenRatio,std::shared_ptr<FrameBuffer> target,bool smoothCam = false);
            void BindTargetBuffer();
            glm::vec3 GetPosition() const {return m_pPosition;}
            glm::vec3 GetRotation() const {return m_pRotation;}
            glm::vec3 GetForward() const;
            glm::vec3 GetRight() const;
            virtual void SetPosition(const glm::vec3& position);
            void SetRotation(const glm::vec3& rotation);
            void SetTarget(const glm::vec3& position);
            void SetFOV(float newFov);
            void SetScreenRatio(float newScreenRatio);
            void SetTargetFromRotation();
            glm::mat4 GetViewMatrix() const;
            glm::mat4 GetProjectionMatrix() const;
            void PostProcess();
            void Update();
            bool InFrustum(const std::shared_ptr<Culling::CullCollider> & collider);
            bool InFrustum(const Culling::CullCollider & collider);
            void ApplyFrustum();
#ifndef NDEBUG
            void DrawDebugFrustum();
            void GenerateDebugFrustumLines();
#endif
        };
    }
}
#endif //WORLD_GENERATOR_CORE_CAMERA_H
