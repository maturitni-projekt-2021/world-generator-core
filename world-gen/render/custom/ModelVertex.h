//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_MODELVERTEX_H
#define WORLD_GENERATOR_CORE_MODELVERTEX_H
#include "../Vertex.h"
namespace WorldGenerator {
    namespace Render {
        namespace Custom {
            class ModelVertex final : public Vertex {
            public:
                static VertexLayout GetModelVertexLayout();
                ModelVertex(const glm::vec3& pos, const glm::vec4& color, const glm::vec2& uv, const glm::vec3 & normal = glm::vec3(1.0f));
                uint32_t getFloatCount() override;
                VertexLayout getVertexLayout() override;
            };
        }
    }
}
#endif //WORLD_GENERATOR_CORE_MODELVERTEX_H
