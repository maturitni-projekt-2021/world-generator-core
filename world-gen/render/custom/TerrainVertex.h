//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_TERRAINVERTEX_H
#define WORLD_GENERATOR_CORE_TERRAINVERTEX_H
#include "../Vertex.h"
namespace WorldGenerator {
    namespace Render {
        namespace Custom {
            class TerrainVertex final : public Vertex {
            public:
                static VertexLayout GetModelVertexLayout();
                TerrainVertex(const glm::vec3& pos, const glm::vec3& normal,
                        const glm::vec3& b1color, const glm::vec3& b2color, float biomeVal);
                uint32_t getFloatCount() override;
                VertexLayout getVertexLayout() override;
            };
        }
    }
}
#endif //WORLD_GENERATOR_CORE_TERRAINVERTEX_H
