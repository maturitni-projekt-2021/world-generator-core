//
// Created by Matty on 11/2/2020.
//

#ifndef WORLD_GENERATOR_CORE_DEBUGVERTEX_H
#define WORLD_GENERATOR_CORE_DEBUGVERTEX_H

#include "../Vertex.h"

namespace WorldGenerator {
    namespace Render {
        namespace Custom {
            class DebugVertex : public Vertex {
            public:
                static VertexLayout GetDebugVertexLayout();
                DebugVertex(const glm::vec3& pos);
                uint32_t getFloatCount() override;
                VertexLayout getVertexLayout() override;

            };
        }
    }
}


#endif //WORLD_GENERATOR_CORE_DEBUGVERTEX_H
