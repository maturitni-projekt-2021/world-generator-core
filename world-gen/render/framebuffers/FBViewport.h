//
// Created by Matty on 30.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_FBVIEWPORT_H
#define WORLD_GENERATOR_CORE_FBVIEWPORT_H

#include "../FrameBuffer.h"

namespace WorldGenerator {
    namespace Render {
        class FBViewport final : public FrameBuffer {
        private:
            std::shared_ptr<ColorAttachment> m_pBaseColorAttachment;
        public:
            FBViewport(uint32_t width, uint32_t height,bool useMultisample = false);
            std::shared_ptr<ColorAttachment> GetMainColorAttachment();
        };
    }
}
#endif //WORLD_GENERATOR_CORE_FBVIEWPORT_H
