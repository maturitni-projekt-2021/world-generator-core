//
// Created by Matty on 5.9.2020.
//

#ifndef WORLD_GENERATOR_CORE_FBVIEWPORTCAMERA_H
#define WORLD_GENERATOR_CORE_FBVIEWPORTCAMERA_H
#include "../FrameBuffer.h"

namespace WorldGenerator {
    namespace Render {
        class FBViewportCamera final : public FrameBuffer {
        private:
            std::shared_ptr<ColorAttachment> m_pBaseColorAttachment;
        public:
            FBViewportCamera(uint32_t width, uint32_t height, bool useMultisample = false);
            std::shared_ptr<ColorAttachment> GetMainColorAttachment();

        };
    }
}
#endif //WORLD_GENERATOR_CORE_FBVIEWPORTCAMERA_H
