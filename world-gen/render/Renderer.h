//
// Created by Matty on 29.8.2020.
//

#ifndef WORLD_GENERATOR_CORE_RENDERER_H
#define WORLD_GENERATOR_CORE_RENDERER_H

#ifdef MINGW_WINDOWS_THREAD_LIB
#include <mingw_threads/mingw.thread.h>
#else
#include <thread>
#endif

#include "FrameBuffer.h"
#include "Model.h"
#include "RenderTask.h"
#include "../util/UtilThread.h"

#include <GLFW/glfw3.h>
#include <memory>
#include <vector>

namespace WorldGenerator {
    namespace Render {
        class Renderer {
        private:
            Util::ThreadSafeQueue<std::shared_ptr<RenderTask>> m_pRenderTasks;
            std::set<std::shared_ptr<RenderTask>> m_pIndependentRenderTasks;
            bool m_pRunning = true;
            void _Init();
        public:
            /// Renderer
            static std::unique_ptr<Renderer> m_Renderer;
            ~Renderer();
            Renderer();
            void Start();
            void Update();
            void PreRender();
            void SetWireframeMode(bool wireframeActive);
            void AddRenderTask(const std::shared_ptr<RenderTask> & task);
        };
    }
}

#endif //WORLD_GENERATOR_CORE_RENDERER_H
