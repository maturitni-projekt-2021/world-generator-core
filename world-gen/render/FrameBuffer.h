//
// Created by Matty on 14.6.2020.
//

#ifndef WG_FRAMEBUFFER_H
#define WG_FRAMEBUFFER_H

#include "ColorAttachment.h"

#include <glm/glm.hpp>
#include <cstdint>

namespace WorldGenerator {
    namespace Render {

        class FrameBuffer {
        protected:
            std::vector<std::shared_ptr<ColorAttachment>> m_pAttachments;
            void _LoadAttachments();
        private:
            uint32_t m_pID, m_pWidth, m_pHeight;
            uint32_t m_pRBO;
            glm::vec4 m_pClearColor = glm::vec4(0.0f);
            bool m_pDepthTestingEnabled = true;
            bool m_pMultiSamplingEnabled = false;
        public:
            void SetDepthTestingEnabled(bool newValue) { m_pDepthTestingEnabled = true; }
            static void UnbindAll();
            ~FrameBuffer();
            FrameBuffer(const FrameBuffer& other) = delete;
            FrameBuffer& operator=(const FrameBuffer& other) = delete;
            FrameBuffer(FrameBuffer&& other) noexcept;
            FrameBuffer(uint32_t width, uint32_t height,bool useMultisample = false);
            void SetClearColor(const glm::vec4& newColor);
            void Bind();
            void Clear();
            void SetViewPort() const;
            void FullBind();
            void PostProcess();
        };
    }
}
#endif //WG_FRAMEBUFFER_H
