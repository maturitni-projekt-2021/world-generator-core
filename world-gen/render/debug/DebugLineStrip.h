//
// Created by Matty on 11/2/2020.
//

#ifndef WORLD_GENERATOR_CORE_DEBUGLINESTRIP_H
#define WORLD_GENERATOR_CORE_DEBUGLINESTRIP_H

#ifndef NDEBUG
#include "DebugPrimitive.h"
#include "../custom/DebugVertex.h"

namespace WorldGenerator {
    namespace Render {
        namespace Debug {
            class DebugLineStrip : public DebugPrimitive {
            private:
                std::vector<Custom::DebugVertex> m_pPoints;
                uint32_t m_pLastIndex = 0;
            public:
                DebugLineStrip(const glm::vec3 & pos, const glm::vec3 & color = glm::vec3(1.0f), const glm::vec3 & rot = glm::vec3(0.0f), const glm::vec3 & scale = glm::vec3(1.0f),uint32_t expectedMaxPoints = 16);
                void Clear();
                void AddPoint(const Custom::DebugVertex & point);
                void SetPoints(std::vector<Custom::DebugVertex> points);
                void Render(const glm::mat4 & viewMatrix,const glm::mat4 & projectionMatrix) override;

            protected:
                void _LoadDataToVertexBuffer() override;
            };
        }
    }
}
#endif


#endif //WORLD_GENERATOR_CORE_DEBUGLINESTRIP_H
