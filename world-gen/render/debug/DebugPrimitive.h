//
// Created by Matty on 11/2/2020.
//

#ifndef WORLD_GENERATOR_CORE_DEBUGPRIMITIVE_H
#define WORLD_GENERATOR_CORE_DEBUGPRIMITIVE_H

#ifndef NDEBUG
#include <glm/glm.hpp>
#include "../VertexBuffer.h"

namespace WorldGenerator {
    namespace Render {
        namespace Debug {
            class DebugPrimitive {
            protected:
                glm::vec3 m_pPosition;
                glm::vec3 m_pRotation;
                glm::vec3 m_pScale;
                glm::vec3 m_pColor;
                glm::mat4 _GetModelMatrix();
                std::unique_ptr<VertexBuffer> m_pVertexBuffer;
                std::vector<uint32_t> m_pIndices;
                bool m_pModified = true;
                virtual void _LoadDataToVertexBuffer() = 0;
            public:
                DebugPrimitive(const glm::vec3 &position, const glm::vec3 &rotation, const glm::vec3 &scale, const glm::vec3 & color);
                virtual void Render(const glm::mat4 & viewMatrix,const glm::mat4 & projectionMatrix) = 0;
            };
        }
    }
}
#endif

#endif //WORLD_GENERATOR_CORE_DEBUGPRIMITIVE_H
